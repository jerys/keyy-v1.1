from __future__ import unicode_literals

from django.db import models

# Create your models here.
class ModuleComment(models.Model):
    parent_id = models.IntegerField(default=0)
    user_id = models.IntegerField(default=0)
    module_id = models.IntegerField(default=0)
    title = models.CharField("Title", max_length=250) 
    comment = models.TextField()
    comment_approved = models.CharField(max_length=250) 
    
    class Meta:
        verbose_name = 'Module Comment'
        verbose_name_plural = 'Module Comment'
        
        
    def __unicode__(self):
        return self.comment

class StudentJournalComment(models.Model):
    parent_id = models.IntegerField(default=0)
    user_id = models.IntegerField(default=0)
    journal_id = models.IntegerField(default=0)
    title = models.CharField("Title", max_length=250) 
    comment = models.TextField()
    comment_approved = models.CharField(max_length=250) 
    
    class Meta:
        verbose_name = 'Student Journal Comment'
        verbose_name_plural = 'Student Journal Comment'
        
        
    def __unicode__(self):
        return self.comment   