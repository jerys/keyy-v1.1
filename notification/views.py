from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse, Http404
from rest_framework import status
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from notification.models import StudentCoachRequest
from notification.models import Notification

from general.models import StudentCoach

from django.conf import settings
import os

from django.db.models import Q
from rest_framework.pagination import LimitOffsetPagination

from notification.serializers import StudentCoachSerializer, NotificationSerializer, ListStudentCoachNSerializer
# Create your views here.

# list all student
class GetUpdateNotification(APIView):
    serializer_class = StudentCoachSerializer
    def get(self, request, userID, format=None):
        try:
            studentobj = StudentCoach.objects.get(pk=userID)
            serializer = StudentCoachSerializer(studentobj)
            
            return Response({"status":True, "response_msg": "Notification list", "studentobj":serializer.data })
        except StudentCoach.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "studentobj": {} })
    def post(self, request, userID, format=None):
        studentobj = StudentCoach.objects.get(pk=userID)
        serializer = StudentCoachSerializer(studentobj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status":True, "response_msg": "Notification update successfully", "studentobj":serializer.data })
        return JsonResponse({"status":False, "response_msg": "Update failed", "studentobj": {} })
    
    
    
#list Notification by user wise
class ListUserWiseNotification(APIView):
    serializer_class = NotificationSerializer
    def get(self, request, userID, format=None):
        try:
            notificationobj = Notification.objects.filter(Q(notify_to=userID), Q(is_deleted=0)).order_by('-id')
            
            paginator   = LimitOffsetPagination()
            result_page = paginator.paginate_queryset(notificationobj, request)
            serializer = NotificationSerializer(result_page,many=True)
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            notificationarr = []
            for notival in notificationobj:
                paramList = {}
                paramList['id'] = notival.id
                paramList['title'] = notival.title
                paramList['main_id'] = notival.main_id
                paramList['type_of_notification'] = notival.type_of_notification
                paramList['date_time'] = notival.date_time
                paramList['is_read'] = int(notival.is_read)
                
                
                if (notival.type_of_notification == settings.NOTIFI_TYPE_ARR[0]): #CommentModule
                    paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[0] #commented on your Module
                    
                elif (notival.type_of_notification == settings.NOTIFI_TYPE_ARR[1]): #CommentJournal
                    paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[1] #commented on your journal entry
                    
                elif (notival.type_of_notification == settings.NOTIFI_TYPE_ARR[2]): #RequestCoach
                    paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[2] #requested to connect as your coach
                    
                elif (notival.type_of_notification == settings.NOTIFI_TYPE_ARR[4]): #RequestApprove
                    paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[4] #accepted your request
                    
                elif (notival.type_of_notification == settings.NOTIFI_TYPE_ARR[3]): #RequestStudent
                    paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[3] #requested to connect as your student
                    
                elif (notival.type_of_notification == settings.NOTIFI_TYPE_ARR[5]): #AssignModule
                    paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[5] #assign you a new module
                    
                elif (notival.type_of_notification == settings.NOTIFI_TYPE_ARR[6]): #SharedJournal
                    paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[6] #shared a new journal entry for you to leave feedback on
                    
                elif (notival.type_of_notification == settings.NOTIFI_TYPE_ARR[7]): #NewLessonCreated
                    paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[7] #created a new lesson
                    
                elif (notival.type_of_notification == settings.NOTIFI_TYPE_ARR[8]): #NewModuleCreated
                    paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[7] #created a new module
                    
                elif (notival.type_of_notification == settings.NOTIFI_TYPE_ARR[9]): #CoachReply
                    paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[8] #created a new module
                    
                else:
                    paramList['message'] = ''
                
                
                userobj = StudentCoach.objects.get(pk=notival.user_id_id)
                serializer1 = ListStudentCoachNSerializer(userobj)
                userarr = []
                userParlist = {}
                userParlist['id'] = serializer1.data['id']
                userParlist['first_name'] = serializer1.data['first_name']
                userParlist['last_name'] = serializer1.data['last_name']
                userParlist['account_type'] = serializer1.data['account_type']
                userParlist['updated'] = serializer1.data['updated']
                
                if(serializer1.data['profile_picture']):
                    #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(userobj.profile_picture)
                    #if os.path.isfile(profile_picture):
                    profile_picture = serializer1.data['profile_picture']
                    #else:
                        #profile_picture = ''
                elif(serializer1.data['profile_picture2']):
                    profile_picture = serializer1.data['profile_picture2']
                elif (serializer1.data['hidden_profile_picture'] !='' and serializer1.data['social_login'] == 'True'):
                    profile_picture = serializer1.data['hidden_profile_picture']
                else:
                    profile_picture = ''
                    
                userParlist['profile_picture'] = profile_picture
                userarr.append(userParlist)
                paramList['userobj'] = userarr
                
                notificationarr.append(paramList)
            
            return paginator.get_paginated_response({"status":True, "response_msg": "Notification list", "notificationobj":notificationarr })
        except Notification.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "notificationobj": {} })





#Notification detail page and is_read by user wise
class NotificationDetail(APIView):
    serializer_class = NotificationSerializer
    def get(self, request,userID, pk, format=None):
        try:
            notificationobj = Notification.objects.get(pk=pk,is_deleted=0)
            serializer = NotificationSerializer(notificationobj,many=True)
            notificationobj.is_read = 1
            notificationobj.save()
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            notificationarr = []
            
            paramList = {}
            paramList['id'] = notificationobj.id
            paramList['title'] = notificationobj.title
            paramList['main_id'] = notificationobj.main_id
            paramList['type_of_notification'] = notificationobj.type_of_notification
            paramList['date_time'] = notificationobj.date_time
            paramList['is_read'] = int(notificationobj.is_read)
            
            
            if (notificationobj.type_of_notification == settings.NOTIFI_TYPE_ARR[0]): #CommentModule
                paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[0] #commented on your Module
                
            elif (notificationobj.type_of_notification == settings.NOTIFI_TYPE_ARR[1]): #CommentJournal
                paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[1] #commented on your journal entry
                
            elif (notificationobj.type_of_notification == settings.NOTIFI_TYPE_ARR[2]): #RequestCoach
                paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[2] #requested to connect as your coach
                
            elif (notificationobj.type_of_notification == settings.NOTIFI_TYPE_ARR[3]): #RequestStudent
                paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[3] #requested to connect as your student
                
            elif (notificationobj.type_of_notification == settings.NOTIFI_TYPE_ARR[5]): #AssignModule
                paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[5] #assign you a new module
                
            elif (notificationobj.type_of_notification == settings.NOTIFI_TYPE_ARR[6]): #SharedJournal
                paramList['message'] = settings.NOTIFI_TYPE_MESSAGE[6] #shared a new journal entry for you to leave feedback on
                
            else:
                paramList['message'] = ''
            
            
            userobj = StudentCoach.objects.get(pk=notificationobj.user_id_id)
            serializer1 = ListStudentCoachNSerializer(userobj)
            userarr = []
            userParlist = {}
            userParlist['id'] = serializer1.data['id']
            userParlist['first_name'] = serializer1.data['first_name']
            userParlist['last_name'] = serializer1.data['last_name']
            userParlist['updated'] = serializer1.data['updated']
            
            if(serializer1.data['profile_picture']):
                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(userobj.profile_picture)
                #if os.path.isfile(profile_picture):
                profile_picture = serializer1.data['profile_picture']
                #else:
                    #profile_picture = ''
            elif(serializer1.data['profile_picture2']):
                profile_picture = serializer1.data['profile_picture2']
            elif (serializer1.data['hidden_profile_picture'] !='' and serializer1.data['social_login'] == 'True'):
                profile_picture = serializer1.data['hidden_profile_picture']
            else:
                profile_picture = ''
                
            userParlist['profile_picture'] = profile_picture
            userarr.append(userParlist)
            paramList['userobj'] = userarr
            
            notificationarr.append(paramList)
            
            return Response({"status":True, "response_msg": "Notification detail", "notificationobj":notificationarr })
            
        except Notification.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "notificationobj": {} })
        
        
        
#Notification detail page and is_read by user wise
class DelteNotificationDetail(APIView):
    serializer_class = NotificationSerializer
    def get(self, request,userID, pk, format=None):
        try:
            notificationobj = Notification.objects.get(pk=pk)
            serializer = NotificationSerializer(notificationobj,many=True)
            notificationobj.is_deleted = 1
            notificationobj.is_read = 1
            notificationobj.save()
            return Response({"status":True, "response_msg": "Notification remove successfully", "notificationobj":{} })
        except Notification.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "notificationobj": {} })



#unread notification
class UnreadNotification(APIView):
    serializer_class = NotificationSerializer
    def get(self, request,userID, format=None):
        try:
            notificationobj = Notification.objects.filter(Q(notify_to=userID), Q(is_read=0)).count()
            if(notificationobj > 0 ):
                return Response({"status":True, "response_msg": "Total unread notification", "unreadnotification":notificationobj })
            return Response({"status":False, "response_msg": "Data not found", "unreadnotification": "" })
        except Notification.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "unreadnotification": "" })