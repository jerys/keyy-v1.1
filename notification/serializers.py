from rest_framework import serializers

from notification.models import StudentCoachRequest
from notification.models import Notification

from general.models import StudentCoach


#tag 
class StudentCoachSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','noti_jounral_entry','noti_module_assign','noti_conn_request','noti_personal_reminder','noti_sales_promoation_offer','noti_announce_update','noti_coach_publish_new_module',)
        #fields = '__all__'
    


class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ('id','user_id','notify_to','title','type_of_notification','main_id','date_time','is_read','is_deleted',)
        
        
class ListStudentCoachNSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','first_name','last_name','user_name','e_mail','skill','profile_picture','profile_picture2','account_type','hidden_profile_picture','training_location','social_login','updated')
