# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-20 07:13
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0028_auto_20170919_1428'),
        ('notification', '0006_auto_20170901_0822'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='studentcoachrequest',
            name='coach',
        ),
        migrations.RemoveField(
            model_name='studentcoachrequest',
            name='student',
        ),
        migrations.AddField(
            model_name='studentcoachrequest',
            name='user',
            field=models.ForeignKey(blank=True, default=1, on_delete=django.db.models.deletion.CASCADE, related_name='UserRequest', to='general.StudentCoach'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='studentcoachrequest',
            name='user_to',
            field=models.ForeignKey(blank=True, default=1, on_delete=django.db.models.deletion.CASCADE, related_name='UserToRequest', to='general.StudentCoach'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='studentcoachrequest',
            name='request_date',
            field=models.DateTimeField(blank=True, verbose_name='Date'),
        ),
    ]
