# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0021_userreportproblem'),
        ('notification', '0002_auto_20170826_0624'),
    ]

    operations = [
        migrations.CreateModel(
            name='Notification',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=250, blank=True)),
                ('type_of_notification', models.CharField(blank=True, max_length=250, choices=[(b'CommentModule', b'CommentModule'), (b'CommentJournal', b'CommentJournal'), (b'RequestCoach', b'RequestCoach'), (b'RequestStudent', b'RequestStudent'), (b'RequestApprove', b'RequestApprove'), (b'AssignModule', b'AssignModule'), (b'SharedJournal', b'SharedJournal')])),
                ('main_id', models.CharField(max_length=250, blank=True)),
                ('date_time', models.DateTimeField(default=datetime.datetime.now, verbose_name='Date / Time', blank=True)),
                ('is_read', models.CharField(default=0, max_length=250, blank=True, choices=[(1, b'Yes'), (0, b'No')])),
                ('is_deleted', models.CharField(default=0, max_length=250, blank=True, choices=[(1, b'Yes'), (0, b'No')])),
                ('comment_on', models.ForeignKey(related_name='CommentOnID', to='general.StudentCoach')),
                ('user_id', models.ForeignKey(related_name='User_ID', to='general.StudentCoach')),
            ],
            options={
                'verbose_name': 'Notification',
                'verbose_name_plural': 'Notification',
            },
        ),
    ]
