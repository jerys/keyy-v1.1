# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('notification', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentcoachrequest',
            name='request_date',
            field=models.DateTimeField(default=datetime.datetime.now, verbose_name='Date', blank=True),
        ),
    ]
