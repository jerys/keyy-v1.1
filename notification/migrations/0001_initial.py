# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0008_auto_20170818_1450'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentCoachRequest',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('request_to', models.CharField(blank=True, max_length=250, choices=[('Coach', 'Coach'), ('Student', 'Student')])),
                ('status', models.CharField(default='Pending', max_length=250, blank=True, choices=[('Approve', 'Approve'), ('Reject', 'Reject'), ('Pending', 'Pending')])),
                ('request_date', models.DateTimeField(auto_now_add=True, verbose_name='date published')),
                ('coach', models.ForeignKey(related_name='CoachRequest', to='general.StudentCoach')),
                ('student', models.ForeignKey(related_name='StudentRequest', to='general.StudentCoach')),
            ],
            options={
                'verbose_name': 'Student Coach Request',
                'verbose_name_plural': 'Student Coach Request',
            },
        ),
    ]
