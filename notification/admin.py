from django.contrib import admin

# Register your models here.
from .models import StudentCoachRequest
class StudentRequestAdmin(admin.ModelAdmin):
    list_display=["user","user_to","request_to","status","request_date"]
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["user","user_to","request_to","request_date"]
        else:
            return self.readonly_fields
    def has_add_permission(self, request):
        return False
admin.site.register(StudentCoachRequest,StudentRequestAdmin)


from .models import Notification
class NotificationAdmin(admin.ModelAdmin):
    list_display=["user_id","notify_to","title","type_of_notification","is_read","is_deleted","date_time"]
    def has_add_permission(self, request):
        return False
admin.site.register(Notification,NotificationAdmin )