from __future__ import unicode_literals
from datetime import datetime
from django.db import models
from django.conf import settings

# Create your models here.

class StudentCoachRequest(models.Model):
    REQUEST_TO = (('Coach', 'Coach'),('Student', 'Student'),)
    STATUS = (('Approve', 'Approve'),('Reject', 'Reject'),('Pending', 'Pending'),)
    user = models.ForeignKey("general.StudentCoach", related_name="UserRequest", blank=True)
    user_to = models.ForeignKey("general.StudentCoach", related_name="UserToRequest", blank=True)
    request_to = models.CharField(max_length=250,choices=REQUEST_TO, blank=True)
    coach_flag = models.CharField(max_length=1, default=0, blank=True)
    temp_status = models.CharField(max_length=250, blank=True)
    status = models.CharField(max_length=250,choices=STATUS,default='Pending', blank=True)
    request_date = models.DateTimeField('Date', auto_now_add=True,blank=True)
    
    class Meta:
        verbose_name = 'Student Coach Request'
        verbose_name_plural = 'Student Coach Request'
        
        
        

class Notification(models.Model):
    user_id = models.ForeignKey("general.StudentCoach", related_name="User_ID")
    notify_to = models.ForeignKey("general.StudentCoach", related_name="CommentOnID")
    title = models.CharField(max_length=250,blank=True)
    type_of_notification = models.CharField(max_length=250,choices=settings.NOTIFI_TYPE, blank=True)
    main_id = models.CharField(max_length=250,blank=True)
    date_time = models.DateTimeField('Date / Time', default=datetime.now, blank=True)
    is_read = models.IntegerField(choices=settings.NOTI_STATUS,default=0, blank=True)
    is_deleted = models.IntegerField(choices=settings.NOTI_STATUS,default=0, blank=True)
    
    class Meta:
        verbose_name = 'Notification'
        verbose_name_plural = 'Notification'
        

