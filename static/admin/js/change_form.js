/*global showAddAnotherPopup, showRelatedObjectLookupPopup showRelatedObjectPopup updateRelatedObjectLinks*/

(function($) {
    'use strict';
    $(document).ready(function() {
        var modelName = $('#django-admin-form-add-constants').data('modelName');
        $('body').on('click', '.add-another', function(e) {
            e.preventDefault();
            var event = $.Event('django:add-another-related');
            $(this).trigger(event);
            if (!event.isDefaultPrevented()) {
                showAddAnotherPopup(this);
            }
        });

        if (modelName) {
            $('form#' + modelName + '_form :input:visible:enabled:first').focus();
        }
        
        //custom code
        if ($('#customer_data').length){
            var object_id = $("#object_id").val();
            
            if(object_id != '' && typeof object_id != 'undefined'){
                
                var data = ''
                var html = ''
                $.ajax({
                    url: "/api/gettotalmodule/"+object_id+"/",
                    type: "get",
                    data: data,
                    success: function(response) {
                        //alert(response)
                         $.each(response, function(index, element) {
                            //alert(index+'--'+ element);
                            if(index == 'total_module'){
                                $(".field-subscription_amount").after('<div class="form-row field-total_module"><div><label>Total published module:</label><div class="readonly">'+element+'</div></div></div>');
                                
                            }
                            if(index == 'order_history'){
                                if(element.length > 0){
                                    html +='<table id="result_list" width="100%"><thead><tr><th>#</th><th>Module ID</th><th>Module Name</th><th>Transection ID</th><th>Status</th><th>Date</th></tr><thead><tbody>';
                                    var i=1
                                    $.each(element, function(key, data) {
                                        if(i % 2 != 0){
                                            var row_class = 'row1';
                                        }
                                        else{
                                            var row_class = 'row2';
                                        }
                                            
                                        html +='<tr class="'+row_class+'"><td>'+i+'</td><td><a href="/admin/coaches/coachmodule/'+data.module_id+'/change/">'+data.module_id+'</a></td><td>'+data.module_name+'</td><td>'+data.transaction_id+'</td><td>'+data.status+'</td><td>'+data.created_date+'</td></tr>';
                                        i++;
                                    });
                                    html +='</tbody></table>';
                                    
                                    //refenceuser
                                    $("#module_log").html(html);
                                }
                            }
                            /*
                            if(index == 'total_refernce_user'){
                                var htmltotalrefuser = '';
                                
                                //htmltotalrefuser += '<div class="form-row"><div><label>Total Number Of Sign Up User:</label><div class="readonly">'+element+'</div></div></div>';
                                //alert(htmltotalrefuser);
                                //$( htmltotalrefuser ).insertAfter( $( "#studentcoach_form .field-total_module" ) );
                                $("#refenceuser").html(element);
                            }
                            if(index == 'refencecode'){
                                $("#refencecode").html(element);
                            }
                            */
                         
                        });
                    },
                    error:function(xhr,errmsg,err) {
                            
                    }
                    
                })
            }
            
            
            if($(".field-invite_count .readonly").text().length > 0){
                
                if(object_id != '' && typeof object_id != 'undefined'){
                
                    var data = ''
                    var html = ''
                    $.ajax({
                        url: "/api/getreferenceuserdetail/"+object_id+"/",
                        type: "get",
                        data: data,
                        success: function(response) {
                            //alert(response)
                            $.each(response, function(index, element) {
                                //alert(index+'--'+ element);
                                
                                if(index == 'userdetail'){
                                    var k=1;
                                    var userhtml = '';
                                    userhtml +='<table width="100%"><thead><tr><th>#</th><th>Fisrt Name</th><th>Last Name</th><th>Email</th><th>Account Type</th></tr></thead><tbody>';
                                    $.each(element, function(key, value) {
                                        //alert(key+'--'+ value.first_name);
                                        userhtml +='<tr><td>'+k+'</td><td>'+value.first_name+'</td><td>'+value.last_name+'</td><td>'+value.e_mail+'</td><td>'+value.account_type+'</td></tr>';
                                        
                                        k= k +1;
                                    });
                                    userhtml +'</tbody></table>';
                                    
                                    $(".field-invite_count").find('.readonly').html(k-1);
                                    $(userhtml).insertAfter(".field-invite_count");
                                }
                                
                            
                            });
                            
                        },
                        error:function(xhr,errmsg,err) {
                                
                        }
                        
                    })
                }
                
               
               
               
                
            }
            
        }
        
        if ($('#coachmodule_form .field-philosophy_video').length){
            var vhref = $('.field-philosophy_video').find('a').attr('href');
            var href = $('.field-philosophy_video_thumb').find('a').attr('href');
            
            if(typeof href != 'undefined'){
                var newhref = href.replace("https://", "http://");
            }
            if(typeof vhref != 'undefined'){
                var newvhref = vhref.replace("https://", "http://");
                var video = '<div><video width="400" poster="'+newhref+'" controls>\
                                <source src="'+newvhref+'" type="video/mp4">\
                            </video></div>';
                $(video).insertAfter('.field-philosophy_video .clearable-file-input');
            }
        }
        if ($('#coachmodule_form .field-technique_video').length){
            var vhref = $('.field-technique_video').find('a').attr('href');
            
            var href = $('.field-technique_video_thumb').find('a').attr('href');
            
            if(typeof href != 'undefined'){
                var newhref = href.replace("https://", "http://");
            }
            
            if(typeof vhref != 'undefined'){
                var newvhref = vhref.replace("https://", "http://");
                var video = '<div><video width="400" poster="'+newhref+'" controls>\
                                <source src="'+newvhref+'" type="video/mp4">\
                            </video></div>';
                $(video).insertAfter('.field-technique_video .clearable-file-input');
            }
        }
        if ($('#coachmodule_form .field-coach_video').length){
            var vhref = $('.field-coach_video').find('a').attr('href');
            
            var href = $('.field-coach_video_thumb').find('a').attr('href');
            
            if(typeof href != 'undefined'){
                var newhref = href.replace("https://", "http://");
            }
            
            if(typeof vhref != 'undefined'){
                var newvhref = vhref.replace("https://", "http://");
                var video = '<div><video poster="'+newhref+'" width="400" controls>\
                                <source src="'+newvhref+'" type="video/mp4">\
                            </video></div>';
                $(video).insertAfter('.field-coach_video .clearable-file-input');
            }
        }
        
        if ($('#coachmodule_form .field-philosophy_video_thumb').length){
            var href = $('.field-philosophy_video_thumb').find('a').attr('href');
            if(typeof href != 'undefined'){
                var newhref = href.replace("https://", "http://");
                var image = '<div><img width="130px" height="130px" src="'+newhref+'"></div>';
                $(image).insertAfter('.field-philosophy_video_thumb .clearable-file-input');
            }
            
        }
        if ($('#coachmodule_form .field-technique_video_thumb').length){
            var href = $('.field-technique_video_thumb').find('a').attr('href');
            if(typeof href != 'undefined'){
                var newhref = href.replace("https://", "http://");
                var image = '<div><img width="130px" height="130px" src="'+newhref+'"></div>';
                $(image).insertAfter('.field-technique_video_thumb .clearable-file-input');
            }
            
        }
        if ($('#coachmodule_form .field-coach_video_thumb').length){
            var href = $('.field-coach_video_thumb').find('a').attr('href');
            if(typeof href != 'undefined'){
                var newhref = href.replace("https://", "http://");
                var image = '<div><img width="130px" height="130px" src="'+newhref+'"></div>';
                $(image).insertAfter('.field-coach_video_thumb .clearable-file-input');
            }
            
        }

        //For IOS module Start
        var philosophy_video_thumb2 = $('#id_philosophy_video_thumb2').val();
        if(philosophy_video_thumb2){
            if(typeof philosophy_video_thumb2 != 'undefined' && philosophy_video_thumb2 != ''){
                var newhref = philosophy_video_thumb2.replace("https://", "http://");
                var image = '<div><img width="130px" height="130px" src="'+newhref+'"></div>';
                $(image).insertAfter('.field-philosophy_video_thumb #id_philosophy_video_thumb');
            }
        }
        $('.field-philosophy_video_thumb2').remove();
        
        var technique_video_thumb2 = $('#id_technique_video_thumb2').val();
        if(technique_video_thumb2){
            if(typeof technique_video_thumb2 != 'undefined' && technique_video_thumb2 != ''){
                var newhref = technique_video_thumb2.replace("https://", "http://");
                var image = '<div><img width="130px" height="130px" src="'+newhref+'"></div>';
                $(image).insertAfter('.field-technique_video_thumb #id_technique_video_thumb');
            }
        }
        $('.field-technique_video_thumb2').remove();

        var coach_video_thumb2 = $('#id_coach_video_thumb2').val();
        if(coach_video_thumb2){
            if(typeof coach_video_thumb2 != 'undefined' && coach_video_thumb2 != ''){
                var newhref = coach_video_thumb2.replace("https://", "http://");
                var image = '<div><img width="130px" height="130px" src="'+newhref+'"></div>';
                $(image).insertAfter('.field-coach_video_thumb #id_coach_video_thumb');
            }
        }
        $('.field-coach_video_thumb2').remove();
        
        var philosophy_video2 = $('#id_philosophy_video2').val();
        if(philosophy_video2){
            if(typeof philosophy_video2 != 'undefined' && philosophy_video2 != ''){
                var philosophy_video2 = philosophy_video2.replace("https://", "http://");
                var video = '<div><video width="400" controls>\
                                <source src="'+philosophy_video2+'" type="video/mp4">\
                            </video></div>';
                $(video).insertAfter('.field-philosophy_video #id_philosophy_video');
                
            }
        }
        $(".field-philosophy_video2").remove();
        
        var technique_video2 = $('#id_technique_video2').val();
        if(technique_video2){
            if(typeof technique_video2 != 'undefined' && technique_video2 != ''){
                var technique_video2 = technique_video2.replace("https://", "http://");
                var video = '<div><video width="400" controls>\
                                <source src="'+technique_video2+'" type="video/mp4">\
                            </video></div>';
                $(video).insertAfter('.field-technique_video #id_technique_video');
                
            }
        }
        $(".field-technique_video2").remove();
        
        var coach_video2 = $('#id_coach_video2').val();
        if(coach_video2){
            if(typeof coach_video2 != 'undefined' && coach_video2 != ''){
                var coach_video2 = coach_video2.replace("https://", "http://");
                var video = '<div><video width="400" height="400" controls>\
                                <source src="'+coach_video2+'" type="video/mp4">\
                            </video></div>';
                $(video).insertAfter('.field-coach_video #id_coach_video');
                
            }
        }
        $(".field-coach_video2").remove();
        //For IOS module End

        if ($('#coachmodule_form .field-coach_video_thumb').length){
            var moduleid = $("#module_id").val();
             
            $.ajax({
                    url: "/api/getmodulehomeworklink/"+moduleid+"/",
                    type: "get",
                    data: data,
                    success: function(response) {
                        
                        $('<div class="form-row field-total_module"><div><label>Task & Homework:</label><div class="readonly" id="link_text"></div></div></div>').insertAfter(".field-coach_video_thumb");
                        
                        $.each(response, function(index, element) {
                            console.log(index+'=='+element);
                            if(index == 'link'){
                                var links = '';
                                $.each(element, function(key, data) {
                                    console.log(data.links);
                                    links +='<div>'+data.links+'</div>';
                                });
                                $("#link_text").append(links);
                                
                            }
                           
                        });
                        
                        
                    },
                    error:function(xhr,errmsg,err) {
                            
                    }
                    
                })
            
            
        }
        
        
        
        //custom code end
        
    });
})(django.jQuery);
