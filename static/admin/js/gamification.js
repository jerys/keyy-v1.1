/*global showAddAnotherPopup, showRelatedObjectLookupPopup showRelatedObjectPopup updateRelatedObjectLinks*/

(function($) {
    'use strict';
    $(document).ready(function() {
    
        
        if($("#id_task_type").val() == 'Points'){
            $(".field-badge_title").hide();
            $(".field-badge_image").hide();
            $(".field-points").show();
        }
        else if($("#id_task_type").val() == 'Badge'){
            $(".field-points").hide();
            $(".field-badge_title").show();
            $(".field-badge_image").show();
        }
        else{
            $(".field-points").hide();
            $(".field-badge_title").hide();
            $(".field-badge_image").hide();
        }
        
        
        //custom code Start
        $('#id_task_type').change(function(){
            //do something on select change
            var typeval = $(this).val();
            if(typeval != '' && typeval == 'Badge'){
                $(".field-points").hide();
                $(".field-badge_title").show();
                $(".field-badge_image").show();
            }else if(typeval != '' && typeval == 'Points'){
                $(".field-points").show();
                $(".field-badge_title").hide();
                $(".field-badge_image").hide();
            }else{
                $(".field-points").hide();
                $(".field-badge_title").hide();
                $(".field-badge_image").hide();
            }
        });
        
        var $dropdown1 = $("select[name='condition_1']");
        var $dropdown2 = $("select[name='condition_2']");
        var $dropdown3 = $("select[name='condition_3']");

        var myArray = {condition1: '', condition2: '', condition3: ''};

        comman_function($dropdown1,$dropdown2,$dropdown3);

        function comman_function(){

            if($dropdown1){

                var selectedItem = $dropdown1.val();
                $dropdown2.find('option').prop("disabled", false);
                $dropdown3.find('option').prop("disabled", false);
                myArray.condition1 = selectedItem;

                $.each(myArray, function( index, value ) {
                    if( $("select[name='condition_2'] option:selected").val() != value){
                        $dropdown2.find('option[value="' + value + '"]').prop("disabled", true);
                    }
                    if( $("select[name='condition_3'] option:selected").val() != value){
                        $dropdown3.find('option[value="' + value + '"]').prop("disabled", true);
                    }
                });
                
                if(selectedItem == 'is-a-keyypro-member'){ // 10 for Is a KeyyPRO member
                    var id_condition1_value = $("#id_condition3_value").val();
                    if(id_condition1_value == 1){
                        var yesselect = 'selected';
                        var noselect = '';
                    }else if(id_condition1_value == 0){
                        var yesselect = '';
                        var noselect = 'selected';
                    }
                    $("#id_condition1_value").remove();
                    $(".field-condition1_value").html('<div><label for="id_condition1_value">Condition1 value:</label><div class="related-widget-wrapper"><select name="condition1_value" id="id_condition1_value" class="vTextField"><option value="1" '+yesselect+'>Yes</option><option value="0" '+noselect+'>No</option></select></div></div>');
                }
                if(selectedItem == 'user-has-earned-badges'){ 
                    ge_all_badge(1);
                }

            }

            if($dropdown2){
                $dropdown1.find('option').prop("disabled", false);
                $dropdown3.find('option').prop("disabled", false);
                var selectedItem = $dropdown2.val();
                myArray.condition2 = selectedItem;

                $.each(myArray, function( index, value ) {
                    if( $("select[name='condition_1'] option:selected").val() != value){
                        $dropdown1.find('option[value="' + value + '"]').prop("disabled", true);
                    }
                    if( $("select[name='condition_3'] option:selected").val() != value){
                        $dropdown3.find('option[value="' + value + '"]').prop("disabled", true);
                    }
                });

                if(selectedItem == 'is-a-keyypro-member'){ // 10 for Is a KeyyPRO member
                    var id_condition2_value = $("#id_condition3_value").val();
                    if(id_condition2_value == 1){
                        var yesselect = 'selected';
                        var noselect = '';
                    }else if(id_condition2_value == 0){
                        var yesselect = '';
                        var noselect = 'selected';
                    }
                    $("#id_condition2_value").remove();
                    $(".field-condition2_value").html('<div><label for="id_condition2_value">Condition2 value:</label><div class="related-widget-wrapper"><select name="condition2_value" id="id_condition2_value" class="vTextField"><option value="1" '+yesselect+'>Yes</option><option value="0" '+noselect+'>No</option></select></div></div>');
                }
                if(selectedItem == 'user-has-earned-badges'){ 
                    ge_all_badge(2);
                }

            }

            if($dropdown3){
                $dropdown1.find('option').prop("disabled", false);
                $dropdown2.find('option').prop("disabled", false);
                var selectedItem = $dropdown3.val();
                myArray.condition3 = selectedItem;
                $.each(myArray, function( index, value ) {
                    if( $("select[name='condition_1'] option:selected").val() != value){
                        $dropdown1.find('option[value="' + value + '"]').prop("disabled", true);
                    }
                    if( $("select[name='condition_2'] option:selected").val() != value){
                        $dropdown2.find('option[value="' + value + '"]').prop("disabled", true);
                    }
                });

                if(selectedItem == 'is-a-keyypro-member'){ // 10 for Is a KeyyPRO member
                    var id_condition3_value = $("#id_condition3_value").val();
                    if(id_condition3_value == 1){
                        var yesselect = 'selected';
                        var noselect = '';
                    }else if(id_condition3_value == 0){
                        var yesselect = '';
                        var noselect = 'selected';
                    }
                    $("#id_condition3_value").remove();
                    $(".field-condition3_value").html('<div><label for="id_condition3_value">Condition3 value:</label><div class="related-widget-wrapper"><select name="condition3_value" id="id_condition3_value" class="vTextField"><option value="1" '+yesselect+'>Yes</option><option value="0" '+noselect+'>No</option></select></div></div>');
                }
                if(selectedItem == 'user-has-earned-badges'){ 
                    ge_all_badge(3);
                }
            }
        }

        
        $dropdown1.change(function() {

            var selectedItem = $(this).val();
            $dropdown2.find('option').prop("disabled", false);
            $dropdown3.find('option').prop("disabled", false);
            myArray.condition1 = selectedItem;

            $.each(myArray, function( index, value ) {
                if( $("select[name='condition_2'] option:selected").val() != value){
                    $dropdown2.find('option[value="' + value + '"]').prop("disabled", true);
                }
                if( $("select[name='condition_3'] option:selected").val() != value){
                    $dropdown3.find('option[value="' + value + '"]').prop("disabled", true);
                }
            });
            if(selectedItem == 'is-a-keyypro-member'){ // 10 for Is a KeyyPRO member
                $("#id_condition1_value").remove();
                $(".field-condition1_value").html('<div><label for="id_condition1_value">Condition1 value:</label><div class="related-widget-wrapper"><select name="condition1_value" id="id_condition1_value" class="vTextField"><option value="1">Yes</option><option value="0">No</option></select></div></div>');
            }else{
                
                $(".field-condition1_value").html('<div><label for="id_condition1_value">Condition1 value:</label><input type="text" name="condition1_value" value="0" id="id_condition1_value" class="vTextField" maxlength="250"></div>');
            }
            if(selectedItem == 'user-has-earned-badges'){ 
                ge_all_badge(1);
            }

        });
        
        $dropdown2.change(function() {
            $dropdown1.find('option').prop("disabled", false);
            $dropdown3.find('option').prop("disabled", false);
            var selectedItem = $(this).val();
            myArray.condition2 = selectedItem;

            $.each(myArray, function( index, value ) {
                if( $("select[name='condition_1'] option:selected").val() != value){
                    $dropdown1.find('option[value="' + value + '"]').prop("disabled", true);
                }
                if( $("select[name='condition_3'] option:selected").val() != value){
                    $dropdown3.find('option[value="' + value + '"]').prop("disabled", true);
                }
            });

            if(selectedItem == 'is-a-keyypro-member'){ // 10 for Is a KeyyPRO member
                $("#id_condition2_value").remove();
                $(".field-condition2_value").html('<div><label for="id_condition2_value">Condition2 value:</label><div class="related-widget-wrapper"><select name="condition2_value" id="id_condition2_value" class="vTextField"><option value="1">Yes</option><option value="0">No</option></select></div></div>');
            }else{
                $(".field-condition2_value").html('<div><label for="id_condition2_value">Condition2 value:</label><input type="text" name="condition2_value" value="0" id="id_condition2_value" class="vTextField" maxlength="250"></div>');
            }
            if(selectedItem == 'user-has-earned-badges'){ 
                ge_all_badge(2);
            }

        });
        
        $dropdown3.change(function() {
            $dropdown1.find('option').prop("disabled", false);
            $dropdown2.find('option').prop("disabled", false);
            var selectedItem = $(this).val();
            myArray.condition3 = selectedItem;
            $.each(myArray, function( index, value ) {
                if( $("select[name='condition_1'] option:selected").val() != value){
                    $dropdown1.find('option[value="' + value + '"]').prop("disabled", true);
                }
                if( $("select[name='condition_2'] option:selected").val() != value){
                    $dropdown2.find('option[value="' + value + '"]').prop("disabled", true);
                }
            });

            if(selectedItem == 'is-a-keyypro-member'){ // 10 for Is a KeyyPRO member
                $("#id_condition3_value").remove();
                $(".field-condition3_value").html('<div><label for="id_condition3_value">Condition3 value:</label><div class="related-widget-wrapper"><select name="condition3_value" id="id_condition3_value" class="vTextField"><option value="1">Yes</option><option value="0">No</option></select></div></div>');
            }else{
                $(".field-condition3_value").html('<div><label for="id_condition3_value">Condition3 value:</label><input type="text" name="condition3_value" value="0" id="id_condition3_value" class="vTextField" maxlength="250"></div>');
            }

            if(selectedItem == 'user-has-earned-badges'){ 
                ge_all_badge(3);
            }
            
        });
        //custom code end

        function ge_all_badge(d_id){
            var badge_id = $("#badge_id").val();
            var data = '&pk='+badge_id
            $.ajax({
                url: "/api/getall_badge/",
                type: "get",
                data: data,
                success: function(response) {
                    //alert(response);
                    $.each(response, function(index, element) {
                        if(index == 'badge'){
                            //console.log(element);
                            
                            var html = '';
                            var condition_val = '';
                            var hiddenval = $("#id_condition"+d_id+"_value").val();
                            
                            if(hiddenval != '' && hiddenval != '0'){
                                condition_val = hiddenval;
                                var arconditaionvalray = condition_val.split(',');
                                console.log(arconditaionvalray);
                            }

                            $("#id_condition"+d_id+"_value").remove();

                            html += '<div><label for="id_condition'+d_id+'_value">Condition'+d_id+' value:</label><div class="related-widget-wrapper"><input type="hidden" name="condition'+d_id+'_value" value="'+condition_val+'" id="id_condition'+d_id+'_value" class="vTextField" maxlength="250">';

                            
                            
                            $.each(element, function(key, value) {
                                //alert(key+'--'+ value.badge_title);
                                var checke = '';
                                var t = value.id.toString();
                                if($.inArray(t, arconditaionvalray) !== -1){
                                    checke = 'checked';
                                }

                                html +='<div class="checkbox-row"><input class="badge" data-attr-id="'+d_id+'" type="checkbox" name="badge" id="badge_'+key+'" value="'+value.id+'" '+checke+'><label class="vCheckboxLabel" for="badge_'+key+'">'+value.badge_title+'</label></div>';
                            });
                            html += '</div></div>';
                            $(".field-condition"+d_id+"_value").html(html);
                            
                            
                        }
                    });
                    
                },
                error:function(xhr,errmsg,err) {
                    alert('Something went wrong!');    
                }
                
            })
            
            var queryArr = [];
            $(document).on('click', '.badge', function(e) {
                var value = $(this).val();
                var attr_id = $(this).attr('data-attr-id');
                
                var textboxval = $("#id_condition"+attr_id+"_value").val();
                if(textboxval != ''){
                    queryArr = textboxval.split(',');
                }
                if ($(this).is(":checked")){
                    queryArr.push(value);
                }else{
                    queryArr.splice($.inArray(value, queryArr),1);
                }
                
                $("#id_condition"+attr_id+"_value").val(queryArr);

            });
        }
        
    });
})(django.jQuery);

