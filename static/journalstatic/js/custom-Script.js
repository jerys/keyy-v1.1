$(document).ready(function() {
	var owl = $('.owl-carousel');
	owl.owlCarousel({
		items: 4,
		margin: 10,
		autoplay: false,
		autoplayTimeout: 5000,
		autoplayHoverPause: true
	});
})