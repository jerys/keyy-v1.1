"""
WSGI config for ebdjango project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.9/howto/deployment/wsgi/
"""

import os
import sys

from django.core.wsgi import get_wsgi_application

sys.path.append('/usr/local/lib/python2.7/site-packages')
#sys.path.append('/usr/local/lib64/python2.7/site-packages')
sys.path.append('/usr/lib/python2.7/dist-packages')




os.environ.setdefault("DJANGO_SETTINGS_MODULE", "ebdjango.settings")

#from static_ranges import Ranges
#from dj_static import Cling, MediaCling

application = get_wsgi_application()

#application = Ranges(Cling(MediaCling(application)))
