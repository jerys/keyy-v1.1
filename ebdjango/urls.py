"""ebdjango URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import include,url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings


from rest_framework import routers
from rest_framework.urlpatterns import format_suffix_patterns

from general import views
import coaches.views
import students.views

import notification.views

import gamification.views


#router = routers.DefaultRouter()
#router.register(r'users', views.UserViewSet)
#router.register(r'groups', views.GroupViewSet)


urlpatterns = [
    
    
    
    url(r'^admin/', admin.site.urls),
    #url(r'^api/', include(router.urls)),
    #url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
    
    url(r'^api/login', views.UserLoginAPIView.as_view(), name='login'),
    url(r'^api/signup', views.UserRegisterAPIView.as_view(), name='signup'),
    #url(r'^api/user/?(?P<pk>[^/]+)/delete', views.UserDelete.as_view(), name='delete_user'),
    url(r'^api/forgotpass/$', views.ForgotPass.as_view(), name="forgot_pass"),
    url(r'^api/checkuniquecode/$', views.ForgotPassCheckUniqueCode.as_view(), name="check_valid_code"),
    url(r'^api/user/?(?P<user_id>[^/]+)/resetpassword/$', views.ResetPassword.as_view(), name="reset_password"),
    
    url(r'^api/user/?(?P<pk>[^/]+)/updatelastlogin/$', views.UserUpdateLastLogin.as_view(), name="UserUpdateLastLogin"),
    
    
    
    url(r'^api/user/?(?P<pk>[^/]+)/changepass/$', views.ChangePass.as_view(), name="change_pass"),
    
    
    url(r'^api/commanscrren/(?P<pk>[0-9]+)/$', views.UserUpdateType.as_view(), name="update_type"),
    url(r'^api/commanvideo/$', views.CommanVideoView),
    url(r'^api/trainingactivity/$', views.TariningActivityList),
    url(r'^api/student/?(?P<pk>[^/]+)/edit', views.UpdateStudent.as_view(), name="student_update"),
    url(r'^api/student/?(?P<pk>[^/]+)/avtar', views.UpdateStudentProfile.as_view(), name="student_update_profilepic"),
    
    url(r'^api/student/?(?P<pk>[^/]+)/V1avtar', views.UpdateStudentProfileV1.as_view(), name="student_update_profilepic"),
    
    
    url(r'^api/user/?(?P<userID>[^/]+)/uniquecode', views.UserUniqueID.as_view(), name="user_uniqueid"),
    url(r'^api/user/?(?P<uniqueID>[^/]+)/myreferenceuser', views.ListMyReferenceUser.as_view(), name="list_my_reference_user"),
    url(r'^api/user/?(?P<userID>[^/]+)/stripcustomerid', views.UpdateStripCustomerID.as_view(), name="update_strip_customer_id"),
    url(r'^api/user/?(?P<userID>[^/]+)/stripresponse', views.UpdateStripResponse.as_view(), name="update_strip_response"),
    
    
    
    url(r'^api/student/$', views.StudentView.as_view(), name="student_view"),
    
    #coach section
    #coach add edit moduel
    url(r'^api/taglist/$',coaches.views.ListTagView.as_view(), name="tag_list"),
    url(r'^api/user/?(?P<pk>[^/]+)/addmodule',coaches.views.CreateModule.as_view(), name="add_module"),
    url(r'^api/user/?(?P<coachID>[^/]+)/listmodule',coaches.views.ListMyModule.as_view(), name="list_my_module"),
    url(r'^api/user/?(?P<coachID>[^/]+)/coachlistmodule',coaches.views.CoachModuleList.as_view(), name="coach_list_module"),
    url(r'^api/user/?(?P<coachID>[^/]+)/selectmodule',coaches.views.Select30Module.as_view(), name="Select_Module"),
    
    
    url(r'^api/user/?(?P<coachID>[^/]+)/deletemodule/(?P<pk>\d+)/$',coaches.views.DeleteModule.as_view(), name="delete_module"),
    url(r'^api/user/?(?P<coachID>[^/]+)/searchmoduelbycoach/$',coaches.views.SearchCoachModuleByCoach.as_view(), name="search_coach_module_by_coach"),
    
    url(r'^api/user/?(?P<coachID>[^/]+)/draftmodule',coaches.views.ListDraftModule.as_view(), name="list_my_draft_module"),
    url(r'^api/user/?(?P<coachID>[^/]+)/editmodule/(?P<pk>\d+)/$',coaches.views.GetUpdateModule.as_view(), name="edit_module"),
    url(r'^api/user/?(?P<coachID>[^/]+)/modulevideo/(?P<pk>\d+)/$',coaches.views.UpdatevideoModule.as_view(), name="edit_module_video"),
    
    url(r'^api/user/?(?P<coachID>[^/]+)/iosmodulevideo/(?P<pk>\d+)/$',coaches.views.UpdatevideoModulev1.as_view(), name="edit_module_video"),
    
    
    url(r'^api/user/?(?P<coachID>[^/]+)/module/(?P<moduleID>\d+)/addlink/$',coaches.views.CreateModuleLink.as_view(), name="add_module_link"),
    url(r'^api/user/?(?P<coachID>[^/]+)/module/(?P<moduleID>\d+)/deletelink/(?P<pk>\d+)/$',coaches.views.DeleteModuleLink.as_view(), name="delete_module_link"),
    
    url(r'^api/user/?(?P<coachID>[^/]+)/module/(?P<moduleID>\d+)/assigntostudent/$',coaches.views.AssignModuleToMyStudent.as_view(), name="module_assign_to_my_student"),
    
    url(r'^api/customaddStudent/$',coaches.views.Customaddstident.as_view(), name="Customaddstident"),
    
    
    #student section
    url(r'^api/user/?(?P<pk>[^/]+)/home', views.StudentHomeProfile.as_view(), name="student_home"),
    url(r'^api/user/?(?P<pk>[^/]+)/goal', views.StudentGoalview.as_view(), name="student_goal"),
    url(r'^api/user/?(?P<pk>[^/]+)/addgoal', views.UserGoalAdd.as_view(), name="student_goal_add"),
    url(r'^api/user/?(?P<userID>[^/]+)/editgoal/?(?P<pk>[^/]+)/$', views.UserGoalEdit.as_view(), name="student_goal_edit"),
    url(r'^api/updategoal/?(?P<pk>[^/]+)/user/(?P<StudentCoach_pk>\d+)/$', views.UserGoalUpdate.as_view(), name="student_goal_update"),
    url(r'^api/deletegoal/?(?P<pk>[^/]+)/user/(?P<StudentCoach_pk>\d+)/$', views.UserGoalDelete.as_view(), name="student_goal_delete"),
    url(r'^api/user/?(?P<student_id>[^/]+)/assignment', views.StudentAssignmentview.as_view(), name="student_assignemnt"),
    url(r'^api/user/(?P<StudentID>\d+)/module/(?P<moduleID>\d+)/assigcomplete/$', views.StudentMarkascompleteAssignment.as_view(), name="student_assignemnt_complete"),
    url(r'^api/user/(?P<StudentID>\d+)/module/(?P<moduleID>\d+)/enrollunenroll/$', views.StudentEnrollUnenrollAssignment.as_view(), name="student_assignemnt_unenroll"),
    url(r'^api/user/(?P<StudentID>\d+)/module/(?P<moduleID>\d+)/enrollunenrollv1/$', views.StudentEnrollUnenrollAssignmentV1.as_view(), name="student_assignemnt_unenroll"),
    
    
    # MKT change
    url(r'^api/user/?(?P<userID>[^/]+)/recommendedmodule/$',coaches.views.ListRecommendedModule.as_view(), name="list_recommended_module"),
    url(r'^api/user/?(?P<userID>[^/]+)/searchrecommended/$',coaches.views.SearchListRecommendedModule.as_view(), name="search_list_recommended_module"),
    url(r'^api/user/?(?P<userID>[^/]+)/browsebytagmodule/$',coaches.views.BrowsebyTagModuleList.as_view(), name="browse_bytag_list_coach_module"),
    url(r'^api/user/?(?P<userID>[^/]+)/listlatestmodule',coaches.views.CoachModuleLatestList.as_view(), name="list_my_latest_module"),
    
    url(r'^api/user/?(?P<studentID>[^/]+)/moduledetail/(?P<pk>\d+)/$',coaches.views.GetModuleDetail.as_view(), name="detail_module"),
    #url(r'^api/user/?(?P<studentID>[^/]+)/module/(?P<pk>\d+)/coach/?(?P<coachID>[^/]+)/enroll/$',coaches.views.EnrollModule.as_view(), name="enroll_module"),
    url(r'^api/user/?(?P<studentID>[^/]+)/module/(?P<moduleID>\d+)/markaslink/?(?P<pk>[^/]+)/$',coaches.views.MarkasCompleteModuleLink.as_view(), name="mark_as_module_link"),
    url(r'^api/user/?(?P<studentID>[^/]+)/module/(?P<moduleID>\d+)/unmarkaslink/?(?P<pk>[^/]+)/$',coaches.views.UnMarkasCompleteModuleLink.as_view(), name="unmark_as_module_link"),
    
    url(r'^api/user/?(?P<studentID>[^/]+)/module/(?P<moduleID>\d+)/addcomment/$',coaches.views.AddCoachModuleComment.as_view(), name="add_module_comment"),
    url(r'^api/user/?(?P<studentID>[^/]+)/module/(?P<moduleID>\d+)/listcomment/$',coaches.views.ListCoachModuleComment.as_view(), name="list_module_comment"),
    url(r'^api/user/?(?P<userID>[^/]+)/module/(?P<moduleID>\d+)/like/$',coaches.views.AddCoachModuleLike.as_view(), name="like_module"),
    
    url(r'^api/user/?(?P<userID>[^/]+)/module/(?P<moduleID>\d+)/likev1/$',coaches.views.AddCoachModuleLikeV1.as_view(), name="like_module_v1"),
    
    url(r'^api/module/(?P<moduleID>\d+)/countcommentlike/$',coaches.views.CountCoachModuleLikeComment.as_view(), name="count_comment_like_module"),
    url(r'^api/module/(?P<moduleID>\d+)/userlike/$',coaches.views.CountCoachModuleLikeUSerwise.as_view(), name="userwise_like_module"),
    url(r'^api/user/?(?P<userID>[^/]+)/module/(?P<moduleID>\d+)/report/$',coaches.views.AddCoachModuleReport.as_view(), name="report_module"),
    
    
    #request section student caoch
    url(r'^api/user/?(?P<coachID>[^/]+)/searchmystudent/$',coaches.views.SearchMyAcceptStudent.as_view(), name="search_my_student_accept_request"),
    url(r'^api/user/?(?P<userID>[^/]+)/detail/$',coaches.views.StudentDetailPage.as_view(), name="student_detailpage"),
    url(r'^api/user/?(?P<coachID>[^/]+)/student/?(?P<userID>[^/]+)/sharedjournal/$',coaches.views.StudentDetailSharedJournal.as_view(), name="student_detailpage_sharedjournal"),
    
    #comman boath coach and student
    url(r'^api/user/?(?P<userID>[^/]+)/request/(?P<pk>\d+)/approve/$',coaches.views.UserApproveRequest.as_view(), name="new_user_accept_request"),
    url(r'^api/user/?(?P<userID>[^/]+)/request/(?P<pk>\d+)/reject/$',coaches.views.UserRejectRequest.as_view(), name="new_user_reject_request"),
    url(r'^api/user/?(?P<userID>[^/]+)/request/(?P<pk>\d+)/remove/$',coaches.views.UserRemoveRequest.as_view(), name="new_user_remove_request"),
    
    url(r'^api/user/?(?P<userID>[^/]+)/findcoach/$',coaches.views.FindCoach.as_view(), name="find_coach"),
    url(r'^api/user/?(?P<coachID>[^/]+)/findstudent/$',coaches.views.FindStudent.as_view(), name="find_student"),
    ######33
    url(r'^api/user/?(?P<coachID>[^/]+)/student/?(?P<studentID>[^/]+)/request/$',coaches.views.RequestCoachToStudent.as_view(), name="request_coach_to_student"),
    url(r'^api/user/?(?P<userID>[^/]+)/coach/?(?P<coachID>[^/]+)/request/$',coaches.views.RequestStudentToCoach.as_view(), name="request_student_to_coach"),
    
    url(r'^api/user/?(?P<coachID>[^/]+)/newstudent/$',coaches.views.MyNewStudent.as_view(), name="new_student"),
    url(r'^api/user/?(?P<userID>[^/]+)/newcoach/$',coaches.views.MyNewCoach.as_view(), name="new_coach"),
    
    url(r'^api/user/?(?P<coachID>[^/]+)/listmystudent/$',coaches.views.ListMyAcceptStudent.as_view(), name="list_my_student_accept_request"),
    url(r'^api/user/?(?P<userID>[^/]+)/mycoach/$',coaches.views.ListMyCoach.as_view(), name="List_request_accecpt_coach"),
    
    url(r'^api/user/?(?P<coachID>[^/]+)/module/(?P<moduleID>\d+)/liststudent/$',coaches.views.ListMyStudent.as_view(), name="list_my_student"),
    ######33
    
    
    #request section student caoch end
    
    
    #journal section
    url(r'^api/goingonlist',students.views.GoingOnToadylist.as_view(), name="what_going_on_today"),
    url(r'^api/user/?(?P<studentID>[^/]+)/createjournal',students.views.CreateJournalFront.as_view(), name="create_journal"),
    url(r'^api/user/uploadjournalattachment',students.views.UploadJournalAttachment.as_view(), name="upload_journal_attachment"),
    url(r'^api/user/?(?P<studentID>[^/]+)/editjournal/?(?P<pk>[^/]+)/$',students.views.EditJournal.as_view(), name="edit_journal"),
    url(r'^api/user/?(?P<studentID>[^/]+)/deletejournal/?(?P<pk>[^/]+)/$',students.views.DeleteJournal.as_view(), name="edit_journal"),
    
    url(r'^api/user/?(?P<studentID>[^/]+)/addjournal',students.views.CreateJournal.as_view(), name="add_journal"),
    url(r'^api/user/?(?P<userID>[^/]+)/journal/?(?P<pk>[^/]+)/sharelistcoach/$',students.views.ListMyCoachForStudentJournal.as_view(), name="List_coach_shared_journal"),
    url(r'^api/user/?(?P<userID>[^/]+)/journal/?(?P<pk>[^/]+)/sharemycoach/$',students.views.SharedJournalToCoach.as_view(), name="shared_journal_to_coach"),
    url(r'^api/user/?(?P<userID>[^/]+)/journal/?(?P<pk>[^/]+)/shareunsharelist/$',students.views.ListMyCoachForStudentJournalSharedUnshared.as_view(), name="List_coach_shared_unshared_journal"),
    url(r'^api/journal/?(?P<pk>[^/]+)/unsharemycoach/$',students.views.UnSharedJournalToCoach.as_view(), name="unshared_journal_to_coach"),
    
    url(r'^api/journal/?(?P<pk>[^/]+)/feedback/$',students.views.FeedbackToSharedJournal.as_view(), name="list_add_feedback_shared_journal"),
    url(r'^api/user/uploadjournalfeedbackattachment',students.views.UploadJournalFeedbackAttachment.as_view(), name="add_feedback_attachment_journal"),
    
    
    url(r'^api/user/?(?P<studentID>[^/]+)/journallist/$',students.views.ListJournalModuleList.as_view(), name="journal_list"),
    url(r'^api/user/?(?P<studentID>[^/]+)/searchjournallist/$',students.views.SearchListJournalModule.as_view(), name="search_journal_list"),
    url(r'^api/journal/?(?P<pk>[^/]+)/detail/$',students.views.JournalDetailpage.as_view(), name="journal_detailpage"),
    
    #notification section
    url(r'^api/user/?(?P<userID>[^/]+)/notification',notification.views.GetUpdateNotification.as_view(), name="list_student_notification"),
    url(r'^api/user/?(?P<userID>[^/]+)/updatepushtoken',students.views.UpdatePushNotificationToken.as_view(), name="update_push_notification_token"),
    url(r'^api/user/?(?P<userID>[^/]+)/v1updatepushtoken',students.views.UpdatePushNotificationTokenv1.as_view(), name="update_push_notification_token"),
    
    #user add report problem
    url(r'^api/user/?(?P<userID>[^/]+)/addreport',views.AddUserReportProblem.as_view(), name="user_add_report_problem"),
    
    #ListUserWiseNotification
    url(r'^api/user/?(?P<userID>[^/]+)/usernotification',notification.views.ListUserWiseNotification.as_view(), name="user_wise_notification"),
    url(r'^api/user/?(?P<userID>[^/]+)/detailnotification/?(?P<pk>[^/]+)/$',notification.views.NotificationDetail.as_view(), name="user_wise_notification_update_detail_page"),
    url(r'^api/user/?(?P<userID>[^/]+)/deletenotification/?(?P<pk>[^/]+)/$',notification.views.DelteNotificationDetail.as_view(), name="user_wise_notification_delete"),
    url(r'^api/user/?(?P<userID>[^/]+)/unreadnotification',notification.views.UnreadNotification.as_view(), name="unread_notification"),
    
    
    #get genral settings
    url(r'^api/genralsetting/$',views.ListGeneralSetting.as_view(), name="get_genral_setting"),
    
    #Strip Subscription
    url(r'^api/createsubscription/$',views.CreateSubsciption.as_view(), name="create_subscription"),
    url(r'^api/unsubscription/$',views.UnSubsciption.as_view(), name="un_subscription"),
    url(r'^api/user/?(?P<userID>[^/]+)/orderhistory/$',views.GetUserOrderHistory.as_view(), name="get_user_order_history"),
    url(r'^api/user/?(?P<userID>[^/]+)/v1orderhistory/$',views.IOSGetUserOrderHistoryV1.as_view(), name="get_user_order_history"),
    url(r'^api/user/?(?P<userID>[^/]+)/module/(?P<moduleID>\d+)/createcharge/$',views.CreateChargeModule.as_view(), name="create_charge_module"),
    url(r'^api/invitationmessage/$',views.GetInvitationWriteMessage.as_view(), name="get_inviation_message"),
    url(r'^api/journalentrymessage/$',views.GetJournalEntryWriteMessage.as_view(), name="get_journalentry_message"),
    
    #testing porpose
    url(r'^api/testmail/$',views.TestSendMail.as_view(), name="send_mail"),
    url(r'^api/testmailV1/$',views.TestSendMailV1.as_view(), name="test_send_mail"),
    
    url(r'^api/getcustomer/$',views.TestCustomerCURL.as_view(), name="get_customer"),
    url(r'^api/getsubscription/$',views.GetSubsciption.as_view(), name="get_subscription"),
    url(r'^api/snippets/$', views.student_list),
    url(r'^api/snippets/(?P<pk>[0-9]+)/$', views.student_detail),
    
    #url(r'^api/frontjournal/(?P<pk>[0-9]+)/$', students.views.GetJournalFront),
    url(r'^api/frontjournal/(?P<pk>[0-9]+)/$', students.views.GetJournalFront.as_view(), name="Get_journal_detail"),
    
    url(r'^journal/(?P<pk>[0-9]+)/$', students.views.GetJournalFrontV1.as_view(), name="Get_journal_detail_v1"),
    
    url(r'^.well-known/assetlinks.json', students.views.RunJson, name='RunJson'),
    url(r'^.well-known/apple-app-site-association', students.views.RunAppleJson, name='RunAppleJson'),
    
    
    url(r'^api/webhooksubscribe/$', views.WebhookSubscribe, name='webhooksubscribe'),
    url(r'^api/webhooksubscribedone/$', views.WebhookSubscribeDone.as_view(), name='WebhookSubscribeDone'),
    url(r'^api/webhookchargemoduledone/$', views.WebhookChargeModuleDone.as_view(), name='WebhookChargeModuleDone'),
    
    url(r'^api/webhookunsubscribe/$', views.WebhookUnSubscribe, name='webhookunsubscribe'),
    
    
    #export csv url admin
    url(r'^admin/export/goalcsv/$', students.views.Export_to_csv_goal, name='export_goal_csv'),
    url(r'^admin/export/journalcsv/$', students.views.Export_to_csv_journal, name='export_journal_csv'),
    url(r'^admin/export/journalfeddbackcsv/$', students.views.Export_to_csv_journal_feedback, name='export_journal__feddback_csv'),
    url(r'^admin/export/studentassignmnet/$', students.views.Export_to_csv_assignment, name='export_assignment_csv'),
    url(r'^admin/export/studentcoach/$', views.Export_to_csv_studentcoach, name='export_studentcoach_csv'),
    
    #admin Customize
    url(r'^api/gettotalmodule/?(?P<pk>[^/]+)/$', coaches.views.GetModuleTotal.as_view(), name='Get_number_of_module'),
    url(r'^api/getreferenceuserdetail/?(?P<pk>[^/]+)/$', coaches.views.GetUserReferenceUserdetail.as_view(), name='Get_number_of_module'),
    url(r'^api/getmodulehomeworklink/?(?P<pk>[^/]+)/$', coaches.views.GetModuleHomeworkLink.as_view(), name='getmodulehomeworklink'),
    
    #V1.1
    url(r'^api/generateimagelink/$', coaches.views.GetAWSLink.as_view(), name='GetAWSLink'),
    url(r'^api/userassignbadges/?(?P<pk>[^/]+)/?(?P<param>[^/]+)/$', gamification.comman_function.AssignUserBadgePoints, name='UserAssignBadge'),
    url(r'^api/getall_badge/$', gamification.views.GetAllBadge.as_view(), name='GetAllBadge'),
    
    
    
]


from django.conf.urls import handler404, handler500

handler404 = students.views.error_404
#handler500 = students.views.error_500

if settings.DEBUG:
        urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT) 



# Change admin site title
admin.site.site_header = ("Keyy Administration")
admin.site.site_title = ("Keyy Site Admin")