from django.contrib import admin
from django.conf import settings
import datetime
import dateutil.parser

from coaches.models import Coachmodule

# Register your models here.

from .models import Trainingactivity
admin.site.register(Trainingactivity,list_display=["title"])

from .models import journalentryoption
admin.site.register(journalentryoption,list_display=["title"])

from .models import StudentCoach


class StudentCoachAdmin(admin.ModelAdmin):
    def last_login_date(self):
        s = self.last_login_date
        d = dateutil.parser.parse(s)
        return d
    
    fieldsets = [
        (None,               {'fields': ['first_name','last_name','account_type','e_mail','user_name','id','pub_date','last_login_date','password','training_location','training_activity','other_activity','gender','date_of_birth','phone_number','profile_picture','profile_picture2','skill','profile_bio','social_login','social_token','status','hidden_profile_picture']}),
        ('Subscription', {'fields': ['subscription','subscription_start_date','subscription_period','subscription_amount']}),
        ('Notification', {'fields': ['noti_jounral_entry','noti_module_assign','noti_conn_request','noti_personal_reminder','noti_sales_promoation_offer','noti_announce_update','noti_coach_publish_new_module']}),
        ('User Referce Data', {'fields': ['unique_id','invite_count']}),
    ]
    
    def subscription(self):
        if(self.subscription == True):
            subscriptiondata ='Yes'
        elif(self.subscription_invite == True):
            subscriptiondata ='Yes'
        else:
            subscriptiondata ='No'
        
        return subscriptiondata
    
    
    def modules_published(self):
        html = ''
        getmodule = Coachmodule.objects.filter(coach=self.id,module_status='publish').count()
        if(getmodule > 0):
            return getmodule
        else:
            return '-'
    
    
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["id","pub_date","last_login_date","subscription_start_date","subscription_period","subscription_amount","unique_id","invite_count"]
        else:
            return self.readonly_fields
    
    
    list_display=("first_name","last_name","e_mail","pub_date",last_login_date,subscription,"account_type",modules_published)
    #'device_type','push_token'

admin.site.register(StudentCoach,StudentCoachAdmin)

from .models import Videos
class CommanScreenVideo(admin.ModelAdmin):
    
    def get_actions(self, request):
        #Disable delete
        actions = super(CommanScreenVideo, self).get_actions(request)
        del actions['delete_selected']
        return actions
    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False
    def has_add_permission(self, request):
        return False
    
admin.site.register(Videos,CommanScreenVideo)


from .models import UserReportProblem
class UserReportAdmin(admin.ModelAdmin):
    list_display=["user_id","title","date"]
    fields = ("user_id","title","message","date")
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["user_id"]
        else:
            return self.readonly_fields
    def has_add_permission(self, request):
        return False
admin.site.register(UserReportProblem,UserReportAdmin)


from .models import GeneralSetting
class GeneralSettingAdmin(admin.ModelAdmin):
    list_display=("monthly_plan_id","yearly_plan_id","publishable_key","secret_key")
    def get_actions(self, request):
        #Disable delete
        actions = super(GeneralSettingAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False
    
    def has_add_permission(self, request):
        return False
    

admin.site.register(GeneralSetting,GeneralSettingAdmin)


from .models import WriteJournalEntryMessage
class WriteJournalEntryMessageAdmin(admin.ModelAdmin):
    def messages(self):
        s = '<a href="%s">%s</a>' % (self.id, 'Message Settings')
        return s
    messages.allow_tags = True
    
    list_display=(messages,)
    
    def get_actions(self, request):
        #Disable delete
        actions = super(WriteJournalEntryMessageAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False
    
    def has_add_permission(self, request):
        return False
    
admin.site.register(WriteJournalEntryMessage,WriteJournalEntryMessageAdmin)


from .models import WriteinvitationMessage
class WriteInvitationMessage(admin.ModelAdmin):
    
    def get_actions(self, request):
        #Disable delete
        actions = super(WriteInvitationMessage, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False
    
    def has_add_permission(self, request):
        return False
    
    
admin.site.register(WriteinvitationMessage,WriteInvitationMessage)

from .models import OrderHistory
class OrderHistoryAdmin(admin.ModelAdmin):
    list_display=["user","object_type","status","created_date"]
    
    fields = ["user","customer_id","subscription_id","plan_type","charge_id","object_type","module_id","transaction_id","status","created_date"]
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["user","customer_id","subscription_id","plan_type","charge_id","object_type","module_id","transaction_id","status","created_date"]
        else:
            return self.readonly_fields
    
    def has_add_permission(self, request):
        return False
    
admin.site.register(OrderHistory,OrderHistoryAdmin)



from .models import SiteSetting
class SiteSettingAdmin(admin.ModelAdmin):
    def get_actions(self, request):
        #Disable delete
        actions = super(SiteSettingAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions

    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False
    
    def has_add_permission(self, request):
        return False
admin.site.register(SiteSetting,SiteSettingAdmin)



#from .models import WebhookTransaction
#admin.site.register(WebhookTransaction)

from .models import AppIntroductionPage
admin.site.register(AppIntroductionPage)

from .models import Onboarding
admin.site.register(Onboarding)