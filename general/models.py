from __future__ import unicode_literals

import datetime

import random
import string

from django.db import models
from datetime import datetime

# Create your models here.
from django.conf import settings
from django.core.exceptions import ValidationError
import os

# Create your models here.
class Trainingactivity(models.Model):
    title = models.CharField(max_length=250,unique=True)
    class Meta:
        verbose_name = 'Training Activity'
        verbose_name_plural = 'Training Activity Option'
    def __unicode__(self):
        return '%s' % (self.title)
    

class journalentryoption(models.Model):
    title = models.CharField(max_length=250,unique=True)
    class Meta:
        verbose_name = 'Journal entry option'
        verbose_name_plural = 'What is going on today option'
    def __unicode__(self):
        return self.title
    

def random_key(length):
    key = ''
    key += ''.join(random.choice(string.ascii_uppercase) for _ in range(8))
    return key

class StudentCoach(models.Model):
    first_name = models.CharField(max_length=250,blank=True)
    last_name = models.CharField(max_length=250,blank=True)
    account_type = models.CharField(max_length=250, choices=settings.ACCOUNT_TYPE,blank=True)
    e_mail = models.EmailField(blank=True)
    user_name = models.CharField(max_length=250,blank=True)
    password = models.CharField(max_length=250,blank=True)
    training_location = models.CharField(max_length=250,blank=True)
    training_activity = models.ManyToManyField(Trainingactivity, verbose_name="What activities are you currently training?",blank=True)
    other_activity = models.CharField(max_length=250,blank=True)
    gender = models.CharField(max_length=250,choices=settings.CHOICES_GENDER,blank=True)
    date_of_birth = models.DateField(default='1970-01-01',blank=True)
    phone_number = models.CharField(max_length=50,blank=True)
    profile_picture = models.FileField(upload_to = 'student/student_profilepic/', blank=True)
    profile_picture2 = models.CharField(max_length=255,blank=True)
    hidden_profile_picture = models.CharField(max_length=250,blank=True)
    skill = models.CharField(max_length=250,blank=True)
    profile_bio = models.TextField(blank=True)
    social_login = models.CharField(max_length=25,default=False,blank=True)
    social_token = models.CharField(max_length=250,blank=True)
    device_type = models.CharField(max_length=250,blank=True)
    push_token = models.CharField(max_length=250,blank=True)
    noti_jounral_entry = models.CharField(max_length=1,verbose_name="Jounral entry comments ", choices=settings.NPTIFICATION_CHOICES,default=1)
    noti_module_assign = models.CharField(max_length=1, verbose_name="New modules assigned", choices=settings.NPTIFICATION_CHOICES,default=1)
    noti_conn_request = models.CharField(max_length=1, verbose_name="New connection request", choices=settings.NPTIFICATION_CHOICES,default=1)
    noti_personal_reminder = models.CharField(max_length=1, verbose_name="Personalized reminders", choices=settings.NPTIFICATION_CHOICES,default=1)
    noti_sales_promoation_offer = models.CharField(max_length=1, verbose_name="Sales promoation and offers", choices=settings.NPTIFICATION_CHOICES,default=1)
    noti_announce_update = models.CharField(max_length=1, verbose_name="Keyy announcements and updates", choices=settings.NPTIFICATION_CHOICES,default=1)
    noti_coach_publish_new_module = models.CharField(max_length=1, verbose_name="Coaches publish new module", choices=settings.NPTIFICATION_CHOICES,default=1)
    status = models.CharField(max_length=1, choices=settings.STATUS_CHOICES,blank=True)
    subscription = models.BooleanField(default=False)
    subscription_invite = models.BooleanField(default=False)
    invite_count = models.CharField("Total Number Of Sign Up User",max_length=250,blank=True)
    subscription_expiredate = models.DateField('Subscription Expire Date', auto_now_add=False, blank=True, null=True)
    paymentdata = models.TextField(blank=True)
    last_login_date = models.CharField('User Last Login Date',max_length=250,default=datetime.now, blank=True)
    total_numberof_openapp = models.CharField('Total Number of App Open',max_length=250,blank=True)
    subscription_start_date = models.DateField('Subscription Start Date', auto_now_add=False, blank=True, null=True)
    subscription_period = models.CharField(max_length=250,blank=True)
    subscription_amount = models.CharField(max_length=250,blank=True)
    strip_customer_id = models.CharField(max_length=250,blank=True)
    unique_id = models.CharField("My Unique Code",max_length=250,blank=True)
    reference_code = models.CharField(max_length=250,blank=True)
    pub_date = models.DateTimeField('User Sign Up Date', default=datetime.now, blank=True)
    updated = models.CharField(max_length=250,blank=True)
    
    class Meta:
        verbose_name = 'User'
        verbose_name_plural = 'Student & Coach'
        
    def __unicode__(self):
        return self.first_name
    
    def delete(self, *args, **kwargs):
        profile_picture = settings.BASE_DIR + settings.MEDIA_URL + str(self.profile_picture)
        if os.path.isfile(profile_picture):
            os.unlink(profile_picture)

        super(StudentCoach, self).delete(*args, **kwargs)
    


# Receive the pre_delete signal and delete the file associated with the model instance.
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

@receiver(pre_delete, sender=StudentCoach)
def studentcoach_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    if(instance.profile_picture != ''):
        instance.profile_picture.delete(False)





class Videos(models.Model):
    coach_video = models.FileField(upload_to = 'screenVideo/',blank=True)
    student_video = models.FileField(upload_to = 'screenVideo/',blank=True)
    coach_video_thumb = models.FileField(upload_to = 'screenVideo/thumb/',blank=True)
    student_video_thumb = models.FileField(upload_to = 'screenVideo/thumb/',blank=True)
    status = models.CharField(max_length=1, choices=settings.STATUS_CHOICES)
    class Meta:
        verbose_name = 'Common Screen Video'
        verbose_name_plural = 'Common Screen Video'
        
        

class UserReportProblem(models.Model):
    user_id = models.ForeignKey(StudentCoach, related_name="UserReportProblem")
    title = models.CharField(max_length=250,blank=True)
    message = models.TextField(blank=True)
    date = models.DateTimeField(default=datetime.now,blank=True)
    class Meta:
        verbose_name = 'User Report a problem'
        verbose_name_plural = 'Report a problem'


class GeneralSetting(models.Model):
    monthly_plan_id = models.CharField(max_length=250)
    yearly_plan_id = models.CharField(max_length=250)
    publishable_key = models.TextField("Strip Publishable key",blank=True)
    secret_key = models.TextField("Strip Secret key",blank=True)
    class Meta:
        verbose_name = 'Stripe General Setting'
        verbose_name_plural = 'Stripe General Setting'
        

class WriteinvitationMessage(models.Model):
    message = models.TextField("Write invitation Message",max_length=260,help_text=("<br><label style='width:auto;'>Please add tag for display link: <b>{link}</b></label> "),blank=True)
    def clean(self):
        # Don't allow draft entries to have a pub_date.
        data = self.message
        if "{link}" not in data:
            raise ValidationError('Please add tag for display link: {link}')
        
    class Meta:
        verbose_name = 'Write Invitation Message'
        verbose_name_plural = 'Write Invitation Message'
        
class WriteJournalEntryMessage(models.Model):
    feedback_message = models.TextField("Feedback Message",max_length=260,blank=True)
    social_sharing_message = models.TextField("Social Sahring Message",max_length=260,help_text=("<br><label style='width:auto;'>Please add tag for display link: <b>{link}</b></label> "),blank=True)
    module_upgrade_message = models.TextField("Module Upgrade Message",max_length=260,blank=True)
    
    def clean(self):
        # Don't allow draft entries to have a pub_date.
        data = self.social_sharing_message
        if "{link}" not in data:
            raise ValidationError('Please add tag for display link: {link}')
        
        
    class Meta:
        verbose_name = 'Message Setting'
        verbose_name_plural = 'Message Setting'
        

class UserUniqueCodeForgotPass(models.Model):
    user = models.ForeignKey(StudentCoach, related_name="UserForgotUniqueCode")
    unique_code = models.CharField("Unique Code",max_length=250,blank=True)
    created_datetime = models.DateTimeField('Generated Date', auto_now_add=False, blank=True)
    expire_datetime = models.DateTimeField('Expire Date', auto_now_add=False, blank=True)
    class Meta:
        verbose_name = 'User Forgot Password'
        verbose_name_plural = 'User Forgot Password'
        
        

class OrderHistory(models.Model):
    user = models.ForeignKey(StudentCoach, related_name="OrderUser")
    customer_id = models.CharField("Customer ID",max_length=250,blank=True)
    subscription_id = models.TextField("subscription ID",blank=True)
    charge_id = models.CharField("Charge ID",max_length=250,blank=True)
    object_type = models.CharField("Type of Object",max_length=250,blank=True)
    plan_type = models.CharField("Plan",max_length=250,blank=True)
    module_id = models.CharField("Module ID",max_length=250,blank=True)
    payment_data = models.TextField("Payment Data",blank=True)
    status = models.CharField("Status",max_length=250,blank=True)
    transaction_id = models.CharField("Transation ID",max_length=250,blank=True)
    created_date = models.DateTimeField(default=datetime.now, blank=True)
    subscription_date = models.CharField(max_length=250,default=datetime.now, blank=True)

    class Meta:
        verbose_name = 'Order History'
        verbose_name_plural = 'Order History'
        
        
        
        
class SiteSetting(models.Model):
    isPaymentEnable = models.BooleanField(default=False,verbose_name="Is Payment Enable?")
    class Meta:
        verbose_name = 'General Setting'
        verbose_name_plural = 'General Setting'
        
        
        
class WebhookTransaction(models.Model):
    body = models.TextField(blank=True)
    request_meta = models.TextField(blank=True)
    
    class Meta:
        verbose_name = 'Webhook'
        verbose_name_plural = 'Webhook'


class AppIntroductionPage(models.Model):
    title = models.CharField(max_length=250)
    description = models.TextField()

    class Meta:
        verbose_name = 'App Introduction Page'
        verbose_name_plural = 'App Introduction Page'


class Onboarding(models.Model):
    PAGENAME = (('1', 'Search / invite Students'),('2', 'Add Goal'),('3', 'Assignments'), ('4', 'Start'))
    user_type = models.CharField(max_length=250, choices=settings.ACCOUNT_TYPE)
    title = models.CharField(max_length=250)
    description = models.TextField()
    page_name = models.CharField(max_length=250, choices=PAGENAME)

    class Meta:
        verbose_name = 'User Onboarding Flow'
        verbose_name_plural = 'User Onboarding Flow'


