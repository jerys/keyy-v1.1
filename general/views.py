from django.shortcuts import render
#from django.core.mail import send_mail
from django.core.mail import EmailMessage
from django.template import Context
from django.template.loader import render_to_string, get_template

from django.contrib.auth.models import User, Group

from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view

from rest_framework import permissions,viewsets
from rest_framework.generics import CreateAPIView, UpdateAPIView

from general.models import StudentCoach
from general.models import UserUniqueCodeForgotPass
from general.models import OrderHistory

from general.models import Videos
from general.models import Trainingactivity
from general.models import UserReportProblem
from general.models import GeneralSetting
from general.models import WriteinvitationMessage
from general.models import WriteJournalEntryMessage
from general.models import SiteSetting
from general.models import WebhookTransaction

from students.models import StudentGoal
from students.models import Journal
from students.models import StudentAssignment
from students.models import StudentAssignUnassignTrack

from notification.models import Notification


from coaches.models import Coachmodule
from coaches.models import Tag

from notification.models import StudentCoachRequest

from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.views import APIView

from django.http import HttpResponse, JsonResponse, Http404
from django.views.decorators.csrf import csrf_exempt
from rest_framework.renderers import JSONRenderer
from rest_framework.parsers import JSONParser

from rest_framework import authentication

from general.serializers import StudentSerializer, StudentUpdateSerializer, ForgotPassSerializer, UserLoginSerializer, RegisterSerializer, ChangePassSerializer, UserCommanscrrenSerializer, TrainingAcivitySerializer, StudentHomeDataSerializer, StudentGoalSerializer, CommanVideoSerializer, StudentGoalAddSerializer, StudentGoalDeleteSerializer, StudentGoalUpdateSerializer, StudentUpdateProfileSerializer, StudentAssignmentSerializer, StudentUnenrollSerializer, StudentMarkasCompleteSerializer, CoachSerializer, RegisterUniqueIDSerializer, UserReportProblemSerializer, ReferenceUserSerializer, UserStripCustomerIDSerializer, UserStripResponseSerializer, UniqueCodeCheckSerializer, GeneralSettingSerializer, ResetPassSerializer, OrderSerializer, CreateSubscriptionSerializer, WriteMessageSerializer, CreateChargeSerializer, UnSubscriptionSerializer, TestEmailSerializer, StudentGoalEditSerializer, JournalEntryMessageSerializer, GeneralSettingUnSubSerializer, StudentUpdateProfileSerializerV1, CommanUserSerializer, IOSSubscriptionSerializer, IOSCreateChargeSerializer, UserLastLoginUpdateSerializer

from django.conf import settings
import os
import datetime
import dateutil.parser
import uuid
import re

import calendar
import urllib2
import urllib
from urllib2 import Request, urlopen, URLError
import json
from django.core import serializers
from decimal import Decimal

from django.db.models import Q
import csv

import boto3
import botocore
from django.core.files.storage import default_storage

import gamification
# Create your views here.

#login api

class UserLoginAPIView(APIView):
    serializer_class = UserLoginSerializer
    def post(self, request, *args, **kwargs):
        
        bot_email = request.POST['e_mail']
        bot_password = request.POST['password']
        
        data = request.data
        #serializer = UserLoginSerializer(data=data)
        if (bot_email == '' or bot_password == ''):
            #return JsonResponse({"success":False, "message": "Please enter required field", "user":{} })
            return JsonResponse({"response_code":"1", "response_msg": "Please enter required field", "user": {"e_mail":bot_email} })
        else:
            try:
                useremailcheck = StudentCoach.objects.filter(e_mail=bot_email).count()
                if(useremailcheck == 0):
                    return JsonResponse({"response_code":"102", "response_msg": "Email address wrong", "user": {"e_mail":bot_email} })
                
                userpasscheck = StudentCoach.objects.filter(e_mail=bot_email,password=bot_password).count()
                if(userpasscheck == 0):
                    return JsonResponse({"response_code":"103", "response_msg": "Password wrong", "user": {"e_mail":bot_email} })
                
                #userobj = StudentCoach.objects.filter(e_mail=bot_email,password=bot_password)
                userobj = StudentCoach.objects.get(e_mail=bot_email,password=bot_password)
                userobj.last_login_date = datetime.datetime.now()
                userobj.save()
                serializer = UserLoginSerializer(userobj, data=request.data)
                if serializer.is_valid():
                    if(serializer.data['profile_picture']):
                        ProfilepicURL = serializer.data['profile_picture']
                    elif(serializer.data['profile_picture2']):
                        ProfilepicURL = serializer.data['profile_picture2']
                    else:
                        ProfilepicURL = ''
                        
                
                return JsonResponse({"response_code":"0", "response_msg": "You have login successfully", "user":   {"user_id":userobj.pk,"first_name":userobj.first_name,"last_name":userobj.last_name,"e_mail":userobj.e_mail,"account_type":userobj.account_type, "subscription":userobj.subscription,"subscription_invite":userobj.subscription_invite,"user_name":userobj.user_name,"profile_picture":ProfilepicURL,"hidden_profile_picture":userobj.hidden_profile_picture,'unique_id':userobj.unique_id,"updated":userobj.updated } })
            except StudentCoach.DoesNotExist:
                #return JsonResponse({"success":False, "message": "Invlaid username or password", "data": serializer.data})
                return JsonResponse({"response_code":"1", "response_msg": "login failed", "user": {} })
        
        
def add_months(sourcedate,months):
    month = sourcedate.month - 1 + months
    year = int(sourcedate.year + month / 12 )
    month = month % 12 + 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return datetime.date(year,month,day)



def days_between(year, month, day):
    #from datetime import datetime
    today = datetime.date.today()
    someday = datetime.date(int(year), int(month), int(day))
    diff = today - someday
    d = diff.days
    return d

class UserUpdateLastLogin(APIView):
    from datetime import datetime
    serializer_class = UserLastLoginUpdateSerializer
    def get(self, request, pk, format=None):


        userobj = StudentCoach.objects.get(pk=pk)
        lastloginday = dateutil.parser.parse(userobj.last_login_date).strftime("%d")
        currentday = datetime.date.today().strftime("%d")

        lastloginmonth = dateutil.parser.parse(userobj.last_login_date).strftime("%m")
        currentmonth = datetime.date.today().strftime("%m")
        
        if(lastloginmonth != currentmonth):
            totalcount = 1
            userobj.total_numberof_openapp = totalcount

        else:
            if(lastloginday != currentday):
                #update total number of time app open in month
                totalcount = 1
                if(userobj.total_numberof_openapp != ''):
                    totalcount = int(userobj.total_numberof_openapp) + 1
                
                userobj.total_numberof_openapp = totalcount
        
        userobj.last_login_date = datetime.datetime.now()
        userobj.save()

        
        #Number of days since sign up date
        year = userobj.pub_date.strftime("%Y")
        month = userobj.pub_date.strftime("%m")
        day = userobj.pub_date.strftime("%d")
        totaldays = days_between(year, month, day)
        gamification.comman_function.update_user_data(userobj.id,'numberofdaysincesignup',totaldays)

        #Number of times app used
        total_app_open_in_one_month = userobj.total_numberof_openapp
        gamification.comman_function.update_user_data(userobj.id,'numberoftimesappopen',total_app_open_in_one_month)

        return Response({"response_code":True, "response_msg": "User Last Login Updates", "user": {} })
    
#register api
class UserRegisterAPIView(CreateAPIView):
    serializer_class = RegisterSerializer
    def create(self, request, *args, **kwargs):
        
        if request.GET.get('code', None):
            UniqueCode = request.GET.get('code')
        else:
            UniqueCode = ''
        
        e_mail = request.POST.get('e_mail')
        user_name = request.POST.get('user_name')
        social_login = request.POST.get('social_login')
        #return JsonResponse({"response_code":"101", "response_msg": "Username already register", "user": {"res":user_name } })
        
        flag = ''
        if e_mail != '':
            try:
                checkemail = StudentCoach.objects.get(e_mail=request.POST['e_mail'])
                if (checkemail.social_login == 'True' and social_login == 'True'):
                    return Response({"response_code":"0", "response_msg": "You have login successfully", "user": {"user_id":checkemail.pk,"first_name":checkemail.first_name,"last_name":checkemail.last_name,"e_mail":checkemail.e_mail,"account_type":checkemail.account_type, "subscription":checkemail.subscription,"user_name":checkemail.user_name,"hidden_profile_picture":checkemail.hidden_profile_picture,'unique_id':checkemail.unique_id } })
                elif (checkemail.e_mail == e_mail):
                    return Response({"response_code":"101", "response_msg": "Email already register", "user": {} })
                elif (user_name != ''):
                    try:
                        checkuser = StudentCoach.objects.get(user_name=request.POST['user_name'])
                        if(checkuser.user_name == user_name):
                            return Response({"response_code":"105", "response_msg": "User name already register", "user": {} })
                        else:
                            flag = True
                    except:
                        flag = True
            except:
                if (user_name != ''):
                    try:
                        checkuser = StudentCoach.objects.get(user_name=request.POST['user_name'])
                        if(checkuser.user_name == user_name):
                            return Response({"response_code":"105", "response_msg": "User name already register", "user": {} })
                        else:
                            flag = True
                    except:
                        flag = True
                else:
                    flag = True
        elif social_login == 'True':
            if user_name != '' :
                try:
                    checkusername = StudentCoach.objects.get(user_name=user_name)
                    if (checkusername.social_login == 'True' and social_login == 'True'):
                        return Response({"response_code":"0", "response_msg": "You have login successfully", "user": {"user_id":checkusername.pk,"first_name":checkusername.first_name,"last_name":checkusername.last_name,"e_mail":checkusername.e_mail,"account_type":checkusername.account_type, "subscription":checkusername.subscription,"user_name":checkusername.user_name,"hidden_profile_picture":checkusername.hidden_profile_picture,'unique_id':checkusername.unique_id } })
                    
                    elif (checkusername.user_name == user_name):
                        return Response({"response_code":"105", "response_msg": "Username already register", "user": {} })
                except:
                    flag = True
        else:
            return Response({"response_code":"1", "response_msg": "Sign up failed", "user": {} })

        if flag == True:
            serialized = RegisterSerializer(data=request.data)
            if serialized.is_valid():
                userobj = serialized.save()
                
                #get record and generate unique id 
                datenow = datetime.datetime.now().strftime('%m%d')
                if(userobj.user_name != ''):
                    mystring = userobj.user_name.upper()
                    name = re.sub('[^A-Za-z0-9]+', '', mystring)
                    
                elif (userobj.first_name != ''):
                    mystring = userobj.first_name.upper()
                    name = re.sub('[^A-Za-z0-9]+', '', mystring)
                else:
                    mystring = uuid.uuid4().hex[:4].upper()
                    name = re.sub('[^A-Za-z0-9]+', '', mystring)
                    
                uniqueID = str(userobj.pk) + name + datenow
                user = StudentCoach.objects.get(pk=userobj.pk)
                user.unique_id = uniqueID
                if(UniqueCode != ''):
                    user.reference_code = UniqueCode
                
                user.save()
                
                #checkuniquecode and update referense
                if(UniqueCode != ''):
                    refrenceuserobj = StudentCoach.objects.get(unique_id=UniqueCode)
                    invite_count = refrenceuserobj.invite_count
                    if(invite_count == ''):
                        InviteCount = 1
                    else:
                        InviteCount = int(invite_count) + 1
                    
                    #check if 5 to get pro for 3 months
                    if(InviteCount == 5):
                        refrenceuserobj.subscription_invite = True
                        
                        #if refrenceuserobj.subscription_expiredate is None :
                        somedate = datetime.date.today()
                        ExpireDate = add_months(somedate,3)
                        '''
                        else:
                            somedate = refrenceuserobj.subscription_expiredate
                            ExpireDate = add_months(somedate,3)
                        '''
                        refrenceuserobj.subscription_expiredate = ExpireDate
                        
                    refrenceuserobj.invite_count = InviteCount
                    refrenceuserobj.save()
                    
                    #add total referal user signup
                    gamification.comman_function.update_user_data(refrenceuserobj.id,'totalreferralsignup',InviteCount)
                    
                    
                
                #return Response(serialized.data, status=status.HTTP_201_CREATED)
                return Response({"response_code":"0", "response_msg": "You have sign up successfully", "user": {"user_id":userobj.pk,"first_name":userobj.first_name,"last_name":userobj.last_name,"e_mail":userobj.e_mail,"account_type":userobj.account_type,"subscription":userobj.subscription,"user_name":userobj.user_name,"hidden_profile_picture":userobj.hidden_profile_picture,'unique_id':uniqueID } })
                #return Response(serialized._errors, status=status.HTTP_400_BAD_REQUEST)
            return Response({"response_code":"1", "response_msg": "Sign up failed", "user": {} })
        else:
            return Response({"response_code":"1", "response_msg": "Sign up failed", "user": {} })
        
    
#user delte api
class UserDelete(APIView):
    def get_object(self, pk):
        try:
            return StudentCoach.objects.get(pk=pk)
        except StudentCoach.DoesNotExist:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "user": {} })
    def delete(self, request, pk, format=None):
        snippet = self.get_object(pk)
        snippet.delete()
        return JsonResponse({"status":True, "response_msg": "User delte successfully", "user": {"user_id":snippet.id,"first_name":snippet.first_name,"last_name":snippet.last_name,"e_mail":snippet.e_mail,"user_name":snippet.user_name} })
        #return Response(status=status.HTTP_204_NO_CONTENT)

#Comman screen
class UserUpdateType(APIView):
    serializer_class = UserCommanscrrenSerializer
    def get_object(self, pk):
        try:
            return StudentCoach.objects.get(pk=pk)
        except StudentCoach.DoesNotExist:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "user": {} })
    def get(self, request, pk, format=None):
        userobj = self.get_object(pk)
        serializer = UserCommanscrrenSerializer(userobj)
        return Response({"response_code":"0", "response_msg": "User Type", "user": {"user_id":userobj.pk,"account_type":userobj.account_type}})
        
    def post(self, request, pk, format=None):
        userobj = self.get_object(pk)
        serializer = UserCommanscrrenSerializer(userobj, data=request.data)
        if serializer.is_valid():
            serializer.save()
            
            #assign to module to student
            #get module
            
            if(serializer.data['account_type'] == 'Coach'):
                moduleobj = Coachmodule.objects.filter(Q(module_status='publish'), Q(assignto_new_coach=True))
            elif(serializer.data['account_type'] == 'Student'):
                moduleobj = Coachmodule.objects.filter(Q(module_status='publish'), Q(assignto_new_student=True))
                
            if(moduleobj.count() > 0):
                for each in moduleobj:
                    if(each.student_id != ''):
                        newstudentid = each.student_id + ',' + pk
                    else:
                        newstudentid = pk
                    
                    each.student_id = newstudentid
                    each.save()
            
            
            #add default coach to user
            checkuserexits = StudentCoach.objects.filter(pk='136').count()
            if(checkuserexits > 0 ):
                requestcheck = StudentCoachRequest.objects.filter(Q(user_id='136'), Q(user_to_id=userobj.pk)).count()
                if(requestcheck == 0):
                    requestobj = StudentCoachRequest(user_id='136',user_to_id=userobj.pk,status='Approve',request_to=serializer.data['account_type'],temp_status='S')
                    requestobj.save()
            
            return Response({"response_code":"0", "response_msg": "User type update successfully", "user": {"user_id":userobj.pk,"account_type":userobj.account_type} })
        return Response({"response_code":"1", "response_msg": "Somthing wrong", "user": {} })
    

def CommanVideoView(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        try:
            snippets = Videos.objects.all()
            serializer = CommanVideoSerializer(snippets, many=True)
            #ids = serializer.data[0]['id']
            
            coach_video = serializer.data[0]['coach_video']
            student_video = serializer.data[0]['student_video']
            
            if(serializer.data[0]['student_video_thumb']):
                student_video_thumb = serializer.data[0]['student_video_thumb']
            else:
                student_video_thumb = '';
                
            
            if(serializer.data[0]['coach_video_thumb']):
                coach_video_thumb = serializer.data[0]['coach_video_thumb']
            else:
                coach_video_thumb = '';
                    
            
            #return JsonResponse({"success":True, "data": serializer.data})
            #return JsonResponse({"response_code":"0", "response_msg": "Comman video", "video":serializer.data })
            return JsonResponse({"response_code":"0", "response_msg": "Comman video", "video":{"student_video": student_video,"student_video_thumb":student_video_thumb, "coach_video_thumb":coach_video_thumb,"coach_video": coach_video} })
        except Videos.DoesNotExist:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "user": {} })


#student update edit profile
'''
class UpdateStudent(generics.RetrieveUpdateDestroyAPIView):
    queryset = StudentCoach.objects.all()
    serializer_class = StudentUpdateSerializer
'''
class UpdateStudent(APIView):
    serializer_class = StudentUpdateSerializer
    def get_object(self, pk):
        try:
            return StudentCoach.objects.get(pk=pk)
        except StudentCoach.DoesNotExist:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "user": {} })
    def get(self, request, pk, format=None):
        try:
            userobj = StudentCoach.objects.get(pk=pk)
            serializer = StudentUpdateSerializer(userobj)
            #return Response({"response_code":"0", "response_msg": "User detail", "user":serializer.data })
            #return Response({"response_code":"0", "response_msg": "User detail", "user":serializer.data })
            #
            tag = userobj.training_activity.all()
            
            
            activityarr = []
            for each in tag:
                partList = {}
                partList['id'] = each.id
                partList['title'] = each.title
                activityarr.append(partList)
            
            if(serializer.data['profile_picture']):
                ProfilepicURL = serializer.data['profile_picture']
            elif(serializer.data['profile_picture2']):
                ProfilepicURL = serializer.data['profile_picture2']
            else:
                ProfilepicURL = ''
            
            
            settingarr = []
            sitesetting = SiteSetting.objects.all()
            for eachsetting in sitesetting:
                isPaymentEnable = {}
                isPaymentEnable['isPaymentEnable'] = eachsetting.isPaymentEnable
                settingarr.append(isPaymentEnable)
            
            
            return Response({"response_code":"0", "response_msg": "User detail", "user": {"user_id":userobj.pk,"first_name":userobj.first_name,"last_name":userobj.last_name,"user_name":userobj.user_name,"e_mail":userobj.e_mail,"training_location":userobj.training_location, "profile_picture":ProfilepicURL, "skill":userobj.skill, "profile_bio":userobj.profile_bio, "phone_number":userobj.phone_number, "account_type":userobj.account_type,"gender":userobj.gender, "date_of_birth":userobj.date_of_birth, "training_activity":activityarr, "other_activity":userobj.other_activity,"hidden_profile_picture":userobj.hidden_profile_picture,"subscription":userobj.subscription,"subscription_invite":userobj.subscription_invite,'unique_id':userobj.unique_id,"social_login":userobj.social_login,"updated":userobj.updated,"isPaymentEnable":settingarr } })
        
        except:
            return Response({"response_code":"1", "response_msg": "Data not found", "user": {} }) 
    def post(self, request, pk, format=None):
        userobj = self.get_object(pk)
        serializer = StudentUpdateSerializer(userobj, data=request.data)
        if serializer.is_valid():
            serializer.save()

            #complete all fiedl in profile
            fname = request.POST.get('first_name')
            lname = request.POST.get('last_name')
            training_location = request.POST.get('training_location')
            training_activity = request.POST.get('training_activity')
            skill = request.POST.get('skill')
            profile_bio = request.POST.get('profile_bio')
            phone_number = request.POST.get('phone_number')
            gender = request.POST.get('gender')
            date_of_birth = request.POST.get('date_of_birth')
            if(fname != '' and lname !='' and training_location != '' and training_activity !='' and skill !='' and profile_bio !='' and phone_number != '' and gender != '' and date_of_birth !='' ):
                gamification.comman_function.update_user_data(pk,'completeallfieldprofile',True)
                
            
            return Response({"response_code":"0", "response_msg": "User update successfully", "user":serializer.data })
        return JsonResponse({"response_code":"1", "response_msg": "Update failed", "user": {} })
    
#change student profile pic
class UpdateStudentProfile(APIView):
    serializer_class = StudentUpdateProfileSerializer
    def post(self, request, pk, format=None):
        try:
            profilepic = request.POST.get('profile_picture')
            userobj = StudentCoach.objects.get(pk=pk)
            serializer = StudentUpdateProfileSerializer(userobj, data=request.data)
            
            '''
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
            if(userobj.profile_picture):
                oldimg = settings.BASE_DIR + settings.MEDIA_URL + str(userobj.profile_picture)
                if os.path.isfile(oldimg):
                    os.unlink(oldimg)
            '''
            
            #ret = urllib2.urlopen('http://app.keyy.io.s3.amazonaws.com/media/student/student_profilepic/blue.jpg')
            #if ret.code == 200:
                #return JsonResponse({"response_code":"0", "response_msg": "Profile change successfullysss", "user":ret.code })
            #else:
                #return JsonResponse({"response_code":"0", "response_msg": "Profile change successfully", "user":ret.code })
            
            
            if default_storage.exists(str(userobj.profile_picture)):
                default_storage.delete(str(userobj.profile_picture))
            
            if serializer.is_valid():
                if (profilepic != ''):
                    userobjupdte = serializer.save()
                    profile_pic = serializer.data['profile_picture']
                    return JsonResponse({"response_code":"0", "response_msg": "Profile change successfully", "user":{"user_id":userobjupdte.id,"profile_picture":profile_pic} })
                return JsonResponse({"response_code":"1", "response_msg": "Update failed", "user": {} })
        except:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "user": {} })
    



class UpdateStudentProfileV1(APIView):
    serializer_class = StudentUpdateProfileSerializerV1
    def post(self, request, pk, format=None):
        try:
            profilepic = request.POST.get('profile_picture2')
            userobj = StudentCoach.objects.get(pk=pk)
            serializer = StudentUpdateProfileSerializerV1(userobj, data=request.data)
            
            '''
            if default_storage.exists(str(userobj.profile_picture2)):
                #default_storage.delete(str(userobj.profile_picture2))
                return JsonResponse({"response_code":"1", "response_msg": "Update failed" })
            else:
                return JsonResponse({"response_code":"2", "response_msg": "Update failed" })
            '''
                
            if serializer.is_valid():
                if (profilepic != ''):
                    userobjupdte = serializer.save()
                    profile_pic = serializer.data['profile_picture2']
                    updated = serializer.data['updated']
                    
                    return JsonResponse({"response_code":"0", "response_msg": "Profile change successfully", "user":{"user_id":userobjupdte.id,"profile_picture":profile_pic,"updated":updated} })
                return JsonResponse({"response_code":"1", "response_msg": "Update failed", "user": {} })
        except:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "user": {} })


#training activity of student
def TariningActivityList(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        snippets = Trainingactivity.objects.all()
        serializer = TrainingAcivitySerializer(snippets, many=True)
        #return JsonResponse({"success":True, "data": serializer.data})
        return JsonResponse({"response_code":"0", "response_msg": "Acitivity option", "activity":serializer.data })

'''
class TariningActivityList(generics.ListAPIView):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Trainingactivity.objects.all()
    serializer_class = TrainingAcivitySerializer
'''


#Student Home scrren
class StudentHomeProfile(APIView):
    serializer_class = StudentHomeDataSerializer
    def get_object(self, pk):
        try:
            snippet = StudentCoach.objects.get(pk=pk)
            return snippet 
        except StudentCoach.DoesNotExist:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "user": {} })
    def get(self, request, pk, format=None):
        userobj = self.get_object(pk)
        serializer = StudentHomeDataSerializer(userobj)
        
        user_id = serializer.data['id']
        first_name = serializer.data['first_name']
        last_name = serializer.data['last_name']
        user_name = serializer.data['user_name']
        skill = serializer.data['skill']
        training_location = serializer.data['training_location']
        subscription = serializer.data['subscription']
        subscription_invite = serializer.data['subscription_invite']
        unique_id = serializer.data['unique_id']
        updated = serializer.data['updated']
        
        
        
        account_type = serializer.data['account_type']
        
        #profile_picture = settings.MEDIA_ROOT + serializer.data['profile_picture']
        
        if(serializer.data['profile_picture']):
            profile_picture = serializer.data['profile_picture']
        elif(serializer.data['profile_picture2']):
            profile_picture = serializer.data['profile_picture2']
        elif (serializer.data['hidden_profile_picture'] !='' and serializer.data['social_login'] == 'True'):
            profile_picture = serializer.data['hidden_profile_picture']
        else:
            profile_picture = ''
            
        data = {"response_code":"0", "response_msg": "Student home data", 'user': {"user_id":user_id, "first_name": first_name, "last_name":  last_name, "user_name": user_name, "profile_picture":profile_picture,"skill":skill,"training_location":training_location,"account_type":account_type,"subscription":subscription,"subscription_invite":subscription_invite,"unique_id":unique_id,"updated":updated }}
        return JsonResponse(data)
        
        #return JsonResponse({"response_code":"0", "response_msg": "Student home data", "user": serializer.data})
        #return JsonResponse({"response_code":"0", "response_msg": "Student home data", "user":{"user_id":userobj.pk,"first_name": userobj.first_name, "last_name":  userobj.last_name, "user_name": userobj.user_name, "profile_picture":userobj.profile_picture } })


#student goal view
class StudentGoalview(APIView):
    serializer_class = StudentGoalSerializer
    def get(self, request, pk, format=None):
        try:
            goalobj = StudentGoal.objects.filter(student=pk,is_deleted='No')
            if(goalobj):
                partList = {}
                quirtlly = []
                yearly = []
                resultModel = {}
                for each in goalobj:
                    qurt_rec = {}
                    year_rec = {}
                    if(each.goal_type == 'My Yearly Goals'):
                        year_rec['id'] = each.id
                        year_rec['goal_option'] = each.goal_option
                        year_rec['is_completed'] = each.is_completed
                        yearly.append(year_rec)
                    else:
                        qurt_rec['id'] = each.id
                        qurt_rec['option'] = each.goal_option
                        qurt_rec['goal_option'] = each.goal_option
                        qurt_rec['is_completed'] = each.is_completed
                        quirtlly.append(qurt_rec)
                    
                partList['quarterly'] = quirtlly
                partList['yearly'] = yearly
                resultModel['response_code'] = "1"
                resultModel['response_msg'] = "Student goal"
                resultModel['goal'] = partList
                return Response(resultModel)
                #serializer = StudentGoalSerializer(goalobj, many=True)
                #return JsonResponse({"response_code":"0", "response_msg": "Student goal", "goal": serializer.data})
            else:
                return Response({"response_code":"1", "response_msg": "You haven't set any goal yet", "goal": {} })
        except StudentGoal.DoesNotExist:
            return Response({"response_code":"1", "response_msg": "You haven't set any goal yet", "goal": {} })
        
#student goal add
class UserGoalAdd(CreateAPIView):
    serializer_class = StudentGoalAddSerializer
    def create(self, request, pk, format=None):
        serialized = StudentGoalAddSerializer(data=request.data)
        if serialized.is_valid(raise_exception=True):
            currentdate = datetime.datetime.now()
            goalobj = serialized.save(student_id=pk,creation_date=currentdate)
            
            #goal created
            goalcount = StudentGoal.objects.filter(Q(student_id=pk),Q(is_deleted='No')).count()
            gamification.comman_function.update_user_data(pk,'goaladd',goalcount)

            return JsonResponse({"response_code":"0", "response_msg": "Goal add successfully", "goal": serialized.data })
        return JsonResponse({"response_code":"1", "response_msg": "Goal add failed", "goal": {} })
        

#student goal update
class UserGoalUpdate(APIView):
    serializer_class = StudentGoalUpdateSerializer
    def post(self, request, pk, StudentCoach_pk,format=None):
        try:
            goalobj = StudentGoal.objects.get(pk=pk,student=StudentCoach_pk)
            serialized = StudentGoalUpdateSerializer(goalobj, data=request.data)
            if serialized.is_valid():
                currentdate = datetime.datetime.now()
                goalobj = serialized.save(completed_date=currentdate)
                
                #add to jounroal entry while complete goal
                if(serialized.data['is_completed'] == 'Yes'):

                    #check if exits
                    journalobj = Journal.objects.filter(Q(object_id=pk), Q(object_type='Goal'))
                    if(journalobj.count() > 0):
                        #update
                        for each in journalobj:
                            each.journal_title = 'Goal Complete!'
                            each.journal_desc = goalobj.goal_option
                            each.save()
                        
                    else:
                        #insert
                        journalinsetobj = Journal(student_id=StudentCoach_pk,today_fill='5',journal_title='Goal Complete!',journal_desc=goalobj.goal_option,object_id=pk,object_type='Goal')
                        journalinsetobj.save()

                        #gamification.comman_function.update_user_data(StudentCoach_pk,'goalcomplete',True)
                        #goal completed
                        goalcount = StudentGoal.objects.filter(Q(student_id=StudentCoach_pk),Q(is_deleted='No'),Q(is_completed='Yes')).count()
                        gamification.comman_function.update_user_data(StudentCoach_pk,'goalcomplete',goalcount)
                        
                        #total journal entry
                        journalcount = Journal.objects.filter(Q(student_id=StudentCoach_pk)).count()
                        gamification.comman_function.update_user_data(StudentCoach_pk,'journalentrypublished',journalcount)

                elif(serialized.data['is_completed'] == 'No'):

                    #check if exits
                    journalobj = Journal.objects.filter(Q(object_id=pk), Q(object_type='Goal'))
                    if(journalobj.count() > 0):
                        #delete
                        for each in journalobj:
                            each.delete()
                        
                        #goal completed
                        goalcount = StudentGoal.objects.filter(Q(student_id=StudentCoach_pk),Q(is_deleted='No'),Q(is_completed='Yes')).count()
                        gamification.comman_function.update_user_data(StudentCoach_pk,'goalcomplete',goalcount)

                        #gamification.comman_function.update_user_data(StudentCoach_pk,'goalcomplete',False)
                        journalcount = Journal.objects.filter(Q(student_id=StudentCoach_pk)).count()
                        gamification.comman_function.update_user_data(StudentCoach_pk,'journalentrypublished',journalcount)
                        
                
                return Response({"response_code":"0", "response_msg": "Goal completed successfully", "goal": {"user_id":StudentCoach_pk,"id":goalobj.pk,"is_completed":goalobj.is_completed,"goal_option":goalobj.goal_option} })
            return Response({"response_code":"1", "response_msg": "Goal completed failed", "goal": {} })
        except StudentGoal.DoesNotExist:
            return Response({"response_code":"1", "response_msg": "Goal not found of this user", "goal": {} })


#Edit Goal 
class UserGoalEdit(APIView):
    serializer_class = StudentGoalEditSerializer
    def get(self, request,userID, pk,format=None):
        try:
            goalobj = StudentGoal.objects.get(pk=pk)
            return Response({"status":True, "response_msg": "detail goal", "goal": {"user_id":userID,"id":goalobj.pk,"goal_type":goalobj.goal_type,"is_completed":goalobj.is_completed,"goal_option":goalobj.goal_option} })
        except StudentGoal.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "goal": {} })
        
    def post(self, request,userID, pk,format=None):
        try:
            goalobj = StudentGoal.objects.get(pk=pk)
            serialized = StudentGoalEditSerializer(goalobj, data=request.data)
            if serialized.is_valid():
                goalobj = serialized.save()
                return Response({"status":True, "response_msg": "Update successfully", "goal": {"user_id":userID,"id":goalobj.pk,"is_completed":goalobj.is_completed,"goal_option":goalobj.goal_option} })
            return Response({"status":False, "response_msg": "Update failed", "goal": {} })
        except StudentGoal.DoesNotExist:
            return Response({"status":False, "response_msg": "Goal not found of this user", "goal": {} })


#student goal delete
class UserGoalDelete(APIView):
    serializer_class = StudentGoalDeleteSerializer
    def post(self, request, pk, StudentCoach_pk, format=None):
        try:
            goalobj = StudentGoal.objects.get(pk=pk,student=StudentCoach_pk)
            serialized = StudentGoalDeleteSerializer(goalobj, data=request.data)
            
            if serialized.is_valid():
                goalobj = serialized.save(is_deleted='Yes')
                
                #gamification.comman_function.update_user_data(StudentCoach_pk,'goaladd',False)
                goalcount1 = StudentGoal.objects.filter(Q(student_id=StudentCoach_pk),Q(is_deleted='No')).count()
                gamification.comman_function.update_user_data(StudentCoach_pk,'goaladd',goalcount1)
                
                #goal completed
                goalcount = StudentGoal.objects.filter(Q(student_id=StudentCoach_pk),Q(is_deleted='No'),Q(is_completed='Yes')).count()
                gamification.comman_function.update_user_data(StudentCoach_pk,'goalcomplete',goalcount)
                #gamification.comman_function.update_user_data(StudentCoach_pk,'goalcomplete',False)

                return JsonResponse({"response_code":"0", "response_msg": "Goal delete successfully", "goal": {"user_id":StudentCoach_pk,"id":goalobj.pk,"is_deleted":goalobj.is_deleted,"goal_option":goalobj.goal_option} })
            return JsonResponse({"response_code":"1", "response_msg": "Goal delete failed", "goal": {} })
        except StudentGoal.DoesNotExist:
            return JsonResponse({"response_code":"1", "response_msg": "Goal not found of this user", "goal": {} })

#Student Assignment list
class StudentAssignmentview(APIView):
    serializer_class = StudentAssignmentSerializer
    def get(self, request, student_id, format=None):
        try:
            moduleobj  = Coachmodule.objects.filter(module_status='publish')
            serializer = StudentAssignmentSerializer(moduleobj,many=True)
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                    
            
            modelinfoarr = []
            for moduleinfo in serializer.data:
                partmoduleList = {}
                
                student_IDs = moduleinfo['student_id']
                student_IDs = student_IDs.split(",")
                
                if student_id in student_IDs and student_IDs != '' :
                    
                    partmoduleList['philosophy_video_updated'] = moduleinfo['philosophy_video_updated']
                    partmoduleList['technique_video_updated'] = moduleinfo['technique_video_updated']
                    partmoduleList['coach_video_updated'] = moduleinfo['coach_video_updated']
                    
                    partmoduleList['module_name'] =  moduleinfo['module_name']
                    partmoduleList['viddd'] =  str(moduleinfo['philosophy_video'])
                    
                    if(moduleinfo['philosophy_video']):
                        partmoduleList['philosophy_video'] = moduleinfo['philosophy_video']
                    elif(moduleinfo['philosophy_video2']):
                        partmoduleList['philosophy_video'] = moduleinfo['philosophy_video2']
                    else:
                        partmoduleList['philosophy_video'] = ''
                        partmoduleList['philosophy_video_thumb'] = ''
                    
                    if(moduleinfo['philosophy_video_thumb']):
                        partmoduleList['philosophy_video_thumb'] = moduleinfo['philosophy_video_thumb']
                    elif(moduleinfo['philosophy_video_thumb2']):
                        partmoduleList['philosophy_video_thumb'] = moduleinfo['philosophy_video_thumb2']
                    else:
                        partmoduleList['philosophy_video_thumb'] = ''
                        
                    if(moduleinfo['technique_video']):
                        partmoduleList['technique_video'] = moduleinfo['technique_video']
                    elif(moduleinfo['technique_video2']):
                        partmoduleList['technique_video'] = moduleinfo['technique_video2']
                    else:
                        partmoduleList['technique_video'] = ''
                        
                    if(moduleinfo['technique_video_thumb']):
                        partmoduleList['technique_video_thumb'] = moduleinfo['technique_video_thumb']
                    elif(moduleinfo['technique_video_thumb2']):
                        partmoduleList['technique_video_thumb'] = moduleinfo['technique_video_thumb2']
                    else:
                        partmoduleList['technique_video_thumb'] = ''
                    
                    if(moduleinfo['coach_video']):
                        #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(moduleinfo.coach_video)
                        #if os.path.exists(coachvideo):
                        partmoduleList['coach_video'] = moduleinfo['coach_video']
                        #else:
                            #partmoduleList['coach_video'] = ''
                    elif(moduleinfo['coach_video2']):
                        partmoduleList['coach_video'] = moduleinfo['coach_video2']
                    else:
                        partmoduleList['coach_video'] = ''
                      
                    if(moduleinfo['coach_video_thumb']):
                        partmoduleList['coach_video_thumb'] = moduleinfo['coach_video_thumb']
                    elif(moduleinfo['coach_video_thumb2']):
                        partmoduleList['coach_video_thumb'] = moduleinfo['coach_video_thumb2']
                    else:
                        partmoduleList['coach_video_thumb'] = ''
                    
                    partmoduleList['id'] =  moduleinfo['id']
                    partmoduleList['coach_id'] =  moduleinfo['coach']
                    
                    coachobj = StudentCoach.objects.filter(pk=moduleinfo['coach'])
                    coacharr = []
                    for coachval in coachobj:
                        coachParlist = {}
                        coachParlist['id'] = coachval.id
                        coachParlist['first_name'] = coachval.first_name
                        coachParlist['last_name'] = coachval.last_name
                        coacharr.append(coachParlist)
                    partmoduleList['coachobj'] = coacharr
                    
                    taglist = moduleinfo['tag']
                    
                    
                    tagobj = Tag.objects.filter(pk__in=taglist)
                    
                    tagarr = []
                    for tag_val in tagobj:
                        partagList = {}
                        partagList['tag_id'] = tag_val.id
                        partagList['tag_title'] = tag_val.tag_title
                        tagarr.append(partagList)
                    
                    partmoduleList['tag'] = tagarr
                    
                    #check if assignemnt complete
                    assignmentobj  = StudentAssignment.objects.filter(Q(coach_id=coachval.id), Q(student_id=student_id), Q(is_completed='Yes')).count()
                    if(assignmentobj > 0):
                        partmoduleList['is_completed'] = True
                    else:
                        partmoduleList['is_completed'] = False
                    
                    modelinfoarr.append(partmoduleList)
                    
            if(modelinfoarr):        
                return Response({"status":True, "response_msg": "Assignment module list", "user_id":student_id,"moduleobj": modelinfoarr })
            else:
                return Response({"status":False, "response_msg": "Data not found", "moduleobj":{} })
        except Coachmodule.DoesNotExist:
            return JsonResponse({"status":False, "response_msg": "Data not found", "moduleobj": {} })
    

#Student Assignment Unenroll
class StudentEnrollUnenrollAssignment(APIView):
    serializer_class = StudentUnenrollSerializer
    def post(self, request,StudentID,moduleID, format=None):
        try:
            moduleobj = Coachmodule.objects.get(pk=moduleID)
            if(moduleobj.student_id != ''):
                student_IDs = moduleobj.student_id
                student_IDs = student_IDs.split(",")
            else:
                student_IDs = ''
            
            if StudentID in student_IDs:
                if(student_IDs != ''):
                    student_IDs.remove(StudentID)
                    student_ids = ",".join(student_IDs)
                    enroll = True
                else:
                    student_ids = StudentID
                    enroll = False
            else:
                if(moduleobj.student_id == ''):
                    student_ids = StudentID
                else:
                    student_ids = moduleobj.student_id + "," + StudentID
                enroll = False
                
            
            #assignmentobj = StudentAssignment.objects.get(pk=pk,student_id=StudentID)
            serialized = StudentUnenrollSerializer(moduleobj, data=request.data)
            if serialized.is_valid():
                serialized.save(student_id=student_ids)
                #assignmentobj = StudentAssignment.objects.filter(module_id=moduleID,student_id=StudentID).count()
                #if(assignmentobj > 0):
                    #assignmentdelobj = StudentAssignment.objects.get(module_id=moduleID,student_id=StudentID).delete()
                
                return Response({"response_code":"0", "response_msg": "Assignment unenroll successfully", "user_id":StudentID,"module_id":moduleID,"enroll":enroll, "assignment":serialized.data} )
                #return JsonResponse({"response_code":"0", "response_msg": "Goal completed successfully", "goal": serialized.data })
            return Response({"response_code":"1", "response_msg": "Update failed", "assignment": {} })
        except Coachmodule.DoesNotExist:
            return Response({"response_code":"1", "response_msg": "Not found data", "assignment": {} })
        
        


class StudentEnrollUnenrollAssignmentV1(APIView):
    serializer_class = StudentUnenrollSerializer
    def post(self, request,StudentID,moduleID, format=None):
        try:
            moduleobj = Coachmodule.objects.get(pk=moduleID)
            if(moduleobj.student_id != ''):
                student_IDs = moduleobj.student_id
                student_IDs = student_IDs.split(",")
            else:
                student_IDs = ''
                
            enroll = request.POST.get('enroll')
            
            if enroll == 'True':
                if StudentID in student_IDs:
                    update = False
                else:
                    update = True
                    enroll = False
                    if(moduleobj.student_id == ''):
                        student_ids = StudentID
                    else:
                        student_ids = moduleobj.student_id + "," + StudentID  
            else:
                if StudentID in student_IDs:
                    update = True
                    enroll = True
                    student_IDs.remove(StudentID)
                    student_ids = ",".join(student_IDs)
                else:
                    update = False
            
            
            serialized = StudentUnenrollSerializer(moduleobj, data=request.data)
            if serialized.is_valid() and update == True:
                serialized.save(student_id=student_ids)
                
                #2018 unassign set no this table
                try:
                    assigntrack = StudentAssignUnassignTrack.objects.get(Q(module_id=moduleID),Q(student_id=StudentID))
                    if(assigntrack):
                        assigntrack.is_assign = 'No'
                        assigntrack.save()
                except:
                    v = True
                
                notiobj = Notification.objects.filter(notify_to=StudentID,main_id=moduleID).count()
                if(notiobj > 0):
                    notiobj = Notification.objects.get(notify_to=StudentID,main_id=moduleID).delete()
                
                #assignmentobj = StudentAssignment.objects.filter(module_id=moduleID,student_id=StudentID).count()
                #if(assignmentobj > 0):
                    #assignmentdelobj = StudentAssignment.objects.get(module_id=moduleID,student_id=StudentID).delete()
                
                return Response({"response_code":"0", "response_msg": "Assignment unenroll successfully", "user_id":StudentID,"module_id":moduleID,"enroll":enroll, "assignment":serialized.data} )
                #return JsonResponse({"response_code":"0", "response_msg": "Goal completed successfully", "goal": serialized.data })
            return Response({"response_code":"1", "response_msg": "Update failed", "assignment": {} })
        except Coachmodule.DoesNotExist:
            return Response({"response_code":"1", "response_msg": "Not found data", "assignment": {} })
        
        
#Student Assignment Mark as complete
class StudentMarkascompleteAssignment(APIView):
    serializer_class = StudentMarkasCompleteSerializer
    def post(self, request,StudentID,moduleID, format=None):
        try:
            assignmentobj = StudentAssignment.objects.get(module_id=moduleID,student_id=StudentID)
            serialized = StudentMarkasCompleteSerializer(assignmentobj, data=request.data)
            if serialized.is_valid():
                currentdate = datetime.date.today()
                serialized.save(completed_date=currentdate)
                
                #entry in journal entry when complete assignment
                if(serialized.data['is_completed'] == 'Yes'):
                    moduleobj = Coachmodule.objects.get(pk=moduleID)
                    Module_title = ''
                    if(moduleobj.module_name != ''):
                        Module_title = moduleobj.module_name
                    #check if exits
                    journalobj = Journal.objects.filter(Q(object_id=moduleID),Q(student_id=StudentID), Q(object_type='Module'))
                    if(journalobj.count() > 0):
                        #update
                        for each in journalobj:
                            each.journal_title = 'Module Complete!'
                            each.journal_desc= Module_title
                            each.save()
                        
                    else:
                        #insert
                        journalinsetobj = Journal(student_id=StudentID,today_fill='5',journal_title='Module Complete!',journal_desc=Module_title,object_id=moduleID,object_type='Module')
                        journalinsetobj.save()
                elif(serialized.data['is_completed'] == 'No'):
                    #check if exits
                    journalobj = Journal.objects.filter(Q(object_id=moduleID),Q(student_id=StudentID), Q(object_type='Module'))
                    if(journalobj.count() > 0):
                        #update
                        for each in journalobj:
                            each.delete()
                
            return Response({"status":True, "response_msg": "Assignment completed successfully", "user_id":StudentID, "assignment":serialized.data })
        except:
            serialized = StudentMarkasCompleteSerializer(data=request.data)
            if serialized.is_valid():
                iscompleted = 'Yes'
                currentdate = datetime.date.today()
                serialized.save(completed_date=currentdate)
                
                #entry in journal entry when complete assignment
                if(serialized.data['is_completed'] == 'Yes'):
                    moduleobj = Coachmodule.objects.get(pk=moduleID)
                    Module_title = ''
                    if(moduleobj.module_name != ''):
                        Module_title = moduleobj.module_name
                    #check if exits
                    journalobj = Journal.objects.filter(Q(object_id=moduleID), Q(object_type='Module'),Q(student_id=StudentID))
                    if(journalobj.count() > 0):
                        #update
                        for each in journalobj:
                            each.journal_title = 'Module Complete!'
                            each.journal_desc= Module_title
                            each.save()
                        
                    else:
                        #insert
                        journalinsetobj = Journal(student_id=StudentID,today_fill='5',journal_title='Module Complete!',journal_desc=Module_title,object_id=moduleID,object_type='Module')
                        journalinsetobj.save()
            
            return Response({"status":True, "response_msg": "Assignment completed successfully", "user_id":StudentID, "assignment":serialized.data})
            #return JsonResponse({"response_code":"0", "response_msg": "Goal completed successfully", "goal": serialized.data })
            
        

#get user uniqueid
class UserUniqueID(APIView):
    serializer_class = RegisterUniqueIDSerializer
    def get(self, request, userID, format=None):
        try:
            userobj = StudentCoach.objects.get(pk=userID)
            serializer = RegisterUniqueIDSerializer(userobj,many=True)
            if(userobj.unique_id == '') :
                #genrate unique code
                
                #get record and generate unique id 
                datenow = datetime.datetime.now().strftime('%m%d')
                if(userobj.user_name != ''):
                    mystring = userobj.user_name.upper()
                    name = re.sub('[^A-Za-z0-9]+', '', mystring)
                    
                elif (userobj.first_name != ''):
                    mystring = userobj.first_name.upper()
                    name = re.sub('[^A-Za-z0-9]+', '', mystring)
                else:
                    mystring = uuid.uuid4().hex[:4].upper()
                    name = re.sub('[^A-Za-z0-9]+', '', mystring)
                    
                uniqueID = str(userobj.id) + name + datenow
                userobj.unique_id = uniqueID
                userobj.save()
                return Response({"status":True, "response_msg": "User unique id", "user_id":userID, "userobj":{"unique_id":userobj.unique_id} })
            
            return Response({"status":True, "response_msg": "User unique id", "user_id":userID, "userobj":{"unique_id":userobj.unique_id} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "user_id":userID, "userobj":{} })
    
    

#get user uniqueid
class ListMyReferenceUser(APIView):
    serializer_class = ReferenceUserSerializer
    def get(self, request, uniqueID, format=None):
        try:
            userobj = StudentCoach.objects.filter(reference_code=uniqueID)
            serializer = ReferenceUserSerializer(userobj,many=True)
            
            userarr = []
            for userobjval in userobj:
                paramList = {}
                paramList['id'] = userobjval.id
                paramList['first_name'] = userobjval.first_name
                paramList['last_name'] = userobjval.last_name
                paramList['unique_id'] = userobjval.unique_id
                paramList['reference_code'] = userobjval.reference_code
                
                checkchilduser = ChildUser(userobjval.unique_id, paramList)
                paramList['child_user'] = checkchilduser
                
                
                userarr.append(paramList)
            
            return Response({"status":True, "response_msg": "User unique id", "userobj":userarr })
        except:
            return Response({"status":False, "response_msg": "Data not found", "userobj":{} })    


def ChildUser(Uniqueid,paramList):
    try:
        userobj = StudentCoach.objects.filter(reference_code=Uniqueid)
        userarr = []
        for userobjval in userobj:
            paramList = {}
            paramList['id'] = userobjval.id
            paramList['first_name'] = userobjval.first_name
            paramList['last_name'] = userobjval.last_name
            paramList['unique_id'] = userobjval.unique_id
            paramList['reference_code'] = userobjval.reference_code
            
            checkchilduser = ChildUser(userobjval.unique_id, paramList)
            paramList['child_user'] = checkchilduser
            
            userarr.append(paramList)
        
        return userarr
    except:
        return paramList


#user report a proble spam
class AddUserReportProblem(APIView):
    serializer_class = UserReportProblemSerializer
    def post(self, request, userID, format=None):
        serializer = UserReportProblemSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status":True, "response_msg": "Report send successfully", "reportobj":serializer.data })
        return Response({"status":False, "response_msg": "Data not found", "reportobj":{} })
        
        

#forgotpassword send unique code
class ForgotPass(APIView):
    serializer_class = ForgotPassSerializer
    def post(self, request, format=None):
        post_mail = request.POST.get('e_mail')
        try:
            queryset = StudentCoach.objects.get(e_mail=post_mail)
            
            if(queryset.social_login == 'True'):
                '''
                subject = "keyy - Forgot Password"
                to = [post_mail]
                from_email = settings.EMAIL_HOST_USER
                message = 'Please try to login with facebook, twitter or gmail, whichever you choose for sign up'
                msg = EmailMessage(subject, message, to=to, from_email=from_email)
                msg.content_subtype = 'html'
                msg.send()
                '''
                return Response({"status":False, "response_msg": "Please try to login with facebook, twitter or gmail, whichever you choose for sign up","uniquecode":"", "code": {} })
            
            mystring = uuid.uuid4().hex[:6].upper()
            code = re.sub('[^A-Za-z0-9]+', '', mystring)
            
            #check uniqcode exits or not
            uniquecode = CheckUniqueID(code)
            
            Createddate = datetime.datetime.now()
            ExpireDate = Createddate + datetime.timedelta(minutes = 30)
            userobj = UserUniqueCodeForgotPass(user_id=queryset.id,unique_code=uniquecode,created_datetime=Createddate,expire_datetime=ExpireDate)
            userobj.save()
            
            subject = "keyy - Forgot Password"
            to = [post_mail]
            from_email = settings.FROM_EMAIL
            
            ctx = {
                'user': queryset.first_name +' '+ queryset.last_name,
                'uniquecode': uniquecode
            }
            message = get_template('email/email.html').render(Context(ctx))
            msg = EmailMessage(subject, message, to=to, from_email=from_email)
            msg.content_subtype = 'html'
            msg.send(fail_silently=False)
            
            
            return Response({"status":True, "response_msg": "we will send uniqe code with email please check email.","uniquecode":uniquecode, "code": {} })
        except:
            return Response({"status":False, "response_msg": "Email address wrong", "code": {} })


#check user uniquecode for forgot password
def CheckUniqueID(code):
    try:
        userobj = UserUniqueCodeForgotPass.objects.filter(unique_code=code)
        mystring = uuid.uuid4().hex[:6].upper()
        newcode = re.sub('[^A-Za-z0-9]+', '', mystring)
        checkiniquecode = CheckUniqueID(newcode)
        return checkiniquecode
    except:
        return code
        
#forgotpassword check unique code
class ForgotPassCheckUniqueCode(APIView):
    serializer_class = UniqueCodeCheckSerializer
    def post(self, request, format=None):
        post_unique_code = request.POST.get('unique_code')
        
        try:
            queryset = UserUniqueCodeForgotPass.objects.filter(unique_code=post_unique_code)
            uniquecode = ''
            for data in queryset:
                uniquecode = data.created_datetime
                currentdate = datetime.datetime.now()
                if (data.created_datetime.isoformat() < currentdate.isoformat() < data.expire_datetime.isoformat()):
                    Expire = True
                else:
                    Expire = False
                    
            if(Expire == False):
                return Response({"status":False, "response_msg": "Unique code expire", "codeobj":{} })
            
            codearr = []
            for each in queryset:
                paramList = {}
                paramList['unique_code'] = each.unique_code
                paramList['user_id'] = each.user_id
                codearr.append(paramList)
            
            return Response({"status":True, "response_msg": "Unique code validate succesfull", "codeobj":codearr })
        except:
            return Response({"status":False, "response_msg": "Invalid unique code", "codeobj": {} })
    
#reset password 
class ResetPassword(APIView):
    serializer_class = ResetPassSerializer
    def post(self, request, user_id, *args, **kwargs):
        confirm_password = request.POST.get('confirm_password')
        new_password = request.POST.get('password')
        if(new_password == '' ):
            return Response({"status":False, "response_msg": "Please enter the new password.", "data": {} })
        elif(confirm_password == '' ):
            return Response({"status":False, "response_msg": "Please enter the confirm password.", "data": {} })
        elif(new_password != confirm_password):
            return Response({"status":False, "response_msg": "Password dose not match.", "data": {} })
        
        try:
            snippet = StudentCoach.objects.get(pk=user_id)
            serializer = ResetPassSerializer(snippet, data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({"status":True, "response_msg": "Password reset successful.", "data": serializer.data})
        except StudentCoach.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "data": {} })
        


#changepassword
class ChangePass(APIView):
    serializer_class = ChangePassSerializer
    def get(self, request, pk, format=None):
        try:
            snippet = StudentCoach.objects.get(pk=pk)
            serializer = ChangePassSerializer()
            return Response({"success":True, "data": serializer.data})
        except StudentCoach.DoesNotExist:
            return JsonResponse({"success":False, "data": "Not found"})
    def post(self, request, pk, *args, **kwargs):
        old_password = request.POST.get('old_password')
        try:
            snippet = StudentCoach.objects.get(password=old_password,pk=pk)
            serializer = ChangePassSerializer(snippet, data=request.data)
            if serializer.is_valid(raise_exception=True):
                try:
                    serializer.save()
                    return Response({"status":True, "response_msg": "Password reset successful.", "data": serializer.data})
                except StudentCoach.DoesNotExist:
                    return Response({"status":False, "response_msg": "Enable to save password", "data": serializer.data})
        except StudentCoach.DoesNotExist:
            return Response({"status":False, "response_msg": "Wrong old password", "data": {} })
        
    
    
#update strip customer id
class UpdateStripCustomerID(APIView):
    serializer_class = UserStripCustomerIDSerializer
    def get(self, request, userID, format=None):
        try:
            snippet = StudentCoach.objects.get(pk=userID)
            serializer = UserStripCustomerIDSerializer(snippet)
            return Response({"status":True, "response_msg": "Get Strip Customer ID", "customerID":serializer.data })
        except:
            return Response({"status":False, "response_msg": "Data not found", "customerID":{} })
        
    def post(self, request, userID, format=None):
        snippet = StudentCoach.objects.get(pk=userID)
        serializer = UserStripCustomerIDSerializer(snippet,data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status":True, "response_msg": "Get Strip Customer ID", "customerID":serializer.data })
        return Response({"status":False, "response_msg": "Data not found", "customerID":{} })

class UpdateStripResponse(APIView):
    serializer_class = UserStripResponseSerializer
    def get(self, request, userID, format=None):
        try:
            snippet = StudentCoach.objects.get(pk=userID)
            serializer = UserStripResponseSerializer(snippet)
            return Response({"status":True, "response_msg": "Get Strip Response", "object":serializer.data })
        except:
            return Response({"status":False, "response_msg": "Data not found", "object":{} })
        
    def post(self, request, userID, format=None):
        snippet = StudentCoach.objects.get(pk=userID)
        serializer = UserStripResponseSerializer(snippet,data=request.data)
        
        if serializer.is_valid():
            serializer.save()
            return Response({"status":True, "response_msg": "Strip Response Update successfully", "object":serializer.data })
        return Response({"status":False, "response_msg": "Data not found", "object":{} })


#get genral settings
class ListGeneralSetting(APIView):
    serializer_class = GeneralSettingSerializer
    def get(self, request, format=None):
        try:
            snippet = GeneralSetting.objects.all()
            serializer = GeneralSettingSerializer(snippet, many=True)
            return Response({"status":True, "response_msg": "General Settiing", "settings":serializer.data })
        except:
            return Response({"status":False, "response_msg": "Data not found", "settings":{} })




#get general write message
class GetInvitationWriteMessage(APIView):
    serializer_class = WriteMessageSerializer
    def get(self, request, format=None):
        try:
            snippet = WriteinvitationMessage.objects.all()
            serializer = WriteMessageSerializer(snippet, many=True)
            return Response({"status":True, "response_msg": "Invitation Message", "message":serializer.data })
        except:
            return Response({"status":False, "response_msg": "Data not found", "message":{} })
        
 
#get journal entry message
class GetJournalEntryWriteMessage(APIView):
    serializer_class = JournalEntryMessageSerializer
    def get(self, request, format=None):
        try:
            snippet = WriteJournalEntryMessage.objects.all()
            serializer = JournalEntryMessageSerializer(snippet, many=True)
            return Response({"status":True, "response_msg": "Journal Entry Message", "message":serializer.data })
        except:
            return Response({"status":False, "response_msg": "Data not found", "message":{} })

#all student list
class StudentView(generics.ListAPIView):
    """ Only Authenticate User perform CRUD Operations on Respective Task
    """
    queryset = StudentCoach.objects.filter(account_type='Student')
    serializer_class = StudentSerializer
    

''' 
class CoachView(generics.ListAPIView):
    queryset = StudentCoach.objects.filter(account_type='Coach')
    serializer_class = CoachSerializer
    filter_fields = ('user_name')
    
'''    
class CoachView(APIView):
    def get(self, request, format=None):
        try:
            user_name = request.GET.get('user_name')
            if(user_name and user_name != ''):
                snippet = StudentCoach.objects.filter(account_type='Coach',user_name=user_name)
            else:
                snippet = StudentCoach.objects.filter(account_type='Coach')
                
            serializer = CoachSerializer(snippet,many=True) 
            return Response({"success":True, "coach": serializer.data})
        except StudentCoach.DoesNotExist:
            return JsonResponse({"success":False, "data": "Not found"})
   
  

#student list api
@csrf_exempt
def student_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        snippets = StudentCoach.objects.filter(account_type='Student')
        serializer = StudentSerializer(snippets, many=True)
        return JsonResponse({"success":True, "data": serializer.data})

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = StudentSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            #return JsonResponse(serializer.data, status=201)
            return JsonResponse({"success":True, "data": serializer.data})
        #return JsonResponse(serializer.errors, status=400)
        return JsonResponse({"success":False, "data": serializer.errors})

@csrf_exempt
def student_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        snippet = StudentCoach.objects.get(pk=pk)
    except StudentCoach.DoesNotExist:
        #return HttpResponse(status=404)
        return JsonResponse({"success":False, "data": "Not found"})

    if request.method == 'GET':
        serializer = StudentSerializer(snippet)
        #return JsonResponse(serializer.data)
        return JsonResponse({"success":True, "data": serializer.data})

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = StudentSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            #return JsonResponse(serializer.data)
            return JsonResponse({"success":True, "data": serializer.data})
        #return JsonResponse(serializer.errors, status=400)
        return JsonResponse({"success":True, "data": serializer.errors})

    elif request.method == 'DELETE':
        snippet.delete()
        return HttpResponse(status=204)
    
    
class CreateChargeModule(APIView):
    serializer_class = CreateChargeSerializer
    def post(self, request,userID,moduleID, format=None):

        user_name = request.POST.get('user_name')
        user_id = request.POST.get('user_id')
        module_id = request.POST.get('module_id')
        email = request.POST.get('email')
        token = request.POST.get('token')
        amount = request.POST.get('amount')
        module_title = request.POST.get('module_title')
        currency = 'usd'
        
        '''
        user_name = 'jignesh'
        user_id = '121'
        email = 'sojitra.jignesh5@gmail.com'
        token = 'tok_1B7PkoGjCLPDybqAgdJJex7X' #tok_1B7hWxGjCLPDybqABXVfsi4L
        amount = '50'
        plan_type = 'm'
        currency = 'usd'
        module_title = 'Test'
        '''
        
        if(user_id == ''):
            return Response({"success":False, "response_msg": "User id not found", "data":{} })
        elif(module_id == ''):
            return Response({"success":False, "response_msg": "Module id not found", "data":{} })
        elif(token == ''):
            return Response({"success":False, "response_msg": "Token mismatch", "data":{} })
        elif(amount == '' ):
            return Response({"success":False, "response_msg": "Amount not found", "data":{} })
        
        
        #check if user already purchase this module
        orderobj = OrderHistory.objects.filter(Q(user=user_id), Q(object_type='charge'), Q(module_id=module_id), Q(status='succeeded') )
        if(orderobj.count() > 0):
            return Response({"success":False, "response_msg": "You have allready purchase this module.", "data":{} })

    
        
        #get strip key
        Strip_publishable_key = ''
        Strip_secret_key = ''
        try:
            snippet = GeneralSetting.objects.all()
            serializer = GeneralSettingUnSubSerializer(snippet, many=True)
            Strip_publishable_key = serializer.data[0]['publishable_key']
            Strip_secret_key = serializer.data[0]['secret_key']
            Strip_monthly_plan_id = serializer.data[0]['monthly_plan_id']
            Strip_yearly_plan_id = serializer.data[0]['yearly_plan_id']
        except:
            Strip_publishable_key = ''
            Strip_secret_key = ''
            Strip_monthly_plan_id = ''
            Strip_yearly_plan_id = ''
        
        if(Strip_secret_key == '' ):
            return Response({"success":False, "response_msg": "Secret key not found", "data":{} })
        
        
        #create charge
        amount = int(float(amount) * 100)
        charge_jsondata = {"amount" : amount,"currency":currency,"source":token,"description":"Charge for "+module_title}
        charge_req = urllib2.Request("https://api.stripe.com/v1/charges")
        charge_req.add_header("Authorization","Bearer "+Strip_secret_key)
        charge_data = urllib.urlencode(charge_jsondata)
        charge_req.add_data(charge_data)
        try:
            charge_resp = urllib2.urlopen(charge_req)
            charge_content = charge_resp.read()
            charge_data = json.loads(charge_content)
            charge_id = charge_data['id']
            charge_object = charge_data['object']
            charge_customer = charge_data['customer']
            charge_status = charge_data['status']
            charge_balance_transaction = charge_data['balance_transaction']
            
        except URLError as e:
            error = e.read()
            charge_data_error = json.loads(error)
            message = charge_data_error['error']['message']
            restype = charge_data_error['error']['type']
            return Response({"success":False,"response_msg":message, "type":restype,"data":{} })
        #create charge end
        
        
        #save response data into order table
        order = OrderHistory(user_id=int(user_id),object_type=charge_object,payment_data=charge_content,charge_id=charge_id,status=charge_status,transaction_id=charge_balance_transaction,module_id=module_id)
        order.save()
        
        
        return Response({"success":True, "response_msg": "Charged successfully", "data":charge_data })
        
    
class CreateSubsciption(APIView):
    serializer_class = CreateSubscriptionSerializer
    def post(self, request, format=None):

        user_name = request.POST.get('user_name')
        user_id = request.POST.get('user_id')
        email = request.POST.get('email')
        token = request.POST.get('token')
        amount = request.POST.get('amount')
        plan_type = request.POST.get('plan_type')
        currency = 'usd'
        
        '''
        user_name = 'jignesh'
        user_id = '121'
        email = 'sojitra.jignesh5@gmail.com'
        token = 'tok_1B7PkoGjCLPDybqAgdJJex7X' #tok_1B7hWxGjCLPDybqABXVfsi4L
        amount = '50'
        plan_type = 'm'
        currency = 'usd'
        '''
        
        if(user_id == ''):
            return Response({"success":False, "response_msg": "User id not found", "data":{} })
        elif(token == ''):
            return Response({"success":False, "response_msg": "Token mismatch", "data":{} })
        elif(plan_type == ''):
            return Response({"success":False, "response_msg": "Plan not found", "data":{} })
        
        
        #get strip key
        Strip_publishable_key = ''
        Strip_secret_key = ''
        try:
            snippet = GeneralSetting.objects.all()
            serializer = GeneralSettingUnSubSerializer(snippet, many=True)
            Strip_publishable_key = serializer.data[0]['publishable_key']
            Strip_secret_key = serializer.data[0]['secret_key']
            Strip_monthly_plan_id = serializer.data[0]['monthly_plan_id']
            Strip_yearly_plan_id = serializer.data[0]['yearly_plan_id']
        except:
            Strip_publishable_key = ''
            Strip_secret_key = ''
            Strip_monthly_plan_id = ''
            Strip_yearly_plan_id = ''
        
        if(Strip_secret_key == '' ):
            return Response({"success":False, "response_msg": "Secret key not found", "data":{} })
        
        
        
        if(plan_type == 'm'):
            #plan = "keyy-pro-monthly"
            plan = Strip_monthly_plan_id
        elif(plan_type == 'y'):
            #plan = "keyy-pro-yearly"
            plan = Strip_yearly_plan_id
        else:
            return Response({"success":False,"response_msg":"Plan not found.", "data":{} })
        
        
        #check plan 
        plan_req = urllib2.Request("https://api.stripe.com/v1/plans/"+plan)
        plan_req.add_header("Authorization","Bearer "+Strip_secret_key)
        try:
            plan_resp = urllib2.urlopen(plan_req)
            plan_content = plan_resp.read()
        except URLError as e:
            error = e.read()
            plant_data_error = json.loads(error)
            message = plant_data_error['error']['message']
            restype = plant_data_error['error']['type']
            return Response({"success":False,"response_msg":message, "type":restype,"data":{} })
        #check plan end
        
        #create customer
        customer_json_data = {'email' : email, 'description' : "Customer "+ user_name, 'source' : token} #, 'account_balance' : amount
        cust_req = urllib2.Request("https://api.stripe.com/v1/customers")
        cust_req.add_header("Authorization","Bearer "+Strip_secret_key)
        cust_data = urllib.urlencode(customer_json_data)
        cust_req.add_data(cust_data)
        try:
            cust_resp = urllib2.urlopen(cust_req)
            cust_content = cust_resp.read()
            cust_data = json.loads(cust_content)
            customer_id = cust_data['id']
        except URLError as e:
            error = e.read()
            cust_data_error = json.loads(error)
            message = cust_data_error['error']['message']
            restype = cust_data_error['error']['type']
            return Response({"success":False,"response_msg":message, "type":restype,"data":{} })
            #return Response({"success":False, "data":data},status=400)
        #customer_id = 'cus_BUMJPFHu4aARxS'
        #create customer end
        
        #create Subscription
        subscription_jsondata = {"plan" : plan,"customer":customer_id}
        subs_req = urllib2.Request("https://api.stripe.com/v1/subscriptions")
        subs_req.add_header("Authorization","Bearer "+Strip_secret_key)
        subs_data = urllib.urlencode(subscription_jsondata)
        subs_req.add_data(subs_data)
        try:
            subs_resp = urllib2.urlopen(subs_req)
            subs_content = subs_resp.read()
            subs_data = json.loads(subs_content)
            subs_id = subs_data['id']
            subs_object = subs_data['object']
            subs_status = subs_data['status']
        except URLError as e:
            error = e.read()
            subs_data_error = json.loads(error)
            message = subs_data_error['error']['message']
            restype = subs_data_error['error']['type']
            return Response({"success":False,"response_msg":message, "type":restype,"data":{} })
        #create Subscription end
        
        
        #save response data into studentcoach table
        user = StudentCoach.objects.get(pk=user_id)
        user.subscription = True
        user.paymentdata = subs_content
        user.subscription_start_date = datetime.date.today()
        user.subscription_period = subs_data['plan']['interval']
        
        apple_cost_in_cents = subs_data['plan']['amount']
        cents_to_dollars_format_string = '{:,.2f}'
        total_apple_cost_in_dollars_as_string = cents_to_dollars_format_string.format(apple_cost_in_cents/100.0)
        user.subscription_amount = total_apple_cost_in_dollars_as_string
        
        #user.subscription_amount = float(int(subs_data['plan']['amount']) / 100)

        user.save()
        gamification.comman_function.update_user_data(user_id,'iskeypro',True)
        
        #save response data into order table
        order = OrderHistory(user_id=int(user_id),customer_id=customer_id,subscription_id=subs_id,object_type=subs_object,plan_type=plan_type,payment_data=subs_content,status=subs_status)
        order.save()
        
        
        return Response({"success":True, "data":subs_data })
  

def minusmonths(sourcedate,months):
    month = sourcedate.month + 1 - months
    year = int(sourcedate.year - month / 12 )
    month = month % 12 - 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return datetime.date(year,month,day)


class UnSubsciption(APIView):
    serializer_class = UnSubscriptionSerializer
    def post(self, request, format=None):
        subscription_id = request.POST.get('subscription_id')
        
        if(subscription_id == ''):
            return Response({"success":False,"response_msg":"Data not found.", "data":{} })
        
        
        #get strip key
        Strip_publishable_key = ''
        Strip_secret_key = ''
        try:
            snippet = GeneralSetting.objects.all()
            serializer = GeneralSettingUnSubSerializer(snippet, many=True)
            Strip_publishable_key = serializer.data[0]['publishable_key']
            Strip_secret_key = serializer.data[0]['secret_key']
            Strip_monthly_plan_id = serializer.data[0]['monthly_plan_id']
            Strip_yearly_plan_id = serializer.data[0]['yearly_plan_id']
        except:
            Strip_publishable_key = ''
            Strip_secret_key = ''
            Strip_monthly_plan_id = ''
            Strip_yearly_plan_id = ''
        
        if(Strip_secret_key == '' ):
            return Response({"success":False, "response_msg": "Secret key not found", "data":{} })
        
        
        #create un Subscription
        
        subs_req = urllib2.Request("https://api.stripe.com/v1/subscriptions/"+subscription_id)
        subs_req.add_header("Authorization","Bearer "+Strip_secret_key)
        
        subs_req.get_method = lambda: 'DELETE'
        try:
            subs_resp = urllib2.urlopen(subs_req)
            subs_content = subs_resp.read()
            subs_data = json.loads(subs_content)
            subs_id = subs_data['id']
            subs_object = subs_data['object']
            subs_status = subs_data['status']
        except URLError as e:
            error = e.read()
            subs_data_error = json.loads(error)
            message = subs_data_error['error']['message']
            restype = subs_data_error['error']['type']
            return Response({"success":False,"response_msg":message, "type":restype,"data":{} })
        #create un Subscription end
        
        order = OrderHistory.objects.get(subscription_id=subs_id)
        order.payment_data=subs_content
        order.status=subs_status
        order.save()
        
        
        #save response data into studentcoach table
        user = StudentCoach.objects.get(pk=order.user_id)
        user.subscription = False
        user.paymentdata = subs_content
        user.subscription_start_date = datetime.date.today()
        user.subscription_period = subs_data['plan']['interval']
        
        apple_cost_in_cents = subs_data['plan']['amount']
        cents_to_dollars_format_string = '{:,.2f}'
        total_apple_cost_in_dollars_as_string = cents_to_dollars_format_string.format(apple_cost_in_cents/100.0)
        user.subscription_amount = total_apple_cost_in_dollars_as_string
        user.save()
        gamification.comman_function.update_user_data(user_id,'iskeypro',False)
        
        #change status of journal entry of older six months
        start_date = datetime.date.today()
        end_date = minusmonths(start_date,6)
        journalobj  = Journal.objects.filter(Q(student_id=order.user_id),Q(date__lt=(end_date))).order_by('-date')
        if(journalobj.count() > 0):
            for each in journalobj:
                each.journal_status = 'Archive'
                each.save()
                
        
        return Response({"success":True, "data":subs_data })

@csrf_exempt
def WebhookSubscribe(request):
    
    null = ''
    true = True
    false = False
    
    
    bodydata = request.body
    
    jsondata = ''
    
    handle1=open(settings.BASE_DIR+'/log/log.txt','r+')
    #handle1.write("============Log=================\n")
    if(bodydata):
        handle1.write(bodydata)
        '''
        data = json.loads(bodydata)
        otherjson = json.dumps(bodydata)
        jsondaata = WebhookTransaction(body=data,request_meta=otherjson)
        jsondaata.save()
        '''
        bodydata = json.loads(bodydata)
        #handle1.write(bodydata)
        
    #handle1.write("============End Log=============")
    handle1.close()
    #return HttpResponse("Success")
    
    bodydata = json.dumps(bodydata['transaction']['appStoreReceipt'])
    #return HttpResponse(bodydata, content_type='application/json')
    
    content = ''
    
    if(bodydata):
        jsonobject = {"receipt-data":bodydata,"password":"50b993db1fa447be8105cb313f8dad5e","exclude-old-transactions":True}
        #return HttpResponse(json.dumps(jsonobject).replace('\\"',""))
        
        jsondata = json.dumps(jsonobject).replace('\\"',"")
        clen = len(jsondata)
        
        #https://sandbox.itunes.apple.com/verifyReceipt
        requrl = urllib2.Request("https://buy.itunes.apple.com/verifyReceipt", data=jsondata, headers={'Content-Type': 'application/json', 'Accept': 'application/json', 'Content-Length': clen})
        try:
            resp = urllib2.urlopen(requrl)
            content = resp.read()
            return HttpResponse(content)
        except URLError as e:
            error = e.read()
            subs_data_error = json.loads(error)
            message = subs_data_error['error']['message']
            restype = subs_data_error['error']['type']
            return HttpResponse({"success":False,"response_msg":message, "type":restype,"data":{} })
    
    return HttpResponse({"success":False,"response_msg":"Data not found"})
 


@csrf_exempt
def WebhookUnSubscribe(request):
    
    null = ''
    true = True
    false = False
    
    
    bodydata = request.body
    
    jsondata = ''
    
    handle1=open(settings.BASE_DIR+'/log/unsubscribelog.txt','r+')
    #handle1.write("============Log=================\n")
    if(bodydata):
        handle1.write(bodydata)
        
        data = json.loads(bodydata)
        otherjson = json.dumps(bodydata)
        jsondaata = WebhookTransaction(body=data,request_meta=otherjson)
        jsondaata.save()
        
        bodydata = json.loads(bodydata)
        #handle1.write(bodydata)
        
    #handle1.write("============End Log=============")
    handle1.close()
    return HttpResponse("Success")
    

class WebhookSubscribeDone(APIView):
    serializer_class = IOSSubscriptionSerializer
    def post(self, request, format=None):
        
        user_id = request.POST.get('user_id')
        plan_type = request.POST.get('plan_type')
        subscription_id = request.POST.get('subscription_id')
        status = 'active'
        subs_object = 'subscription'
        
        
        if(user_id == '' or plan_type == '' or subscription_id == ''):
            return Response({"success":False, "response_msg": "Please enter required data", "data":{} })
        
        
        #save response data into studentcoach table
        user = StudentCoach.objects.get(pk=user_id)
        user.subscription = True
        user.subscription_start_date = datetime.date.today()
        if(plan_type == 'm'):
            subscription_period = 'month'
        else:
            subscription_period = 'year'
            
        user.subscription_period = subscription_period
        user.save()
        
        #save response data into order table
        subscription_date = datetime.datetime.now()
        order = OrderHistory(user_id=int(user_id),subscription_id=subscription_id,object_type=subs_object,plan_type=plan_type,status=status,subscription_date=subscription_date)
        order.save()
        
        return Response({"success":True, "response_msg": "Subscribe successfully", "data":{} })
        


class WebhookChargeModuleDone(APIView):
    serializer_class = IOSCreateChargeSerializer
    def post(self, request, format=None):
        user_id = request.POST.get('user_id')
        module_id = request.POST.get('module_id')
        charge_id = request.POST.get('charge_id')
        currency = 'usd'
        object_type = 'charge'
        
        
        if(user_id == ''):
            return Response({"success":False, "response_msg": "User id not found", "data":{} })
        elif(module_id == ''):
            return Response({"success":False, "response_msg": "Module id not found", "data":{} })
        
        
        
        #check if user already purchase this module
        orderobj = OrderHistory.objects.filter(Q(user=user_id), Q(object_type='charge'), Q(module_id=module_id), Q(status='succeeded') )
        if(orderobj.count() > 0):
            return Response({"success":False, "response_msg": "You have allready purchase this module.", "data":{} })

    
        #save response data into order table
        subscription_date = datetime.datetime.now()
        order = OrderHistory(user_id=int(user_id),object_type=object_type,charge_id=charge_id,status='succeeded',transaction_id=charge_id,module_id=module_id,subscription_date=subscription_date)
        order.save()
        
        
        return Response({"success":True, "response_msg": "Charged successfully", "data":{} })
        

class GetUserOrderHistory(APIView):
    def get(self, request,userID, format=None):
        try:
            order = OrderHistory.objects.filter(user=userID).order_by('-id')
            serializer = OrderSerializer(order)
            current_url = request.get_host()
            if request.is_secure():
                current_url = current_url
            else:
                current_url = current_url
                
            orderarr = []
            for each in order:
                paramList = {}
                paramList['id'] = each.id
                paramList['customer_id'] = each.customer_id
                paramList['plan_type'] = each.plan_type
                paramList['subscription_id'] = each.subscription_id
                paramList['object_type'] = each.object_type
                paramList['module_id'] = each.module_id
                paramList['order_status'] = each.status
                payment_data = json.loads(each.payment_data)
                paramList['payment_data'] = payment_data
                
                
                userobj = StudentCoach.objects.get(pk=each.user_id)
                serializer2 = CommanUserSerializer(userobj)
                userarr = []
                
                userParlist = {}
                userParlist['id'] = serializer2.data['id']
                userParlist['first_name'] = serializer2.data['first_name']
                userParlist['last_name'] = serializer2.data['last_name']
                userParlist['updated'] = serializer2.data['updated']
                
                
                if(serializer2.data['profile_picture']):
                    profile_picture =  serializer2.data['profile_picture']
                elif(serializer2.data['profile_picture2']):
                    profile_picture =  serializer2.data['profile_picture2']
                elif (serializer2.data['hidden_profile_picture'] !='' and serializer2.data['social_login'] == 'True'):
                    profile_picture = serializer2.data['hidden_profile_picture']
                else:
                    profile_picture = ''
                    
                userParlist['profile_picture'] = profile_picture
                userarr.append(userParlist)
                
                paramList['userobj'] = userarr
                
                orderarr.append(paramList)
            return Response({"status":True, "response_msg": "Order history", "order":orderarr })
        except:
            return Response({"status":False, "response_msg": "Data not found", "order":{} })
        


class IOSGetUserOrderHistoryV1(APIView):
    def get(self, request,userID, format=None):
        try:
            order = OrderHistory.objects.filter(user=userID).order_by('-id')
            serializer = OrderSerializer(order)
            current_url = request.get_host()
            if request.is_secure():
                current_url = current_url
            else:
                current_url = current_url
                
            orderarr = []
            for each in order:
                paramList = {}
                paramList['id'] = each.id
                paramList['customer_id'] = each.customer_id
                paramList['plan_type'] = each.plan_type
                paramList['subscription_id'] = each.subscription_id
                paramList['object_type'] = each.object_type
                paramList['module_id'] = each.module_id
                paramList['order_status'] = each.status
                paramList['charge_id'] = each.charge_id
                
                description = ''
                if(each.object_type == 'charge'):
                    module_name = ''
                    try:
                        moduleobj = Coachmodule.objects.get(pk=int(each.module_id))
                        if(moduleobj.module_name != ''):
                            module_name = moduleobj.module_name
                    except:
                        module_name = ''
                    
                    description = 'Charge for '+module_name
                elif(each.object_type == 'subscription'):
                    duration = ''
                    if(each.plan_type == 'm'):
                        duration = 'Monthly'
                    elif(each.plan_type == 'y'):
                        duration = 'Yearly'
                    description = duration+' KeyPro plan'
                    
                
                paramList['description'] = description
                        
                payment_data = ''
                if each.payment_data:
                    payment_data = json.loads(each.payment_data)
                paramList['payment_data'] = payment_data
                paramList['created_date'] = each.subscription_date
                
                
                userobj = StudentCoach.objects.get(pk=each.user_id)
                serializer2 = CommanUserSerializer(userobj)
                userarr = []
                
                userParlist = {}
                userParlist['id'] = serializer2.data['id']
                userParlist['first_name'] = serializer2.data['first_name']
                userParlist['last_name'] = serializer2.data['last_name']
                userParlist['updated'] = serializer2.data['updated']
                
                
                if(serializer2.data['profile_picture']):
                    profile_picture =  serializer2.data['profile_picture']
                elif(serializer2.data['profile_picture2']):
                    profile_picture =  serializer2.data['profile_picture2']
                elif (serializer2.data['hidden_profile_picture'] !='' and serializer2.data['social_login'] == 'True'):
                    profile_picture = serializer2.data['hidden_profile_picture']
                else:
                    profile_picture = ''
                    
                userParlist['profile_picture'] = profile_picture
                userarr.append(userParlist)
                
                paramList['userobj'] = userarr
                
                orderarr.append(paramList)
            return Response({"status":True, "response_msg": "Order history", "order":orderarr })
        except:
            return Response({"status":False, "response_msg": "Data not found", "order":{} })
        
        
class TestSendMail(APIView):
    def get(self, request, format=None):
        
        #subject, from_email, to = 'hello', 'luke@keyy.io', 'jsojitra.webdesk@gmail.com'
        #message = 'This is an important message.'
        
        subject = "I am a text email"
        to = ['jsojitra.webdesk@gmail.com']
        from_email = 'luke@keyy.io'
        
        ctx = {
            'user': 'buddy',
            'purchase': 'Books'
        }
        message = get_template('email/email.html').render(Context(ctx))
        msg = EmailMessage(subject, message, to=to, from_email=from_email)
        msg.content_subtype = 'html'
        msg.send()
        
        #EmailMessage(subject, message, to=to, from_email=from_email).send()
        
        
        return JsonResponse({"success":True, "data":"" })
        

class TestCustomerCURL(APIView):
    def get(self, request, format=None):
        
        req = urllib2.Request("https://api.stripe.com/v1/customers/cus_BUMJPFHu4aARxS")
        req.add_header("Authorization","Bearer sk_test_q3JrLZjPauq2dDN43PxQppfb")
        resp = urllib2.urlopen(req)
        content = resp.read()
        data = json.loads(content)

        
        return Response({"success":True, "data":data })
    
class GetSubsciption(APIView):
    def get(self, request, format=None):
        subs_req = urllib2.Request("https://api.stripe.com/v1/subscriptions")
        subs_req.add_header("Authorization","Bearer sk_test_q3JrLZjPauq2dDN43PxQppfb")
        subs_resp = urllib2.urlopen(subs_req)
        subs_content = subs_resp.read()
        #subs_data = json.loads(subs_content)
        
        return Response({"success":True, "data":subs_content })
    
    
    

class TestSendMailV1(APIView):
    serializer_class = TestEmailSerializer
    def post(self, request, format=None):
        
        #subject, from_email, to = 'hello', 'luke@keyy.io', 'jsojitra.webdesk@gmail.com'
        #message = 'This is an important message.'
        toemail = request.POST.get('email')
        
        if(toemail == ''):
            return Response({"success":False,"response_msg": "Enter Email", "data":{} })
        
        subject = "I am a text email"
        to = [toemail]
        from_email = 'luke@keyy.io'
        
        message = '</p>Hello this is testing email, please ignore it.</p>'
        msg = EmailMessage(subject, message, to=to, from_email=from_email)
        msg.content_subtype = 'html'
        msg.send(fail_silently=False)
        
        #EmailMessage(subject, message, to=to, from_email=from_email).send()
        
        
        return JsonResponse({"success":True, "response_msg": "sent successfully", "data":str(msg) })
    
    
    
def Export_to_csv_studentcoach(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="student-coach-'+ str(datetime.datetime.today()) +'.csv"'
    
    writer = csv.writer(response)
    writer.writerow(["id","first_name","last_name","account_type","e_mail","user_name","training_location","training_activity","other_activity","gender","date_of_birth","phone_number","profile_picture","hidden_profile_picture","skill","profile_bio","social_login","noti_jounral_entry","noti_module_assign","noti_conn_request","noti_personal_reminder","noti_sales_promoation_offer","noti_announce_update","status","subscription","subscription_invite","Total_Number_Of_Sign_Up","subscription_expiredate","last_login_date","subscription_start_date","subscription_period","subscription_amount","unique_id","reference_code","pub_date"])
    
    users = StudentCoach.objects.all().values_list("id","first_name","last_name","account_type","e_mail","user_name","training_location","training_activity","other_activity","gender","date_of_birth","phone_number","profile_picture","hidden_profile_picture","skill","profile_bio","social_login","noti_jounral_entry","noti_module_assign","noti_conn_request","noti_personal_reminder","noti_sales_promoation_offer","noti_announce_update","status","subscription","subscription_invite","invite_count","subscription_expiredate","last_login_date","subscription_start_date","subscription_period","subscription_amount","unique_id","reference_code","pub_date")
    for user in users:
        writer.writerow([unicode(s).encode("utf-8") for s in user])

    return response

