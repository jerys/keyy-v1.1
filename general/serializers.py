from django.contrib.auth.models import User, Group
from rest_framework import serializers

from general.models import StudentCoach
from general.models import UserUniqueCodeForgotPass
from general.models import OrderHistory
from general.models import Videos
from general.models import Trainingactivity
from general.models import UserReportProblem
from general.models import GeneralSetting
from general.models import WriteinvitationMessage
from general.models import WriteJournalEntryMessage
from general.models import SiteSetting



from students.models import StudentGoal
from students.models import StudentAssignment

from coaches.models import Coachmodule



'''
class TrainingactivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Trainingactivity
        fields = '__all__'
'''

'''
class StudentViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('first_name','last_name','e_mail','user_name','password')
'''

'''
class StudentSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)
    def create(self, validated_data):
        user = StudentCoach.objects.create(
            first_name = validated_data['first_name'],
            last_name = validated_data['last_name'],
            e_mail = validated_data['e_mail'],
            user_name = validated_data['user_name'],
            password = validated_data['password']
        )
        user.save()
        return user
    class Meta:
        model = StudentCoach
        fields = ('first_name','last_name','e_mail','user_name','password')
        
'''


'''
class StudentUpdatePassSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = [
            'e_mail','password'
        ]
        extra_kwargs = {
            "e_mail": {"write_only": True},
            "password": {"write_only": True},
            
        }
'''

        
#register
class RegisterSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('first_name','last_name','e_mail','password','social_login','date_of_birth','user_name','hidden_profile_picture',)

class RegisterUniqueIDSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','unique_id','user_name','first_name',)



class ReferenceUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','unique_id','user_name','first_name','reference_code',)

    
#login api
class UserLoginSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','e_mail','password','profile_picture','profile_picture2',)
        

class UserLastLoginUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','last_login_date',)

# comman sreen
class UserCommanscrrenSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','account_type')
 
class CommanVideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Videos
        fields = ('id','coach_video','student_video','student_video_thumb','coach_video_thumb',)

        
#student edit profile
class StudentUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','first_name','last_name','user_name','training_location','training_activity','other_activity','profile_picture','profile_picture2','skill','profile_bio','e_mail','phone_number','gender','date_of_birth','hidden_profile_picture','social_login','updated',)
        #fields = '__all__'
        

class StudentUpdateProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','profile_picture','profile_picture2',)


class StudentUpdateProfileSerializerV1(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','profile_picture2','updated',)

#Training Activity
class TrainingAcivitySerializer(serializers.ModelSerializer):
    class Meta:
        model = Trainingactivity
        #fields = '__all__'
        fields = ('id','title')


#Student home screen
class StudentHomeDataSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','first_name','last_name','user_name','profile_picture','profile_picture2','skill','training_location','hidden_profile_picture','social_login','account_type','subscription','subscription_invite','unique_id','updated',)
        
#student goal
class StudentGoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentGoal
        fields = ('id','goal_type','goal_option','is_completed',)
        
        
class StudentGoalAddSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentGoal
        fields = ('id','goal_type','goal_option',)
        
        
class StudentGoalDeleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentGoal
        fields = ('id','is_deleted',)

class StudentGoalUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentGoal
        fields = ('id','is_completed',)


class StudentGoalEditSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentGoal
        fields = ('id','goal_option',)
        

class StudentAssignmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodule
        fields = '__all__'
        
class StudentUnenrollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodule
        #fields = '__all__'
        fields = ('id','student_id',)
        

class StudentMarkasCompleteSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentAssignment
        #fields = '__all__'
        fields = ('id','is_completed','student_id','module_id','coach_id',)


class UserReportProblemSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserReportProblem
        fields = ('id','user_id','title','message',)


class ForgotPassSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('e_mail',)
        #fields = '__all__'
        
class UniqueCodeCheckSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserUniqueCodeForgotPass
        fields = ('id','unique_code',)

        
class ResetPassSerializer(serializers.ModelSerializer):
    confirm_password = serializers.CharField(required=True)
    class Meta:
        model = StudentCoach
        fields = ('id','password','confirm_password',)
        

class ChangePassSerializer(serializers.ModelSerializer):
    old_password = serializers.CharField(required=True)
    #password = serializers.CharField(required=True)
    class Meta:
        model = StudentCoach
        fields = ('id','old_password','password',)
        
    

class UserStripCustomerIDSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','strip_customer_id',)
        
class UserStripResponseSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','paymentdata','subscription',)

    
class StudentSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        #fields = ('id','e_mail', 'password')
        fields = '__all__'
        

class CoachSerializer(serializers.ModelSerializer):
    search_name = serializers.SerializerMethodField('first_name')
    
    def first_name(self, foo):
      return ''
  
    class Meta:
        model = StudentCoach
        fields = ('id','user_name', 'first_name','profile_picture','profile_picture2','search_name',)
        #fields = '__all__'
        


class GeneralSettingSerializer(serializers.ModelSerializer):
    class Meta:
        model = GeneralSetting
        fields = ('id','monthly_plan_id','yearly_plan_id','publishable_key',)
        #fields = '__all__'


class GeneralSettingUnSubSerializer(serializers.ModelSerializer):
    class Meta:
        model = GeneralSetting
        fields = ('id','monthly_plan_id','yearly_plan_id','publishable_key','secret_key',)
        

class SiteSettingUnSubSerializer(serializers.ModelSerializer):
    class Meta:
        model = SiteSetting
        fields = ('id','isPaymentEnable',)

class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderHistory
        fields = ('id','user_id','customer_id', 'subscription_id', 'object_type', 'plan_type','module_id','payment_data','created_date')
        

class WriteMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = WriteinvitationMessage
        fields = ('id','message')
        
class JournalEntryMessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = WriteJournalEntryMessage
        fields = ('id','feedback_message','social_sharing_message','module_upgrade_message')
        

class IOSSubscriptionSerializer(serializers.ModelSerializer):
    user_id = serializers.CharField()
    plan_type = serializers.CharField()
    subscription_id = serializers.CharField()
    class Meta:
        model = OrderHistory
        fields = ('id','user_id','plan_type','subscription_id')


class IOSCreateChargeSerializer(serializers.ModelSerializer):
    user_id = serializers.CharField()
    module_id = serializers.CharField()
    charge_id = serializers.CharField()
    
    class Meta:
        model = OrderHistory
        fields = ('id','user_id','module_id','charge_id')
        


class CreateSubscriptionSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField()
    user_id = serializers.CharField()
    email = serializers.CharField()
    token = serializers.CharField()
    amount = serializers.CharField()
    plan_type = serializers.CharField()
    class Meta:
        model = OrderHistory
        fields = ('id','user_name','user_id','email','token','amount','plan_type')



class CreateChargeSerializer(serializers.ModelSerializer):
    user_name = serializers.CharField()
    user_id = serializers.CharField()
    module_id = serializers.CharField()
    email = serializers.CharField()
    token = serializers.CharField()
    amount = serializers.CharField()
    module_title = serializers.CharField()
    class Meta:
        model = OrderHistory
        fields = ('id','user_name','user_id','module_id','email','token','amount','module_title')
        

class UnSubscriptionSerializer(serializers.ModelSerializer):
    subscription_id = serializers.CharField()
    class Meta:
        model = OrderHistory
        fields = ('subscription_id',)


class TestEmailSerializer(serializers.ModelSerializer):
    email = serializers.CharField()
    class Meta:
        model = OrderHistory
        fields = ('email',)


class CommanUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','first_name','last_name','user_name','e_mail','skill','profile_picture','profile_picture2','account_type','hidden_profile_picture','training_location','social_login','updated',)