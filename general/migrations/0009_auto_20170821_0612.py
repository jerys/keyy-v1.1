# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0008_auto_20170818_1450'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentcoach',
            name='date_of_birth',
            field=models.DateField(default=datetime.date.today, blank=True),
        ),
    ]
