# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0007_auto_20170817_1120'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentcoach',
            name='paymentdata',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='studentcoach',
            name='subscription',
            field=models.BooleanField(default=False),
        ),
    ]
