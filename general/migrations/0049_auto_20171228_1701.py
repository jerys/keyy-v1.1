# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-12-28 17:01
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0048_webhooktransaction'),
    ]

    operations = [
        migrations.AlterField(
            model_name='webhooktransaction',
            name='body',
            field=models.TextField(blank=True),
        ),
        migrations.AlterField(
            model_name='webhooktransaction',
            name='request_meta',
            field=models.TextField(blank=True),
        ),
    ]
