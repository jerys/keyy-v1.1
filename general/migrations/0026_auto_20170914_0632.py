# -*- coding: utf-8 -*-
# Generated by Django 1.11.4 on 2017-09-14 06:32
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0025_generalsetting'),
    ]

    operations = [
        migrations.AlterField(
            model_name='generalsetting',
            name='publishable_key',
            field=models.TextField(blank=True, verbose_name='Strip Publishable key'),
        ),
        migrations.AlterField(
            model_name='generalsetting',
            name='secret_key',
            field=models.TextField(blank=True, verbose_name='Strip Secret key'),
        ),
    ]
