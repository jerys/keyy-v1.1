# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0019_auto_20170830_0538'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentcoach',
            name='unique_id',
            field=models.CharField(max_length=250, blank=True),
        ),
    ]
