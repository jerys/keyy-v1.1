# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0009_auto_20170821_0612'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='studentcoach',
            name='login_with',
        ),
        migrations.AddField(
            model_name='studentcoach',
            name='social_login',
            field=models.BooleanField(default=False),
        ),
    ]
