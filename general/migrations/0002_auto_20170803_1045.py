# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-08-03 10:45
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentcoach',
            name='user_name',
            field=models.CharField(blank=True, max_length=250),
        ),
    ]
