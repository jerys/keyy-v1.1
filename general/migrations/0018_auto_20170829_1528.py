# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0017_auto_20170825_0912'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentcoach',
            name='invite_count',
            field=models.CharField(max_length=250, blank=True),
        ),
        migrations.AddField(
            model_name='studentcoach',
            name='subscription_expiredate',
            field=models.DateField(null=True, verbose_name='Subscription Expire Date', blank=True),
        ),
        migrations.AddField(
            model_name='studentcoach',
            name='subscription_invite',
            field=models.BooleanField(default=False),
        ),
    ]
