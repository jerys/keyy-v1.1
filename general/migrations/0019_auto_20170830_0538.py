# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0018_auto_20170829_1528'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentcoach',
            name='noti_announce_update',
            field=models.CharField(default=0, max_length=1, verbose_name='Keyy announcements and updates', choices=[(b'1', b'On'), (b'0', b'Off')]),
        ),
        migrations.AlterField(
            model_name='studentcoach',
            name='noti_conn_request',
            field=models.CharField(default=0, max_length=1, verbose_name='New connection request', choices=[(b'1', b'On'), (b'0', b'Off')]),
        ),
        migrations.AlterField(
            model_name='studentcoach',
            name='noti_jounral_entry',
            field=models.CharField(default=0, max_length=1, verbose_name='Jounral entry comments ', choices=[(b'1', b'On'), (b'0', b'Off')]),
        ),
        migrations.AlterField(
            model_name='studentcoach',
            name='noti_module_assign',
            field=models.CharField(default=0, max_length=1, verbose_name='New modules assigned', choices=[(b'1', b'On'), (b'0', b'Off')]),
        ),
        migrations.AlterField(
            model_name='studentcoach',
            name='noti_personal_reminder',
            field=models.CharField(default=0, max_length=1, verbose_name='Personalized reminders', choices=[(b'1', b'On'), (b'0', b'Off')]),
        ),
        migrations.AlterField(
            model_name='studentcoach',
            name='noti_sales_promoation_offer',
            field=models.CharField(default=0, max_length=1, verbose_name='Sales promoation and offers', choices=[(b'1', b'On'), (b'0', b'Off')]),
        ),
    ]
