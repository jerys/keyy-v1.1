# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0016_auto_20170824_1646'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='videos',
            options={'verbose_name': 'Common Screen Video', 'verbose_name_plural': 'Common Screen Video'},
        ),
    ]
