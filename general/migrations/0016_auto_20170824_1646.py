# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0015_auto_20170824_0716'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentcoach',
            name='date_of_birth',
            field=models.DateField(default='1970-01-01', blank=True),
        ),
    ]
