# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0010_auto_20170821_1200'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentcoach',
            name='e_mail',
            field=models.EmailField(max_length=254, blank=True),
        ),
    ]
