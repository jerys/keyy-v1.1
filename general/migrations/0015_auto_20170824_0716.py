# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0014_auto_20170822_1126'),
    ]

    operations = [
        migrations.AddField(
            model_name='videos',
            name='coach_video_thumb',
            field=models.FileField(upload_to='screenVideo/thumb/', blank=True),
        ),
        migrations.AddField(
            model_name='videos',
            name='student_video_thumb',
            field=models.FileField(upload_to='screenVideo/thumb/', blank=True),
        ),
    ]
