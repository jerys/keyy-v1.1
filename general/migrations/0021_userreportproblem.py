# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0020_studentcoach_unique_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserReportProblem',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('title', models.CharField(max_length=250, blank=True)),
                ('message', models.TextField(blank=True)),
                ('date', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('user_id', models.ForeignKey(related_name='UserReportProblem', to='general.StudentCoach')),
            ],
            options={
                'verbose_name': 'User Report a problem',
                'verbose_name_plural': 'Report a problem',
            },
        ),
    ]
