# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0012_auto_20170821_1226'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentcoach',
            name='social_login',
            field=models.CharField(default=False, max_length=25, blank=True),
        ),
    ]
