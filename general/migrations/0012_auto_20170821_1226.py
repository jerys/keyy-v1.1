# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0011_auto_20170821_1216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentcoach',
            name='first_name',
            field=models.CharField(max_length=250, blank=True),
        ),
        migrations.AlterField(
            model_name='studentcoach',
            name='last_name',
            field=models.CharField(max_length=250, blank=True),
        ),
        migrations.AlterField(
            model_name='studentcoach',
            name='password',
            field=models.CharField(max_length=250, blank=True),
        ),
    ]
