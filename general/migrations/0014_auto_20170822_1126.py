# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0013_auto_20170821_1326'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentcoach',
            name='hidden_profile_picture',
            field=models.CharField(max_length=250, blank=True),
        ),
        migrations.AlterField(
            model_name='studentcoach',
            name='profile_picture',
            field=models.FileField(upload_to='student/student_profilepic/', blank=True),
        ),
    ]
