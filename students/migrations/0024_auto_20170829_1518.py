# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0023_remove_studentassignment_unenroll'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='studentassignment',
            options={'verbose_name': 'Student Assignment ', 'verbose_name_plural': 'Student Assignment Moduel'},
        ),
    ]
