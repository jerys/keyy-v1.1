# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0025_journal_date'),
    ]

    operations = [
        migrations.CreateModel(
            name='JournalFeedbackAttachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('journal_feedback_thumb', models.CharField(max_length=250, blank=True)),
                ('journal_feedback_attachment', models.FileField(upload_to='student/student_journal_feedback/', blank=True)),
                ('journal_feedback_type', models.CharField(max_length=250, blank=True)),
            ],
            options={
                'verbose_name': 'Journal Feedback Attachment',
                'verbose_name_plural': 'Journal Feedback Attachment',
            },
        ),
        migrations.AddField(
            model_name='studentjournalfeedback',
            name='journal_feedback_attachments',
            field=models.TextField(verbose_name='Journal Feedback Attachments', blank=True),
        ),
    ]
