# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0008_auto_20170818_1450'),
        ('students', '0005_auto_20170808_1219'),
    ]

    operations = [
        migrations.AddField(
            model_name='studentassignment',
            name='coach_id',
            field=models.ForeignKey(related_name='Coach', default=1, to='general.StudentCoach'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='studentassignment',
            name='student_id',
            field=models.ForeignKey(related_name='Student', to='general.StudentCoach'),
        ),
    ]
