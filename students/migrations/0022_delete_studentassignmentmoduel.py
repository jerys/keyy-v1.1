# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0021_auto_20170828_1423'),
    ]

    operations = [
        migrations.DeleteModel(
            name='StudentAssignmentModuel',
        ),
    ]
