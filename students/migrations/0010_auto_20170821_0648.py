# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0009_auto_20170821_0643'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journal',
            name='whats_going_on_today',
            field=models.ForeignKey(to='general.journalentryoption', null=True),
        ),
    ]
