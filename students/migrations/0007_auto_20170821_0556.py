# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0006_auto_20170818_1450'),
    ]

    operations = [
        migrations.RenameField(
            model_name='journal',
            old_name='student',
            new_name='student_id',
        ),
        migrations.RemoveField(
            model_name='journal',
            name='coach',
        ),
        migrations.RemoveField(
            model_name='journal',
            name='status',
        ),
        migrations.AlterField(
            model_name='journal',
            name='video1',
            field=models.FileField(upload_to='student/student_journal/', blank=True),
        ),
        migrations.AlterField(
            model_name='journal',
            name='video2',
            field=models.FileField(upload_to='student/student_journal/', blank=True),
        ),
        migrations.AlterField(
            model_name='journal',
            name='video3',
            field=models.FileField(upload_to='student/student_journal/', blank=True),
        ),
    ]
