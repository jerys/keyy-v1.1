# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0007_auto_20170821_0556'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journal',
            name='journal_title',
            field=models.CharField(max_length=250, verbose_name='Journal Title', blank=True),
        ),
        migrations.AlterField(
            model_name='journal',
            name='place',
            field=models.CharField(max_length=250, verbose_name='Check In', blank=True),
        ),
        migrations.AlterField(
            model_name='journal',
            name='student_id',
            field=models.ForeignKey(related_name='StudentJournal', blank=True, to='general.StudentCoach'),
        ),
        migrations.AlterField(
            model_name='journal',
            name='today_fill',
            field=models.CharField(blank=True, max_length=1, verbose_name='Today I feel', choices=[('1', 'Bad'), ('2', 'Average'), ('3', 'Medium'), ('4', 'Good'), ('5', 'Very good')]),
        ),
        migrations.AlterField(
            model_name='journal',
            name='whats_going_on_today',
            field=models.ForeignKey(blank=True, to='general.journalentryoption', max_length=1),
        ),
    ]
