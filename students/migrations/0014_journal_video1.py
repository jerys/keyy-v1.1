# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0013_remove_journal_video1'),
    ]

    operations = [
        migrations.AddField(
            model_name='journal',
            name='video1',
            field=models.FileField(upload_to='student/student_journal/', blank=True),
        ),
    ]
