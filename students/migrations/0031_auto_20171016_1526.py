# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-16 15:26
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0030_auto_20171016_1502'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentgoal',
            name='completed_date',
            field=models.DateTimeField(blank=True, default='1970-01-01'),
        ),
        migrations.AlterField(
            model_name='studentgoal',
            name='creation_date',
            field=models.DateTimeField(blank=True, default='1970-01-01'),
        ),
    ]
