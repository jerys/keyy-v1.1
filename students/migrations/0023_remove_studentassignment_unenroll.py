# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0022_delete_studentassignmentmoduel'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='studentassignment',
            name='unenroll',
        ),
    ]
