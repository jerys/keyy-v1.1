# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0017_auto_20170825_0912'),
        ('students', '0018_auto_20170826_0624'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentJournalFeedback',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('type_of_user', models.CharField(blank=True, max_length=50, verbose_name='User Type', choices=[('Student', 'Student'), ('Coach', 'Coach')])),
                ('feedback', models.TextField(blank=True)),
                ('date', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('journal_id', models.ForeignKey(related_name='JournalID', to='students.Journal')),
                ('user_id', models.ForeignKey(related_name='UserID', to='general.StudentCoach')),
            ],
            options={
                'verbose_name': 'Student Journal Feedback',
                'verbose_name_plural': 'Student Journal Feedback',
            },
        ),
    ]
