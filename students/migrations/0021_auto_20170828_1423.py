# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0020_studentassignmentmoduel'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentassignment',
            name='coach_id',
            field=models.ForeignKey(related_name='Coach', to='general.StudentCoach', null=True),
        ),
        migrations.AlterField(
            model_name='studentassignment',
            name='student_id',
            field=models.ForeignKey(related_name='Student', to='general.StudentCoach', null=True),
        ),
    ]
