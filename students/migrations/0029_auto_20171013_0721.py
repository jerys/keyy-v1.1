# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-10-13 07:21
from __future__ import unicode_literals

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0028_auto_20171013_0648'),
    ]

    operations = [
        migrations.AlterField(
            model_name='journal',
            name='date',
            field=models.DateTimeField(blank=True, default=datetime.datetime.now, verbose_name='Creation Date'),
        ),
    ]
