# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0016_auto_20170824_1626'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='JournalAttachment',
            new_name='JournalEntryAttachment',
        ),
    ]
