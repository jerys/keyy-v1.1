# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0014_journal_video1'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='journal',
            name='video1',
        ),
    ]
