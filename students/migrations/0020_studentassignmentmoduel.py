# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0019_studentjournalfeedback'),
    ]

    operations = [
        migrations.CreateModel(
            name='StudentAssignmentModuel',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('student_id', models.CharField(max_length=50, blank=True)),
                ('coach_id', models.CharField(max_length=50, blank=True)),
                ('module_id', models.CharField(max_length=50, blank=True)),
                ('is_completed', models.CharField(default='No', max_length=50, blank=True, choices=[('Yes', 'Yes'), ('No', 'No')])),
                ('unenroll', models.CharField(default='No', max_length=50, verbose_name='Unenroll', blank=True, choices=[('Yes', 'Yes'), ('No', 'No')])),
            ],
            options={
                'verbose_name': 'Student Assignment Moduel',
                'verbose_name_plural': 'Student Assignment Moduel',
            },
        ),
    ]
