# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0009_auto_20170821_0612'),
        ('students', '0008_auto_20170821_0631'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='journal',
            options={'verbose_name': 'Student Journal Entry', 'verbose_name_plural': 'Student Journal'},
        ),
        migrations.RemoveField(
            model_name='journal',
            name='student_id',
        ),
        migrations.AddField(
            model_name='journal',
            name='student',
            field=models.ForeignKey(related_name='StudentJournal', default=1, to='general.StudentCoach'),
            preserve_default=False,
        ),
    ]
