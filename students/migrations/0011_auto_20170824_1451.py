# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0010_auto_20170821_0648'),
    ]

    operations = [
        migrations.CreateModel(
            name='JournalAttachment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('journal_thumb', models.CharField(max_length=250, blank=True)),
                ('journal_attachment', models.FileField(upload_to='student/student_journal/', blank=True)),
                ('journal_type', models.CharField(max_length=250, blank=True)),
            ],
            options={
                'verbose_name': 'Journal Attachment',
                'verbose_name_plural': 'Journal Attachment',
            },
        ),
        migrations.RemoveField(
            model_name='journal',
            name='video1',
        ),
        migrations.RemoveField(
            model_name='journal',
            name='video2',
        ),
        migrations.RemoveField(
            model_name='journal',
            name='video3',
        ),
        migrations.AddField(
            model_name='journal',
            name='journal_attachments',
            field=models.TextField(verbose_name='Journal Attachments', blank=True),
        ),
    ]
