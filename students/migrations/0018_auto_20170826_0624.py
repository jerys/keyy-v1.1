# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0017_auto_20170824_1634'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='studentassignment',
            options={'verbose_name': 'Student Assignment', 'verbose_name_plural': 'Student Assignment Moduel'},
        ),
        migrations.AddField(
            model_name='journal',
            name='coach_id',
            field=models.TextField(blank=True),
        ),
    ]
