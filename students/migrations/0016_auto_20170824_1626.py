# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('students', '0015_remove_journal_video1'),
    ]

    operations = [
        migrations.AlterField(
            model_name='studentassignment',
            name='unenroll',
            field=models.CharField(default='No', max_length=50, verbose_name='Unenroll', blank=True, choices=[('Yes', 'Yes'), ('No', 'No')]),
        ),
    ]
