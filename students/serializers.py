from rest_framework import serializers

from general.models import journalentryoption
from students.models import Journal
from students.models import JournalEntryAttachment
from students.models import StudentJournalFeedback
from students.models import JournalFeedbackAttachment
from general.models import StudentCoach


from notification.models import StudentCoachRequest
from fcm_django.models import FCMDevice


class GoingOnTodaySerializer(serializers.ModelSerializer):
    class Meta:
        model = journalentryoption
        #fields = '__all__'
        fields = ('id','title',)


#tag 
class StudentJournalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Journal
        #fields = '__all__'
        fields = ('id','today_fill','whats_going_on_today','place','journal_title','journal_desc','journal_attachments')


class StudentJournalSerializerFront(serializers.ModelSerializer):
    class Meta:
        model = Journal
        #fields = '__all__'
        fields = ('id','today_fill','whats_going_on_today','place','journal_title','journal_desc','journal_attachments','place')


class StudentJournalAttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = JournalEntryAttachment
        #fields = '__all__'
        fields = ('journal_thumb','journal_attachment','journal_type')
        

class StudentCoachSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        #fields = '__all__'
        fields = ('id','account_type','user_name','first_name','last_name','profile_picture','profile_picture2','hidden_profile_picture','social_login','updated','noti_jounral_entry',)
        
class StudentCoachRequestListSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoachRequest
        #fields = '__all__'
        fields = ('id','student','coach',)


class StudentSharedJournalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Journal
        #fields = '__all__'
        fields = ('id','coach_id',)
        
        
class StudentSharedJournalFeedbackSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentJournalFeedback
        #fields = '__all__'
        fields = ('id','journal_id','user_id','feedback','date','journal_feedback_attachments',)
        

class StudentJournalFeedbackAttachmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = JournalFeedbackAttachment
        #fields = '__all__'
        fields = ('journal_feedback_thumb','journal_feedback_attachment','journal_feedback_type')

#JournalList
class StudentJournalListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Journal
        #fields = '__all__'
        fields = ('id','today_fill','whats_going_on_today','place','journal_title','journal_desc','journal_attachments','object_type','object_id','journal_status','date')
        
#Journal detail
class StudentJournalDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Journal
        #fields = '__all__'
        fields = ('id','student','today_fill','whats_going_on_today','place','journal_title','journal_desc','journal_attachments','coach_id','object_type','object_id','journal_status','date',)
        
        

# Update push notification token
class UpdatePushNotificationTokenSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        #fields = '__all__'
        fields = ('id','push_token','device_type')
        

class UpdatePushNotificationTokenSerializerv1(serializers.ModelSerializer):
    class Meta:
        model = FCMDevice
        #fields = '__all__'
        fields = ('id','registration_id','type')
