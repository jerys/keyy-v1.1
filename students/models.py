from __future__ import unicode_literals

import datetime
from django.db import models
from datetime import datetime

from django.conf import settings

# Create your models here.
class StudentGoal(models.Model):
    COMPLETE_STATUS = (('Yes', 'Yes'),('No', 'No'),)
    DELETE_STATUS = (('Yes', 'Yes'),('No', 'No'),)
    student = models.ForeignKey("general.StudentCoach", related_name="StudentGoal",on_delete=models.CASCADE, blank=True)
    goal_type = models.CharField(max_length=250, choices=settings.GOAL_TYPE)
    goal_option = models.CharField(max_length=250)
    is_completed = models.CharField(max_length=50, choices=COMPLETE_STATUS,default='No',blank=True)
    is_deleted = models.CharField(max_length=50, choices=DELETE_STATUS,default='No',blank=True)
    creation_date = models.DateField('Creation Date',blank=True,null=True)
    completed_date = models.DateField('Completed Date',blank=True,null=True)
    
    class Meta:
        verbose_name = 'Student Goal'
        verbose_name_plural = 'Student Goal'
        
        
class JournalEntryAttachment(models.Model):
    journal_thumb = models.CharField(max_length=250,blank=True)
    journal_attachment = models.FileField(upload_to = 'student/student_journal/',blank=True)
    journal_type = models.CharField(max_length=250,blank=True)
    
    class Meta:
        verbose_name = 'Journal Attachment'
        verbose_name_plural = 'Journal Attachment'
        

class Journal(models.Model):
    CHOICES_EMOJI = (
        ('1', 'Bad'),
        ('2', 'Average'),
        ('3', 'Medium'),
        ('4', 'Good'),
        ('5', 'Very good'),
    )
    CHOICES_TODAY = (
        ('Class', 'Class'),
        ('Private Lesson', 'Private Lesson'),
        ('Competition', 'Competition'),
        ('Milestone', 'Milestone'),
        
    )
    JOURNAL_STATUS = (
        ('Active', 'Active'),
        ('Archive', 'Archive'),
    )
    #"What is going on today?", 
    student = models.ForeignKey("general.StudentCoach", related_name="StudentJournal")
    today_fill = models.CharField("Today I feel", max_length=1, choices=CHOICES_EMOJI, blank=True)
    whats_going_on_today = models.ForeignKey("general.journalentryoption", null=True)
    place = models.CharField("Check In", max_length=250, blank=True)
    journal_title = models.CharField("Journal Title", max_length=250, blank=True)
    journal_desc = models.TextField("Tell me more", blank=True)
    journal_attachments = models.TextField("Journal Attachments",blank=True)
    coach_id = models.TextField(blank=True)
    object_type = models.CharField("Type of Journal", max_length=250, default='Journal',blank=True)
    object_id = models.CharField("Object ID", max_length=250, blank=True)
    date = models.DateTimeField('Creation Date',default=datetime.now,blank=True)
    journal_status = models.CharField("Status", max_length=250, choices=JOURNAL_STATUS,default='Active', blank=True)

    class Meta:
        verbose_name = 'Student Journal Entry'
        verbose_name_plural = 'Student Journal'
        
    def __unicode__(self):
        return self.journal_title 
    

class StudentAssignment(models.Model):
    COMPLETE_STATUS = (('Yes', 'Yes'),('No', 'No'),)
    UNENROLL_STATUS = (('Yes', 'Yes'),('No', 'No'),)
    student_id = models.ForeignKey("general.StudentCoach",related_name="Student", limit_choices_to={'account_type': 'Student'},null=True)
    coach_id = models.ForeignKey("general.StudentCoach", related_name="Coach", limit_choices_to={'account_type': 'Coach'},null=True)
    module_id = models.ForeignKey("coaches.Coachmodule",blank=True)
    is_completed = models.CharField(max_length=50, choices=COMPLETE_STATUS,default='No',blank=True)
    completed_date = models.DateField('Completed Date',blank=True,null=True)
    
    
    class Meta:
        verbose_name = 'Student Assignment '
        verbose_name_plural = 'Student Assignment Complete'


class JournalFeedbackAttachment(models.Model):
    journal_feedback_thumb = models.CharField(max_length=250,blank=True)
    journal_feedback_attachment = models.FileField(upload_to = 'student/student_journal_feedback/',blank=True)
    journal_feedback_type = models.CharField(max_length=250,blank=True)
    
    class Meta:
        verbose_name = 'Journal Feedback Attachment'
        verbose_name_plural = 'Journal Feedback Attachment'


class StudentJournalFeedback(models.Model):
    USER_TYPE = (('Student', 'Student'),('Coach', 'Coach'),)
    journal_id = models.ForeignKey(Journal, related_name="JournalID")
    user_id = models.ForeignKey("general.StudentCoach",related_name="UserID")
    type_of_user = models.CharField("User Type",max_length=50, choices=USER_TYPE,blank=True)
    feedback = models.TextField(blank=True)
    journal_feedback_attachments = models.TextField("Journal Feedback Attachments",blank=True)
    date = models.DateTimeField(default=datetime.now,blank=True)
    
    class Meta:
        verbose_name = 'Student Journal Feedback'
        verbose_name_plural = 'Student Journal Feedback'
        

class StudentAssignUnassignTrack(models.Model):
    COMPLETE_STATUS = (('Yes', 'Yes'),('No', 'No'),)
    student_id = models.ForeignKey("general.StudentCoach",related_name="StudentTrack", limit_choices_to={'account_type': 'Student'},null=True)
    coach_id = models.ForeignKey("general.StudentCoach", related_name="CoachTrack", limit_choices_to={'account_type': 'Coach'},null=True)
    module_id = models.ForeignKey("coaches.Coachmodule",blank=True)
    is_assign = models.CharField(max_length=50, choices=COMPLETE_STATUS,default='No',blank=True)
    
    class Meta:
        verbose_name = 'Student Assignment Track'
        verbose_name_plural = 'Student Assignment Complete Track'