from django.contrib import admin
from django.conf import settings
import datetime
import csv
from django.http import HttpResponse

# Register your models here.
from .models import StudentGoal
class StudentGoalAdmin(admin.ModelAdmin):
    '''
    def export_csv(modeladmin, request, queryset):
        response = HttpResponse(content_type='text/csv')
        response['Content-Disposition'] = 'attachment;     filename="goal-'+ str(datetime.datetime.today()) +'.csv"'
        writer = csv.writer(response)
        writer.writerow(["student","goal_type","goal_option","creation_date","is_completed","completed_date","is_deleted",])
        for obj in queryset:
            writer.writerow([
                obj.student,
                obj.goal_type,
                obj.goal_option,
                obj.creation_date,
                obj.is_completed,
                obj.completed_date,
                obj.is_deleted,
            ])
        return response
    actions = [export_csv]
    '''
    readonly_fields = ['creation_date', 'completed_date']
    list_display=["student","goal_type","goal_option","creation_date","is_completed","completed_date","is_deleted"]
    fields = ("student","goal_type","goal_option","is_deleted","is_completed","creation_date","completed_date")
    list_per_page = settings.ADMINPERPAGE
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["student","goal_type","goal_option","is_deleted","creation_date","completed_date"]
        else:
            return self.readonly_fields
    def has_add_permission(self, request):
        return False
    
    
        
admin.site.register(StudentGoal,StudentGoalAdmin)

from .models import Journal
class JournalAdmin(admin.ModelAdmin):
    def Shared_with_User_ID(self):
        html = ''
        coach_idarr = self.coach_id.split(",")
        for cid in coach_idarr:
            if(cid):
                html +='<a href="/admin/general/studentcoach/%s/change/">%s</a> | ' % (cid, cid)
        return html
    Shared_with_User_ID.allow_tags = True
    
    list_display=["student","journal_title","today_fill","place","date",Shared_with_User_ID]
    #fields = '__all__'
    fields = ("student","today_fill","whats_going_on_today","place","journal_title","journal_desc","journal_attachments","coach_id","date","journal_status")
    
    
    
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["student","today_fill","whats_going_on_today","place","journal_title","journal_desc","journal_attachments","coach_id","date"]
        else:
            return self.readonly_fields
    def has_add_permission(self, request):
        return False
admin.site.register(Journal,JournalAdmin)

from .models import StudentAssignment
class StudentAssignmentAdmin(admin.ModelAdmin):
    list_display=["module_id","student_id","coach_id","is_completed","completed_date"]
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["module_id","student_id","coach_id","is_completed","completed_date"]
        else:
            return self.readonly_fields
    def has_add_permission(self, request):
        return False

admin.site.register(StudentAssignment,StudentAssignmentAdmin)

#from .models import StudentAssignmentModuel
#admin.site.register(StudentAssignmentModuel,list_display=["module_id","student_id","coach_id","is_completed","unenroll"])


from .models import StudentJournalFeedback
class JournalFeddbackAdmin(admin.ModelAdmin):
    list_display=["journal_id","user_id","feedback","date"]
    #fields = '__all__'
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["journal_id","user_id","type_of_user","feedback","journal_feedback_attachments","date"]
        else:
            return self.readonly_fields
    def has_add_permission(self, request):
        return False
admin.site.register(StudentJournalFeedback,JournalFeddbackAdmin)


'''
from .models import StudentAssignUnassignTrack
class StudentAssignUnassignTrackAdmin(admin.ModelAdmin):
    list_display=["student_id","coach_id","module_id","is_assign"]
    
    
admin.site.register(StudentAssignUnassignTrack,StudentAssignUnassignTrackAdmin)
'''

