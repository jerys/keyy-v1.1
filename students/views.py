from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse, Http404
from rest_framework import status
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from rest_framework.renderers import TemplateHTMLRenderer

from django.shortcuts import render_to_response

from general.models import StudentCoach
from general.models import journalentryoption
from students.models import Journal
from students.models import StudentJournalFeedback
from students.models import JournalFeedbackAttachment
from students.models import StudentGoal
from students.models import StudentAssignment


from notification.models import StudentCoachRequest
from notification.models import Notification

from fcm_django.models import FCMDevice


from django.conf import settings
import os
from django.db.models import Q
import datetime
import calendar
import json
import csv

import gamification

#MKT add
from rest_framework.pagination import PageNumberPagination
from rest_framework.pagination import LimitOffsetPagination

from django.http import HttpResponseRedirect

from students.serializers import StudentJournalSerializer, StudentJournalAttachmentSerializer, StudentCoachRequestListSerializer, StudentSharedJournalSerializer, StudentSharedJournalFeedbackSerializer, StudentJournalListSerializer, GoingOnTodaySerializer, StudentJournalFeedbackAttachmentSerializer, UpdatePushNotificationTokenSerializer, UpdatePushNotificationTokenSerializerv1, StudentJournalDetailSerializer, StudentJournalSerializerFront, StudentCoachSerializer


def error_404(request):
    data = {}
    return render(request,'404.html', data)
    #return HttpResponseRedirect("https://developer.mozilla.org/en-US/docs/Learn/Server-side/Django/Forms")
    
def error_500(request):
    data = {}
    return render(request,'404.html', data)


# what is going on today
class GoingOnToadylist(APIView):
    serializer_class = GoingOnTodaySerializer
    def get(self, request, format=None):
        try:
            goingobj = journalentryoption.objects.all()
            serializer = GoingOnTodaySerializer(goingobj,many=True)
            return Response({"status":True, "response_msg": "What is going on today list", "goingonobj":serializer.data })
        except:
            return Response({"status":False, "response_msg": "Data not found", "goingonobj":{} })
    

# Create your views here.
class CreateJournal(APIView):
    serializer_class = StudentJournalSerializer
    def post(self, request,studentID, format=None):
        serializer = StudentJournalSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(student_id=studentID)
            return Response({"status":True, "response_msg": "Journal entry added successfully", "journal":serializer.data })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)
    

# Edit your journal here.
class EditJournal(APIView):
    serializer_class = StudentJournalSerializer
    def get(self, request,studentID,pk, format=None):
        try:
            journalobj = Journal.objects.get(pk=pk)
            serializer = StudentJournalSerializer(journalobj)
            return Response({"status":True, "response_msg": "Journal entry update successfully", "journal":serializer.data })
        except:
            return Response({"status":False, "response_msg": "Data not found", "journal":{} })
    def post(self, request,studentID,pk, format=None):
        try:
            journalobj = Journal.objects.get(pk=pk)
            serializer = StudentJournalSerializer(journalobj,data=request.data)
            if serializer.is_valid():
                serializer.save(student_id=studentID)
                return Response({"status":True, "response_msg": "Journal entry update successfully", "journal":serializer.data })
            return Response({"status":False, "response_msg": "Update failed", "journal":{} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "journal":{} })
    
#delete journal
class DeleteJournal(APIView):
    serializer_class = StudentJournalSerializer
    def get(self, request,studentID,pk, format=None):
        try:
            journalobj = Journal.objects.get(pk=pk)
            
            attachments = journalobj.journal_attachments
            if(attachments):
                attachment_data = json.loads(attachments)
                if(attachment_data):    
                    for attchment_val in attachment_data:
                        if(attchment_val):
                            video_URL = settings.BASE_DIR + str(attchment_val['video_url'])
                            if os.path.isfile(video_URL):
                                os.unlink(video_URL)
                            
                        if(attchment_val):
                            thumb_url = settings.BASE_DIR + str(attchment_val['thumb_url'])
                            if os.path.isfile(thumb_url):
                                os.unlink(thumb_url)
                                
                
            journalobj.delete()
            
            journalcount = Journal.objects.filter(Q(student_id=studentID)).count()
            gamification.comman_function.update_user_data(studentID,'journalentrypublished',journalcount)

            #update unique place in user master
            journalunique = Journal.objects.filter(Q(student=studentID), ~Q(place='')).values('place').distinct()
            if(journalunique.count() > 0):
                uniquecount = journalunique.count()
                gamification.comman_function.update_user_data(studentID,'journalentrypublisheduniquelocation',uniquecount)


            return Response({"status":True, "response_msg": "Journal delete successfully", "journal":{} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "journal":{} })



class UploadJournalAttachment(APIView):
    serializer_class = StudentJournalAttachmentSerializer
    def post(self, request, format=None):
        serializer = StudentJournalAttachmentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status":True, "response_msg": "Journal attachment upload successfully", "journal_attachment":serializer.data })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)

class CreateJournalFront(APIView):
    serializer_class = StudentJournalSerializer
    def post(self, request,studentID, format=None):
        serializer = StudentJournalSerializer(data=request.data)
        place = request.POST.get('place')
        if serializer.is_valid():
            serializer.save(student_id=studentID)
            
            journalcount = Journal.objects.filter(Q(student_id=studentID)).count()
            gamification.comman_function.update_user_data(studentID,'journalentrypublished',journalcount)

            
            if(place != ''):
                #update unique place in user master
                journalunique = Journal.objects.filter(Q(student=studentID), ~Q(place='')).values('place').distinct()
                if(journalunique.count() > 0):
                    uniquecount = journalunique.count()
                    gamification.comman_function.update_user_data(studentID,'journalentrypublisheduniquelocation',uniquecount)

            return Response({"status":True, "response_msg": "Journal entry added successfully", "journal":serializer.data })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)



#my coah list for studdent share his jounrl entry
class ListMyCoachForStudentJournal(APIView):
    serializer_class = StudentCoachRequestListSerializer
    def get(self, request,userID,pk, format=None):
        try:
            #requestobj = StudentCoachRequest.objects.filter(Q(student=userID), Q(request_to='Coach'), Q(status='Approve') )
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            
            
            Coachidarr = []
            requestobj = StudentCoachRequest.objects.filter(Q(user=userID), Q(status='Approve') )
            if(requestobj.count() > 0):
                for requestval in requestobj:
                    Coachidarr.append(requestval.user_to_id)
            
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=userID), Q(status='Approve') )
            if(requestobj1.count() > 0):
                for requestval in requestobj1:
                    Coachidarr.append(requestval.user_id)
                    
            
        
            if Coachidarr:
                
                coahobj = StudentCoach.objects.filter(pk__in=Coachidarr,account_type='Coach')
                serializer = StudentCoachSerializer(coahobj,many=True)
                
                coacharr = []
                for coachval in serializer.data:
                    coachParlist = {}
                    coachParlist['id'] = coachval['id']
                    coachParlist['first_name'] = coachval['first_name']
                    coachParlist['last_name'] = coachval['last_name']
                    coachParlist['user_name'] = coachval['user_name']
                    coachParlist['account_type'] = coachval['account_type']
                    coachParlist['updated'] = coachParlist['updated']
                    
                    if(coachval['profile_picture']):
                        #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(coachval.profile_picture)
                        #if os.path.isfile(profile_picture):
                        profile_picture = coachval['profile_picture']
                        #else:
                            #profile_picture = ''
                    elif(coachval['profile_picture2']):
                        profile_picture = coachval['profile_picture2']
                    elif (coachval['hidden_profile_picture'] !='' and coachval['social_login'] == 'True'):
                        profile_picture = coachval['hidden_profile_picture']
                    else:
                        profile_picture = ''
                        
                    coachParlist['profile_picture'] = profile_picture
                    
                    coacharr.append(coachParlist)
                
                    
                return Response({"status":True, "response_msg": "My Coach list", "user_id":userID,"journal_id":pk, "coachobj":coacharr })
            return Response({"status":False, "response_msg": "Data not found", "user_id":userID, "coachobj": {} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "user_id":userID, "coachobj": {} })
        
 
def addmonths(sourcedate,months):
    month = sourcedate.month - 1 + months
    year = int(sourcedate.year + month / 12 )
    month = month % 12 + 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return datetime.date(year,month,day)

# shared student journal entry to coach // add entry to journal table
class SharedJournalToCoach(APIView):
    serializer_class = StudentSharedJournalSerializer
    def post(self, request,userID,pk, format=None):
        try:
            #check user pro or free
            userobj = StudentCoach.objects.filter(pk=userID).values('subscription','subscription_invite','subscription_expiredate')
            if(userobj[0]['subscription'] == True):
                subscription = True
            elif (userobj[0]['subscription_invite'] == True):
                subscription = True
            else:
                subscription = False
            
            #check journal entry to share
            journalentrycount = Journal.objects.filter(student_id=userID)
            
            lastdate = ''
            if(journalentrycount.count() > 0):
                for each in journalentrycount:
                    if(each.coach_id != ''):
                        lastdate = each.date
                        
                
            if(lastdate):
                currentdate = datetime.date.today()
                next90days = addmonths(lastdate,3)
                
                if currentdate > next90days:
                    shared =True
                else:
                    shared =False
            else:
                shared =True
            
            
            if(subscription == True):
                sharejournal = True
            elif(shared == True ):
                sharejournal = True
            else:
                sharejournal = False
            
            #return Response({"status":True, "response_msg": "My Coach list", "coachobj":sharejournal })
        
            if(sharejournal == True):
                journalobj = Journal.objects.get(pk=pk)
                serializer = StudentSharedJournalSerializer(journalobj,data=request.data)
                
                if serializer.is_valid():
                    #coach_idarr = ['2','1'] # coach id avse array ma 
                    coach_idarr = request.POST.get('coach_id')
                    #coach_id = ",".join(coach_idarr)
                    coach_id = coach_idarr
                    
                    serializer.save(coach_id=coach_id)
                    
                    #add gmaification user count
                    journalcount = Journal.objects.filter(Q(student_id=userID), ~Q(coach_id=''))
                    if(journalcount.count() > 0):
                        countsharedjournalentry = journalcount.count()
                        gamification.comman_function.update_user_data(userID,'totalsharedjournal',countsharedjournalentry)
                        
                    #add notification
                    type_of_notification = settings.NOTIFI_TYPE_ARR[6] #SharedJournal
                    main_id = journalobj.id
                    title = journalobj.journal_title
                    
                    coach_idarr = coach_id.split(",")
                    
                    for coach_id in coach_idarr:
                        #check if student enable notification
                        notiobj = Notification(user_id_id=userID,notify_to_id=coach_id,type_of_notification=type_of_notification,main_id=main_id,title=title)
                        notiobj.save()
                        
                        #send pushnotification
                        device = FCMDevice.objects.filter(user=coach_id)
                        message = settings.NOTIFI_TYPE_MESSAGE[6] #shared a new journal entry for you to leave feedback on
                        r = ''
                        
                        userobj = StudentCoach.objects.filter(pk=userID).values('first_name','last_name')
                        notititle = userobj[0]['first_name'] + " " +userobj[0]['last_name']
                        if(device.count() > 0):
                            #r = device.send_message(data={"title": notititle,"body":message})
                            r = device.send_message(title=notititle, body=message,data={"main_id": main_id,"type_of_notification":type_of_notification,"id":notiobj.id})
                    #add notification end

                    return Response({"status":True, "response_msg": "Journal shared successfully", "journal":serializer.data })
                return Response({"status":False, "response_msg": "Data not found", "journal": {} })
            else:
                return Response({"status":False, "response_msg": "You have reached maximum shared journal limit.", "journal": {} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "journal": {} })
        
        
#shared unshared list
class ListMyCoachForStudentJournalSharedUnshared(APIView):
    serializer_class = StudentCoachRequestListSerializer
    def get(self, request,userID,pk, format=None):
        try:
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            
            
            Coachidarr = []
            requestobj = StudentCoachRequest.objects.filter(Q(user=userID), Q(status='Approve') )
            if(requestobj.count() > 0):
                for requestval in requestobj:
                    Coachidarr.append(requestval.user_to_id)
            
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=userID), Q(status='Approve') )
            if(requestobj1.count() > 0):
                for requestval in requestobj1:
                    Coachidarr.append(requestval.user_id)
                    
            
            
        
            if Coachidarr:
                
                coahobj = StudentCoach.objects.filter(pk__in=Coachidarr,account_type='Coach')
                serializer = StudentCoachSerializer(coahobj,many=True)
                
                
                #checked if shared or not
                journalsharedcoacharr = []
                try:
                    journalcheckshared = Journal.objects.get(pk=pk)
                    if(journalcheckshared):
                        if(journalcheckshared.coach_id != ''):
                            journalsharedcoacharr = journalcheckshared.coach_id.split(',')
                except:
                    journalsharedcoacharr = []
                
                
                coacharr = []
                for coachval in serializer.data:
                    coachParlist = {}
                    coachParlist['id'] = coachval['id']
                    coachParlist['first_name'] = coachval['first_name']
                    coachParlist['last_name'] = coachval['last_name']
                    coachParlist['user_name'] = coachval['user_name']
                    coachParlist['account_type'] = coachval['account_type']
                    coachParlist['updated'] = coachval['updated']
                    
                    if(coachval['profile_picture']):
                        #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(coachval.profile_picture)
                        #if os.path.isfile(profile_picture):
                        profile_picture = coachval['profile_picture']
                        #else:
                            #profile_picture = ''
                    elif(coachval['profile_picture2']):
                        profile_picture = coachval['profile_picture2']
                    elif (coachval['hidden_profile_picture'] !='' and coachval['social_login'] == 'True'):
                        profile_picture = coachval['hidden_profile_picture']
                    else:
                        profile_picture = ''
                      
                    
                    coachParlist['shared'] = False
                    if str(coachval['id']) in journalsharedcoacharr:
                        coachParlist['shared'] = True
                        
                        
                    coachParlist['profile_picture'] = profile_picture
                    
                    
                        
                    coacharr.append(coachParlist)
                
                    
                return Response({"status":True, "response_msg": "My Coach list", "user_id":userID,"journal_id":pk, "coachobj":coacharr })
            return Response({"status":False, "response_msg": "Data not found", "user_id":userID, "coachobj": {} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "user_id":userID, "coachobj": {} })


class UnSharedJournalToCoach(APIView):
    serializer_class = StudentSharedJournalSerializer
    def post(self, request,pk, format=None):
        try:
            #check journal entry to share
            postcoach_id = request.POST.get('coach_id')
            if(postcoach_id == ''):
                return Response({"status":False, "response_msg": "Please select the user.", "journal":{} })
            
            journalentry = Journal.objects.get(pk=pk)
            if(journalentry):
                coach_idarr = []
                if(journalentry.coach_id != ''):
                    coach_idarr = journalentry.coach_id.split(',')
                    if postcoach_id in coach_idarr:
                        coach_idarr.remove(postcoach_id)
                        coach_ids = ",".join(coach_idarr)
                        
                        journalentry.coach_id = coach_ids
                        journalentry.save()
                
            return Response({"status":True, "response_msg": "Journal unshared successfully", "journal":{} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "journal": {} })


#feedback to student journal entry add / list
class FeedbackToSharedJournal(APIView):
    serializer_class = StudentSharedJournalFeedbackSerializer
    def get(self, request,pk, format=None):
        try:
            feedbackobj = StudentJournalFeedback.objects.filter(journal_id=pk)
            serializer = StudentSharedJournalFeedbackSerializer(feedbackobj, many=True)
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
            if(feedbackobj):
                
                feedbackarr = []
                for feedbackval in feedbackobj:
                    feedbackParlist = {}
                    feedbackParlist['id'] = feedbackval.id
                    feedbackParlist['journal_id'] = feedbackval.journal_id_id
                    feedbackParlist['feedback'] = feedbackval.feedback
                    feedbackParlist['date'] = feedbackval.date
                    feedbackParlist['journal_feedback_attachments'] = feedbackval.journal_feedback_attachments
                    
                    
                    userobj = StudentCoach.objects.get(pk=feedbackval.user_id_id)
                    serializer = StudentCoachSerializer(userobj)
                    
                    userarr = []
                    userParlist = {}
                    userParlist['id'] = serializer.data['id']
                    userParlist['first_name'] = serializer.data['first_name']
                    userParlist['last_name'] = serializer.data['last_name']
                    userParlist['updated'] = serializer.data['updated']
                    
                    if(serializer.data['profile_picture']):
                        #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(userobj.profile_picture)
                        #if os.path.isfile(profile_picture):
                        profile_picture = serializer.data['profile_picture']
                        #else:
                            #profile_picture = ''
                    elif(serializer.data['profile_picture2']):
                        profile_picture = serializer.data['profile_picture2']
                    elif (serializer.data['hidden_profile_picture'] !='' and serializer.data['social_login'] == 'True'):
                        profile_picture = serializer.data['hidden_profile_picture']
                    else:
                        profile_picture = ''
                    userParlist['profile_picture'] = profile_picture
                    userarr.append(userParlist)
                    
                    feedbackParlist['userobj'] = userarr
                    
                    feedbackarr.append(feedbackParlist)
                return Response({"status":True, "response_msg": "List Feedback", "journalfeedback":feedbackarr })
            return Response({"status":False, "response_msg": "Data not found", "journalfeedback": {} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "journalfeedback": {} })
    def post(self, request,pk, format=None):
        serializer = StudentSharedJournalFeedbackSerializer(data=request.data)
        
        #check if student replay on jounral feedback and get last coach id
        notifyuser_id = ''
        journalfeedbackobj = StudentJournalFeedback.objects.filter(journal_id=pk).order_by('id')
        if(journalfeedbackobj):
            for eachobj in journalfeedbackobj:
                notifyuserID = eachobj.user_id_id
                
                userobj = StudentCoach.objects.get(pk=notifyuserID)
                if(userobj.account_type == 'Coach'):
                    notifyuser_id = userobj.id
                
                
        if serializer.is_valid():
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url

            serializer.save()
            #serializer.save(type_of_user=userobj.account_type)
            
            
            userobj = StudentCoach.objects.get(pk=serializer.data['user_id'])
            serializer2 = StudentCoachSerializer(userobj)
            
            userarr = []
            userParlist = {}
            userParlist['id'] = serializer2.data['id']
            userParlist['first_name'] = serializer2.data['first_name']
            userParlist['last_name'] = serializer2.data['last_name']
            userParlist['user_name'] = serializer2.data['user_name']
            userParlist['account_type'] = serializer2.data['account_type']
            userParlist['updated'] = serializer2.data['updated']
                
            if(serializer2.data['profile_picture']):
                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(userobj.profile_picture)
                #if os.path.isfile(profile_picture):
                profile_picture = serializer2.data['profile_picture']
                #else:
                    #profile_picture = ''
            elif(serializer2.data['profile_picture2']):
                profile_picture = serializer2.data['profile_picture2']
            elif (serializer2.data['hidden_profile_picture'] !='' and serializer2.data['social_login'] == 'True'):
                profile_picture = serializer2.data['hidden_profile_picture']
            else:
                profile_picture = ''
                
            userParlist['profile_picture'] = profile_picture
            userarr.append(userParlist)
            
            Parlist = {}
            feedbackarr = []
            Parlist['id'] = serializer.data['id']
            Parlist['journal_id'] = serializer.data['journal_id']
            Parlist['feedback'] = serializer.data['feedback']
            Parlist['date'] = serializer.data['date']
            Parlist['journal_feedback_attachments'] = serializer.data['journal_feedback_attachments']
            Parlist['userobj'] = userarr 
            feedbackarr.append(Parlist)
            
            #add notification
            
            titleobj = Journal.objects.filter(pk=pk).values('journal_title','student')
            title = titleobj[0]['journal_title']
            notify_to = titleobj[0]['student']
            
            userid = serializer.data['user_id']
            
            
        
            if(userobj.account_type == 'Coach' and userid != notify_to):
                
                #check user settings 
                notificationuserobj = StudentCoach.objects.filter(pk=int(notify_to)).values('noti_jounral_entry')
                noti_jounral_entry = notificationuserobj[0]['noti_jounral_entry']
                if(noti_jounral_entry == '1' and notificationuserobj.count() > 0 ):
                    
                    #count total commented on journal entry only for coach
                    feedbackcoomnetobj = StudentJournalFeedback.objects.filter(Q(user_id_id=userid))
                    if(feedbackcoomnetobj.count() > 0):
                        totalcommentcount = feedbackcoomnetobj.count()
                        gamification.comman_function.update_user_data(userid,'totaljournalfeedbackcomment',totalcommentcount)


                    type_of_notification = settings.NOTIFI_TYPE_ARR[1] #CommentJournal
                    main_id = pk
                    notiobj = Notification(user_id_id=userobj.id,notify_to_id=notify_to,type_of_notification=type_of_notification,main_id=main_id,title=title)
                    notiobj.save()
                
                    #send pushnotification
                    device = FCMDevice.objects.filter(user=notify_to)
                    message = settings.NOTIFI_TYPE_MESSAGE[1] #commented on your journal entry
                    r = ''
                    notititle = userobj.first_name + " " +userobj.last_name
                    if(device.count() > 0):
                        #r = device.send_message(data={"title": notititle,"body":message})
                        r = device.send_message(title=notititle, body=message,sound='default',data={"main_id": main_id,"type_of_notification":type_of_notification,"id":notiobj.id})
                    #add notification end
            
            
            #2018
            elif(userobj.account_type == 'Student' and notifyuser_id != ''):
                #check user last comment on this journal
                
                    #check user type
                    userobj2 = StudentCoach.objects.get(pk=notifyuser_id)
                    serializer3 = StudentCoachSerializer(userobj2)
                    #return Response({"status":serializer3.data['id'], "response_msg": serializer3.data['noti_jounral_entry'], "journalfeedback":serializer3.data['account_type'] })
                
                    if(serializer3.data['account_type'] == 'Coach' and serializer3.data['noti_jounral_entry'] == '1' ):
                        
                        type_of_notification = settings.NOTIFI_TYPE_ARR[9] #CoachReply
                        main_id = pk
                        notiobj = Notification(user_id_id=userobj.id,notify_to_id=notifyuser_id,type_of_notification=type_of_notification,main_id=main_id,title=title)
                        notiobj.save()
                        
                        #send pushnotification
                        device = FCMDevice.objects.filter(user=notifyuser_id)
                        message = settings.NOTIFI_TYPE_MESSAGE[8] #commented on coach feedback
                        r = ''
                        notititle = userobj.first_name + " " +userobj.last_name
                        if(device.count() > 0):
                            #r = device.send_message(data={"title": notititle,"body":message})
                            r = device.send_message(title=notititle, body=message,sound='default',data={"main_id": main_id,"type_of_notification":type_of_notification,"id":notiobj.id})
                            #add notification end
            
            
            return Response({"status":True, "response_msg": "Journal shared successfully", "journalfeedback":feedbackarr  })
        return Response({"status":False, "response_msg": "Data not found", "journalfeedback": {} })
    

class UploadJournalFeedbackAttachment(APIView):
    serializer_class = StudentJournalFeedbackAttachmentSerializer
    def post(self, request, format=None):
        serializer = StudentJournalFeedbackAttachmentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status":True, "response_msg": "Journal feedback attachment upload successfully", "journal_feed_attachment":serializer.data })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)
    


def minusmonths(sourcedate,months):
    month = sourcedate.month + 1 - months
    year = int(sourcedate.year - month / 12 )
    month = month % 12 - 1
    day = min(sourcedate.day,calendar.monthrange(year,month)[1])
    return datetime.date(year,month,day)


#JournalModuleList
class ListJournalModuleList(APIView):
    serializer_class = StudentJournalListSerializer
    def get(self, request,studentID , format=None):
        try:
            #check user pro or free
            userobj = StudentCoach.objects.filter(pk=studentID).values('subscription','subscription_invite','subscription_expiredate')
            if(userobj[0]['subscription'] == True):
                subscription = True
            elif (userobj[0]['subscription_invite'] == True):
                subscription = True
            else:
                subscription = False
                
            '''
            if(subscription == False):
                start_date = datetime.date.today()
                end_date = minusmonths(start_date,6)
                journalobj  = Journal.objects.filter(Q(student_id=studentID),Q(date__range=(end_date, start_date))).order_by('-date')
            else:
                journalobj  = Journal.objects.filter(student_id=studentID).order_by('-date')
            '''
                
            journalobj  = Journal.objects.filter(Q(student_id=studentID), Q(journal_status='Active')).order_by('-date')
            
            paginator   = LimitOffsetPagination()
            result_page = paginator.paginate_queryset(journalobj, request)
            serializer  = StudentJournalListSerializer(result_page, many=True)
            
            journallistarr = []
            for each in serializer.data:
                Parlist = {}
                Parlist['id'] = each['id']
                Parlist['today_fill'] = each['today_fill']
                Parlist['place'] = each['place']
                Parlist['date'] = each['date']
                Parlist['journal_title'] = each['journal_title']
                Parlist['journal_desc'] = each['journal_desc']
                Parlist['journal_attachments'] = each['journal_attachments']
                Parlist['object_type'] = each['object_type']
                Parlist['object_id'] = each['object_id']
                Parlist['journal_status'] = each['journal_status']
                #optionobj = journalentryoption.objects.filter(pk=each['whats_going_on_today']).values('title')
                Parlist['whats_going_on_today'] = each['whats_going_on_today']
                
                
                journallistarr.append(Parlist)
        
            return paginator.get_paginated_response({"response_code":"0", "response_msg": "Journal List", "journallist":journallistarr })
        except:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "journallist": {} })
      
class SearchListJournalModule(APIView):
    serializer_class = StudentJournalListSerializer
    def get(self, request, studentID , format=None):
        try:
            search = request.GET['search']
            
            #check user pro or free
            userobj = StudentCoach.objects.filter(pk=studentID).values('subscription','subscription_invite','subscription_expiredate')
            if(userobj[0]['subscription'] == True):
                subscription = True
            elif (userobj[0]['subscription_invite'] == True):
                subscription = True
            else:
                subscription = False
                
            '''
            if(subscription == False):
                start_date = datetime.date.today()
                end_date = minusmonths(start_date,6)
                journalobj  = Journal.objects.filter(Q(journal_title__icontains=search),Q(student_id=studentID),Q(date__range=(end_date, start_date))).order_by('-date')
            else:
                journalobj = Journal.objects.filter(journal_title__icontains=search,student_id=studentID).order_by('-date')
                
            '''
            
            journalobj = Journal.objects.filter(Q(journal_title__icontains=search),Q(student_id=studentID), Q(journal_status='Active')).order_by('-date')
            
            paginator   = LimitOffsetPagination()
            result_page = paginator.paginate_queryset(journalobj, request)
            serializer = StudentJournalListSerializer(result_page, many=True)
            
            journallistarr = []
            for each in serializer.data:
                Parlist = {}
                Parlist['id'] = each['id']
                Parlist['today_fill'] = each['today_fill']
                Parlist['place'] = each['place']
                Parlist['date'] = each['date']
                Parlist['journal_title'] = each['journal_title']
                Parlist['journal_desc'] = each['journal_desc']
                Parlist['journal_attachments'] = each['journal_attachments']
                Parlist['object_type'] = each['object_type']
                Parlist['object_id'] = each['object_id']
                Parlist['journal_status'] = each['journal_status']
                #optionobj = journalentryoption.objects.filter(pk=each['whats_going_on_today']).values('title')
                Parlist['whats_going_on_today'] = each['whats_going_on_today']
                
                
                journallistarr.append(Parlist)
         
            return paginator.get_paginated_response({"response_code":"0", "response_msg": "Search Journal List", "searchjournallist":journallistarr })
        except:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "searchjournallist": {} })


#journal detail page
class JournalDetailpage(APIView):
    serializer_class = StudentJournalDetailSerializer
    def get(self, request, pk , format=None):
        try:
            journalobj = Journal.objects.get(pk=pk)
            serializer = StudentJournalDetailSerializer(journalobj)
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
                
            journallistarr = []
            Parlist = {}
            Parlist['id'] = journalobj.id
            Parlist['today_fill'] = journalobj.today_fill
            Parlist['place'] = journalobj.place
            Parlist['date'] = journalobj.date
            Parlist['journal_title'] = journalobj.journal_title
            Parlist['journal_desc'] = journalobj.journal_desc
            Parlist['journal_attachments'] = journalobj.journal_attachments
            Parlist['whats_going_on_today'] = journalobj.whats_going_on_today_id
            Parlist['object_type'] = journalobj.object_type
            Parlist['object_id'] = journalobj.object_id
            Parlist['journal_status'] = journalobj.journal_status
            
            userobj = StudentCoach.objects.get(pk=journalobj.student_id)
            serializer2 = StudentCoachSerializer(userobj)
            userarr = []
            userParlist = {}
            userParlist['id'] = serializer2.data['id']
            userParlist['first_name'] = serializer2.data['first_name']
            userParlist['last_name'] = serializer2.data['last_name']
            userParlist['user_name'] = serializer2.data['user_name']
            userParlist['account_type'] = serializer2.data['account_type']
            userParlist['updated'] = serializer2.data['updated']
                
            if(serializer2.data['profile_picture']):
                profile_picture = serializer2.data['profile_picture']
            elif(serializer2.data['profile_picture2']):
                profile_picture = serializer2.data['profile_picture2']
            elif (serializer2.data['hidden_profile_picture'] !='' and serializer2.data['social_login'] == 'True'):
                profile_picture = serializer2.data['hidden_profile_picture']
            else:
                profile_picture = ''
                
            userParlist['profile_picture'] = profile_picture
            userarr.append(userParlist)
            
            Parlist['userobj'] = userarr
                
                
            journallistarr.append(Parlist)
            
            
            return Response({"status":True, "response_msg": "Journal detail page", "journalobj":journallistarr })
        except:
            return Response({"status":False, "response_msg": "Data not found", "journalobj": {} })


#update pusshnotifcation
class UpdatePushNotificationToken(APIView):
    serializer_class = UpdatePushNotificationTokenSerializer
    def post(self, request,userID, format=None):
        try:
            notificationobj = StudentCoach.objects.get(id=userID)
            serializer = UpdatePushNotificationTokenSerializer(notificationobj,data=request.data)
            if serializer.is_valid():
                serializer.save(id=userID)
                return Response({"status":True, "response_msg": "Push notification token update successfully", "token data":serializer.data })
            return Response({"status":False, "response_msg": "Data not found", "token data": {} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "token data": {} })
        
class UpdatePushNotificationTokenv1(APIView):
    serializer_class = UpdatePushNotificationTokenSerializerv1
    def post(self, request,userID, format=None):
        try:
            notificationobj = FCMDevice.objects.get(user_id=userID)
            serializer = UpdatePushNotificationTokenSerializerv1(notificationobj,data=request.data)
            if serializer.is_valid():
                serializer.save()
                return Response({"status":True, "response_msg": "Push notification token update successfully", "token data":serializer.data })
            return Response({"status":False, "response_msg": "Please enter required field", "token data": {} })
        except:
            serializer = UpdatePushNotificationTokenSerializerv1(data=request.data)
            if serializer.is_valid():
                serializer.save(user_id=userID)
                return Response({"status":True, "response_msg": "Push notification token add successfully", "token data":serializer.data })
            return Response({"status":False, "response_msg": "Please enter required field", "token data": {} })
        
        

# Create your views here.
#def GetJournalFront(request, pk):
class GetJournalFront(APIView):
    #if request.method == 'GET':
    serializer_class = StudentJournalSerializerFront
    def get(self, request, pk, format=None):
        try:
            journalobj = Journal.objects.get(pk=int(pk))
            serializer = StudentJournalSerializerFront(journalobj)
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
            journallistarr = []
            Parlist = {}
            Parlist['id'] = journalobj.id
            Parlist['today_fill'] = journalobj.today_fill
            Parlist['place'] = journalobj.place
            Parlist['journal_title'] = journalobj.journal_title
            Parlist['journal_desc'] = journalobj.journal_desc
            Parlist['journal_attachments'] = journalobj.journal_attachments
            
            if(journalobj.whats_going_on_today_id != ''):
                activity = journalentryoption.objects.get(pk=int(journalobj.whats_going_on_today_id))
                Parlist['activity'] = activity.title
            else:
                Parlist['activity'] = ''
            
            userobj = StudentCoach.objects.get(pk=journalobj.student_id)
            serializer2 = StudentCoachSerializer(userobj)
            userarr = []
            userParlist = {}
            Parlist['user_id'] = serializer2.data['id']
            Parlist['first_name'] = serializer2.data['first_name']
            Parlist['last_name'] = serializer2.data['last_name']
            Parlist['user_name'] = serializer2.data['user_name']
            Parlist['account_type'] = serializer2.data['account_type']
            Parlist['updated'] = serializer2.data['updated']
                
            if(serializer2.data['profile_picture']):
                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(userobj.profile_picture)
                #if os.path.isfile(profile_picture):
                profile_picture = serializer2.data['profile_picture']
                #else:
                    #profile_picture = ''
            elif(serializer2.data['profile_picture2']):
                profile_picture = serializer2.data['profile_picture2']
            elif (serializer2.data['hidden_profile_picture'] !='' and serializer2.data['social_login'] == 'True'):
                profile_picture = serializer2.data['hidden_profile_picture']
            else:
                profile_picture = ''
                
            Parlist['profile_picture'] = profile_picture
            #userarr.append(userParlist)
            
            Parlist['userobj'] = userarr
                
                
            journallistarr.append(Parlist)
            
            
            #return Response({'profiles': serializer.data})
            #return HttpResponse(journallistarr,content_type="application/json")
            #return HttpResponse(json.dumps(journallistarr),content_type="application/json")
            return JsonResponse({"status":True, "response_msg": "Journal detail", "journalobj":journallistarr })
        except:
            #return Response({'profiles':{"journal_title":'Data not found..'} })
            return JsonResponse({"status":False, "response_msg": "Data not found", "journalobj": {} })
        
        
        

class GetJournalFrontV1(APIView):
    serializer_class = StudentJournalSerializerFront
    renderer_classes = [TemplateHTMLRenderer]
    template_name = 'journal.html'
    def get(self, request, pk, format=None):
        try:
            journalobj = Journal.objects.get(pk=int(pk))
            serializer = StudentJournalSerializerFront(journalobj)
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
            journallistarr = []
            Parlist = {}
            Parlist['id'] = journalobj.id
            Parlist['today_fill'] = journalobj.today_fill
            Parlist['place'] = journalobj.place
            Parlist['journal_title'] = journalobj.journal_title
            Parlist['journal_desc'] = journalobj.journal_desc
            Parlist['journal_attachments'] = json.loads(journalobj.journal_attachments)
            
            if(journalobj.whats_going_on_today_id != ''):
                activity = journalentryoption.objects.get(pk=int(journalobj.whats_going_on_today_id))
                Parlist['activity'] = activity.title
            else:
                Parlist['activity'] = ''
            
            userobj = StudentCoach.objects.get(pk=journalobj.student_id)
            serializer2 = StudentCoachSerializer(userobj)
            userarr = []
            userParlist = {}
            Parlist['user_id'] = serializer2.data['id']
            Parlist['first_name'] = serializer2.data['first_name']
            Parlist['last_name'] = serializer2.data['last_name']
            Parlist['user_name'] = serializer2.data['user_name']
            Parlist['account_type'] = serializer2.data['account_type']
            Parlist['updated'] = serializer2.data['updated']
                
            if(serializer2.data['profile_picture']):
                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(userobj.profile_picture)
                #if os.path.isfile(profile_picture):
                profile_picture = serializer2.data['profile_picture']
                #else:
                    #profile_picture = ''
            elif(serializer2.data['profile_picture2']):
                profile_picture = serializer2.data['profile_picture2']
            elif (serializer2.data['hidden_profile_picture'] !='' and serializer2.data['social_login'] == 'True'):
                profile_picture = serializer2.data['hidden_profile_picture']
            else:
                profile_picture = ''
                
            Parlist['profile_picture'] = profile_picture
            Parlist['userobj'] = userarr
                
                
            journallistarr.append(Parlist)
            return Response({"status":True, "response_msg": "Journal detail", "journalobj":Parlist })
        except:
            return Response({"status":False, "response_msg": "Data not found", "journalobj": {} })


def RunJson(request):
    return render_to_response('.well-known/assetlinks.json', content_type="application/json")


def RunAppleJson(request):
    return render_to_response('.well-known/apple-app-site-association.txt', content_type="application/json")


def Export_to_csv_goal(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="goal-'+ str(datetime.datetime.today()) +'.csv"'

    writer = csv.writer(response)
    writer.writerow(['id','student', 'goal_type', 'goal_option', 'creation_date','is_completed','completed_date','is_deleted'])

    users = StudentGoal.objects.all().values_list('id','student', 'goal_type', 'goal_option', 'creation_date','is_completed','completed_date','is_deleted')
    for user in users:
        writer.writerow([unicode(s).encode("utf-8") for s in user])

    return response


def Export_to_csv_journal(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="journal-'+ str(datetime.datetime.today()) +'.csv"'
    
    writer = csv.writer(response)
    writer.writerow(["student","today_fill","whats_going_on_today","place","journal_title","journal_desc","coach_id","object_type","object_id","date"])

    users = Journal.objects.all().values_list("student","today_fill","whats_going_on_today","place","journal_title","journal_desc","coach_id","object_type","object_id","date")
    for user in users:
        writer.writerow([unicode(s).encode("utf-8") for s in user])

    return response


def Export_to_csv_journal_feedback(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="journal-feedback-'+ str(datetime.datetime.today()) +'.csv"'
    
    writer = csv.writer(response)
    writer.writerow(["id","journal_id","user_id","type_of_user","feedback","journal_feedback_attachments","date"])

    users = StudentJournalFeedback.objects.all().values_list("id","journal_id","user_id","type_of_user","feedback","journal_feedback_attachments","date")
    for user in users:
        writer.writerow([unicode(s).encode("utf-8") for s in user])

    return response


def Export_to_csv_assignment(request):
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="student-assignment-'+ str(datetime.datetime.today()) +'.csv"'
    
    writer = csv.writer(response)
    writer.writerow(["id","student_id","coach_id","module_id","is_completed"])

    users = StudentAssignment.objects.all().values_list("id","student_id","coach_id","module_id","is_completed")
    for user in users:
        writer.writerow([unicode(s).encode("utf-8") for s in user])

    return response


