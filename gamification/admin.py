# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from gamification.models import Conditions
class ConditionAdmin(admin.ModelAdmin):
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["condition_key"]
        else:
            return self.readonly_fields
admin.site.register(Conditions,ConditionAdmin)

'''
from gamification.models import BadgePoint
class BadgePointAdmin(admin.ModelAdmin):
    class Media:
        js = ("admin/js/gamification.js",)
admin.site.register(BadgePoint,BadgePointAdmin)
'''


from gamification.models import BadgePointNew
class BadgePointAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,          {'fields': ['user_type','task_type','points','badge_title','badge_image']}),
        ('Condition 1', {'fields': ['condition_1','condition1_value','relation1']}),
        ('Condition 2', {'fields': ['condition_2','condition2_value','relation2']}),
        ('Condition 3', {'fields': ['condition_3','condition3_value']}),
    ]
    list_display=["task_type","user_type","condition_1","condition1_value","relation1","condition_2","condition2_value","relation2","condition_3","condition3_value"]
    class Media:
        js = ("admin/js/gamification.js",)
admin.site.register(BadgePointNew,BadgePointAdmin)


from gamification.models import Level
#class LevelAdmin(admin.ModelAdmin):
admin.site.register(Level)

from gamification.models import UserPointBadgeHistory
admin.site.register(UserPointBadgeHistory)


from gamification.models import UserMasterHistory
admin.site.register(UserMasterHistory)

