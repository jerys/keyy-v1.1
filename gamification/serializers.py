from rest_framework import serializers

from general.models import StudentCoach

from gamification.models import Conditions
from gamification.models import BadgePointNew

from students.models import Journal
from notification.models import StudentCoachRequest


#tag 
class BadgePointSerializer(serializers.ModelSerializer):
    class Meta:
        model = BadgePointNew
        fields = '__all__'
        #fields = ('id','tag_title',)
    
    
