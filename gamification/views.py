# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse, Http404
from rest_framework import status
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST
from django.conf import settings
import os
from django.db.models import Count
from django.db.models import Q
import datetime
import calendar

from gamification.models import Conditions
from gamification.models import BadgePointNew


from general.models import StudentCoach
from students.models import StudentGoal

from gamification.serializers import BadgePointSerializer

# Create your views here.

import comman_function

'''
class UserAssignBadge(APIView):
    serializer_class = BadgePointSerializer
    def get(self, request,pk, format=None):
        try:
            conditionobj = BadgePoint.objects.filter()
            for eachcondition in conditionobj:

                condition2 = str(eachcondition.condition_2)

            return Response({"response_code":True, "response_msg": "Codition list" })
        except:
            return Response({"response_code":False, "response_msg": "Data not found", "tag": '' })

'''

def GetUserGoal(id):
    try:
        usergoal = StudentGoal.objects.filter(Q(student_id=id), Q(is_deleted='No') ).count()
        return usergoal
    except:
        return 0


class GetAllBadge(APIView):
    serializer_class = BadgePointSerializer
    def get(self, request, format=None):
        try:
            pk = request.GET.get('pk')
            if(pk):
                badgeobj = BadgePointNew.objects.filter(Q(task_type='Badge'),~Q(pk=pk))
            else:
                badgeobj = BadgePointNew.objects.filter(Q(task_type='Badge'))

            badge_arr = []
            for eachbadge in badgeobj:
                parm = {}
                parm['badge_title'] = eachbadge.badge_title
                parm['id'] = eachbadge.id
                badge_arr.append(parm)
                

            return Response({"response_code":True, "response_msg": "Badge list", "badge":badge_arr })
        except:
            return Response({"response_code":False, "response_msg": "Badge List", "badge":'' })
