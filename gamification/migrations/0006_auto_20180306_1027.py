# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-03-06 15:27
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('gamification', '0005_badgepoint_points'),
    ]

    operations = [
        migrations.CreateModel(
            name='Level',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('level_name', models.CharField(max_length=250, unique=True)),
                ('required_badge', models.SlugField(blank=True, max_length=250, unique=True)),
                ('relation', models.CharField(blank=True, choices=[(b'AND', b'AND'), (b'OR', b'OR')], max_length=250)),
                ('minimum_points', models.CharField(max_length=250, unique=True)),
                ('default_level', models.BooleanField(default=False)),
                ('user_type', models.CharField(choices=[('Student', 'Student'), ('Coach', 'Coach')], max_length=250)),
            ],
            options={
                'verbose_name': 'Levels',
                'verbose_name_plural': 'Levels',
            },
        ),
        migrations.AlterField(
            model_name='badgepoint',
            name='task_type',
            field=models.CharField(choices=[(b'Badge', b'Badge'), (b'Points', b'Points')], max_length=250),
        ),
        migrations.AlterField(
            model_name='badgepoint',
            name='user_type',
            field=models.CharField(choices=[(b'Student', b'Student'), (b'Coach', b'Coach')], max_length=250),
        ),
    ]
