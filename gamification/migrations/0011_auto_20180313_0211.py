# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-03-13 07:11
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gamification', '0010_usermasterhistory'),
    ]

    operations = [
        migrations.RenameField(
            model_name='usermasterhistory',
            old_name='total_journal_feedback',
            new_name='total_journal_feedback_comment',
        ),
    ]
