# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-03-15 10:17
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('gamification', '0013_badgepointnew'),
    ]

    operations = [
        migrations.AddField(
            model_name='badgepointnew',
            name='type_of_condition1',
            field=models.CharField(choices=[('number', 'Number'), ('boolean', 'Boolean'), ('in', 'IN')], default=django.utils.timezone.now, max_length=250),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='badgepointnew',
            name='type_of_condition2',
            field=models.CharField(choices=[('number', 'Number'), ('boolean', 'Boolean'), ('in', 'IN')], default=1, max_length=250),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='badgepointnew',
            name='type_of_condition3',
            field=models.CharField(choices=[('number', 'Number'), ('boolean', 'Boolean'), ('in', 'IN')], default=django.utils.timezone.now, max_length=250),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='badgepointnew',
            name='condition_1',
            field=models.CharField(choices=[('number-of-published-lesson', 'Number of published lesson'), ('number-of-lessons-assigned-to-students', 'Number of lessons assigned to students'), ('number-of-goals-created', 'Number of Goals created'), ('number-of-goals-completed', 'Number of goals completed'), ('number-of-journal-entries-published', 'Number of Journal Entries published'), ('number-of-journal-entry-at-unique-location', 'Number of Journal Entry at unique location'), ('number-of-shared-journal-entries-with-a-coach', 'Number of shared journal entries with a coach'), ('number-of-student-journal-feedback-comments-user-has-created', 'Number of student journal feedback comments user has created'), ('number-of-students-connected-to', 'Number of Students connected to'), ('number-of-coaches-connected-to', 'Number of coaches connected to'), ('is-a-keyypro-member', 'Is a KeyyPRO member'), ('current-number-of-points', 'Current number of points'), ('complete-all-fields-in-profile', 'Complete all fields in profile'), ('number-of-referral-signups', 'Number of referral signups'), ('number-of-days-since-sign-up-date', 'Number of days since sign up date'), ('number-of-times-app-used-during-the-calendar-month', 'Number of times app used during the calendar month'), ('user-has-earned-badges', 'User has earned badges')], max_length=250),
        ),
        migrations.AlterField(
            model_name='badgepointnew',
            name='condition_2',
            field=models.CharField(choices=[('number-of-published-lesson', 'Number of published lesson'), ('number-of-lessons-assigned-to-students', 'Number of lessons assigned to students'), ('number-of-goals-created', 'Number of Goals created'), ('number-of-goals-completed', 'Number of goals completed'), ('number-of-journal-entries-published', 'Number of Journal Entries published'), ('number-of-journal-entry-at-unique-location', 'Number of Journal Entry at unique location'), ('number-of-shared-journal-entries-with-a-coach', 'Number of shared journal entries with a coach'), ('number-of-student-journal-feedback-comments-user-has-created', 'Number of student journal feedback comments user has created'), ('number-of-students-connected-to', 'Number of Students connected to'), ('number-of-coaches-connected-to', 'Number of coaches connected to'), ('is-a-keyypro-member', 'Is a KeyyPRO member'), ('current-number-of-points', 'Current number of points'), ('complete-all-fields-in-profile', 'Complete all fields in profile'), ('number-of-referral-signups', 'Number of referral signups'), ('number-of-days-since-sign-up-date', 'Number of days since sign up date'), ('number-of-times-app-used-during-the-calendar-month', 'Number of times app used during the calendar month'), ('user-has-earned-badges', 'User has earned badges')], max_length=250),
        ),
        migrations.AlterField(
            model_name='badgepointnew',
            name='condition_3',
            field=models.CharField(choices=[('number-of-published-lesson', 'Number of published lesson'), ('number-of-lessons-assigned-to-students', 'Number of lessons assigned to students'), ('number-of-goals-created', 'Number of Goals created'), ('number-of-goals-completed', 'Number of goals completed'), ('number-of-journal-entries-published', 'Number of Journal Entries published'), ('number-of-journal-entry-at-unique-location', 'Number of Journal Entry at unique location'), ('number-of-shared-journal-entries-with-a-coach', 'Number of shared journal entries with a coach'), ('number-of-student-journal-feedback-comments-user-has-created', 'Number of student journal feedback comments user has created'), ('number-of-students-connected-to', 'Number of Students connected to'), ('number-of-coaches-connected-to', 'Number of coaches connected to'), ('is-a-keyypro-member', 'Is a KeyyPRO member'), ('current-number-of-points', 'Current number of points'), ('complete-all-fields-in-profile', 'Complete all fields in profile'), ('number-of-referral-signups', 'Number of referral signups'), ('number-of-days-since-sign-up-date', 'Number of days since sign up date'), ('number-of-times-app-used-during-the-calendar-month', 'Number of times app used during the calendar month'), ('user-has-earned-badges', 'User has earned badges')], max_length=250),
        ),
        migrations.AlterField(
            model_name='badgepointnew',
            name='relation1',
            field=models.CharField(choices=[(b'and', b'AND'), (b'or', b'OR')], max_length=250),
        ),
        migrations.AlterField(
            model_name='badgepointnew',
            name='relation2',
            field=models.CharField(choices=[(b'and', b'AND'), (b'or', b'OR')], max_length=250),
        ),
    ]
