from general.models import StudentCoach
from gamification.models import UserMasterHistory
from gamification.models import BadgePoint
from gamification.models import BadgePointNew
from gamification.models import UserPointBadgeHistory

from gamification.models import Conditions
from notification.models import StudentCoachRequest

from django.db.models import Count
from django.db.models import Q

from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse, Http404
from rest_framework import status
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

def update_user_data(pk,parameter='',parameter2=False):

    userobj = StudentCoach.objects.filter(pk=pk).values('account_type')
    user_type = userobj[0]['account_type']

    try:
        usermaster = UserMasterHistory.objects.get(Q(user_id=pk))
        usermaster.user_type = user_type

        #goal add
        if(parameter == 'goaladd' and parameter2 != ''):
            usermaster.total_goal_created = parameter2
            usermaster.save()

            AssignUserBadgePoints(pk,'number-of-goals-created')
        '''
        elif(parameter == 'goaladd' and parameter2 == False):
            if(usermaster.total_goal_created == ''):
                totalgoalcreated = '0'
            else:
                totalgoalcreated = int(usermaster.total_goal_created) - 1
            
            usermaster.total_goal_created = totalgoalcreated
            usermaster.save()
        '''
        
        #goal completed
        if(parameter == 'goalcomplete' and parameter2 != ''):
            usermaster.total_goal_completed = parameter2
            usermaster.save()
            AssignUserBadgePoints(pk,'number-of-goals-completed')

        '''
        elif(parameter == 'goalcomplete' and parameter2 == False):
            if(usermaster.total_goal_completed == ''):
                totalgoalcompleted = '0'
            else:
                totalgoalcompleted = int(usermaster.total_goal_completed) - 1
            
            usermaster.total_goal_completed = totalgoalcompleted
            usermaster.save()
        '''

        #Journal entry publish
        if(parameter == 'journalentrypublished' and parameter2 != ''):
            usermaster.total_journal_publish = parameter2
            usermaster.save()
            AssignUserBadgePoints(pk,'number-of-journal-entries-published')
        
        '''
        if(parameter == 'journalentrypublished' and parameter2 == False):
            if(usermaster.total_journal_publish == ''):
                totalgoalcompleted = '1'
            else:
                totalgoalcompleted = int(usermaster.total_journal_publish) - 1
            
            usermaster.total_journal_publish = totalgoalcompleted
            usermaster.save()
        '''
        
        #Journal entry publish at unique location
        if(parameter == 'journalentrypublisheduniquelocation' and parameter2 != ''):
            usermaster.total_journal_unique_location= parameter2
            usermaster.save()
            AssignUserBadgePoints(pk,'number-of-journal-entry-at-unique-location')

        #publish lession
        if(parameter == 'publishlession' and parameter2 == True):
            if(usermaster.total_publish_lession == ''):
                totalpublishlession = '1'
            else:
                totalpublishlession = int(usermaster.total_publish_lession) + 1
            
            usermaster.total_publish_lession = totalpublishlession
            usermaster.save()
        elif(parameter == 'publishlession' and parameter2 == False):
            if(usermaster.total_publish_lession == ''):
                totalpublishlession = '0'
            else:
                totalpublishlession = int(usermaster.total_publish_lession) - 1
            
            usermaster.total_publish_lession = totalpublishlession
            usermaster.save()

        #Complete All field in profile
        if(parameter == 'completeallfieldprofile' and parameter2 == True):
            completeallfieldprofile = '1'
            usermaster.complete_allfield_profile = completeallfieldprofile
            usermaster.save()

        #add total referal user signup
        if(parameter == 'totalreferralsignup' and parameter2 != ''):
            usermaster.total_referral_signup = parameter2
            usermaster.save()

        #is key pro member
        if(parameter == 'iskeypro' and parameter2 == True):
            usermaster.is_key_pro = 1
            usermaster.save()
        elif(parameter == 'iskeypro' and parameter2 == False):
            usermaster.is_key_pro = 0
            usermaster.save()

        #Number of lessons assigned to students 
        if(parameter == 'totalassignlession' and parameter2 != ''):
            usermaster.total_assign_lession = parameter2
            usermaster.save()

        #Number of shared journal entries with a coach
        if(parameter == 'totalsharedjournal' and parameter2 != ''):
            usermaster.total_shared_journal = parameter2
            usermaster.save()

        #Number of Students connected to
        if(parameter == 'totalcoachstudentconnect' and parameter2 == 'Student'):
            requestobj = StudentCoachRequest.objects.filter(Q(user=pk), Q(status='Approve'), Q(temp_status='C') )
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=pk), Q(status='Approve'), ~Q(temp_status='C') )
            totalcoachcount = 0
            if(requestobj.count() > 0 ):
                totalcoachcount = totalcoachcount + int(requestobj.count())
            if(requestobj1.count() > 0 ):
                totalcoachcount = totalcoachcount + int(requestobj1.count())

            usermaster.total_coach_connect = totalcoachcount
            usermaster.save()

        #Number of coaches connected to
        if(parameter == 'totalcoachstudentconnect' and parameter2 == 'Coach'):
            requestobj = StudentCoachRequest.objects.filter(Q(user=pk), Q(status='Approve'), Q(temp_status='S') )
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=pk), Q(status='Approve'), Q(request_to='Coach') )
            totalstudentcount = 0
            if(requestobj.count() > 0 ):
                totalstudentcount = totalstudentcount + int(requestobj.count())
            if(requestobj1.count() > 0 ):
                totalstudentcount = totalstudentcount + int(requestobj1.count())

            usermaster.total_student_connect = totalstudentcount
            usermaster.save()
        
        #Number of student journal feedback comments user has created
        if(parameter == 'totaljournalfeedbackcomment' and parameter2 != ''):
            usermaster.total_journal_feedback_comment = parameter2
            usermaster.save()


        #Number of days since sign up date
        if(parameter == 'numberofdaysincesignup' and parameter2 != ''):
            usermaster.number_of_day_since_signup = parameter2
            usermaster.save()
        
        #Number of times app used
        if(parameter == 'numberoftimesappopen' and parameter2 != ''):
            usermaster.number_of_times_app_open = parameter2
            usermaster.save()

        
        #User has has earned #Current number of points
        if(parameter == 'Userhashasearned' and parameter2 == True):
            #get all badge
            badgeobj = UserPointBadgeHistory.objects.filter(Q(user_id=int(pk)))
            if(badgeobj.count() > 0):
                badgearr = []
                earnbadgeid = ''
                totalpoints = 0
                for eachbadge in badgeobj:
                    badgearr.append(eachbadge.badge_id)
                    earnbadgeid = ",".join(badgearr)

                    if(eachbadge.points != ''):
                        totalpoints = totalpoints + int(eachbadge.points)
                
                usermaster.total_earn_badge = earnbadgeid
                usermaster.current_no_point = totalpoints
                usermaster.save()
            
        
        '''
        if(parameter == 'Currentnumberofpoints' and parameter2 == True):
            #get all badge
            badgeobj = UserPointBadgeHistory.objects.filter(Q(user_id=int(pk)))
            if(badgeobj.count() > 0):
                totalpoints = 0
                for eachbadge in badgeobj:
                    totalpoints = totalpoints + int(eachbadge.points)
                    
                usermaster.current_no_point = totalpoints
                usermaster.save()
            
        '''


    except:
        #goal add
        if(parameter == 'goaladd' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_goal_created=parameter2,user_id=pk)
            usermaster.save()
            AssignUserBadgePoints(pk,'number-of-goals-created')
        
        '''
        elif(parameter == 'goaladd' and parameter2 == False):
            totalgoalcreated = 0
            usermaster = UserMasterHistory(user_type=user_type,total_goal_created=totalgoalcreated,user_id=pk)
            usermaster.save()
        '''
        
        #goal completed
        if(parameter == 'goalcomplete' and parameter2 != ''):
            
            usermaster = UserMasterHistory(user_type=user_type,total_goal_completed=parameter2,user_id=pk)
            usermaster.save()
            AssignUserBadgePoints(pk,'number-of-goals-completed')
        '''
        elif(parameter == 'goalcomplete' and parameter2 == False):
            totalgoalcompleted = '0'
            usermaster = UserMasterHistory(user_type=user_type,total_goal_completed=totalgoalcompleted,user_id=pk)
            usermaster.save()
        '''

        #Journal entry publish
        if(parameter == 'journalentrypublished' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_journal_publish=parameter2,user_id=pk)
            usermaster.save()
            AssignUserBadgePoints(pk,'number-of-journal-entries-published')
        '''
        elif(parameter == 'journalentrypublished' and parameter2 == False):
            totaljournalpublish = '0'
            usermaster = UserMasterHistory(user_type=user_type,total_journal_publish=totaljournalpublish,user_id=pk)
            usermaster.save()
        '''

        #Journal entry publish at unique location
        if(parameter == 'journalentrypublisheduniquelocation' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_journal_unique_location=parameter2,user_id=pk)
            usermaster.save()
            AssignUserBadgePoints(pk,'number-of-journal-entry-at-unique-location')
        
        
        #publish lession
        if(parameter == 'publishlession' and parameter2 == True):
            totalpublishlession = '1'
            usermaster = UserMasterHistory(user_type=user_type,total_publish_lession=totalpublishlession,user_id=pk)
            usermaster.save()
        elif(parameter == 'publishlession' and parameter2 == False):
            totalpublishlession = '0'
            usermaster = UserMasterHistory(user_type=user_type,total_publish_lession=totalpublishlession,user_id=pk)
            usermaster.save()

        #Complete All field in profile
        if(parameter == 'completeallfieldprofile' and parameter2 == True):
            completeallfieldprofile = '1'
            usermaster = UserMasterHistory(user_type=user_type,complete_allfield_profile=completeallfieldprofile,user_id=pk)
            usermaster.save()
        
        #add total referal user signup
        if(parameter == 'totalreferralsignup' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_referral_signup=parameter2,user_id=pk)
            usermaster.save()

        #is key pro member
        if(parameter == 'iskeypro' and parameter2 == True):
            usermaster = UserMasterHistory(user_type=user_type,is_key_pro='1',user_id=pk)
            usermaster.save()
        elif(parameter == 'iskeypro' and parameter2 == False):
            usermaster = UserMasterHistory(user_type=user_type,is_key_pro='0',user_id=pk)
            usermaster.save()
        
        #Number of lessons assigned to students 
        if(parameter == 'totalassignlession' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_assign_lession=parameter2,user_id=pk)
            usermaster.save()

        #Number of shared journal entries with a coach
        if(parameter == 'totalsharedjournal' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_shared_journal=parameter2,user_id=pk)
            usermaster.save()

        
        #Number of Students connected to
        if(parameter == 'totalcoachstudentconnect' and parameter2 == 'Student'):
            requestobj = StudentCoachRequest.objects.filter(Q(user=pk), Q(status='Approve'), Q(temp_status='C') )
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=pk), Q(status='Approve'), ~Q(temp_status='C') )
            totalcoachcount = 0
            if(requestobj.count() > 0 ):
                totalcoachcount = totalcoachcount + int(requestobj.count())
            if(requestobj1.count() > 0 ):
                totalcoachcount = totalcoachcount + int(requestobj1.count())

            usermaster = UserMasterHistory(user_type=user_type,total_coach_connect=totalcoachcount,user_id=pk)
            usermaster.save()

        #Number of coaches connected to
        if(parameter == 'totalcoachstudentconnect' and parameter2 == 'Coach'):
            requestobj = StudentCoachRequest.objects.filter(Q(user=pk), Q(status='Approve'), Q(temp_status='S') )
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=pk), Q(status='Approve'), Q(request_to='Coach') )
            totalstudentcount = 0
            if(requestobj.count() > 0 ):
                totalstudentcount = totalstudentcount + int(requestobj.count())
            if(requestobj1.count() > 0 ):
                totalstudentcount = totalstudentcount + int(requestobj1.count())

            usermaster = UserMasterHistory(user_type=user_type,total_student_connect=totalstudentcount,user_id=pk)
            usermaster.save()
        
        #Number of student journal feedback comments user has created
        if(parameter == 'totaljournalfeedbackcomment' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_journal_feedback_comment=parameter2,user_id=pk)
            usermaster.save()
            
        #Number of days since sign up date
        if(parameter == 'numberofdaysincesignup' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,number_of_day_since_signup=parameter2,user_id=pk)
            usermaster.save()
        
        #Number of times app used
        if(parameter == 'numberoftimesappopen' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,number_of_times_app_open=parameter2,user_id=pk)
            usermaster.save()

        #User has has earned
        if(parameter == 'Userhashasearned' and parameter2 == True):
            #get all badge
            badgeobj = UserPointBadgeHistory.objects.filter(Q(user_id=int(pk)))
            if(badgeobj.count() > 0):
                badgearr = []
                earnbadgeid = ''
                totalpoints = 0
                for eachbadge in badgeobj:
                    badgearr.append(eachbadge.badge_id)
                    earnbadgeid = ",".join(badgearr)

                    if(eachbadge.points != ''):
                        totalpoints = totalpoints + int(eachbadge.points)
                
                usermaster = UserMasterHistory(user_type=user_type,total_earn_badge=earnbadgeid,current_no_point=totalpoints,user_id=pk)
                usermaster.save()



        ''' 
        #Current number of points
        if(parameter == 'Currentnumberofpoints' and parameter2 == True):
            #get all badge
            badgeobj = UserPointBadgeHistory.objects.filter(Q(user_id=int(pk)))
            if(badgeobj.count() > 0):
                totalpoints = 0
                for eachbadge in badgeobj:
                    dbpoint = 0
                    if(eachbadge.points !=''):
                        dbpoint = int(eachbadge.points)

                    totalpoints = totalpoints + dbpoint
                    
                usermaster = UserMasterHistory(user_type=user_type,current_no_point=totalpoints,user_id=pk)
                usermaster.save()
        '''

    
    
    return True

#@api_view()
#def AssignUserBadgePoints(request, pk,param):
def AssignUserBadgePoints(pk,param):
    usermasterobj = UserMasterHistory.objects.get(Q(user_id=pk))

    user_type = usermasterobj.user_type
    
    #"SELECT * FROM gamification_badgepointnew WHERE condition_1 = 'number-of-goals-created' or condition_2 = 'number-of-goals-created' or condition_3 ='number-of-goals-created' "
    badgepointnewobj = BadgePointNew.objects.filter(Q(condition_1=param)|Q(condition_2=param)|Q(condition_3=param))
    #badgepointnewobj = BadgePointNew.objects.filter(Q(condition_1='number-of-goals-created')|Q(condition_2='number-of-goals-created')|Q(condition_3='number-of-goals-created'))

    #return Response({"message": "Hello, world!","data":badgepointnewobj.count() })

    if(badgepointnewobj.count() > 0):

        idarr = []
        testidarr = []
        for eachbadge in badgepointnewobj:

            #return Response({"message": "Hello, world!","data":eachbadge.id })

            condition_1 = eachbadge.condition_1
            condition_2 = eachbadge.condition_2
            condition_3 = eachbadge.condition_3

            condition1_value = eachbadge.condition1_value
            condition2_value  = eachbadge.condition2_value
            condition3_value = eachbadge.condition3_value

            relation1 = eachbadge.relation1
            relation2  = eachbadge.relation2

            type_of_condition1 = eachbadge.type_of_condition1
            type_of_condition2 = eachbadge.type_of_condition2
            type_of_condition3 = eachbadge.type_of_condition3
            
            
            #"select GROUP_CONCAT(master_user_colmn) from gamification_conditions where condition_key = 'number-of-goals-created' or condition_key = 'number-of-journal-entries-published' or condition_key = 'number-of-goals-completed' "
            #total_goal_created,total_publish_lession,total_goal_published

            condition1_value_real = 0
            condition2_value_real = 0
            condition3_value_real = 0

            query_set1 = Conditions.objects.filter(Q(condition_key=condition_1)).values('masteruserfieldcolumn')
            condition1_value_real_v = getKey(usermasterobj,query_set1[0]['masteruserfieldcolumn'])
            if(condition1_value_real_v != ''):
                condition1_value_real = condition1_value_real_v

            query_set2 = Conditions.objects.filter(Q(condition_key=condition_2)).values('masteruserfieldcolumn')
            condition2_value_real_v = getKey(usermasterobj,query_set2[0]['masteruserfieldcolumn'])
            if(condition2_value_real_v != ''):
                condition2_value_real = condition2_value_real_v

            query_set3 = Conditions.objects.filter(Q(condition_key=condition_3)).values('masteruserfieldcolumn')
            condition3_value_real_v = getKey(usermasterobj,query_set3[0]['masteruserfieldcolumn'])
            if(condition3_value_real_v != ''):
                condition3_value_real = condition3_value_real_v


            Boolean1 =True
            Boolean2 =True
            Boolean3 =True
            Boolean4 =True

            if(type_of_condition1 == 'number'):
                Boolean1 = int(condition1_value) <= int(condition1_value_real) # 2<=2 True
            elif(type_of_condition1 == 'boolean'):
                Boolean1 = int(condition1_value) == int(condition1_value_real) # 2<=2 True
            elif(type_of_condition1 == 'in'):
                #Boolean1 = int(condition1_value) == int(condition1_value_real) # 2<=2 True
                condition1_value_arr = condition1_value.split(",")
                condition1_value_real_arr = condition1_value_real.split(",")
                Boolean1 = False
                for eachcondition1_value in condition1_value_arr:
                    if eachcondition1_value in condition1_value_real_arr:
                        Boolean1 = True
                    else:
                        Boolean1 = False

            
            if(type_of_condition2 == 'number'):
                Boolean2 = int(condition2_value) <= int(condition2_value_real) #2<=2 True
            elif(type_of_condition2 == 'boolean'):
                Boolean2 = int(condition2_value) == int(condition2_value_real) #2<=2 True
            elif(type_of_condition2 == 'in'):
                #Boolean2 = int(condition2_value) == int(condition2_value_real) #2<=2 True
                condition2_value_arr = condition2_value.split(",")
                condition2_value_real_arr = condition2_value_real.split(",")
                Boolean2 = False
                for eachcondition2_value in condition2_value_arr:
                    if eachcondition2_value in condition2_value_real_arr:
                        Boolean2 = True
                    else:
                        Boolean2 = False

            if(type_of_condition3 == 'number'): 
                Boolean3 = int(condition3_value) <= int(condition3_value_real) # 2<=1 False
            elif(type_of_condition3 == 'boolean'): 
                Boolean3 = int(condition3_value) == int(condition3_value_real) # 2<=1 False
            elif(type_of_condition3 == 'in'):
                condition3_value_arr = condition3_value.split(",")
                condition3_value_real_arr = condition3_value_real.split(",")
                Boolean3 = False
                for eachcondition3_value in condition3_value_arr:
                    if eachcondition3_value in condition3_value_real_arr:
                        Boolean3 = True
                    else:
                        Boolean3 = False

                #Boolean3 = int(condition3_value) == int(condition3_value_real) # 2<=1 False
            
            '''
            param1 = {}
            param1['bid'] = Boolean3
            testidarr.append(param1)
            '''
            
            if(relation1 == 'and'):
                Boolean4 = Boolean1 and Boolean2
            elif(relation1 == 'or'):
                Boolean4 = Boolean1 or Boolean2

            if(relation2 == 'and'):
                Boolean4 = Boolean4 and Boolean3
            elif(relation2 == 'or'):
                Boolean4 = Boolean4 or Boolean3

            
            checkexitsingbadgeobj = UserPointBadgeHistory.objects.filter(Q(badge_id=int(eachbadge.id)))
            if(Boolean4 == True):
                #check if allready assign this badge or not
                
                if(checkexitsingbadgeobj.count() == 0):
                    points = ''
                    if(eachbadge.task_type =='Points'):
                        points = eachbadge.points
                    
                    #assign this badge
                    badgeobj = UserPointBadgeHistory(badge_id=eachbadge.id,user_id=pk,points=points)
                    badgeobj.save()
                    update_user_data(pk,'Userhashasearned',True)

                    #update_user_data(pk,'Currentnumberofpoints',True)
                
                    
                    param = {}
                    param['badge_id'] =str(eachbadge.id)
                    param['points'] =points
                    idarr.append(param)
            
            elif(Boolean4 == False):
                if(checkexitsingbadgeobj.count() > 0):
                    checkexitsingbadgeobj.delete()

                

    return True
    #return Response({"message": "Hello, world!","data":idarr,"testidarr":badgepointnewobj.count() })
    

def getKey(obj,key):

    if(key == 'total_goal_created'):
        return obj.total_goal_created
    elif(key == 'total_publish_lession'):
        return obj.total_publish_lession
    elif(key == 'total_goal_completed'):
        return obj.total_goal_completed
    elif(key == 'total_assign_lession'):
        return obj.total_assign_lession
    elif(key == 'total_journal_publish'):
        return obj.total_journal_publish
    elif(key == 'total_journal_unique_location'):
        return obj.total_journal_unique_location
    elif(key == 'total_student_connect'):
        return obj.total_student_connect
    elif(key == 'total_coach_connect'):
        return obj.total_coach_connect
    elif(key == 'total_journal_feedback_comment'):
        return obj.total_journal_feedback_comment
    elif(key == 'total_shared_journal'):
        return obj.total_shared_journal
    elif(key == 'is_key_pro'):
        return obj.is_key_pro
    elif(key == 'current_no_point'):
        return obj.current_no_point
    elif(key == 'complete_allfield_profile'):
        return obj.complete_allfield_profile
    elif(key == 'total_referral_signup'):
        return obj.total_referral_signup
    elif(key == 'number_of_day_since_signup'):
        return obj.number_of_day_since_signup
    elif(key == 'number_of_times_app_open'):
        return obj.number_of_times_app_open
    elif(key == 'total_earn_badge'):
        return obj.total_earn_badge
    else:
        return ''
        