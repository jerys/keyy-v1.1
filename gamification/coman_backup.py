from general.models import StudentCoach
from gamification.models import UserMasterHistory
from gamification.models import BadgePoint
from gamification.models import BadgePointNew

from gamification.models import Conditions
from notification.models import StudentCoachRequest

from django.db.models import Q

from rest_framework.views import APIView
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse, Http404
from rest_framework import status
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

def update_user_data(pk,parameter='',parameter2=False):

    userobj = StudentCoach.objects.filter(pk=pk).values('account_type')
    user_type = userobj[0]['account_type']

    try:
        usermaster = UserMasterHistory.objects.get(Q(user_id=pk))
        usermaster.user_type = user_type

        #goal add
        if(parameter == 'goaladd' and parameter2 == True):
            if(usermaster.total_goal_created == ''):
                totalgoalcreated = '1'
            else:
                totalgoalcreated = int(usermaster.total_goal_created) + 1
            
            usermaster.total_goal_created = totalgoalcreated
            usermaster.save()
        elif(parameter == 'goaladd' and parameter2 == False):
            if(usermaster.total_goal_created == ''):
                totalgoalcreated = '0'
            else:
                totalgoalcreated = int(usermaster.total_goal_created) - 1
            
            usermaster.total_goal_created = totalgoalcreated
            usermaster.save()
        
        #goal completed
        if(parameter == 'goalcomplete' and parameter2 == True):
            if(usermaster.total_goal_completed == ''):
                totalgoalcompleted = '1'
            else:
                totalgoalcompleted = int(usermaster.total_goal_completed) + 1
            
            usermaster.total_goal_completed = totalgoalcompleted
            usermaster.save()

        elif(parameter == 'goalcomplete' and parameter2 == False):
            if(usermaster.total_goal_completed == ''):
                totalgoalcompleted = '0'
            else:
                totalgoalcompleted = int(usermaster.total_goal_completed) - 1
            
            usermaster.total_goal_completed = totalgoalcompleted
            usermaster.save()

        #Journal entry publish
        if(parameter == 'journalentrypublished' and parameter2 == True):
            if(usermaster.total_journal_publish == ''):
                totalgoalcompleted = '1'
            else:
                totalgoalcompleted = int(usermaster.total_journal_publish) + 1
            
            usermaster.total_journal_publish = totalgoalcompleted
            usermaster.save()
        
        if(parameter == 'journalentrypublished' and parameter2 == False):
            if(usermaster.total_journal_publish == ''):
                totalgoalcompleted = '1'
            else:
                totalgoalcompleted = int(usermaster.total_journal_publish) - 1
            
            usermaster.total_journal_publish = totalgoalcompleted
            usermaster.save()
        
        #Journal entry publish at unique location
        if(parameter == 'journalentrypublisheduniquelocation' and parameter2 != ''):
            usermaster.total_journal_unique_location= parameter2
            usermaster.save()

        #publish lession
        if(parameter == 'publishlession' and parameter2 == True):
            if(usermaster.total_publish_lession == ''):
                totalpublishlession = '1'
            else:
                totalpublishlession = int(usermaster.total_publish_lession) + 1
            
            usermaster.total_publish_lession = totalpublishlession
            usermaster.save()
        elif(parameter == 'publishlession' and parameter2 == False):
            if(usermaster.total_publish_lession == ''):
                totalpublishlession = '0'
            else:
                totalpublishlession = int(usermaster.total_publish_lession) - 1
            
            usermaster.total_publish_lession = totalpublishlession
            usermaster.save()

        #Complete All field in profile
        if(parameter == 'completeallfieldprofile' and parameter2 == True):
            completeallfieldprofile = '1'
            usermaster.complete_allfield_profile = completeallfieldprofile
            usermaster.save()

        #add total referal user signup
        if(parameter == 'totalreferralsignup' and parameter2 != ''):
            usermaster.total_referral_signup = parameter2
            usermaster.save()

        #is key pro member
        if(parameter == 'iskeypro' and parameter2 == True):
            usermaster.is_key_pro = 1
            usermaster.save()
        elif(parameter == 'iskeypro' and parameter2 == False):
            usermaster.is_key_pro = 0
            usermaster.save()

        #Number of lessons assigned to students 
        if(parameter == 'totalassignlession' and parameter2 != ''):
            usermaster.total_assign_lession = parameter2
            usermaster.save()

        #Number of shared journal entries with a coach
        if(parameter == 'totalsharedjournal' and parameter2 != ''):
            usermaster.total_shared_journal = parameter2
            usermaster.save()

        #Number of Students connected to
        if(parameter == 'totalcoachstudentconnect' and parameter2 == 'Student'):
            requestobj = StudentCoachRequest.objects.filter(Q(user=pk), Q(status='Approve'), Q(temp_status='C') )
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=pk), Q(status='Approve'), ~Q(temp_status='C') )
            totalcoachcount = 0
            if(requestobj.count() > 0 ):
                totalcoachcount = totalcoachcount + int(requestobj.count())
            if(requestobj1.count() > 0 ):
                totalcoachcount = totalcoachcount + int(requestobj1.count())

            usermaster.total_coach_connect = totalcoachcount
            usermaster.save()

        #Number of coaches connected to
        if(parameter == 'totalcoachstudentconnect' and parameter2 == 'Coach'):
            requestobj = StudentCoachRequest.objects.filter(Q(user=pk), Q(status='Approve'), Q(temp_status='S') )
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=pk), Q(status='Approve'), Q(request_to='Coach') )
            totalstudentcount = 0
            if(requestobj.count() > 0 ):
                totalstudentcount = totalstudentcount + int(requestobj.count())
            if(requestobj1.count() > 0 ):
                totalstudentcount = totalstudentcount + int(requestobj1.count())

            usermaster.total_student_connect = totalstudentcount
            usermaster.save()
        
        #Number of student journal feedback comments user has created
        if(parameter == 'totaljournalfeedbackcomment' and parameter2 != ''):
            usermaster.total_journal_feedback_comment = parameter2
            usermaster.save()


        #Number of days since sign up date
        if(parameter == 'numberofdaysincesignup' and parameter2 != ''):
            usermaster.number_of_day_since_signup = parameter2
            usermaster.save()
        
        #Number of times app used
        if(parameter == 'numberoftimesappopen' and parameter2 != ''):
            usermaster.number_of_times_app_open = parameter2
            usermaster.save()
            


    except:
        #goal add
        if(parameter == 'goaladd' and parameter2 == True):
            totalgoalcreated = 1
            usermaster = UserMasterHistory(user_type=user_type,total_goal_created=totalgoalcreated,user_id=pk)
            usermaster.save()
        
        elif(parameter == 'goaladd' and parameter2 == False):
            totalgoalcreated = 0
            usermaster = UserMasterHistory(user_type=user_type,total_goal_created=totalgoalcreated,user_id=pk)
            usermaster.save()
        
        #goal completed
        if(parameter == 'goalcomplete' and parameter2 == True):
            totalgoalcompleted = '1'
            usermaster = UserMasterHistory(user_type=user_type,total_goal_completed=totalgoalcompleted,user_id=pk)
            usermaster.save()
        elif(parameter == 'goalcomplete' and parameter2 == False):
            totalgoalcompleted = '0'
            usermaster = UserMasterHistory(user_type=user_type,total_goal_completed=totalgoalcompleted,user_id=pk)
            usermaster.save()

        #Journal entry publish
        if(parameter == 'journalentrypublished' and parameter2 == True):
            totaljournalpublish = '1'
            usermaster = UserMasterHistory(user_type=user_type,total_journal_publish=totaljournalpublish,user_id=pk)
            usermaster.save()
        elif(parameter == 'journalentrypublished' and parameter2 == False):
            totaljournalpublish = '0'
            usermaster = UserMasterHistory(user_type=user_type,total_journal_publish=totaljournalpublish,user_id=pk)
            usermaster.save()

        #Journal entry publish at unique location
        if(parameter == 'journalentrypublisheduniquelocation' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_journal_unique_location=parameter2,user_id=pk)
            usermaster.save()
        
        
        #publish lession
        if(parameter == 'publishlession' and parameter2 == True):
            totalpublishlession = '1'
            usermaster = UserMasterHistory(user_type=user_type,total_publish_lession=totalpublishlession,user_id=pk)
            usermaster.save()
        elif(parameter == 'publishlession' and parameter2 == False):
            totalpublishlession = '0'
            usermaster = UserMasterHistory(user_type=user_type,total_publish_lession=totalpublishlession,user_id=pk)
            usermaster.save()

        #Complete All field in profile
        if(parameter == 'completeallfieldprofile' and parameter2 == True):
            completeallfieldprofile = '1'
            usermaster = UserMasterHistory(user_type=user_type,complete_allfield_profile=completeallfieldprofile,user_id=pk)
            usermaster.save()
        
        #add total referal user signup
        if(parameter == 'totalreferralsignup' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_referral_signup=parameter2,user_id=pk)
            usermaster.save()

        #is key pro member
        if(parameter == 'iskeypro' and parameter2 == True):
            usermaster = UserMasterHistory(user_type=user_type,is_key_pro='1',user_id=pk)
            usermaster.save()
        elif(parameter == 'iskeypro' and parameter2 == False):
            usermaster = UserMasterHistory(user_type=user_type,is_key_pro='0',user_id=pk)
            usermaster.save()
        
        #Number of lessons assigned to students 
        if(parameter == 'totalassignlession' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_assign_lession=parameter2,user_id=pk)
            usermaster.save()

        #Number of shared journal entries with a coach
        if(parameter == 'totalsharedjournal' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_shared_journal=parameter2,user_id=pk)
            usermaster.save()

        
        #Number of Students connected to
        if(parameter == 'totalcoachstudentconnect' and parameter2 == 'Student'):
            requestobj = StudentCoachRequest.objects.filter(Q(user=pk), Q(status='Approve'), Q(temp_status='C') )
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=pk), Q(status='Approve'), ~Q(temp_status='C') )
            totalcoachcount = 0
            if(requestobj.count() > 0 ):
                totalcoachcount = totalcoachcount + int(requestobj.count())
            if(requestobj1.count() > 0 ):
                totalcoachcount = totalcoachcount + int(requestobj1.count())

            usermaster = UserMasterHistory(user_type=user_type,total_coach_connect=totalcoachcount,user_id=pk)
            usermaster.save()

        #Number of coaches connected to
        if(parameter == 'totalcoachstudentconnect' and parameter2 == 'Coach'):
            requestobj = StudentCoachRequest.objects.filter(Q(user=pk), Q(status='Approve'), Q(temp_status='S') )
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=pk), Q(status='Approve'), Q(request_to='Coach') )
            totalstudentcount = 0
            if(requestobj.count() > 0 ):
                totalstudentcount = totalstudentcount + int(requestobj.count())
            if(requestobj1.count() > 0 ):
                totalstudentcount = totalstudentcount + int(requestobj1.count())

            usermaster = UserMasterHistory(user_type=user_type,total_student_connect=totalstudentcount,user_id=pk)
            usermaster.save()
        
        #Number of student journal feedback comments user has created
        if(parameter == 'totaljournalfeedbackcomment' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,total_journal_feedback_comment=parameter2,user_id=pk)
            usermaster.save()
            
        #Number of days since sign up date
        if(parameter == 'numberofdaysincesignup' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,number_of_day_since_signup=parameter2,user_id=pk)
            usermaster.save()
        
        #Number of times app used
        if(parameter == 'numberoftimesappopen' and parameter2 != ''):
            usermaster = UserMasterHistory(user_type=user_type,number_of_times_app_open=parameter2,user_id=pk)
            usermaster.save()

    
    #AssignUserBadgePoints(pk,'number-of-goals-created')
    return True

@api_view()
def AssignUserBadgePoints(request, pk,param):
    usermasterobj = UserMasterHistory.objects.get(Q(user_id=pk))

    user_type = usermasterobj.user_type
    total_publish_lession = usermasterobj.total_publish_lession
    total_assign_lession = usermasterobj.total_assign_lession
    total_goal_created = usermasterobj.total_goal_created
    total_goal_completed = usermasterobj.total_goal_completed
    total_journal_publish = usermasterobj.total_journal_publish
    total_journal_unique_location = usermasterobj.total_journal_unique_location
    total_student_connect = usermasterobj.total_student_connect
    total_coach_connect = usermasterobj.total_coach_connect
    total_journal_feedback_comment = usermasterobj.total_journal_feedback_comment
    total_shared_journal = usermasterobj.total_shared_journal
    is_key_pro = usermasterobj.is_key_pro
    current_no_point = usermasterobj.current_no_point
    complete_allfield_profile = usermasterobj.complete_allfield_profile
    total_referral_signup = usermasterobj.total_referral_signup
    number_of_day_since_signup = usermasterobj.number_of_day_since_signup
    number_of_times_app_open = usermasterobj.number_of_times_app_open
    total_earn_badge = usermasterobj.total_earn_badge


    if(param == 'add_goal'):
        
        #"SELECT * FROM gamification_badgepointnew WHERE condition_1 = 'number-of-goals-created' or condition_2 = 'number-of-goals-created' or condition_3 ='number-of-goals-created' "
        #get 1 record
        for(conditoonloop):
            total_publish_lession #5
            total_goal_created #5
            total_goal_completed #2
            
            condition_1 == 'number-of-journal-entries-published'
            condition_2 == 'number-of-goals-created'
            condition_3 == 'number-of-goals-completed'

            "select GROUP_CONCAT(master_user_colmn) from gamification_conditions where condition_key = 'number-of-goals-created' or condition_key = 'number-of-journal-entries-published' or condition_key = 'number-of-goals-completed' "
            #total_goal_created,total_publish_lession,total_goal_published
            
            getKey(usermasterObject,key,user_id)
            
            #totalgoal = "select total_goal_created from gamification_usermasterhistory where user_id = '288' "

            condition1_value == 2
            condition2_value  == 2
            condition3_value == 2 

            condition1_value_real = totalgoal
            condition2_value_real  == 2
            condition3_value_real == 2 

            Boolean1 =True
            Boolean2 =True
            Boolean3 =True
            Boolean4 =True

            Boolean1 = condition1_value <= condition1_value_real
            Boolean2 = condition2_value <= condition2_value_real
            Boolean3 = condition3_value <= condition3_value_real

            if(relation1 == 'and'):
                Boolean4 = Boolean1 and Boolean2
            elif(relation1 == 'or'):
                Boolean4 = Boolean1 or Boolean2

            if(relation2 == 'and'):
                Boolean4 = Boolean4 and Boolean3
            elif(relation2 == 'or'):
                Boolean4 = Boolean4 or Boolean3

            
            
            


        
    #get all condition

    conditionobj = Conditions.objects.filter()
    #for eachcondition in conditionobj:
        



    #get badgepoint 
    badgepointobj = BadgePointNew.objects.filter()
    objarr = []
    for eachcondition in badgepointobj:

        relation1 = str(eachcondition.relation1)
        relation2 = str(eachcondition.relation2)

        if(eachcondition.condition_1 == 'number-of-goals-created' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'and' and relation2 == 'and'):
            id = 1

        elif(eachcondition.condition_1 == 'number-of-goals-created' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'or' and relation2 == 'or'):
            id = 2

        elif(eachcondition.condition_1 == 'number-of-goals-created' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'and' and relation2 == 'or'):
            id = 3

        elif(eachcondition.condition_1 == 'number-of-goals-created' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'or' and relation2 == 'and'):
            id = 4
        
        
        #####
        elif(eachcondition.condition_1 == 'number-of-goals-completed' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-created' and relation1 == 'and' and relation2 == 'and'):
            id = 5

        elif(eachcondition.condition_1 == 'number-of-goals-completed' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-created' and relation1 == 'or' and relation2 == 'or'):
            id = 6

        elif(eachcondition.condition_1 == 'number-of-goals-completed' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-created' and relation1 == 'and' and relation2 == 'or'):
            id = 7

        elif(eachcondition.condition_1 == 'number-of-goals-completed' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-created' and relation1 == 'or' and relation2 == 'and'):
            id = 8

        ###
        elif(eachcondition.condition_1 == 'number-of-journal-entries-published' and eachcondition.condition_2 == 'number-of-goals-created' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'and' and relation2 == 'and'):
            id = 9
        elif(eachcondition.condition_1 == 'number-of-journal-entries-published' and eachcondition.condition_2 == 'number-of-goals-created' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'or' and relation2 == 'or'):
            id = 10
        elif(eachcondition.condition_1 == 'number-of-journal-entries-published' and eachcondition.condition_2 == 'number-of-goals-created' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'and' and relation2 == 'or'):
            id = 11
        elif(eachcondition.condition_1 == 'number-of-journal-entries-published' and eachcondition.condition_2 == 'number-of-goals-created' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'or' and relation2 == 'and'):
            id = 12

        ###
        elif(eachcondition.condition_1 == 'number-of-goals-created' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'and' and relation2 == 'and'):
            id = 13
        
        elif(eachcondition.condition_1 == 'number-of-goals-created' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'or' and relation2 == 'or'):
            id = 14

        elif(eachcondition.condition_1 == 'number-of-goals-created' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'and' and relation2 == 'or'):
            id = 15

        elif(eachcondition.condition_1 == 'number-of-goals-created' and eachcondition.condition_2 == 'number-of-journal-entries-published' and eachcondition.condition_3 == 'number-of-goals-completed' and relation1 == 'or' and relation2 == 'and'):
            id = 16
            
        
        #objarr.append(Param)


    return Response({"message": "Hello, world!","data":id })

    def getKey(obj,key):
        
        if(key == 'no_of_goal_created'):
            return obj.no_of_goal_created
        