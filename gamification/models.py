# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from django.conf import settings
from django.utils.text import slugify
from django.core.exceptions import ValidationError

# Create your models here.

class Conditions(models.Model):
    condition_name = models.CharField(max_length=250,unique=True)
    condition_key = models.SlugField(max_length=250,unique=True,blank=True)
    masteruserfieldcolumn = models.CharField(max_length=250,blank=True)
    
    def save(self, *args, **kwargs):
        self.condition_key = slugify(self.condition_name)
        super(Conditions, self).save(*args, **kwargs)
        
    class Meta:
        verbose_name = 'Conditions'
        verbose_name_plural = 'Conditions'
    
    def __unicode__(self):
        return self.condition_name
    

class BadgePointNew(models.Model):
    CONDITION = (
            ('number-of-published-lesson', 'Number of published lesson'),
            ('number-of-lessons-assigned-to-students', 'Number of lessons assigned to students'),
            ('number-of-goals-created', 'Number of Goals created'),
            ('number-of-goals-completed', 'Number of goals completed'),
            ('number-of-journal-entries-published', 'Number of Journal Entries published'),
            ('number-of-journal-entry-at-unique-location', 'Number of Journal Entry at unique location'),
            ('number-of-shared-journal-entries-with-a-coach', 'Number of shared journal entries with a coach'),
            ('number-of-student-journal-feedback-comments-user-has-created', 'Number of student journal feedback comments user has created'),
            ('number-of-students-connected-to', 'Number of Students connected to'),
            ('number-of-coaches-connected-to', 'Number of coaches connected to'),
            ('is-a-keyypro-member', 'Is a KeyyPRO member'),
            ('current-number-of-points', 'Current number of points'),
            ('complete-all-fields-in-profile', 'Complete all fields in profile'),
            ('number-of-referral-signups', 'Number of referral signups'),
            ('number-of-days-since-sign-up-date', 'Number of days since sign up date'),
            ('number-of-times-app-used-during-the-calendar-month', 'Number of times app used during the calendar month'),
            ('user-has-earned-badges', 'User has earned badges'),
            
        )
    CONDITION_TYPE = (('number','Number'),('boolean','Boolean'),('in','IN'))
    user_type = models.CharField(max_length=250, choices=settings.ACCOUNT_TYPE)
    task_type = models.CharField(max_length=250, choices=settings.TASK_TYPE)
    points = models.CharField(max_length=250, blank=True)
    badge_title = models.CharField(max_length=250, blank=True)
    badge_image = models.FileField(upload_to = 'badge/', blank=True)
    type_of_condition1 = models.CharField(max_length=250, choices=CONDITION_TYPE,blank=True)
    condition_1 = models.CharField(max_length=250,choices=CONDITION)
    condition1_value = models.CharField(max_length=250,blank=True,default=0)
    relation1 = models.CharField(max_length=250, choices=settings.RELATION)
    type_of_condition2 = models.CharField(max_length=250, choices=CONDITION_TYPE,blank=True)
    condition_2 = models.CharField(max_length=250,choices=CONDITION)
    condition2_value = models.CharField(max_length=250, blank=True,default=0)
    relation2 = models.CharField(max_length=250, choices=settings.RELATION)
    type_of_condition3 = models.CharField(max_length=250, choices=CONDITION_TYPE,blank=True)
    condition_3 = models.CharField(max_length=250,choices=CONDITION)
    condition3_value = models.CharField(max_length=250, blank=True,default=0)
    
    def save(self, *args, **kwargs):
        
        if(self.condition_1 == 'is-a-keyypro-member'):
            self.type_of_condition1 = 'boolean'
        elif(self.condition_1 == 'user-has-earned-badges'):
            self.type_of_condition1 = 'in'
        else:
            self.type_of_condition1 = 'number'

        if(self.condition_2 == 'is-a-keyypro-member'):
            self.type_of_condition2 = 'boolean'
        elif(self.condition_2 == 'user-has-earned-badges'):
            self.type_of_condition2 = 'in'
        else:
            self.type_of_condition2 = 'number'

        if(self.condition_3 == 'is-a-keyypro-member'):
            self.type_of_condition3 = 'boolean'
        elif(self.condition_3 == 'user-has-earned-badges'):
            self.type_of_condition3 = 'in'
        else:
            self.type_of_condition3 = 'number'

        super(BadgePointNew, self).save(*args, **kwargs)


    class Meta:
        verbose_name = 'Points and Badge New'
        verbose_name_plural = 'Points and Badge New'
    
    def __unicode__(self):
        return self.badge_title



class BadgePoint(models.Model):
    user_type = models.CharField(max_length=250, choices=settings.ACCOUNT_TYPE)
    task_type = models.CharField(max_length=250, choices=settings.TASK_TYPE)
    points = models.CharField(max_length=250, blank=True)
    badge_title = models.CharField(max_length=250, blank=True)
    badge_image = models.FileField(upload_to = 'badge/', blank=True)
    condition_1 = models.ForeignKey(Conditions, related_name="Condition1")
    condition1_value = models.CharField(max_length=250,blank=True,default=0)
    relation1 = models.CharField(max_length=250, choices=settings.RELATION,blank=True)
    condition_2 = models.ForeignKey(Conditions, related_name="Condition2")
    condition2_value = models.CharField(max_length=250, blank=True,default=0)
    relation2 = models.CharField(max_length=250, choices=settings.RELATION,blank=True)
    condition_3 = models.ForeignKey(Conditions, related_name="Condition3")
    condition3_value = models.CharField(max_length=250, blank=True,default=0)
    
    class Meta:
        verbose_name = 'Points and Badge'
        verbose_name_plural = 'Points and Badge'
    
    def __unicode__(self):
        return self.badge_title


class Level(models.Model):
    USER_TYPE = (('Student', 'Student'),('Coach', 'Coach'),('All', 'All'),)

    level_name = models.CharField(max_length=250,unique=True)
    required_badge = models.ManyToManyField(BadgePoint,blank=True)
    relation = models.CharField(max_length=250, choices=settings.RELATION,blank=True)
    minimum_points = models.CharField(max_length=250,unique=True)
    default_level = models.BooleanField(default=False)
    user_type = models.CharField(max_length=250, choices=USER_TYPE,blank=True)
    
    class Meta:
        verbose_name = 'Levels'
        verbose_name_plural = 'Levels'
    
    def clean(self):
        # Don't allow draft entries to have a pub_date.
        if self.default_level == True and self.user_type == '' :
            raise ValidationError('Please select user Type')

    def __unicode__(self):
        return self.level_name


class UserPointBadgeHistory(models.Model):
    user = models.ForeignKey("general.StudentCoach", related_name="USER")
    badge_id = models.CharField(max_length=250,blank=True)
    points = models.CharField(max_length=250,blank=True)

    class Meta:
        verbose_name = 'User Badge History'
        verbose_name_plural = 'User Badge History'
    
    def __unicode__(self):
        return self.user



class UserMasterHistory(models.Model):
    user = models.ForeignKey("general.StudentCoach", related_name="USERMASTER")
    user_type = models.CharField(max_length=250,blank=True)
    total_publish_lession = models.CharField(max_length=250,blank=True)
    total_assign_lession = models.CharField(max_length=250,blank=True)
    total_goal_created = models.CharField(max_length=250,blank=True)
    total_goal_completed = models.CharField(max_length=250,blank=True)
    total_journal_publish = models.CharField(max_length=250,blank=True)
    total_journal_unique_location = models.CharField(max_length=250,blank=True)
    total_student_connect = models.CharField(max_length=250,blank=True)
    total_coach_connect = models.CharField(max_length=250,blank=True)
    total_journal_feedback_comment = models.CharField(max_length=250,blank=True)
    total_shared_journal = models.CharField(max_length=250,blank=True)
    is_key_pro = models.CharField(max_length=250,blank=True)
    current_no_point = models.CharField(max_length=250,blank=True)
    complete_allfield_profile = models.CharField(max_length=250,blank=True)
    total_referral_signup = models.CharField(max_length=250,blank=True)
    number_of_day_since_signup = models.CharField(max_length=250,blank=True)
    number_of_times_app_open = models.CharField(max_length=250,blank=True)
    total_earn_badge = models.CharField(max_length=250,blank=True)

    class Meta:
        verbose_name = 'User Master History'
        verbose_name_plural = 'User Master History'


