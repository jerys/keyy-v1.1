from rest_framework import serializers

from general.models import StudentCoach

from coaches.models import Tag
from coaches.models import Coachmodule
from coaches.models import Coachmodulelink
from coaches.models import CoachModuleComment
from coaches.models import CoachModuleReport


from students.models import StudentAssignment
#from students.models import StudentAssignmentModuel


from students.models import Journal

from notification.models import StudentCoachRequest


#tag 
class TagSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tag
        fields = ('id','tag_title',)
    
    
class CreateModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodule
        #fields = '__all__'
        fields = ('id','module_name','tag','philosophy_video','philosophy_video_thumb','technique_video','technique_video_thumb','coach_video','coach_video_thumb','description','see_module','links','module_price','philosophy_video2','technique_video2','coach_video2','philosophy_video_thumb2','technique_video_thumb2','coach_video_thumb2','philosophy_video_updated','technique_video_updated','coach_video_updated',)
        
        
class ModuleSerializer(serializers.ModelSerializer):
    module_id = serializers.CharField(write_only=True)
    class Meta:
        model = Coachmodule
        #fields = '__all__'
        fields = ('id','module_name','tag','philosophy_video','philosophy_video_thumb','technique_video','technique_video_thumb','coach_video','coach_video_thumb','description','see_module','links','paid_free','module_price','module_id','module_status','philosophy_video2','technique_video2','coach_video2','philosophy_video_thumb2','technique_video_thumb2','coach_video_thumb2','philosophy_video_updated','technique_video_updated','coach_video_updated',)
        
        
        
class ModuleSerializeriosV1(serializers.ModelSerializer):
    philosophy_video_fullpath = serializers.CharField(write_only=True)
    philosophy_video = serializers.CharField(write_only=True)
    philosophy_video_thumb = serializers.CharField(write_only=True)
    philosophy_video_thumb_fullpath = serializers.CharField(write_only=True)
    class Meta:
        model = Coachmodule
        fields = ('id','philosophy_video_fullpath','philosophy_video','philosophy_video_thumb','philosophy_video_thumb_fullpath')
        

class CoachModuleListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodule
        #fields = '__all__'
        fields = ('id','coach','module_name','tag','philosophy_video','philosophy_video_thumb','technique_video','technique_video_thumb','coach_video','coach_video_thumb','description','see_module','paid_free','module_price','module_status','philosophy_video2','technique_video2','coach_video2','philosophy_video_thumb2','technique_video_thumb2','coach_video_thumb2','philosophy_video_updated','technique_video_updated','coach_video_updated',)


class CoachModuleSelectSerializer(serializers.ModelSerializer):
    module_id = serializers.CharField(write_only=True)
    class Meta:
        model = Coachmodule
        #fields = '__all__'
        fields = ('id','module_id','coach','module_name','tag','philosophy_video','philosophy_video_thumb','technique_video','technique_video_thumb','coach_video','coach_video_thumb','description','see_module','paid_free','module_price','module_status','philosophy_video2','technique_video2','coach_video2','philosophy_video_thumb2','technique_video_thumb2','coach_video_thumb2','philosophy_video_updated','technique_video_updated','coach_video_updated',)

# MKT Add
class ListCoachModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = ('id','first_name','last_name','user_name','e_mail','skill','profile_picture','profile_picture2','account_type','hidden_profile_picture','training_location','social_login','updated',)
        
class RecommendedModuleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodule
        #fields = '__all__'
        fields = ('id','module_name','tag','philosophy_video','technique_video', 'coach_video', 'philosophy_video_thumb','technique_video_thumb', 'coach_video_thumb', 'description','see_module','module_status','links','paid_free','module_price','student_id','coach','philosophy_video2','technique_video2','coach_video2','philosophy_video_thumb2','technique_video_thumb2','coach_video_thumb2','philosophy_video_updated','technique_video_updated','coach_video_updated',)


class ModuleVideoSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodule
        #fields = '__all__'
        fields = ('id','philosophy_video','technique_video','coach_video','philosophy_video_thumb','technique_video_thumb','coach_video_thumb','philosophy_video2','technique_video2','coach_video2','philosophy_video_thumb2','technique_video_thumb2','coach_video_thumb2',)
        
        
class ModuleVideoSerializerIOS(serializers.ModelSerializer):
    class Meta:
        model = Coachmodule
        #fields = '__all__'
        fields = ('id','philosophy_video2','technique_video2','coach_video2','philosophy_video_thumb2','technique_video_thumb2','coach_video_thumb2','philosophy_video_updated','technique_video_updated','coach_video_updated',)
        

class ModuleDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodule
        fields = '__all__'
        

class ModuleLinkSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodulelink
        #fields = '__all__'
        fields = ('id','module_id','link',)
        
    
class ModuleLinkMarkasSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodulelink
        #fields = '__all__'
        fields = ('id','student_ids','link',)
        
        
class BrowsebyTagModuleListSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodule
        #fields = '__all__'
        fields = ('id','module_name','tag','philosophy_video','technique_video', 'coach_video', 'philosophy_video_thumb','technique_video_thumb', 'coach_video_thumb', 'description','see_module','module_status','links','paid_free','module_price','student_id','coach','philosophy_video2','technique_video2','coach_video2','philosophy_video_thumb2','technique_video_thumb2','coach_video_thumb2','philosophy_video_updated','technique_video_updated','coach_video_updated',)

        
class EnrollmoduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentAssignment
        #fields = '__all__'
        fields = ('student_id','module_id','coach_id',)


class CoachModuleCommentSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoachModuleComment
        #fields = '__all__'
        fields = ('id','module_id','user_id','comment','date',)


class CoachModuleLikeSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoachModuleComment
        #fields = '__all__'
        fields = ('id','module_id','user_id','comment_type',)

class CoachModuleReportSerializer(serializers.ModelSerializer):
    class Meta:
        model = CoachModuleReport
        fields = ('id','module_id','user_id','report',)


class StudentCoachRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoachRequest
        #fields = '__all__'
        fields = ('id','user','user_to',)


class StudentCoachRequestAccepRejectSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoachRequest
        #fields = '__all__'
        fields = ('id','status',)


class MyStudentCoachRequestSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoachRequest
        #fields = '__all__'
        fields = ('id','user','user_to','status',)
        

class StudentAssignmentSerializer(serializers.ModelSerializer):
    class Meta:
        model = Coachmodule
        #fields = '__all__'
        fields = ('id','student_id',)


class StudentCoachSerializer(serializers.ModelSerializer):
    class Meta:
        model = StudentCoach
        fields = '__all__'
        
        

class StudentSharedJournalToCoachSerializer(serializers.ModelSerializer):
    class Meta:
        model = Journal
        fields = ('id','journal_title','date','place',)