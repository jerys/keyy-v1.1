# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coaches', '0011_coachmodulelink'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coachmodule',
            name='module_price',
            field=models.CharField(max_length=250, verbose_name='Module Price', blank=True),
        ),
    ]
