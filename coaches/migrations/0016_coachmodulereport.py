# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0021_userreportproblem'),
        ('coaches', '0015_coachmodule_student_id'),
    ]

    operations = [
        migrations.CreateModel(
            name='CoachModuleReport',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('report', models.BooleanField(default=False, choices=[(1, 'True'), (0, 'False')])),
                ('date', models.DateTimeField(default=datetime.datetime.now, blank=True)),
                ('module_id', models.ForeignKey(related_name='CoachModuleReport', to='coaches.Coachmodule')),
                ('user_id', models.ForeignKey(related_name='ModuelUserReport', to='general.StudentCoach')),
            ],
            options={
                'verbose_name': 'Coach Module Report',
                'verbose_name_plural': 'Coach Module Report',
            },
        ),
    ]
