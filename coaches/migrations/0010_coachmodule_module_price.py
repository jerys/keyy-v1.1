# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coaches', '0009_auto_20170822_0733'),
    ]

    operations = [
        migrations.AddField(
            model_name='coachmodule',
            name='module_price',
            field=models.CharField(max_length=250, blank=True),
        ),
    ]
