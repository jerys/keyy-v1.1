# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coaches', '0008_auto_20170822_0628'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coachmodule',
            name='coach_video_thumb',
            field=models.FileField(upload_to='coach/coach_video/thumb/', blank=True),
        ),
        migrations.AlterField(
            model_name='coachmodule',
            name='philosophy_video_thumb',
            field=models.FileField(upload_to='coach/philosophy_video/thumb/', blank=True),
        ),
        migrations.AlterField(
            model_name='coachmodule',
            name='technique_video_thumb',
            field=models.FileField(upload_to='coach/technique_video/thumb/', blank=True),
        ),
    ]
