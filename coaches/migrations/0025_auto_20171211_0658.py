# -*- coding: utf-8 -*-
# Generated by Django 1.9.12 on 2017-12-11 06:58
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coaches', '0024_coachmodule_technique_video_thumb2'),
    ]

    operations = [
        migrations.AlterField(
            model_name='coachmodule',
            name='technique_video_thumb2',
            field=models.CharField(blank=True, max_length=255),
        ),
    ]
