# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('coaches', '0013_coachmodulecomment'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='coachmodulecomment',
            options={'verbose_name': 'Coach Module Comment', 'verbose_name_plural': 'Coach Module Comment'},
        ),
        migrations.AlterModelOptions(
            name='coachmodulelink',
            options={'verbose_name': 'Coach Module Homework Link', 'verbose_name_plural': 'Coach Module Homework Link'},
        ),
        migrations.AlterField(
            model_name='coachmodulecomment',
            name='comment_approved',
            field=models.CharField(default='No', max_length=25, blank=True, choices=[('Yes', 'Yes'), ('No', 'No')]),
        ),
        migrations.AlterField(
            model_name='coachmodulecomment',
            name='date',
            field=models.DateTimeField(default=datetime.datetime.now, blank=True),
        ),
    ]
