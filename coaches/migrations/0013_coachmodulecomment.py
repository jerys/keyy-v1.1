# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('general', '0016_auto_20170824_1646'),
        ('coaches', '0012_auto_20170822_1513'),
    ]

    operations = [
        migrations.CreateModel(
            name='CoachModuleComment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('comment', models.TextField()),
                ('comment_approved', models.CharField(max_length=250, blank=True)),
                ('date', models.DateTimeField(auto_now_add=True)),
                ('comment_type', models.CharField(blank=True, max_length=250, choices=[('comment', 'comment'), ('like', 'like')])),
                ('like', models.CharField(max_length=250, blank=True)),
                ('module_id', models.ForeignKey(related_name='CoachModule', to='coaches.Coachmodule')),
                ('user_id', models.ForeignKey(related_name='User', to='general.StudentCoach')),
            ],
            options={
                'verbose_name': 'Coach Module Comment',
                'verbose_name_plural': 'Module Comment',
            },
        ),
    ]
