# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coaches', '0014_auto_20170825_0912'),
    ]

    operations = [
        migrations.AddField(
            model_name='coachmodule',
            name='student_id',
            field=models.TextField(blank=True),
        ),
    ]
