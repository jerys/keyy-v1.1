# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('coaches', '0010_coachmodule_module_price'),
    ]

    operations = [
        migrations.CreateModel(
            name='Coachmodulelink',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('link', models.CharField(max_length=250, blank=True)),
                ('mark_as', models.CharField(default=False, max_length=25, blank=True)),
                ('student_ids', models.TextField(blank=True)),
                ('module_id', models.ForeignKey(related_name='Module', to='coaches.Coachmodule')),
            ],
        ),
    ]
