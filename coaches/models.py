from __future__ import unicode_literals

from django.db import models
from datetime import datetime    


from django.conf import settings
from django.core.exceptions import ValidationError
import os

# Create your models here.

class Tag(models.Model):
    tag_title = models.CharField(max_length=250)
    status = models.CharField(max_length=1, choices=settings.STATUS_CHOICES)
    
    class Meta:
        verbose_name = 'Tag'
        verbose_name_plural = 'Tags'
        
    def __unicode__(self):
        return self.tag_title

    
class Coachmodule(models.Model):
    
    BOOL_CHOICES = (('Free', 'Free module'), ('Paid', 'Paid module'))
    MODULE_STATUS = (('Active', 'Active'), ('Archive', 'Archive'))
    coach = models.ForeignKey("general.StudentCoach", related_name="Coachmodule", limit_choices_to={'account_type': 'Coach'})
    module_name = models.CharField("Title",max_length=250,blank=True)
    tag = models.ManyToManyField(Tag, blank=True)
    philosophy_video = models.FileField("Learn video",upload_to = 'coach/philosophy_video/',blank=True)
    technique_video = models.FileField("Apply video",upload_to = 'coach/technique_video/',blank=True)
    coach_video = models.FileField(upload_to = 'coach/coach_video/',blank=True)
    philosophy_video_thumb = models.FileField("Learn video thumb",upload_to = 'coach/philosophy_video/thumb/',blank=True)
    technique_video_thumb = models.FileField("Apply video thumb",upload_to = 'coach/technique_video/thumb/', blank=True)
    coach_video_thumb = models.FileField(upload_to = 'coach/coach_video/thumb/',blank=True)
    philosophy_video2 = models.CharField(max_length=255,blank=True)
    technique_video2 = models.CharField(max_length=255,blank=True)
    coach_video2 = models.CharField(max_length=255,blank=True)
    philosophy_video_thumb2 = models.CharField(max_length=255,blank=True)
    technique_video_thumb2 = models.CharField(max_length=255, blank=True)
    coach_video_thumb2 = models.CharField(max_length=255,blank=True)
    description = models.TextField(blank=True)
    see_module = models.CharField(max_length=250,verbose_name="Who should see this module?", choices=settings.MY_CHOICES, blank=True)
    assignto_new_coach = models.BooleanField(default=False,verbose_name="Automatically assign to new COACHES by default")
    assignto_new_student = models.BooleanField(default=False,verbose_name="Automatically assign to new STUDENTS  by default")
    module_status = models.CharField(max_length=250,choices=settings.STATUS, blank=True)
    links = models.CharField(max_length=250, blank=True)
    paid_free = models.CharField(max_length=250,choices=BOOL_CHOICES,blank=True)
    module_price = models.CharField("Module Price",max_length=250,blank=True)
    student_id = models.TextField(blank=True)
    pub_date = models.DateTimeField('date published', auto_now_add=True, blank=True)
    status = models.CharField(max_length=250,choices=MODULE_STATUS, blank=True)
    philosophy_video_updated = models.CharField(max_length=250,blank=True)
    technique_video_updated = models.CharField(max_length=250,blank=True)
    coach_video_updated = models.CharField(max_length=250,blank=True)
    ios_moduleid = models.CharField("IOS Module ID",max_length=250,blank=True)
    
    class Meta:
        verbose_name = 'Coach Lessons'
        verbose_name_plural = 'Coach Lessons'
        
    def __unicode__(self):
        return self.module_name


    def delete(self, *args, **kwargs):
        coach_video = settings.BASE_DIR + settings.MEDIA_URL + str(self.coach_video)
        if os.path.isfile(coach_video):
            os.unlink(coach_video)
            
        philosophy_video = settings.BASE_DIR + settings.MEDIA_URL + str(self.philosophy_video)
        if os.path.isfile(philosophy_video):
            os.unlink(philosophy_video)
            
        technique_video = settings.BASE_DIR + settings.MEDIA_URL + str(self.technique_video)
        if os.path.isfile(technique_video):
            os.unlink(technique_video)
        
        
        coach_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(self.coach_video_thumb)
        if os.path.isfile(coach_thumb):
            os.unlink(coach_thumb)
            
        philosophy_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(self.philosophy_video_thumb)
        if os.path.isfile(philosophy_thumb):
            os.unlink(philosophy_thumb)
            
        technique_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(self.technique_video_thumb)
        if os.path.isfile(technique_thumb):
            os.unlink(technique_thumb)
        
        super(Coachmodule, self).delete(*args, **kwargs)
    
    def clean(self):
        # Don't allow draft entries to have a pub_date.
        if self.paid_free == 'Paid' and self.module_price == '' :
            raise ValidationError('Please enter the module price')
        elif self.paid_free == 'Paid' and self.module_price == '0' :
            raise ValidationError('Please enter valid price')
        # Set the pub_date for published items if it hasn't been set already.



# Receive the pre_delete signal and delete the file associated with the model instance.
from django.db.models.signals import pre_delete
from django.dispatch.dispatcher import receiver

@receiver(pre_delete, sender=Coachmodule)
def coachmodule_delete(sender, instance, **kwargs):
    # Pass false so FileField doesn't save the model.
    if(instance.coach_video != ''):
        instance.coach_video.delete(False)
    
    if(instance.philosophy_video != ''):
        instance.philosophy_video.delete(False)
        
    if(instance.technique_video != ''):
        instance.technique_video.delete(False)
        
    if(instance.coach_video_thumb != ''):
        instance.coach_video_thumb.delete(False)
        
    if(instance.philosophy_video_thumb != ''):
        instance.philosophy_video_thumb.delete(False)
        
    if(instance.technique_video_thumb != ''):
        instance.technique_video_thumb.delete(False)
        



class Coachmodulelink(models.Model):
    MARK_AS = (('True', 'True'), ('False', 'False'))
    module_id = models.ForeignKey(Coachmodule, related_name="Module")
    link = models.CharField(max_length=250, blank=True)
    mark_as = models.CharField(max_length=25,choices=MARK_AS,default=False,blank=True)
    student_ids = models.TextField(blank=True)
    
    class Meta:
        verbose_name = 'Coach Module Homework Link'
        verbose_name_plural = 'Coach Module Homework Link'


class CoachModuleComment(models.Model):
    COMMENT_CHOICES = (('comment', 'comment'), ('like', 'like'))
    APPROVE_CHOICES = (('Yes', 'Yes'), ('No', 'No'))
    module_id = models.ForeignKey(Coachmodule, related_name="CoachModule")
    user_id = models.ForeignKey("general.StudentCoach", related_name="User", limit_choices_to={'account_type': 'Student'})
    comment = models.TextField()
    comment_approved = models.CharField(max_length=25,blank=True,choices=APPROVE_CHOICES,default='No')
    date = models.DateTimeField(default=datetime.now,blank=True)
    comment_type = models.CharField(max_length=250,choices=COMMENT_CHOICES,blank=True)
    like = models.CharField(max_length=250,blank=True)

    class Meta:
        verbose_name = 'Coach Module Comment'
        verbose_name_plural = 'Coach Module Comment'
        
    def __unicode__(self):
        return self.comment
    
    


class CoachModuleReport(models.Model):
    BOOLEAN_CHOICE = ((1, 'True'), (0, 'False'))
    module_id = models.ForeignKey(Coachmodule, related_name="CoachModuleReport")
    user_id = models.ForeignKey("general.StudentCoach", related_name="ModuelUserReport")
    report = models.BooleanField(default=False,choices=BOOLEAN_CHOICE)
    date = models.DateTimeField(default=datetime.now,blank=True)
    

    class Meta:
        verbose_name = 'Abuse Report'
        verbose_name_plural = 'Abuse Report'
