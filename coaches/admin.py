from django.contrib import admin
from django.conf import settings

# Register your models here.
from .models import Tag
class TagAdmin(admin.ModelAdmin):
    list_display=["tag_title","status"]
    list_filter = ("status",)
    list_per_page = settings.ADMINPERPAGE
admin.site.register(Tag, TagAdmin)

from .models import Coachmodule
class CoachModuleAdmin(admin.ModelAdmin):
    list_display=["module_name","coach","see_module","module_status","pub_date"]
    fields = ("coach","ios_moduleid","module_name","tag","philosophy_video","philosophy_video2","technique_video","technique_video2","coach_video","coach_video2","philosophy_video_thumb","philosophy_video_thumb2","technique_video_thumb","technique_video_thumb2","coach_video_thumb","coach_video_thumb2","description","see_module","assignto_new_coach","assignto_new_student","module_status","paid_free","module_price","student_id","status")
    list_filter = ("see_module","module_status",)
    #search_fields = ('module_name','module_status')
    
    list_per_page = settings.ADMINPERPAGE
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["coach","ios_moduleid"]
        else:
            return self.readonly_fields
    
    def has_add_permission(self, request):
        return False
        
admin.site.register(Coachmodule,CoachModuleAdmin)

'''
from .models import Coachmodulelink
class CoachModuleLinkAdmin(admin.ModelAdmin):
    list_display=["module_id","link"]
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["module_id"]
        else:
            return self.readonly_fields
admin.site.register(Coachmodulelink, CoachModuleLinkAdmin)


from .models import CoachModuleComment
class CoachCommentAdmin(admin.ModelAdmin):
    list_display=["module_id","user_id","comment_type","comment_approved","date"]
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["module_id","user_id","comment_type","comment","like","comment_approved","date"]
        else:
            return self.readonly_fields
        
admin.site.register(CoachModuleComment,CoachCommentAdmin)
'''


from .models import CoachModuleReport
class CoachModuleReportAdmin(admin.ModelAdmin):
    list_display=["module_id","user_id"]
    '''
    def get_actions(self, request):
        #Disable delete
        actions = super(CoachModuleReportAdmin, self).get_actions(request)
        del actions['delete_selected']
        return actions
    def has_delete_permission(self, request, obj=None):
        #Disable delete
        return False
    '''
    def has_add_permission(self, request):
        return False
    def get_readonly_fields(self, request, obj=None):
        if 'edit' not in request.GET:
            return ["module_id","user_id","report","date"]
        else:
            return self.readonly_fields
        
admin.site.register(CoachModuleReport,CoachModuleReportAdmin)
