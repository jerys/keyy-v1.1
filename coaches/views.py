from django.shortcuts import render
from rest_framework.views import APIView
from django.core.mail import EmailMessage
from rest_framework.generics import CreateAPIView, UpdateAPIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse, Http404
from rest_framework import status
from rest_framework.status import HTTP_200_OK, HTTP_400_BAD_REQUEST

from general.models import StudentCoach
from general.models import OrderHistory
from coaches.models import Tag
from coaches.models import Coachmodule
from coaches.models import Coachmodulelink
from coaches.models import CoachModuleComment
from coaches.models import CoachModuleReport

from students.models import StudentAssignment
#from students.models import StudentAssignmentModuel
from students.models import Journal
from students.models import StudentAssignUnassignTrack

from notification.models import StudentCoachRequest
from notification.models import Notification

from fcm_django.models import FCMDevice



from django.conf import settings
import os
from django.db.models import Count

from django.db.models import Q
import datetime
import calendar

import boto3
import botocore
from django.core.files.storage import default_storage

from shutil import copyfile
import sys, base64, hashlib, hmac, urllib
import requests

import gamification


from coaches.serializers import TagSerializer, ModuleSerializer, ModuleVideoSerializer, ModuleDetailSerializer, ModuleLinkSerializer, ModuleLinkMarkasSerializer, RecommendedModuleSerializer, ListCoachModuleSerializer, BrowsebyTagModuleListSerializer, EnrollmoduleSerializer, CoachModuleCommentSerializer, CoachModuleLikeSerializer, StudentCoachRequestSerializer, StudentAssignmentSerializer, StudentCoachRequestAccepRejectSerializer, MyStudentCoachRequestSerializer, StudentCoachSerializer, CoachModuleReportSerializer, CreateModuleSerializer, CoachModuleListSerializer, StudentSharedJournalToCoachSerializer, CoachModuleSelectSerializer, ModuleSerializeriosV1, ModuleVideoSerializerIOS



# Create your views here.
class ListTagView(APIView):
    serializer_class = TagSerializer
    def get(self, request, format=None):
        try:
            tagobj = Tag.objects.all().order_by('tag_title')
            serializer = TagSerializer(tagobj, many=True)
            return Response({"response_code":"0", "response_msg": "Tag list", "tag":serializer.data })
        except Tag.DoesNotExist:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "tag": {} })
        
        
# Create new module APIView
'''
class CreateModule(APIView):
    serializer_class = ModuleSerializer
    def get(self, request,pk, format=None):
        try:
            snippets = Coachmodule.objects.get(coach_id=pk,module_status='draft')
            serializer = ModuleSerializer(snippets)
            return Response({"response_code":"0", "response_msg": "Module added successfully", "module":{ "user_id":pk, "module_id":serializer.data['id'] } })
        except:
            serializer = ModuleSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save(coach_id=pk,module_status='draft')
            return Response({"response_code":"0", "response_msg": "Module added successfully", "module":{ "user_id":pk, "module_id":serializer.data['id'] } })
        #return JsonResponse({"response_code":"1", "response_msg": "Please enter required field", "module": {} })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)
    
    def post(self, request,pk, format=None):
        serializer = ModuleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(coach_id=pk)
            return Response({"response_code":"0", "response_msg": "Module added successfully", "module":serializer.data })
        #return JsonResponse({"response_code":"1", "response_msg": "Please enter required field", "module": {} })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)
    
'''

#add moduel by coach    
class CreateModule(APIView):
    serializer_class = CreateModuleSerializer
    def post(self, request,pk, format=None):
        serializer = CreateModuleSerializer(data=request.data)
        module_status='draft'
        
        #check if member pro or not
        try:
            usercheck  = StudentCoach.objects.get(Q(id=pk), Q(account_type='Coach'))
            subscription = usercheck.subscription
            subscription_invite = usercheck.subscription_invite
            if(subscription == True or subscription_invite == True):
                Unlimited = True
                prouser = True
            else:
                prouser = False
                #check how to add publis
                checkcountmodule = Coachmodule.objects.filter(Q(coach_id=pk), ~Q(status='Archive')).order_by('-id').count()
                if(checkcountmodule >= 30):
                    Unlimited = False
                    return Response({"status":False, "response_msg": "You have reached maximum publish limit.", "user_id":pk, "module":False })
                else:
                    Unlimited = True
                
            
            
            coachmodule = Coachmodule.objects.filter(coach_id=pk,module_status='draft').order_by('-id')
            firstID = ''
            count = 0
            for countmodule_val in coachmodule:
                firstID = countmodule_val.id
                count = count + 1
            
            if(count >= 5 and prouser == False):
                return Response({"status":False, "response_msg": "You have reached maximum draft limit. Please delete any draft module to continue", "user_id":pk, "module":{} })
            
            
            if serializer.is_valid():
                serializer.save(coach_id=pk,module_status=module_status)
                
                partList = {}
                modulearr = []
                partList['id'] = serializer.data['id']
                partList['module_name'] = serializer.data['module_name']
                
                current_url = request.get_host()
                if request.is_secure():
                    current_url = 'https://' + current_url
                else:
                    current_url = 'http://' + current_url
                
                
                partList['philosophy_video_updated'] = serializer.data['philosophy_video_updated']
                partList['technique_video_updated'] = serializer.data['technique_video_updated']
                partList['coach_video_updated'] = serializer.data['coach_video_updated']
                
                if(serializer.data['philosophy_video']):
                    #philosophyvideo = settings.BASE_DIR + str(serializer.data['philosophy_video'])
                    #if os.path.exists(philosophyvideo):
                    partList['philosophy_video'] = serializer.data['philosophy_video']
                    #else:
                        #partList['philosophy_video'] = ''
                elif(serializer.data['philosophy_video2']):
                    partList['philosophy_video'] = serializer.data['philosophy_video2']
                else:
                    partList['philosophy_video'] = ''
                    
                
                if(serializer.data['technique_video']):
                    #techniquevideo = settings.BASE_DIR + str(serializer.data['technique_video'])
                    #if os.path.exists(techniquevideo):
                    partList['technique_video'] = serializer.data['technique_video']
                    #else:
                        #partList['technique_video'] = ''
                elif(serializer.data['technique_video2']):
                    partList['technique_video'] = serializer.data['technique_video2']
                else:
                    partList['technique_video'] = ''
                    
                    
                if(serializer.data['coach_video']):
                    #coachvideo = settings.BASE_DIR + str(serializer.data['coach_video'])
                    #if os.path.exists(coachvideo):
                    partList['coach_video'] = serializer.data['coach_video']
                    #else:
                        #partList['coach_video'] = ''
                elif(serializer.data['coach_video2']):
                    partList['coach_video'] = serializer.data['coach_video2']
                else:
                    partList['coach_video'] = ''
                    
                    
                if(serializer.data['philosophy_video_thumb']):
                    #philosophy_video_thumb = settings.BASE_DIR + str(serializer.data['philosophy_video_thumb'])
                    #if os.path.exists(philosophy_video_thumb):
                    partList['philosophy_video_thumb'] = serializer.data['philosophy_video_thumb']
                    #else:
                        #partList['philosophy_video_thumb'] = ''
                elif(serializer.data['philosophy_video_thumb2']):
                    partList['philosophy_video_thumb'] = serializer.data['philosophy_video_thumb2']
                else:
                    partList['philosophy_video_thumb'] = ''
                    
                if(serializer.data['technique_video_thumb']):
                    #technique_video_thumb = settings.BASE_DIR + str(serializer.data['technique_video_thumb'])
                    #if os.path.exists(technique_video_thumb):
                    partList['technique_video_thumb'] = serializer.data['technique_video_thumb']
                    #else:
                        #partList['technique_video_thumb'] = ''
                elif(serializer.data['technique_video_thumb2']):
                    partList['technique_video_thumb'] = serializer.data['technique_video_thumb2']
                else:
                    partList['technique_video_thumb'] = ''
                    
                if(serializer.data['coach_video_thumb']):
                    #coach_video_thumb = settings.BASE_DIR + str(serializer.data['coach_video_thumb'])
                    #if os.path.exists(coach_video_thumb):
                    partList['coach_video_thumb'] = serializer.data['coach_video_thumb']
                    #else:
                        #partList['coach_video_thumb'] = ''
                elif(serializer.data['coach_video_thumb2']):
                    partList['coach_video_thumb'] = serializer.data['coach_video_thumb2']
                else:
                    partList['coach_video_thumb'] = ''
                    

                partList['description'] = serializer.data['description']
                partList['see_module'] = serializer.data['see_module']
                partList['module_status'] = module_status
                
                #partList['links'] = each.links
                partList['paid_free'] = 'Free'
                
                if(serializer.data['tag']):
                    taglist = serializer.data['tag']
                    partList['tag'] = taglist
                
                modulearr.append(partList)
                
                #return Response({"response_code":"0", "response_msg": "Module added successfully", "module":{ "user_id":pk, "module_id":serializer.data['id'] } })
                return Response({"status":True, "response_msg": "Module added successfully", "user_id":pk, "module":modulearr })
            #return JsonResponse({"response_code":"1", "response_msg": "Please enter required field", "module": {} })
            return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)
        except:
            return Response({"status":False, "response_msg": "You have not permitted to add module to use this id", "user_id":pk, "module":{} })
    
    
#update moduel video bycoach
class UpdatevideoModule(APIView):
    serializer_class = ModuleVideoSerializer
    def get(self, request,coachID,pk, format=None):
        try:
            snippets = Coachmodule.objects.filter(pk=pk)
            serializer = ModuleVideoSerializer(snippets,many=True)
            
            if(snippets):
                modulearr = []
                for each in serializer.data:
                    partList = {}
                    partList['id'] = each['id']
                    
                    current_url = request.get_host()
                    if request.is_secure():
                        current_url = 'https://' + current_url
                    else:
                        current_url = 'http://' + current_url
                    
                    if(each['philosophy_video']):
                        #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                        #if os.path.exists(philosophyvideo):
                        partList['philosophy_video'] = each['philosophy_video']
                        #else:
                            #partList['philosophy_video'] = ''
                    else:
                        partList['philosophy_video'] = ''
                    
                    
                    if(each['philosophy_video_thumb']):
                        #philosophyvideo_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                        #if os.path.exists(philosophyvideo_thumb):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                        #else:
                            #partList['philosophy_video_thumb'] = ''
                    else:
                        partList['philosophy_video_thumb'] = ''
                        
                    
                    if(each['technique_video']):
                        #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                        #if os.path.exists(techniquevideo):
                        partList['technique_video'] = each['technique_video']
                        #else:
                            #partList['technique_video'] = ''
                    else:
                        partList['technique_video'] = ''
                        
                    if(each['technique_video_thumb']):
                        #techniquevideo_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                        #if os.path.exists(techniquevideo_thumb):
                        partList['technique_video_thumb'] = each['technique_video_thumb']
                        #else:
                            #partList['technique_video_thumb'] = ''
                    else:
                        partList['technique_video_thumb'] = ''
                        
                    if(each['coach_video']):
                        #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                        #if os.path.exists(coachvideo):
                        partList['coach_video'] = each['coach_video']
                        #else:
                            #partList['coach_video'] = ''
                    else:
                        partList['coach_video'] = ''
                            
                    
                    if(each['coach_video_thumb']):
                        #coachvideo_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                        #if os.path.exists(coachvideo_thumb):
                        partList['coach_video_thumb'] = each['coach_video_thumb']
                        #else:
                            #partList['coach_video_thumb'] = ''
                    else:
                            partList['coach_video_thumb'] = ''
                    
                    partList['user_id'] = coachID
    
                    modulearr.append(partList)
                return Response({"status":True, "response_msg": "Module video", "module":modulearr })
        except Coachmodule.DoesNotExist:
            raise Http404
    def post(self, request, coachID,pk, *args, **kwargs):
        snippets = Coachmodule.objects.get(pk=pk)
        serializer = ModuleVideoSerializer(snippets, data=request.data)

        current_url = request.get_host()
        if request.is_secure():
            current_url = 'https://' + current_url
        else:
            current_url = 'http://' + current_url
            
        
        #return Response({"module":{"philosophy_video":philosophy_video} })
        
        if request.FILES.get('philosophy_video', None):
            if default_storage.exists(str(snippets.philosophy_video)):
                default_storage.delete(str(snippets.philosophy_video))
        
        '''
        if request.FILES.get('philosophy_video', None):
            old_philosophy_video = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.philosophy_video)
            if os.path.isfile(old_philosophy_video):
                os.unlink(old_philosophy_video)
        
        '''
        
        #for video thumb unlink
        if request.FILES.get('philosophy_video_thumb', None):
            #old_philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.philosophy_video_thumb)
            if default_storage.exists(str(snippets.philosophy_video_thumb)):
                default_storage.delete(str(snippets.philosophy_video_thumb))
            
        if request.FILES.get('technique_video', None):
            #old_technique_video = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.technique_video)
            if default_storage.exists(str(snippets.technique_video)):
                default_storage.delete(str(snippets.technique_video))
                
        #for video thumb unlink
        if request.FILES.get('technique_video_thumb', None):
            #old_technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.technique_video_thumb)
            if default_storage.exists(str(snippets.technique_video_thumb)):
                default_storage.delete(str(snippets.technique_video_thumb))
            
        if request.FILES.get('coach_video', None):
            #old_coach_video = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.coach_video)
            if default_storage.exists(str(snippets.coach_video)):
                default_storage.delete(str(snippets.coach_video))
                
        #for video thumb unlink
        if request.FILES.get('coach_video_thumb', None):
            #old_coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.coach_video_thumb)
            if default_storage.exists(str(snippets.coach_video_thumb)):
                default_storage.delete(str(snippets.coach_video_thumb))
            
        
        
        if serializer.is_valid():
            serializer.save(coach_id=coachID)
            
            
            partList = {}
            modulearr = []
            partList['user_id'] = coachID
            partList['module_id'] = serializer.data['id']
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            
            
            if(serializer.data['philosophy_video']):
                #philosophyvideo = settings.BASE_DIR + str(serializer.data['philosophy_video'])
                #if os.path.exists(philosophyvideo):
                partList['philosophy_video'] = serializer.data['philosophy_video']
                #else:
                    #partList['philosophy_video'] = ''
            else:
                partList['philosophy_video'] = ''
                
            
            if(serializer.data['technique_video']):
                #techniquevideo = settings.BASE_DIR + str(serializer.data['technique_video'])
                #if os.path.exists(techniquevideo):
                partList['technique_video'] = serializer.data['technique_video']
                #else:
                    #partList['technique_video'] = ''
            else:
                partList['technique_video'] = ''
                
                
            if(serializer.data['coach_video']):
                #coachvideo = settings.BASE_DIR + str(serializer.data['coach_video'])
                #if os.path.exists(coachvideo):
                partList['coach_video'] = serializer.data['coach_video']
                #else:
                    #partList['coach_video'] = ''
            else:
                partList['coach_video'] = ''
                
                
            if(serializer.data['philosophy_video_thumb']):
                #philosophy_video_thumb = settings.BASE_DIR + str(serializer.data['philosophy_video_thumb'])
                #if os.path.exists(philosophy_video_thumb):
                partList['philosophy_video_thumb'] = serializer.data['philosophy_video_thumb']
                #else:
                    #partList['philosophy_video_thumb'] = ''
            else:
                partList['philosophy_video_thumb'] = ''
                
            if(serializer.data['technique_video_thumb']):
                #technique_video_thumb = settings.BASE_DIR + str(serializer.data['technique_video_thumb'])
                #if os.path.exists(technique_video_thumb):
                partList['technique_video_thumb'] = serializer.data['technique_video_thumb']
                #else:
                    #partList['technique_video_thumb'] = ''
            else:
                partList['technique_video_thumb'] = ''
                
            if(serializer.data['coach_video_thumb']):
                #coach_video_thumb = settings.BASE_DIR + str(serializer.data['coach_video_thumb'])
                #if os.path.exists(coach_video_thumb):
                partList['coach_video_thumb'] = serializer.data['coach_video_thumb']
                #else:
                    #partList['coach_video_thumb'] = ''
            else:
                partList['coach_video_thumb'] = ''
            
            
            modulearr.append(partList)
            
            return Response({"status":True, "response_msg": "Module video upload successfully", "module":modulearr })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)



#ios

#update moduel video bycoach
class UpdatevideoModulev1(APIView):
    serializer_class = ModuleVideoSerializerIOS
    def get(self, request,coachID,pk, format=None):
        try:
            snippets = Coachmodule.objects.filter(pk=pk)
            serializer = ModuleVideoSerializerIOS(snippets,many=True)
            
            if(snippets):
                modulearr = []
                for each in serializer.data:
                    partList = {}
                    partList['id'] = each['id']
                    
                    
                    if(each['philosophy_video2']):
                        partList['philosophy_video'] = each['philosophy_video2']
                    else:
                        partList['philosophy_video'] = ''
                    
                    if(each['philosophy_video_thumb2']):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                    else:
                        partList['philosophy_video_thumb'] = ''
                        
                    
                    if(each['technique_video2']):
                        partList['technique_video'] = each['technique_video2']
                    else:
                        partList['technique_video'] = ''
                        
                    if(each['technique_video_thumb2']):
                        partList['technique_video_thumb'] = each['technique_video_thumb2']
                    else:
                        partList['technique_video_thumb'] = ''
                        
                    if(each['coach_video2']):
                        partList['coach_video'] = each['coach_video2']
                    else:
                        partList['coach_video'] = ''
                            
                    
                    if(each['coach_video_thumb2']):
                        partList['coach_video_thumb'] = each['coach_video_thumb2']
                    else:
                        partList['coach_video_thumb'] = ''
                    
                    partList['user_id'] = coachID
                    partList['philosophy_video_updated'] = each['philosophy_video_updated']
                    partList['technique_video_updated'] = each['technique_video_updated']
                    partList['coach_video_updated'] = each['coach_video_updated']
    
                    modulearr.append(partList)
                return Response({"status":True, "response_msg": "Module video", "module":modulearr })
        except Coachmodule.DoesNotExist:
            raise Http404
    def post(self, request, coachID,pk, *args, **kwargs):
        snippets = Coachmodule.objects.get(pk=pk)
        serializer = ModuleVideoSerializerIOS(snippets, data=request.data)
        
        '''
        if request.FILES.get('philosophy_video', None):
            if default_storage.exists(str(snippets.philosophy_video)):
                default_storage.delete(str(snippets.philosophy_video))
        
        #for video thumb unlink
        if request.FILES.get('philosophy_video_thumb', None):
            if default_storage.exists(str(snippets.philosophy_video_thumb)):
                default_storage.delete(str(snippets.philosophy_video_thumb))
            
        if request.FILES.get('technique_video', None):
            if default_storage.exists(str(snippets.technique_video)):
                default_storage.delete(str(snippets.technique_video))
                
        #for video thumb unlink
        if request.FILES.get('technique_video_thumb', None):
            if default_storage.exists(str(snippets.technique_video_thumb)):
                default_storage.delete(str(snippets.technique_video_thumb))
            
        if request.FILES.get('coach_video', None):
            if default_storage.exists(str(snippets.coach_video)):
                default_storage.delete(str(snippets.coach_video))
                
        #for video thumb unlink
        if request.FILES.get('coach_video_thumb', None):
            if default_storage.exists(str(snippets.coach_video_thumb)):
                default_storage.delete(str(snippets.coach_video_thumb))
        '''   
        
        
        if serializer.is_valid():
            serializer.save(coach_id=coachID)
            
            partList = {}
            modulearr = []
            partList['user_id'] = coachID
            partList['module_id'] = serializer.data['id']
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            
            
            if(serializer.data['philosophy_video2']):
                partList['philosophy_video'] = serializer.data['philosophy_video2']
            else:
                partList['philosophy_video'] = ''
                
            
            if(serializer.data['technique_video2']):
                partList['technique_video'] = serializer.data['technique_video2']
            else:
                partList['technique_video'] = ''
                
                
            if(serializer.data['coach_video2']):
                partList['coach_video'] = serializer.data['coach_video2']
            else:
                partList['coach_video'] = ''
                
                
            if(serializer.data['philosophy_video_thumb2']):
                partList['philosophy_video_thumb'] = serializer.data['philosophy_video_thumb2']
            else:
                partList['philosophy_video_thumb'] = ''
                
            if(serializer.data['technique_video_thumb2']):
                partList['technique_video_thumb'] = serializer.data['technique_video_thumb2']
            else:
                partList['technique_video_thumb'] = ''
                
            if(serializer.data['coach_video_thumb2']):
                partList['coach_video_thumb'] = serializer.data['coach_video_thumb2']
            else:
                partList['coach_video_thumb'] = ''
            
            
            partList['philosophy_video_updated'] = serializer.data['philosophy_video_updated']
            partList['technique_video_updated'] = serializer.data['technique_video_updated']
            partList['coach_video_updated'] = serializer.data['coach_video_updated']
            
            
            modulearr.append(partList)
            
            return Response({"status":True, "response_msg": "Module video upload successfully", "module":modulearr })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)

#ios end




#Edti moduel by coach
class GetUpdateModule(APIView):
    serializer_class = ModuleSerializer
    def get(self, request,coachID,pk, format=None):
        try:
            snippets = Coachmodule.objects.filter(pk=pk)
            serializer = ModuleSerializer(snippets,many=True)
            if(snippets):
                modulearr = []
                for each in serializer.data:
                    partList = {}
                    partList['id'] = each['id']
                    partList['module_name'] = each['module_name']
                    
                    current_url = request.get_host()
                    if request.is_secure():
                        current_url = 'https://' + current_url
                    else:
                        current_url = 'http://' + current_url
                       
                    partList['philosophy_video_updated'] = each['philosophy_video_updated']
                    partList['technique_video_updated'] = each['technique_video_updated']
                    partList['coach_video_updated'] = each['coach_video_updated']
                    
                    if(each['philosophy_video']):
                        #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                        #if os.path.exists(philosophyvideo):
                        partList['philosophy_video'] = each['philosophy_video']
                        #else:
                            #partList['philosophy_video'] = ''
                    elif(each['philosophy_video2']):
                        partList['philosophy_video'] = each['philosophy_video2']
                    else:
                        partList['philosophy_video'] = ''
                      
                    if(each['philosophy_video_thumb']):
                        #philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                        #if os.path.exists(philosophy_video_thumb):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                        #else:
                            #partList['philosophy_video_thumb'] = ''
                    elif(each['philosophy_video_thumb2']):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                    else:
                        partList['philosophy_video_thumb'] = ''
                    
                    
                    if(each['technique_video']):
                        #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                        #if os.path.exists(techniquevideo):
                        partList['technique_video'] = each['technique_video']
                        #else:
                            #partList['technique_video'] = ''
                    elif(each['technique_video2']):
                        partList['technique_video'] = each['technique_video2']
                    else:
                        partList['technique_video'] = ''
                        
                    
                    if(each['technique_video_thumb']):
                        #technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                        #if os.path.exists(technique_video_thumb):
                        partList['technique_video_thumb'] = each['technique_video_thumb']
                        #else:
                            #partList['technique_video_thumb'] = ''
                    elif(each['technique_video_thumb2']):
                        partList['technique_video_thumb'] = each['technique_video_thumb2']
                    else:
                        partList['technique_video_thumb'] = ''
                        
                    
                    if(each['coach_video']):
                        #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                        #if os.path.exists(coachvideo):
                        partList['coach_video'] = each['coach_video']
                        #else:
                            #partList['coach_video'] = ''
                    elif(each['coach_video2']):
                        partList['coach_video'] = each['coach_video2']
                    else:
                        partList['coach_video'] = ''
                        
                    
                    if(each['coach_video_thumb']):
                        #coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                        #if os.path.exists(coach_video_thumb):
                        partList['coach_video_thumb'] = each['coach_video_thumb']
                        #else:
                            #partList['coach_video_thumb'] = ''
                    elif(each['coach_video_thumb2']):
                        partList['coach_video_thumb'] = each['coach_video_thumb2']
                    else:
                        partList['coach_video_thumb'] = ''
                    
                    partList['description'] = each['description']
                    partList['module_status'] = each['module_status']
                    #partList['links'] = each.links
                    partList['paid_free'] = each['paid_free']
                    partList['module_price'] = each['module_price']
                    partList['see_module'] = each['see_module']
                    
                    taglist = each['tag']
                    
                    tagobj = Tag.objects.filter(pk__in=taglist)
                    
                    tagarr = []
                    for tag_val in tagobj:
                        tagarr.append(tag_val.id)
                        
                    partList['tag'] = tagarr
                    
                    linklist = Coachmodulelink.objects.filter(module_id=pk)
                    linkarr = []
                    for linkval in linklist:
                        linkParlist = {}
                        linkParlist['id'] = linkval.id
                        linkParlist['module_id'] = pk
                        linkParlist['link'] = linkval.link
                        #linkParlist['mark_as'] = linkval.mark_as
                        linkarr.append(linkParlist)
                        
                    partList['links'] = linkarr
                    
                    modulearr.append(partList)
                    
                
                return Response({"response_code":"0", "response_msg": "List module", "module":modulearr })
        except Coachmodule.DoesNotExist:
            raise Http404
    def post(self, request, coachID,pk, format=None):
        snippets = Coachmodule.objects.get(pk=pk)
        serializer = ModuleSerializer(snippets, data=request.data)
        
        notiflagstatus = snippets.module_status
    
        module_name = snippets.module_name
        post_module_name = request.POST.get('module_name')
        if(post_module_name == '' ):
            if(module_name == ''):
                module_status = 'draft'
                return Response({"response_code":"104", "response_msg": "Please enter the module title", "module":request.data })
        
        tag = snippets.tag.all().count()
        post_tag = request.POST.get('tag')
        
        philosophy_video = str(snippets.philosophy_video)
        technique_video = str(snippets.technique_video)
        coach_video = str(snippets.coach_video)
        
        philosophy_video2 = str(snippets.philosophy_video2)
        
        #and (coach_video != '' or request.FILES.get('coach_video', None) )
        #and  (technique_video != '' or request.FILES.get('technique_video', None))
        
        post_see_module = request.POST.get('see_module')
        see_module = snippets.see_module
        
        #return Response({"response_code":"0", "see_module": see_module, "post_see_module":post_see_module })
        
        if ((tag != 0 or post_tag != None) and (philosophy_video != '' or request.FILES.get('philosophy_video', None)) and (post_see_module != '' or see_module != '') ):
            module_status = 'publish'
        elif ((tag != 0 or post_tag != None) and (philosophy_video2 != '' or request.POST.get('philosophy_video')) and (post_see_module != '' or see_module != '') ):
            module_status = 'publish'
        else:
            module_status = 'draft'
        
        
        if serializer.is_valid():
            ios_moduleid = 'MODULES_'+pk
            serializer.save(coach_id=coachID,module_status=module_status,ios_moduleid=ios_moduleid)
            
            if(module_status == 'publish'):
                gamification.comman_function.update_user_data(coachID,'publishlession',True)

            partList = {}
            modulearr = []
            partList['id'] = serializer.data['id']
            partList['module_name'] = serializer.data['module_name']
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            
            
            partList['philosophy_video_updated'] = serializer.data['philosophy_video_updated']
            partList['technique_video_updated'] = serializer.data['technique_video_updated']
            partList['coach_video_updated'] = serializer.data['coach_video_updated']
                
            if(serializer.data['philosophy_video']):
                #philosophyvideo = settings.BASE_DIR + str(serializer.data['philosophy_video'])
                #if os.path.exists(philosophyvideo):
                partList['philosophy_video'] = serializer.data['philosophy_video']
                #else:
                    #partList['philosophy_video'] = ''
            elif(serializer.data['philosophy_video2']):
                partList['philosophy_video'] = serializer.data['philosophy_video2']
            else:
                partList['philosophy_video'] = ''
                
            
            if(serializer.data['technique_video']):
                #techniquevideo = settings.BASE_DIR + str(serializer.data['technique_video'])
                #if os.path.exists(techniquevideo):
                partList['technique_video'] = serializer.data['technique_video']
                #else:
                    #partList['technique_video'] = ''
            elif(serializer.data['technique_video2']):
                partList['technique_video'] = serializer.data['technique_video2']
            else:
                partList['technique_video'] = ''
                
                
            if(serializer.data['coach_video']):
                #coachvideo = settings.BASE_DIR + str(serializer.data['coach_video'])
                #if os.path.exists(coachvideo):
                partList['coach_video'] = serializer.data['coach_video']
                #else:
                    #partList['coach_video'] = ''
            elif(serializer.data['coach_video2']):
                partList['coach_video'] = serializer.data['coach_video2']
            else:
                partList['coach_video'] = ''
                
                
            if(serializer.data['philosophy_video_thumb']):
                #philosophy_video_thumb = settings.BASE_DIR + str(serializer.data['philosophy_video_thumb'])
                #if os.path.exists(philosophy_video_thumb):
                partList['philosophy_video_thumb'] = serializer.data['philosophy_video_thumb']
                #else:
                    #partList['philosophy_video_thumb'] = ''
            elif(serializer.data['philosophy_video_thumb2']):
                partList['philosophy_video_thumb'] = serializer.data['philosophy_video_thumb2']
            else:
                partList['philosophy_video_thumb'] = ''
                
            if(serializer.data['technique_video_thumb']):
                #technique_video_thumb = settings.BASE_DIR + str(serializer.data['technique_video_thumb'])
                #if os.path.exists(technique_video_thumb):
                partList['technique_video_thumb'] = serializer.data['technique_video_thumb']
                #else:
                    #partList['technique_video_thumb'] = ''
            elif(serializer.data['technique_video_thumb2']):
                partList['technique_video_thumb'] = serializer.data['technique_video_thumb2']
            else:
                partList['technique_video_thumb'] = ''
                
            if(serializer.data['coach_video_thumb']):
                #coach_video_thumb = settings.BASE_DIR + str(serializer.data['coach_video_thumb'])
                #if os.path.exists(coach_video_thumb):
                partList['coach_video_thumb'] = serializer.data['coach_video_thumb']
                #else:
                    #partList['coach_video_thumb'] = ''
            elif(serializer.data['coach_video_thumb2']):
                partList['coach_video_thumb'] = serializer.data['coach_video_thumb2']
            else:
                partList['coach_video_thumb'] = ''
                

            partList['description'] = serializer.data['description']
            partList['see_module'] = serializer.data['see_module']
            partList['module_status'] = module_status
            
            #partList['links'] = each.links
            partList['paid_free'] = serializer.data['paid_free']
            
            if(serializer.data['tag']):
                taglist = serializer.data['tag']
                partList['tag'] = taglist
            else:
                partList['tag'] = ''
                
            modulearr.append(partList)
            
            r = ''
            if(module_status =='publish' and notiflagstatus == 'draft'):
                
                Useridarr = []
            
                requestobj = StudentCoachRequest.objects.filter(Q(user=coachID), Q(status='Approve') )
                if(requestobj.count() > 0):
                    for requestval in requestobj:
                        Useridarr.append(requestval.user_to_id)
                
                requestobj2 = StudentCoachRequest.objects.filter(Q(user_to=coachID), Q(status='Approve') )
                if(requestobj2.count() > 0):
                    for requestval in requestobj2:
                        Useridarr.append(requestval.user_id)
                
                #2018
                if(serializer.data['see_module'] != settings.MY_CHOICES_ARR[3] ):
                    #Private - Only visible to me
                    
                    for user_id in Useridarr:
                        
                        #check user settings
                        notificationuserobj = StudentCoach.objects.filter(pk=int(user_id)).values('noti_coach_publish_new_module')
                        noti_coach_publish_new_module = notificationuserobj[0]['noti_coach_publish_new_module']
                        
                        if(noti_coach_publish_new_module == '1' and notificationuserobj.count() > 0 ):
                        
                            #add notification
                            type_of_notification = settings.NOTIFI_TYPE_ARR[7] #NewModuleCreated
                            main_id = serializer.data['id']
                            notify_to_id = user_id
                            title = serializer.data['module_name']
                            notiobj = Notification(user_id_id=coachID,notify_to_id=notify_to_id,type_of_notification=type_of_notification,main_id=main_id,title=title)
                            notiobj.save()
                            
                            #send pushnotification
                            device = FCMDevice.objects.filter(user=notify_to_id)
                            message = settings.NOTIFI_TYPE_MESSAGE[7] #created a new module
                            
                            
                            userobj = StudentCoach.objects.filter(pk=coachID).values('first_name','last_name')
                            notititle = userobj[0]['first_name'] + " " +userobj[0]['last_name']
                            if(device.count() > 0):
                                r = device.send_message(title=notititle, body=message,data={"main_id": main_id,"type_of_notification":type_of_notification,"id":notiobj.id})
                            #add notification end
                            
                            
                #2018
            
                    
            return Response({"response_code":"0", "response_msg": "Module update successfully", "module":modulearr })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)

#delete module
class DeleteModule(APIView):
    serializer_class = CreateModuleSerializer
    def get(self, request,coachID,pk, format=None):
        try:
            deletemodule = Coachmodule.objects.get(pk=pk)
            deletemodule.delete()
            
            gamification.comman_function.update_user_data(coachID,'publishlession',False)
            return Response({"status":True, "response_msg": "Module delete successfully", "module":{} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "module": {} })
        

#add Module link by coach
class CreateModuleLink(APIView):
    serializer_class = ModuleLinkSerializer
    def post(self, request,coachID,moduleID, format=None):
        link = request.POST.get('link')
        if (link == ''):
            return JsonResponse({"status":False, "response_msg": "Please enter homework task & links ", "link": {} })
        
        serializer = ModuleLinkSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status":True, "response_msg": "Link and homework task added successfully", "link":serializer.data })
            #return JsonResponse({"response_code":"1", "response_msg": "Please enter required field", "module": {} })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)

#delete Module link by coach
class DeleteModuleLink(APIView):
    serializer_class = ModuleLinkSerializer
    def get(self, request,coachID,moduleID,pk, format=None):
        try:
            link = Coachmodulelink.objects.get(pk=pk)
            link.delete()
            return Response({"status":True, "response_msg": "Link and homework task delete successfully", "link":{} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "link": {} })
        

#list my module by coach
class ListMyModule(APIView):
    serializer_class = ModuleSerializer
    def get(self, request,coachID, format=None):
        try:
            moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish') )
            serializer = ModuleSerializer(moduleobj, many=True)
            if(moduleobj):
                modulearr = []
                for each in serializer.data:
                    partList = {}
                    partList['id'] = each['id']
                    partList['module_name'] = each['module_name']
                    
                    current_url = request.get_host()
                    if request.is_secure():
                        current_url = 'https://' + current_url
                    else:
                        current_url = 'http://' + current_url
                    
                    
                    partList['philosophy_video_updated'] = each['philosophy_video_updated']
                    partList['technique_video_updated'] = each['technique_video_updated']
                    partList['coach_video_updated'] = each['coach_video_updated']
                
                    if(each['philosophy_video']):
                        #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                        #if os.path.exists(philosophyvideo):
                        partList['philosophy_video'] = each['philosophy_video']
                        #else:
                            #partList['philosophy_video'] = ''
                    elif(each['philosophy_video']):
                        partList['philosophy_video'] = each['philosophy_video2']
                    else:
                        partList['philosophy_video'] = ''
                        
                    
                    if(each['technique_video']):
                        #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                        #if os.path.exists(techniquevideo):
                        partList['technique_video'] = each['technique_video']
                        #else:
                            #partList['technique_video'] = ''
                    elif(each['technique_video2']):
                        partList['technique_video'] = each['technique_video2']
                    else:
                        partList['technique_video'] = ''
                        
                        
                    if(each['coach_video']):
                        #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                        #if os.path.exists(coachvideo):
                        partList['coach_video'] = each['coach_video']
                        #else:
                            #partList['coach_video'] = ''
                    elif(each['coach_video2']):
                        partList['coach_video'] = each['coach_video2']
                    else:
                        partList['coach_video'] = ''
                        
                        
                    if(each['philosophy_video_thumb']):
                        
                        #https://app.keyy.io.s3.amazonaws.com/media/coach/philosophy_video/thumb/1511883182253.jpg.jpg
                        #each['philosophy_video_thumb'].replace("https","http")
                        #philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                        #if os.path.exists(philosophy_video_thumb):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                        #else:
                            #partList['philosophy_video_thumb'] = ''
                    elif(each['philosophy_video_thumb2']):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                    else:
                        partList['philosophy_video_thumb'] = ''
                        
                    if(each['technique_video_thumb']):
                        #technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                        #if os.path.exists(technique_video_thumb):
                        partList['technique_video_thumb'] = each['technique_video_thumb']
                        #else:
                            #partList['technique_video_thumb'] = ''
                    elif(each['technique_video_thumb2']):
                        partList['technique_video_thumb'] = each['technique_video_thumb2']
                    else:
                        partList['technique_video_thumb'] = ''
                        
                    if(each['coach_video_thumb']):
                        #coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                        #if os.path.exists(coach_video_thumb):
                        partList['coach_video_thumb'] = each['coach_video_thumb']
                        #else:
                            #partList['coach_video_thumb'] = ''
                    elif(each['coach_video_thumb2']):
                        partList['coach_video_thumb'] = each['coach_video_thumb2']
                    else:
                        partList['coach_video_thumb'] = ''
                        
    
                    partList['description'] = each['description']
                    partList['see_module'] = each['see_module']
                    partList['module_status'] = each['module_status']
                    
                    #partList['links'] = each.links
                    partList['paid_free'] = each['paid_free']
                    
                    taglist = each['tag']
                    tagobj = Tag.objects.filter(pk__in=taglist)
                    tagarr = []
                    for tag_val in tagobj:
                        partagList = {}
                        partagList['tag_id'] = tag_val.id
                        partagList['tag_title'] = tag_val.tag_title
                        tagarr.append(partagList)
                    
                    partList['tag'] = tagarr
                    modulearr.append(partList)
                    
                return Response({"response_code":"0", "response_msg": "List my module", "module":modulearr })
            return Response({"response_code":"1", "response_msg": "Data not found", "module": {} })
        except Coachmodule.DoesNotExist:
            return Response({"response_code":"1", "response_msg": "Data not found", "module": {} })
        

#CoachModuleList
class CoachModuleList(APIView):
    serializer_class = CoachModuleListSerializer
    def get(self, request,coachID, format=None):
        try:
            #check user pro or free
            userobj = StudentCoach.objects.filter(pk=coachID).values('subscription','subscription_invite','subscription_expiredate')
            if(userobj[0]['subscription'] == True):
                subscription = True
            elif (userobj[0]['subscription_invite'] == True):
                subscription = True
            else:
                subscription = False
                
            '''
            if(subscription == False):
                moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish'), ~Q(status='Archive') ).order_by('-pub_date')
            else:
                moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish') ).order_by('-pub_date')
                
            '''
            
            moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish'), ~Q(status='Archive') ).order_by('-pub_date')
            serializer = CoachModuleListSerializer(moduleobj,many=True)
            if(moduleobj):
                modulearr = []
                for each in serializer.data:
                    partList = {}
                    partList['id'] = each['id']
                    partList['module_name'] = each['module_name']
                    
                    current_url = request.get_host()
                    if request.is_secure():
                        current_url = 'https://' + current_url
                    else:
                        current_url = 'http://' + current_url
                    
                    partList['philosophy_video_updated'] = each['philosophy_video_updated']
                    partList['technique_video_updated'] = each['technique_video_updated']
                    partList['coach_video_updated'] = each['coach_video_updated']

                    if(each['philosophy_video']):
                        #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                        #if os.path.exists(philosophyvideo):
                        partList['philosophy_video'] = each['philosophy_video']
                        #else:
                            #partList['philosophy_video'] = '
                    elif(each['philosophy_video2']):
                        partList['philosophy_video'] = each['philosophy_video2']
                    else:
                        partList['philosophy_video'] = ''
                        
                    
                    if(each['technique_video']):
                        #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                        #if os.path.exists(techniquevideo):
                        partList['technique_video'] = each['technique_video']
                        #else:
                            #partList['technique_video'] = ''
                    elif(each['technique_video2']):
                        partList['technique_video'] = each['technique_video2']
                    else:
                        partList['technique_video'] = ''
                        
                        
                    if(each['coach_video']):
                        #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                        #if os.path.exists(coachvideo):
                        partList['coach_video'] = each['coach_video']
                        #else:
                            #partList['coach_video'] = ''
                    elif(each['coach_video2']):
                        partList['coach_video'] = each['coach_video2']
                    else:
                        partList['coach_video'] = ''
                        
                        
                    if(each['philosophy_video_thumb']):
                        #philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                        #if os.path.exists(philosophy_video_thumb):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                        #else:
                            #partList['philosophy_video_thumb'] = ''
                    elif(each['philosophy_video_thumb2']):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                    else:
                        partList['philosophy_video_thumb'] = ''
                        
                    if(each['technique_video_thumb']):
                        #technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                        #if os.path.exists(technique_video_thumb):
                        partList['technique_video_thumb'] = each['technique_video_thumb']
                        #else:
                            #partList['technique_video_thumb'] = ''
                    elif(each['technique_video_thumb2']):
                        partList['technique_video_thumb'] = each['technique_video_thumb2']
                    else:
                        partList['technique_video_thumb'] = ''
                        
                    if(each['coach_video_thumb']):
                        #coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                        #if os.path.exists(coach_video_thumb):
                        partList['coach_video_thumb'] = each['coach_video_thumb']
                        #else:
                            #partList['coach_video_thumb'] = ''
                    elif(each['coach_video_thumb2']):
                        partList['coach_video_thumb'] = each['coach_video_thumb2']
                    else:
                        partList['coach_video_thumb'] = ''
                        
    
                    partList['description'] = each['description']
                    partList['see_module'] = each['see_module']
                    partList['module_status'] = each['module_status']
                    partList['paid_free'] = each['paid_free']
                    
                    taglist = each['tag']
                    
                    tagobj = Tag.objects.filter(pk__in=taglist)
                    
                    tagarr = []
                    for tag_val in tagobj:
                        partagList = {}
                        partagList['tag_id'] = tag_val.id
                        partagList['tag_title'] = tag_val.tag_title
                        tagarr.append(partagList)
                    
                    partList['tag'] = tagarr
                    
                    linklist = Coachmodulelink.objects.filter(module_id=each['id'])
                    linkarr = []
                    for linkval in linklist:
                        linkParlist = {}
                        linkParlist['id'] = linkval.id
                        linkParlist['module_id'] = linkval.module_id_id
                        linkParlist['link'] = linkval.link
                        linkarr.append(linkParlist)
                        
                    partList['links'] = linkarr
                    
                    modulearr.append(partList)
                    
                    
                return Response({"status":True, "response_msg": "Coach Module List", "module":modulearr })
            return JsonResponse({"status":False, "response_msg": "Data not found", "module": {} })
        except Coachmodule.DoesNotExist:
            return JsonResponse({"status":False, "response_msg": "Data not found", "module": {} })
        
        


   
#search my module and other coach public module
class SearchCoachModuleByCoach(APIView):
    serializer_class = CoachModuleListSerializer
    def get(self, request,coachID, format=None):
        try:
            search = request.GET['search']
            
            #check user pro or free
            userobj = StudentCoach.objects.filter(pk=coachID).values('subscription','subscription_invite','subscription_expiredate')
            if(userobj[0]['subscription'] == True):
                subscription = True
            elif (userobj[0]['subscription_invite'] == True):
                subscription = True
            else:
                subscription = False
                
            '''
            if(subscription == False):
                if(search != ''):
                    moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish'), Q(module_name__icontains=search)).order_by('-pub_date')[:30]
                else:
                    moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish')).order_by('-pub_date')[:30]
                    
            else:
                moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish') ).order_by('-pub_date')
            '''
                
            
            if(search != ''):
                moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish'), Q(module_name__icontains=search), ~Q(status='Archive')).order_by('-pub_date')
            else:
                moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish'), ~Q(status='Archive') ).order_by('-pub_date')
            
            serializer = CoachModuleListSerializer(moduleobj,many=True)
            if(moduleobj):
                modulearr = []
                for each in serializer.data:
                    partList = {}
                    
                    
                    if(each['coach'] == int(coachID)):
                        Show = True
                    elif(each['coach'] != coachID):
                        if(each['see_module'] == 'Public'):
                            Show = True
                        else:
                            Show = False
                    
                    
                
                    if(Show == True):
                        partList['id'] = each['id']
                        partList['module_name'] = each['module_name']
                        
                        current_url = request.get_host()
                        if request.is_secure():
                            current_url = 'https://' + current_url
                        else:
                            current_url = 'http://' + current_url
                        
                        
                        partList['philosophy_video_updated'] = each['philosophy_video_updated']
                        partList['technique_video_updated'] = each['technique_video_updated']
                        partList['coach_video_updated'] = each['coach_video_updated']
                        
                        if(each['philosophy_video']):
                            #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                            #if os.path.exists(philosophyvideo):
                            partList['philosophy_video'] = each['philosophy_video']
                            #else:
                                #partList['philosophy_video'] = ''
                        elif(each['philosophy_video2']):
                            partList['philosophy_video'] = each['philosophy_video2']
                        else:
                            partList['philosophy_video'] = ''
                            
                        
                        if(each['technique_video']):
                            #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                            #if os.path.exists(techniquevideo):
                            partList['technique_video'] = each['technique_video']
                            #else:
                                #partList['technique_video'] = ''
                        elif(each['technique_video2']):
                            partList['technique_video'] = each['technique_video2']
                        else:
                            partList['technique_video'] = ''
                            
                            
                        if(each['coach_video']):
                            #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                            #if os.path.exists(coachvideo):
                            partList['coach_video'] = each['coach_video']
                            #else:
                                #partList['coach_video'] = ''
                        elif(each['coach_video2']):
                            partList['coach_video'] = each['coach_video2']
                        else:
                            partList['coach_video'] = ''
                            
                            
                        if(each['philosophy_video_thumb']):
                            #philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                            #if os.path.exists(philosophy_video_thumb):
                            partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                            #else:
                                #partList['philosophy_video_thumb'] = ''
                        elif(each['philosophy_video_thumb2']):
                            partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                        else:
                            partList['philosophy_video_thumb'] = ''
                            
                        if(each['technique_video_thumb']):
                            #technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                            #if os.path.exists(technique_video_thumb):
                            partList['technique_video_thumb'] = each['technique_video_thumb']
                            #else:
                                #partList['technique_video_thumb'] = ''
                        elif(each['technique_video_thumb2']):
                            partList['technique_video_thumb'] = each['technique_video_thumb2']
                        else:
                            partList['technique_video_thumb'] = ''
                            
                        if(each['coach_video_thumb']):
                            #coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                            #if os.path.exists(coach_video_thumb):
                            partList['coach_video_thumb'] = each['coach_video_thumb']
                            #else:
                                #partList['coach_video_thumb'] = ''
                        elif(each['coach_video_thumb2']):
                            partList['coach_video_thumb'] = each['coach_video_thumb2']
                        else:
                            partList['coach_video_thumb'] = ''
                            
        
                        partList['description'] = each['description']
                        partList['see_module'] = each['see_module']
                        partList['module_status'] = each['module_status']
                        partList['paid_free'] = each['paid_free']
                        
                        taglist = each['tag']
                        tagobj = Tag.objects.filter(pk__in=taglist)
                        tagarr = []
                        for tag_val in tagobj:
                            partagList = {}
                            partagList['tag_id'] = tag_val.id
                            partagList['tag_title'] = tag_val.tag_title
                            tagarr.append(partagList)
                        
                        partList['tag'] = tagarr
                        
                        linklist = Coachmodulelink.objects.filter(module_id=each['id'])
                        linkarr = []
                        for linkval in linklist:
                            linkParlist = {}
                            linkParlist['id'] = linkval.id
                            linkParlist['module_id'] = linkval.module_id_id
                            linkParlist['link'] = linkval.link
                            linkarr.append(linkParlist)
                            
                        partList['links'] = linkarr
                        
                        
                        coachobj = StudentCoach.objects.filter(pk=each['coach'])
                        serializer2 = ListCoachModuleSerializer(coachobj,many=True)
                        coacharr = []
                        for coachval in serializer2.data:
                            coachParlist = {}
                            coachParlist['id'] = coachval['id']
                            coachParlist['first_name'] = coachval['first_name']
                            coachParlist['last_name'] = coachval['last_name']
                            coachParlist['user_name'] = coachval['user_name']
                            coachParlist['training_location'] = coachval['training_location']
                            coachParlist['updated'] = coachval['updated']
                            
                            if(coachval['profile_picture']):
                                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(coachval.profile_picture)
                                #if os.path.isfile(profile_picture):
                                profile_picture = coachval['profile_picture']
                                #else:
                                    #profile_picture = ''
                            elif(coachval['profile_picture2']):
                                profile_picture = coachval['profile_picture2']
                            elif (coachval['hidden_profile_picture'] !=''): #and coachval['social_login'] == 'True'
                                profile_picture = coachval['hidden_profile_picture']
                            else:
                                profile_picture = ''
                                
                            coachParlist['profile_picture'] = profile_picture
                            
                            coacharr.append(coachParlist)
                        partList['coachobj'] = coacharr
                
                        
                        modulearr.append(partList)
                    
                    
                return Response({"status":True, "response_msg": "Coach Module List", "module":modulearr })
            return JsonResponse({"status":False, "response_msg": "Data not found", "module": {} })
        except Coachmodule.DoesNotExist:
            return JsonResponse({"status":False, "response_msg": "Data not found", "module": {} })




#list my draft module 
class ListDraftModule(APIView):
    serializer_class = ModuleSerializer
    def get(self, request,coachID, format=None):
        try:
            moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='draft') ).order_by('-pub_date')
            serializer = ModuleSerializer(moduleobj,many=True)
            if(moduleobj):
                modulearr = []
                for each in serializer.data:
                    partList = {}
                    partList['id'] = each['id']
                    partList['module_name'] = each['module_name']
                    
                    current_url = request.get_host()
                    if request.is_secure():
                        current_url = 'https://' + current_url
                    else:
                        current_url = 'http://' + current_url
                    
                    
                    partList['philosophy_video_updated'] = each['philosophy_video_updated']
                    partList['technique_video_updated'] = each['technique_video_updated']
                    partList['coach_video_updated'] = each['coach_video_updated']
                    
                    if(each['philosophy_video']):
                        #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                        #if os.path.exists(philosophyvideo):
                        partList['philosophy_video'] = each['philosophy_video']
                        #else:
                            #partList['philosophy_video'] = ''
                    elif(each['philosophy_video2']):
                        partList['philosophy_video'] = each['philosophy_video2']
                    else:
                        partList['philosophy_video'] = ''
                        
                    
                    if(each['technique_video']):
                        #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                        #if os.path.exists(techniquevideo):
                        partList['technique_video'] = each['technique_video']
                        #else:
                            #partList['technique_video'] = ''
                    elif(each['technique_video2']):
                        partList['technique_video'] = each['technique_video2']
                    else:
                        partList['technique_video'] = ''
                        
                        
                    if(each['coach_video']):
                        #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                        #if os.path.exists(coachvideo):
                        partList['coach_video'] = each['coach_video']
                        #else:
                            #partList['coach_video'] = ''
                    elif(each['coach_video2']):
                        partList['coach_video'] = each['coach_video2']
                    else:
                        partList['coach_video'] = ''
                        
                        
                    if(each['philosophy_video_thumb']):
                        #philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                        #if os.path.exists(philosophy_video_thumb):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                        #else:
                            #partList['philosophy_video_thumb'] = ''
                    elif(each['philosophy_video_thumb2']):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                    else:
                        partList['philosophy_video_thumb'] = ''
                        
                    if(each['technique_video_thumb']):
                        #technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                        #if os.path.exists(technique_video_thumb):
                        partList['technique_video_thumb'] = each['technique_video_thumb']
                        #else:
                            #partList['technique_video_thumb'] = ''
                    elif(each['technique_video_thumb2']):
                        partList['technique_video_thumb'] = each['technique_video_thumb2']
                    else:
                        partList['technique_video_thumb'] = ''
                        
                    if(each['coach_video_thumb']):
                        #coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                        #if os.path.exists(coach_video_thumb):
                        partList['coach_video_thumb'] = each['coach_video_thumb']
                        #else:
                            #partList['coach_video_thumb'] = ''
                    elif(each['coach_video_thumb2']):
                        partList['coach_video_thumb'] = each['coach_video_thumb2']
                    else:
                        partList['coach_video_thumb'] = ''
                        
    
                    
                    
                    partList['description'] = each['description']
                    partList['see_module'] = each['see_module']
                    partList['module_status'] = each['module_status']
                    partList['paid_free'] = each['paid_free']
                    
                    taglist = each['tag']
                    tagobj = Tag.objects.filter(pk__in=taglist)
                    tagarr = []
                    for tag_val in tagobj:
                        partagList = {}
                        partagList['tag_id'] = tag_val.id
                        partagList['tag_title'] = tag_val.tag_title
                        tagarr.append(partagList)
                    
                    partList['tag'] = tagarr
                    modulearr.append(partList)
                    
                    
                    linklist = Coachmodulelink.objects.filter(module_id=each['id'])
                    linkarr = []
                    for linkval in linklist:
                        linkParlist = {}
                        linkParlist['id'] = linkval.id
                        linkParlist['module_id'] = linkval.module_id_id
                        linkParlist['link'] = linkval.link
                        linkarr.append(linkParlist)
                        
                    partList['links'] = linkarr
                    
                    
                return Response({"response_code":"0", "response_msg": "List draft module", "module":modulearr })
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "module": {} })
        except Coachmodule.DoesNotExist:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "module": {} })

#list all module and select 30 module
class Select30Module(APIView):
    serializer_class = CoachModuleSelectSerializer
    def get(self, request,coachID, format=None):
        try:
            moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish'), ~Q(status='Archive') ).order_by('-pub_date')
            serializer = CoachModuleSelectSerializer(moduleobj,many =True)
            if(moduleobj):
                modulearr = []
                for each in serializer.data:
                    partList = {}
                    partList['id'] = each['id']
                    partList['module_name'] = each['module_name']
                    
                    current_url = request.get_host()
                    if request.is_secure():
                        current_url = 'https://' + current_url
                    else:
                        current_url = 'http://' + current_url
                    
                    
                    partList['philosophy_video_updated'] = each['philosophy_video_updated']
                    partList['technique_video_updated'] = each['technique_video_updated']
                    partList['coach_video_updated'] = each['coach_video_updated']
                    
                    if(each['philosophy_video']):
                        #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                        #if os.path.exists(philosophyvideo):
                        partList['philosophy_video'] = each['philosophy_video']
                        #else:
                            #partList['philosophy_video'] = ''
                    elif(each['philosophy_video2']):
                        partList['philosophy_video'] = each['philosophy_video2']
                    else:
                        partList['philosophy_video'] = ''
                        
                    
                    if(each['technique_video']):
                        #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                        #if os.path.exists(techniquevideo):
                        partList['technique_video'] = each['technique_video']
                        #else:
                            #partList['technique_video'] = ''
                    elif(each['technique_video2']):
                        partList['technique_video'] = each['technique_video2']
                    else:
                        partList['technique_video'] = ''
                        
                        
                    if(each['coach_video']):
                        #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                        #if os.path.exists(coachvideo):
                        partList['coach_video'] = each['coach_video']
                        #else:
                            #partList['coach_video'] = ''
                    elif(each['coach_video2']):
                        partList['coach_video'] = each['coach_video2']
                    else:
                        partList['coach_video'] = ''
                        
                        
                    if(each['philosophy_video_thumb']):
                        #philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                        #if os.path.exists(philosophy_video_thumb):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                        #else:
                            #partList['philosophy_video_thumb'] = ''
                    elif(each['philosophy_video_thumb2']):
                        partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                    else:
                        partList['philosophy_video_thumb'] = ''
                        
                    if(each['technique_video_thumb']):
                        #technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                        #if os.path.exists(technique_video_thumb):
                        partList['technique_video_thumb'] = each['technique_video_thumb']
                        #else:
                            #partList['technique_video_thumb'] = ''
                    elif(each['technique_video_thumb2']):
                        partList['technique_video_thumb'] = each['technique_video_thumb2']
                    else:
                        partList['technique_video_thumb'] = ''
                        
                    if(each['coach_video_thumb']):
                        #coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                        #if os.path.exists(coach_video_thumb):
                        partList['coach_video_thumb'] = each['coach_video_thumb']
                        #else:
                            #partList['coach_video_thumb'] = ''
                    elif(each['coach_video_thumb2']):
                        partList['coach_video_thumb'] = each['coach_video_thumb2']
                    else:
                        partList['coach_video_thumb'] = ''
                        
    
                    partList['description'] = each['description']
                    partList['module_status'] = each['module_status']
                    partList['paid_free'] = each['paid_free']
                    
                    taglist = each['tag']
                    tagobj = Tag.objects.filter(pk__in=taglist)
                    tagarr = []
                    for tag_val in tagobj:
                        partagList = {}
                        partagList['tag_id'] = tag_val.id
                        partagList['tag_title'] = tag_val.tag_title
                        tagarr.append(partagList)
                    
                    partList['tag'] = tagarr
                    
                    modulearr.append(partList)
                    
                    
                return Response({"status":True, "response_msg": "Coach Module List", "module":modulearr })
            return Response({"status":False, "response_msg": "Data not found", "module": {} })
        except Coachmodule.DoesNotExist:
            return JsonResponse({"status":False, "response_msg": "Data not found", "module": {} })
    def post(self, request,coachID, format=None):
        try:
            module_id = request.POST.get('module_id')
            if(module_id == ''):
                moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish'), ~Q(status='Archive') ).order_by('-pub_date')
                if(moduleobj.count()):
                    i = 1
                    for eachobj in moduleobj:
                        if (i > 30):
                            eachobj.status = 'Archive'
                            eachobj.save()
                            
                        i = i + 1
            else:
                module_idarr = module_id.split(",")
                moduleobj = Coachmodule.objects.filter(Q(coach=coachID), Q(module_status='publish'), ~Q(status='Archive') ).order_by('-pub_date')
                if(moduleobj.count()):
                    for eachobj in moduleobj:
                        if str(eachobj.id) not in module_idarr:
                            eachobj.status = 'Archive'
                            eachobj.save()
            
            
            return Response({"status":True, "response_msg": "Update successfully", "module":{} })
        except Coachmodule.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "module": {} })
        

        
        
#Assign module to my stduent by coach
class AssignModuleToMyStudent(APIView):
    serializer_class = StudentAssignmentSerializer
    def post(self, request,coachID,moduleID, format=None):
        snippets = Coachmodule.objects.get(pk=moduleID)
        serializer = StudentAssignmentSerializer(snippets,data=request.data)
        
        if serializer.is_valid():
            #studentarr = ['10','47','48'] # student id avse array ma
            student_id = request.POST.get('student_id')
            studentarr = student_id.split(",")
            
            dbstudentid = snippets.student_id
            update = True
            
            studentdbarr = []
            if(dbstudentid != ''):
                studentdbarr = dbstudentid.split(",")
                if (len(studentarr) == 1):
                    if student_id in studentdbarr:
                        update = False
                
            if(len(studentdbarr) > 0):
                finalarray = list(set(studentarr + studentdbarr))
            else:
                finalarray = studentarr
            
            
            #pro module assign logic
            notassignusername = ''
            notassignmsg = ''
            modulevisibility = snippets.see_module
            checkfinalarray = []
            if(modulevisibility == 'Only keyyPRO members'):
                checkfinalarray = finalarray
                deletearr = []
                for user_id in checkfinalarray:
                    checkuserpro = StudentCoach.objects.filter(Q(pk=int(user_id)))
                    if(checkuserpro.count() > 0 ):
                        for each in checkuserpro:
                            subscription = each.subscription
                            #subscription_invite = checkuserpro.subscription_invite
                            if(subscription == False):
                                deletearr.append(str(each.id))
                                if(notassignusername != ''):
                                    notassignusername +=', '
                                    
                                notassignusername += each.first_name+ " "+each.last_name
            
                finalarray = list(set(finalarray).symmetric_difference(set(deletearr))) 
            #pro module assign logic            
            if(notassignusername != ''):
                notassignmsg = 'You can not assign "PRO" module to these users: ' + notassignusername
            
            student_ids = ",".join(finalarray)
            
            
            if(update == True):
                serializer.save(student_id=student_ids)
                #count number of assign
                assignmodulecount = Coachmodule.objects.filter(Q(coach_id=coachID), ~Q(student_id=''))
                if(assignmodulecount.count() > 0):
                    countassignmodule = assignmodulecount.count()
                    gamification.comman_function.update_user_data(coachID,'totalassignlession',countassignmodule)
            
            else:
               return Response({"status":False, "response_msg": "Allready assign this module" }) 
            
            
            sendnotification = True
            for stud_id in finalarray:
                
                #check user settings 
                notificationuserobj = StudentCoach.objects.filter(pk=int(stud_id)).values('noti_module_assign')
                if(notificationuserobj.count() > 0):
                    noti_module_assign = notificationuserobj[0]['noti_module_assign']
                    if(noti_module_assign == '1' and notificationuserobj.count() > 0 ):
                        
                        checkusernotification = Notification.objects.filter(Q(user_id_id=int(coachID)), Q(notify_to_id=int(stud_id)), Q(main_id=int(moduleID)))
                        if(checkusernotification.count() > 0 ):
                            sendnotification = False
                        else:
                            sendnotification = True
                        
                        
                        if(sendnotification == True):
                            #add notification
                            type_of_notification = settings.NOTIFI_TYPE_ARR[5] #AssignModule
                            main_id = moduleID
                            title = snippets.module_name
                        
                            notiobj = Notification(user_id_id=coachID,notify_to_id=stud_id,type_of_notification=type_of_notification,main_id=main_id,title=title)
                            notiobj.save()
                            
                            #2018 add entery in students_studentassignunassigntrack
                            is_assign = 'Yes'
                            try:
                                assigntrackcheck = StudentAssignUnassignTrack.objects.get(Q(coach_id=coachID),Q(module_id=moduleID),Q(student_id=stud_id))
                                if(assigntrackcheck):
                                    assigntrackcheck.is_assign = 'Yes'
                                    assigntrackcheck.save()
                                
                            except:
                                assigntrack = StudentAssignUnassignTrack(is_assign=is_assign,coach_id_id=coachID,module_id_id=main_id,student_id_id=stud_id)
                                assigntrack.save()
                            
                            
                        
                            #send pushnotification
                            device = FCMDevice.objects.filter(user=stud_id)
                            message = settings.NOTIFI_TYPE_MESSAGE[5] #assign you a new module
                            r = ''
                            
                            userobj = StudentCoach.objects.filter(pk=coachID).values('first_name','last_name')
                            notititle = userobj[0]['first_name'] + " " +userobj[0]['last_name']
                            if(device.count() > 0):
                                #r = device.send_message(data={"title": notititle,"body":message})
                                r = device.send_message(title=notititle, body=message,data={"main_id": main_id,"type_of_notification":type_of_notification,"id":notiobj.id})
                            
                            
                        
                        
                        
            #add notification end
            if(notassignusername != ''):
                Status = False
            else:
                Status = True
                
            return Response({"status":Status, "response_msg": "Module assign to student","notassign_msg":notassignmsg })
        return Response({"status":False, "response_msg": "Data not found" })




class Customaddstident(APIView):
    def get(self, request, format=None):
        try:
             snippets = Coachmodule.objects.filter(Q(see_module='Private - Only visible to me'))
             if(snippets.count() > 0):
                for each in snippets:
                    studentarr = '';
                    if(each.student_id != ''):
                        studentarr = each.student_id.split(",")
                        for seach in studentarr:
                            assigntrack = StudentAssignUnassignTrack(is_assign='Yes',coach_id_id=int(each.coach_id),module_id_id=int(each.id),student_id_id=seach)
                            assigntrack.save()
                        
                return Response({"status":False, "response_msg": studentarr })
        except:
            return Response({"status":False, "response_msg": "Data not found" })
            
        

#Find student by coach
class FindStudent(APIView):
    serializer_class = ListCoachModuleSerializer
    def get(self, request,coachID, format=None):
        search = request.GET['search']
        try:
            #Q(account_type="Student"), 
            studentobj = StudentCoach.objects.filter(Q(user_name__icontains=search)|Q(first_name__icontains=search)|Q(last_name__icontains=search))
            serializer = ListCoachModuleSerializer(studentobj, many=True)
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            
            studentarr = []
            for studentval in serializer.data:
                if(studentval['id'] != int(coachID)):
                    parmList = {}
                    parmList['id'] = studentval['id']
                    parmList['first_name'] = studentval['first_name']
                    parmList['last_name'] = studentval['last_name']
                    parmList['user_name'] = studentval['user_name']
                    parmList['e_mail'] = studentval['e_mail']
                    parmList['skill'] = studentval['skill']
                    parmList['training_location'] = studentval['training_location']
                    parmList['account_type'] = studentval['account_type']
                    parmList['updated'] = studentval['updated']
                    
                    
                    #check if allready request or not 
                    requestobj = StudentCoachRequest.objects.filter(Q(user=coachID),Q(user_to=studentval['id'])).order_by('id')
                    requestobj1 = StudentCoachRequest.objects.filter(Q(user=studentval['id']),Q(user_to=coachID)).order_by('id')
                    parmList['request_status'] = False
                    if(requestobj.count() > 0):
                        for reqval in requestobj:
                            if(reqval.status == 'Reject'):
                                parmList['request_status'] = False
                            else:
                                parmList['request_status'] = True
                                
                    if(requestobj1.count() > 0):
                        for reqval1 in requestobj1:
                            if(reqval1.status == 'Reject'):
                                parmList['request_status'] = False
                            else:
                                parmList['request_status'] = True
                        
                    
                    if(studentval['profile_picture']):
                        #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(studentval.profile_picture)
                        #if os.path.isfile(profile_picture):
                        profile_picture = studentval['profile_picture']
                        #else:
                            #profile_picture = ''
                    elif(studentval['profile_picture2']):
                        profile_picture = studentval['profile_picture2']
                    elif (studentval['hidden_profile_picture'] !='' ): #and studentval['social_login'] == 'True'
                        profile_picture = studentval['hidden_profile_picture']
                    else:
                        profile_picture = ''
                        
                    parmList['profile_picture'] = profile_picture
                    studentarr.append(parmList)
                
                
            return Response({"status":True, "response_msg": "Student list","coach_id":coachID, "studentobj":studentarr })
        except StudentCoach.DoesNotExist:
            return JsonResponse({"status":False, "response_msg": "Data not found", "studentobj": {} })
        
        
        
        
#list my student who accept request
class StudentDetailPage(APIView):
    serializer_class = StudentCoachSerializer
    def get(self, request,userID, format=None):
        try:
            studentobj = StudentCoach.objects.get(pk=userID)
            serializer = StudentCoachSerializer(studentobj)
            parmList = {}
            studentarr = []
            parmList['id'] = serializer.data['id']
            parmList['first_name'] = serializer.data['first_name']
            parmList['last_name'] = serializer.data['last_name']
            parmList['user_name'] = serializer.data['user_name']
            parmList['skill'] = serializer.data['skill']
            parmList['training_location'] = serializer.data['training_location']
            parmList['subscription'] = serializer.data['subscription']
            parmList['updated'] = serializer.data['updated']
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            
            if(serializer.data['profile_picture']):
                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(studentobj.profile_picture)
                #if os.path.isfile(profile_picture):
                profile_picture = serializer.data['profile_picture']
                #else:
                    #profile_picture = ''
            elif(serializer.data['profile_picture2']):
                profile_picture = serializer.data['profile_picture2']
            elif (serializer.data['hidden_profile_picture'] !=''): #and serializer.data['social_login'] == 'True'
                profile_picture = serializer.data['hidden_profile_picture']
            
            parmList['profile_picture'] = profile_picture
            
            
            
            studentarr.append(parmList)
            
            return Response({"status":True, "response_msg": "Student detail","studentobj":studentarr })
            
        except:
            return Response({"status":False, "response_msg": "Data not found", "studentobj": {} })
            
        
#student detail page shared jounrol list who shared coah
class StudentDetailSharedJournal(APIView):
    serializer_class = StudentSharedJournalToCoachSerializer
    def get(self, request,coachID, userID, format=None):
        try:
            journalobj = Journal.objects.filter(student=userID).order_by('-date')
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
            if(journalobj.count() > 0):
                
                journalarr = []
                for each in journalobj:
                    coachid = each.coach_id
                    coachidarr = coachid.split(",")
                    
                    if coachID in coachidarr:
                        show = True
                    else:
                        show = False
                    
                    if(show == True):
                        parmList = {}
                        
                        parmList['id'] = each.id
                        parmList['journal_title'] = each.journal_title
                        parmList['place'] = each.place
                        parmList['date'] = each.date
                        parmList['journal_attachments'] = each.journal_attachments
                        parmList['today_fill'] = each.today_fill
                        parmList['journal_desc'] = each.journal_desc
                        parmList['whats_going_on_today'] = each.whats_going_on_today_id
                        
                        
                        journalarr.append(parmList)
                        
            if journalarr:
                return Response({"status":True, "response_msg": "Journal list","journalobj":journalarr })
            else:
                return Response({"status":False, "response_msg": "Data not found", "journalobj": {} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "journalobj": {} })



#list my student search by firstname lastname username
class SearchMyAcceptStudent(APIView):
    serializer_class = MyStudentCoachRequestSerializer
    def get(self, request,coachID, format=None):
        search = request.GET['search']
        try:
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
            requestarr = []
            requestobj = StudentCoachRequest.objects.filter(Q(user=coachID), Q(status='Approve') )
            
        
            if(requestobj.count() > 0):
                for requestval in requestobj:
                    studentobj = StudentCoach.objects.filter(Q(pk=requestval.user_to_id),Q(account_type='Student'), Q(user_name__icontains=search)|Q(first_name__icontains=search)|Q(last_name__icontains=search))
                    
                    
                    serializer2 = ListCoachModuleSerializer(studentobj,many=True)
                    
                    if(studentobj.count() > 0):
                        partList = {}
                        partList['id'] = requestval.id
                        partList['status'] = requestval.status
                        studentarr = []
                        for studentval in serializer2.data:
                            studentParlist = {}
                            studentParlist['id'] = studentval['id']
                            studentParlist['first_name'] = studentval['first_name']
                            studentParlist['last_name'] = studentval['last_name']
                            studentParlist['user_name'] = studentval['user_name']
                            studentParlist['training_location'] = studentval['training_location']
                            studentParlist['account_type'] = studentval['account_type']
                            studentParlist['updated'] = studentval['updated']
                            
                            if(studentval['profile_picture']):
                                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(studentval.profile_picture)
                                #if os.path.isfile(profile_picture):
                                profile_picture = studentval['profile_picture']
                                #else:
                                    #profile_picture = ''
                            elif(studentval['profile_picture2']):
                                profile_picture = studentval['profile_picture2']
                            elif (studentval['hidden_profile_picture'] !=''): #and studentval['social_login'] == 'True'
                                profile_picture = studentval['hidden_profile_picture']
                            else:
                                profile_picture = ''
                                
                            studentParlist['profile_picture'] = profile_picture
                            studentarr.append(studentParlist)
                        
                        partList['student'] = studentarr
                        requestarr.append(partList)
                
            
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=coachID), Q(status='Approve') )
            if(requestobj1.count() > 0):
            
                studentarr1 = []
                for requestval1 in requestobj1:
                    studentobj = StudentCoach.objects.filter(Q(pk=requestval1.user_id),Q(account_type='Student'), Q(user_name__icontains=search)|Q(first_name__icontains=search)|Q(last_name__icontains=search))
                    
                    serializer3 = ListCoachModuleSerializer(studentobj,many=True)
                    if(studentobj.count() > 0):
                        partList = {}
                        partList['id'] = requestval1.id
                        partList['status'] = requestval1.status
                        for studentval in serializer3.data:
                            studentParlist1 = {}
                            studentParlist1['id'] = studentval['id']
                            studentParlist1['first_name'] = studentval['first_name']
                            studentParlist1['last_name'] = studentval['last_name']
                            studentParlist1['user_name'] = studentval['user_name']
                            studentParlist1['training_location'] = studentval['training_location']
                            studentParlist1['account_type'] = studentval['account_type']
                            studentParlist1['updated'] = studentval['updated']
                            
                            if(studentval['profile_picture']):
                                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(studentval.profile_picture)
                                #if os.path.isfile(profile_picture):
                                profile_picture = studentval['profile_picture']
                                #else:
                                    #profile_picture = ''
                            elif(studentval['profile_picture2']):
                                profile_picture = studentval['profile_picture2']
                            elif (studentval['hidden_profile_picture'] !=''): #and studentval['social_login'] == 'True'
                                profile_picture = studentval['hidden_profile_picture']
                            else:
                                profile_picture = ''
                                
                            studentParlist1['profile_picture'] = profile_picture
                            studentarr1.append(studentParlist1)
                        
                        partList['student'] = studentarr1
                        requestarr.append(partList)
            
                    
            return Response({"status":True, "response_msg": "Student list","coach_id":coachID, "requestobj":requestarr })
            #return Response({"status":False, "response_msg": "Data not found", "requestobj": {} })
        except StudentCoachRequest.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "requestobj": {} })
        
        

#student section
# MKT Add Get RecommendedModule module by student
class ListRecommendedModule(APIView):
    serializer_class = RecommendedModuleSerializer
    def get(self, request, userID , format=None):
        
        try:
            #check user actitvit and coach tag
            #get user activity
            useractobj = StudentCoach.objects.get(pk=userID)
            activity = useractobj.training_activity.all()
            activitytitle = []
            for actval in activity:
                activitytitle.append(actval.title)
                
            
            #get all module like activity
            getmodule = Coachmodule.objects.filter(Q(module_status='publish'), ~Q(status='Archive'))
            
            moduleIDArr = []
            if(getmodule.count() > 0):
                for each in getmodule:
                    
                    tag = each.tag.all()
                    tagarr = [] 
                    
                    for eachtag in tag:
                        tagarr.append(eachtag.tag_title)
                        
                    
                    checkequalvalue = list(set(activitytitle).intersection(tagarr))
                    
                    if(checkequalvalue):
                        moduleIDArr.append(each.id)
                    
            #return Response({"status":True, "response_msg": "List module", "module":moduleIDArr })
            if moduleIDArr :
                
                ###
                moduleobj = Coachmodule.objects.filter(Q(pk__in=moduleIDArr),Q(module_status='publish'), ~Q(status='Archive'))[:5]
                serializer = RecommendedModuleSerializer(moduleobj, many=True)
                
                
                
                if(moduleobj.count() > 0):
                    modulearr = []
                    for each in serializer.data:
                        
                        partList = {}
                        
                        if(each['student_id'] != ''):
                            student_IDs = each['student_id']
                            student_IDs = student_IDs.split(",")
                        else:
                            student_IDs = ''
                        
                        if userID in student_IDs:
                            partList['enroll'] = False
                        else:
                            partList['enroll'] = True
                            
                        
                        #check if module paid and purchaesh moduel
                        if(each['paid_free'] == 'Paid'):
                            #check user order history
                            orderobj = OrderHistory.objects.filter(Q(user=userID), Q(object_type='charge'), Q(module_id=each['id']),Q(status='succeeded') )
                            if(orderobj.count() > 0):
                                partList['purchase_mod'] = True
                            else:
                                partList['purchase_mod'] = False
                        else:
                            partList['purchase_mod'] = False
                            
                        if each['see_module'] in settings.MY_CHOICES_ARR[0]: #Public
                            showmodule = True
                        elif each['see_module'] in settings.MY_CHOICES_ARR[1]: #Only my students 
                        
                            #check module show or not
                            #each.coach userID
                            coacharr = []
                            requestobj = StudentCoachRequest.objects.filter(Q(user=userID), Q(status='Approve') )
                            if(requestobj.count() > 0):
                                for reqval in requestobj:
                                    coacharr.append(reqval.user_to_id)
                            
                            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=userID), Q(status='Approve') )
                            if(requestobj1.count() > 0):
                                for reqval1 in requestobj1:
                                    coacharr.append(reqval1.user_id)
                            
                            if each['coach'] in coacharr:
                                showmodule = True
                            else:
                                showmodule = False
                            
                            
                        elif each['see_module'] in settings.MY_CHOICES_ARR[2]: #Only keyyPRO members
                            '''
                            userobj = StudentCoach.objects.get(pk=userID)
                            if userobj.subscription == 1 :
                                showmodule = True
                            else:
                                if userobj.subscription_invite == 1 :
                                    showmodule = True
                                else:
                                    showmodule = False
                            '''
                            showmodule = True
                        elif each['see_module'] in settings.MY_CHOICES_ARR[3]: #Private - Only visible to me
                            #2018
                            try:
                                assigntrack = StudentAssignUnassignTrack.objects.filter(Q(module_id=each['id']),Q(student_id=userID))
                                if(assigntrack.count() > 0):
                                    showmodule = True
                                else:
                                    showmodule = False 
                            except:
                                showmodule = False 
                        else:
                            showmodule = True
                            
                        if(showmodule == True):
                            partList['id'] = each['id']
                            partList['module_name'] = each['module_name']
                            partList['module_price'] = each['module_price']
                            
                            #check is completed or not
                            assignobj = StudentAssignment.objects.filter(Q(module_id=each['id']),Q(student_id=userID))
                            if(assignobj.count() > 0):
                                if(assignobj[0].is_completed == 'Yes'):
                                    is_completed = True
                                else:
                                    is_completed = False
                            else:
                                is_completed = False
                                
                            partList['is_completed'] = is_completed
                            
                            
                            current_url = request.get_host()
                            if request.is_secure():
                                current_url = 'https://' + current_url
                            else:
                                current_url = 'http://' + current_url
                            
                            partList['philosophy_video_updated'] = each['philosophy_video_updated']
                            partList['technique_video_updated'] = each['technique_video_updated']
                            partList['coach_video_updated'] = each['coach_video_updated']
                            
                            if(each['philosophy_video']):
                                #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                                #if os.path.exists(philosophyvideo):
                                partList['philosophy_video'] = each['philosophy_video']
                                #else:
                                    #partList['philosophy_video'] = ''
                            elif(each['philosophy_video2']):
                                partList['philosophy_video'] = each['philosophy_video2']
                            else:
                                partList['philosophy_video'] = ''
                                
                            
                            if(each['technique_video']):
                                #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                                #if os.path.exists(techniquevideo):
                                partList['technique_video'] = each['technique_video']
                                #else:
                                    #partList['technique_video'] = ''
                            elif(each['technique_video2']):
                                partList['technique_video'] = each['technique_video2']
                            else:
                                partList['technique_video'] = ''
                                
                                
                            if(each['coach_video']):
                                #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                                #if os.path.exists(coachvideo):
                                partList['coach_video'] = each['coach_video']
                                #else:
                                    #partList['coach_video'] = ''
                            elif(each['coach_video2']):
                                partList['coach_video'] = each['coach_video2']
                            else:
                                partList['coach_video'] = ''
                                
                                
                            if(each['philosophy_video_thumb']):
                                #philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                                #if os.path.exists(philosophy_video_thumb):
                                partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                                #else:
                                    #partList['philosophy_video_thumb'] = ''
                            elif(each['philosophy_video_thumb2']):
                                partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                            else:
                                partList['philosophy_video_thumb'] = ''
                                
                            if(each['technique_video_thumb']):
                                #technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                                #if os.path.exists(technique_video_thumb):
                                partList['technique_video_thumb'] = each['technique_video_thumb']
                                #else:
                                    #partList['technique_video_thumb'] = ''
                            elif(each['technique_video_thumb2']):
                                partList['technique_video_thumb'] = each['technique_video_thumb2']
                            else:
                                partList['technique_video_thumb'] = ''
                                
                            if(each['coach_video_thumb']):
                                #coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                                #if os.path.exists(coach_video_thumb):
                                partList['coach_video_thumb'] = each['coach_video_thumb']
                                #else:
                                    #partList['coach_video_thumb'] = ''
                            elif(each['coach_video_thumb2']):
                                partList['coach_video_thumb'] = each['coach_video_thumb2']
                            else:
                                partList['coach_video_thumb'] = ''
                            
                            
                            coachobj = StudentCoach.objects.filter(pk=each['coach'])
                            coacharr = []
                            for coachval in coachobj:
                                coachParlist = {}
                                coachParlist['id'] = coachval.id
                                coachParlist['first_name'] = coachval.first_name
                                coachParlist['last_name'] = coachval.last_name
                                coacharr.append(coachParlist)
                            partList['coachobj'] = coacharr
                            
            
                            partList['description'] = each['description']
                            partList['see_module'] = each['see_module']
                            partList['module_status'] = each['module_status']
                            
                            #partList['links'] = each.links
                            partList['paid_free'] = each['paid_free']
                            
                            taglist = each['tag']
                            
                            tagobj = Tag.objects.filter(pk__in=taglist)
                            
                            tagarr = []
                            for tag_val in tagobj:
                                partagList = {}
                                partagList['tag_id'] = tag_val.id
                                partagList['tag_title'] = tag_val.tag_title
                                tagarr.append(partagList)
                            
                            partList['tag'] = tagarr
                            modulearr.append(partList)
                        
                    return Response({"status":True, "response_msg": "List module", "module":modulearr })
                return JsonResponse({"status":False, "response_msg": "Data not found", "module": {} })
                
                ###
            else:
                return Response({"status":True, "response_msg": "Data not found", "module":{} })
            
            
            
        except:
            return Response({"status":False, "response_msg": "Data not found", "module": {} })

#search module by name by student
class SearchListRecommendedModule(APIView):
    serializer_class = RecommendedModuleSerializer
    def get(self, request, userID , format=None):
        
        try:
            search = request.GET['search']
            #search by coach name
            
            qset = Q()
            for term in search.split():
                qset |= Q(first_name__icontains=term)| Q(last_name__icontains=term)
            
            
            coachsearechobj = StudentCoach.objects.filter(Q(account_type="Coach"), qset)
            userarr = []
            if(coachsearechobj.count() > 0):
                for coachval in coachsearechobj:
                    userarr.append(coachval.id)
            
            
                
            
            if userarr:
                moduleobj = Coachmodule.objects.filter(Q(module_status='publish'), ~Q(status='Archive'), Q(module_name__icontains=search)|Q(coach__in=userarr))
            else:
                moduleobj = Coachmodule.objects.filter(Q(module_status='publish'), ~Q(status='Archive'), Q(module_name__icontains=search))
            
            
            serializer = RecommendedModuleSerializer(moduleobj, many=True)
            if(moduleobj):
                modulearr = []
                for each in serializer.data:
                    
                    partList = {}
                    
                    if(each['student_id'] != ''):
                        student_IDs = each['student_id']
                        student_IDs = student_IDs.split(",")
                    else:
                        student_IDs = ''
                    
                    if userID in student_IDs:
                        partList['enroll'] = False
                    else:
                        partList['enroll'] = True
                        
                        
                    #check if module paid and purchaesh moduel
                    if(each['paid_free'] == 'Paid'):
                        #check user order history
                        orderobj = OrderHistory.objects.filter(Q(user=userID), Q(object_type='charge'), Q(module_id=each['id']),Q(status='succeeded') )
                        if(orderobj.count() > 0):
                            partList['purchase_mod'] = True
                        else:
                            partList['purchase_mod'] = False
                    else:
                        partList['purchase_mod'] = False
                        
                    if each['see_module'] in settings.MY_CHOICES_ARR[0]: #Public
                        showmodule = True
                    elif each['see_module'] in settings.MY_CHOICES_ARR[1]: #Only my students 
                        
                        #check module show or not
                        #each.coach userID
                        coacharr = []
                        requestobj = StudentCoachRequest.objects.filter(Q(user=userID), Q(status='Approve') )
                        if(requestobj.count() > 0):
                            for reqval in requestobj:
                                coacharr.append(reqval.user_to_id)
                        
                        requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=userID), Q(status='Approve') )
                        if(requestobj1.count() > 0):
                            for reqval1 in requestobj1:
                                coacharr.append(reqval1.user_id)
                        
                        if each['coach'] in coacharr:
                            showmodule = True
                        else:
                            showmodule = False
                            
                    elif each['see_module'] in settings.MY_CHOICES_ARR[2]: #Only keyyPRO members
                        '''
                        userobj = StudentCoach.objects.get(pk=userID)
                        if userobj.subscription == 1 :
                            showmodule = True
                        else:
                            if userobj.subscription_invite == 1 :
                                showmodule = True
                            else:
                                showmodule = False
                        '''
                        showmodule = True
                    elif each['see_module'] in settings.MY_CHOICES_ARR[3]: #Private - Only visible to me
                        #2018
                        try:
                            assigntrack = StudentAssignUnassignTrack.objects.filter(Q(module_id=each['id']),Q(student_id=userID))
                            if(assigntrack.count() > 0):
                                showmodule = True
                            else:
                                showmodule = False 
                        except:
                            showmodule = False 
                    else:
                        showmodule = True
                        
                    if(showmodule == True):
                        partList['id'] = each['id']
                        partList['module_name'] = each['module_name']
                        partList['module_price'] = each['module_price']
                        
                        #check is completed or not
                        assignobj = StudentAssignment.objects.filter(Q(module_id=each['id']),Q(student_id=userID))
                        if(assignobj.count() > 0):
                            if(assignobj[0].is_completed == 'Yes'):
                                is_completed = True
                            else:
                                is_completed = False
                        else:
                            is_completed = False
                            
                        partList['is_completed'] = is_completed
                        
                        current_url = request.get_host()
                        if request.is_secure():
                            current_url = 'https://' + current_url
                        else:
                            current_url = 'http://' + current_url
                        
                        partList['philosophy_video_updated'] = each['philosophy_video_updated']
                        partList['technique_video_updated'] = each['technique_video_updated']
                        partList['coach_video_updated'] = each['coach_video_updated']
                        
                        if(each['philosophy_video']):
                            #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                            #if os.path.exists(philosophyvideo):
                            partList['philosophy_video'] = each['philosophy_video']
                            #else:
                                #partList['philosophy_video'] = ''
                        elif(each['philosophy_video2']):
                            partList['philosophy_video'] = each['philosophy_video2']
                        else:
                            partList['philosophy_video'] = ''
                            
                        
                        if(each['technique_video']):
                            #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                            #if os.path.exists(techniquevideo):
                            partList['technique_video'] = each['technique_video']
                            #else:
                                #partList['technique_video'] = ''
                        elif(each['technique_video2']):
                            partList['technique_video'] = each['technique_video2']
                        else:
                            partList['technique_video'] = ''
                            
                            
                        if(each['coach_video']):
                            #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                            #if os.path.exists(coachvideo):
                            partList['coach_video'] = each['coach_video']
                            #else:
                                #partList['coach_video'] = ''
                        elif(each['coach_video2']):
                            partList['coach_video'] = each['coach_video2']
                        else:
                            partList['coach_video'] = ''
                            
                            
                        if(each['philosophy_video_thumb']):
                            #philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                            #if os.path.exists(philosophy_video_thumb):
                            partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                            #else:
                                #partList['philosophy_video_thumb'] = ''
                        elif(each['philosophy_video_thumb2']):
                            partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                        else:
                            partList['philosophy_video_thumb'] = ''
                            
                        if(each['technique_video_thumb']):
                            #technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                            #if os.path.exists(technique_video_thumb):
                            partList['technique_video_thumb'] = each['technique_video_thumb']
                            #else:
                                #partList['technique_video_thumb'] = ''
                        elif(each['technique_video_thumb2']):
                            partList['technique_video_thumb'] = each['technique_video_thumb2']
                        else:
                            partList['technique_video_thumb'] = ''
                            
                        if(each['coach_video_thumb']):
                            #coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                            #if os.path.exists(coach_video_thumb):
                            partList['coach_video_thumb'] = each['coach_video_thumb']
                            #else:
                                #partList['coach_video_thumb'] = ''
                        elif(each['coach_video_thumb2']):
                            partList['coach_video_thumb'] = each['coach_video_thumb2']
                        else:
                            partList['coach_video_thumb'] = ''
                         
                        coachobj = StudentCoach.objects.filter(pk=each['coach'])
                        coacharr = []
                        for coachval in coachobj:
                            coachParlist = {}
                            coachParlist['id'] = coachval.id
                            coachParlist['first_name'] = coachval.first_name
                            coachParlist['last_name'] = coachval.last_name
                            coacharr.append(coachParlist)
                        partList['coachobj'] = coacharr
                        
        
                        partList['description'] = each['description']
                        partList['see_module'] = each['see_module']
                        partList['module_status'] = each['module_status']
                        
                        #partList['links'] = each.links
                        partList['paid_free'] = each['paid_free']
                        
                        taglist = each['tag']
                        
                        tagobj = Tag.objects.filter(pk__in=taglist)
                        tagarr = []
                        for tag_val in tagobj:
                            partagList = {}
                            partagList['tag_id'] = tag_val.id
                            partagList['tag_title'] = tag_val.tag_title
                            tagarr.append(partagList)
                        
                        partList['tag'] = tagarr
                        modulearr.append(partList)
                    
                return Response({"status":True, "response_msg": "List module", "module":modulearr })
            return JsonResponse({"status":False, "response_msg": "Data not found", "module": {} })
        except Coachmodule.DoesNotExist:
            return JsonResponse({"status":False, "response_msg": "Data not found", "module": {} })
        

#CoachModuleList last added max 5
class CoachModuleLatestList(APIView):
    serializer_class = CoachModuleListSerializer
    def get(self, request,userID, format=None):
        try:
            moduleobj = Coachmodule.objects.filter(Q(module_status='publish'), ~Q(status='Archive')).order_by('-id')[:5]
            serializer = RecommendedModuleSerializer(moduleobj, many=True)
            if(moduleobj):
                modulearr = []
                for each in serializer.data:
                    
                    partList = {}
                    
                    if(each['student_id'] != ''):
                        student_IDs = each['student_id']
                        student_IDs = student_IDs.split(",")
                    else:
                        student_IDs = ''
                    
                    if userID in student_IDs:
                        partList['enroll'] = False
                    else:
                        partList['enroll'] = True
                        
                    #check if module paid and purchaesh moduel
                    if(each['paid_free'] == 'Paid'):
                        #check user order history
                        orderobj = OrderHistory.objects.filter(Q(user=userID), Q(object_type='charge'), Q(module_id=each['id']),Q(status='succeeded') )
                        if(orderobj.count() > 0):
                            partList['purchase_mod'] = True
                        else:
                            partList['purchase_mod'] = False
                    else:
                        partList['purchase_mod'] = False
                        
                    
                    
                    if each['see_module'] in settings.MY_CHOICES_ARR[0]: #Public
                        showmodule = True
                    elif each['see_module'] in settings.MY_CHOICES_ARR[1]: #Only my students 
                        
                        #check module show or not
                        #each.coach userID
                        coacharr = []
                        requestobj = StudentCoachRequest.objects.filter(Q(user=userID), Q(status='Approve') )
                        if(requestobj.count() > 0):
                            for reqval in requestobj:
                                coacharr.append(reqval.user_to_id)
                        
                        requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=userID), Q(status='Approve') )
                        if(requestobj1.count() > 0):
                            for reqval1 in requestobj1:
                                coacharr.append(reqval1.user_id)
                        
                        if each['coach'] in coacharr:
                            showmodule = True
                        else:
                            showmodule = False
                            
                        
                    elif each['see_module'] in settings.MY_CHOICES_ARR[2]: #Only keyyPRO members
                        '''
                        userobj = StudentCoach.objects.get(pk=userID)
                        if userobj.subscription == 1 :
                            showmodule = True
                        else:
                            if userobj.subscription_invite == 1 :
                                showmodule = True
                            else:
                                showmodule = False
                        '''
                        showmodule = True
                    elif each['see_module'] in settings.MY_CHOICES_ARR[3]: #Private - Only visible to me
                        showmodule = False
                    else:
                        showmodule = True
                        
                    if(showmodule == True):
                        partList['id'] = each['id']
                        partList['module_name'] = each['module_name']
                        partList['module_price'] = each['module_price']
                        
                        #check is completed or not
                        assignobj = StudentAssignment.objects.filter(Q(module_id=each['id']),Q(student_id=userID))
                        if(assignobj.count() > 0):
                            if(assignobj[0].is_completed == 'Yes'):
                                is_completed = True
                            else:
                                is_completed = False
                        else:
                            is_completed = False
                            
                        partList['is_completed'] = is_completed
                        
                        current_url = request.get_host()
                        if request.is_secure():
                            current_url = 'https://' + current_url
                        else:
                            current_url = 'http://' + current_url
                        
                        
                        partList['philosophy_video_updated'] = each['philosophy_video_updated']
                        partList['technique_video_updated'] = each['technique_video_updated']
                        partList['coach_video_updated'] = each['coach_video_updated']
                        
                        if(each['philosophy_video']):
                            #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                            #if os.path.exists(philosophyvideo):
                            partList['philosophy_video'] = each['philosophy_video']
                            #else:
                                #partList['philosophy_video'] = ''
                        elif(each['philosophy_video2']):
                            partList['philosophy_video'] = each['philosophy_video2']
                        else:
                            partList['philosophy_video'] = ''
                            
                        
                        if(each['technique_video']):
                            #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                            #if os.path.exists(techniquevideo):
                            partList['technique_video'] = each['technique_video']
                            #else:
                                #partList['technique_video'] = ''
                        elif(each['technique_video2']):
                            partList['technique_video'] = each['technique_video2']
                        else:
                            partList['technique_video'] = ''
                            
                            
                        if(each['coach_video']):
                            #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                            #if os.path.exists(coachvideo):
                            partList['coach_video'] = each['coach_video']
                            #else:
                                #partList['coach_video'] = ''
                        elif(each['coach_video2']):
                            partList['coach_video'] = each['coach_video2']
                        else:
                            partList['coach_video'] = ''
                            
                            
                        if(each['philosophy_video_thumb']):
                            #philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                            #if os.path.exists(philosophy_video_thumb):
                            partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                            #else:
                                #partList['philosophy_video_thumb'] = ''
                        elif(each['philosophy_video_thumb2']):
                            partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                        else:
                            partList['philosophy_video_thumb'] = ''
                            
                        if(each['technique_video_thumb']):
                            #technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                            #if os.path.exists(technique_video_thumb):
                            partList['technique_video_thumb'] = each['technique_video_thumb']
                            #else:
                                #partList['technique_video_thumb'] = ''
                        elif(each['technique_video_thumb2']):
                            partList['technique_video_thumb'] = each['technique_video_thumb2']
                        else:
                            partList['technique_video_thumb'] = ''
                            
                        if(each['coach_video_thumb']):
                            #coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                            #if os.path.exists(coach_video_thumb):
                            partList['coach_video_thumb'] = each['coach_video_thumb']
                            #else:
                                #partList['coach_video_thumb'] = ''
                        elif(each['coach_video_thumb2']):
                            partList['coach_video_thumb'] = each['coach_video_thumb2']
                        else:
                            partList['coach_video_thumb'] = ''
                         
                         
                        coachobj = StudentCoach.objects.filter(pk=each['coach'])
                        coacharr = []
                        for coachval in coachobj:
                            coachParlist = {}
                            coachParlist['id'] = coachval.id
                            coachParlist['first_name'] = coachval.first_name
                            coachParlist['last_name'] = coachval.last_name
                            coacharr.append(coachParlist)
                        partList['coachobj'] = coacharr
                        
        
                        partList['description'] = each['description']
                        partList['see_module'] = each['see_module']
                        partList['module_status'] = each['module_status']
                        
                        #partList['links'] = each.links
                        partList['paid_free'] = each['paid_free']
                        
                        taglist = each['tag']
                        tagobj = Tag.objects.filter(pk__in=taglist)
                        tagarr = []
                        for tag_val in tagobj:
                            partagList = {}
                            partagList['tag_id'] = tag_val.id
                            partagList['tag_title'] = tag_val.tag_title
                            tagarr.append(partagList)
                        
                        partList['tag'] = tagarr
                        modulearr.append(partList)
                    
                return Response({"status":True, "response_msg": "List module", "module":modulearr })
            return JsonResponse({"status":False, "response_msg": "Data not found", "module": {} })
        except Coachmodule.DoesNotExist:
            return JsonResponse({"status":False, "response_msg": "Data not found", "module": {} })
        
        

#browse by tag by student
class BrowsebyTagModuleList(APIView):
    serializer_class = BrowsebyTagModuleListSerializer
    def get(self, request,userID, format=None):
        try:
            tag1 =''
            tag2 =''
            tag3 =''
            
            if 'tag1' in request.GET:
                tag1 = request.GET['tag1']
            
            if 'tag2' in request.GET:
                tag2 = request.GET['tag2']
                
            if 'tag3' in request.GET:
                tag3 = request.GET['tag3']
            

            if tag1 != '' and tag2 =='' and tag3 == '':
                moduleobj = Coachmodule.objects.filter(Q(tag__in=[tag1]),Q(module_status='publish'), ~Q(status='Archive')).distinct()
            elif tag1 == '' and tag2 !='' and tag3 == '':
                moduleobj = Coachmodule.objects.filter(Q(tag__in=[tag2]),Q(module_status='publish'), ~Q(status='Archive')).distinct()
            elif tag1 == '' and tag2 =='' and tag3 != '':
                moduleobj = Coachmodule.objects.filter(Q(tag__in=[tag3]),Q(module_status='publish'), ~Q(status='Archive')).distinct()
            elif tag1 != '' and tag2 !='' and tag3 == '':
                moduleobj = Coachmodule.objects.filter(Q(tag__in=[tag1,tag2]),Q(module_status='publish'), ~Q(status='Archive')).distinct()
                
                moduleidarr = [] 
                for data in moduleobj:
                    tag = data.tag.all()
                    tagarr = []
                    for tagval in tag:
                        tagarr.append(tagval.id)
                
                    if int(tag1) in tagarr and int(tag2) in tagarr:
                        moduleidarr.append(data.id)
                    
                moduleobj = Coachmodule.objects.filter(Q(pk__in=moduleidarr),Q(module_status='publish')).distinct()
            
            elif tag1 == '' and tag2 !='' and tag3 != '':
                moduleobj = Coachmodule.objects.filter(tag__in=[tag2,tag3],module_status='publish').distinct()
                
                moduleidarr = [] 
                for data in moduleobj:
                    tag = data.tag.all()
                    tagarr = []
                    for tagval in tag:
                        tagarr.append(tagval.id)
                
                    if int(tag2) in tagarr and int(tag3) in tagarr:
                        moduleidarr.append(data.id)
                    
                moduleobj = Coachmodule.objects.filter(Q(pk__in=moduleidarr),Q(module_status='publish')).distinct()
                
            elif tag1 != '' and tag2 =='' and tag3 != '':
                moduleobj = Coachmodule.objects.filter(tag__in=[tag1,tag3],module_status='publish').distinct()
                
                moduleidarr = [] 
                for data in moduleobj:
                    tag = data.tag.all()
                    tagarr = []
                    for tagval in tag:
                        tagarr.append(tagval.id)
                
                    if int(tag1) in tagarr and int(tag3) in tagarr:
                        moduleidarr.append(data.id)
                    
                moduleobj = Coachmodule.objects.filter(Q(pk__in=moduleidarr),Q(module_status='publish')).distinct()
                
            elif tag1 != '' and tag2 !='' and tag3 != '':
                moduleobj = Coachmodule.objects.filter(tag__in=[tag1,tag2,tag3],module_status='publish').distinct()
                
                moduleidarr = [] 
                for data in moduleobj:
                    tag = data.tag.all()
                    tagarr = []
                    for tagval in tag:
                        tagarr.append(tagval.id)
                
                    if int(tag1) in tagarr and int(tag2) in tagarr and int(tag3) in tagarr:
                        moduleidarr.append(data.id)
                    
                moduleobj = Coachmodule.objects.filter(Q(pk__in=moduleidarr),Q(module_status='publish')).distinct()
              
            else:
                moduleobj = Coachmodule.objects.filter(module_status='publish').distinct()
                
            serializer = BrowsebyTagModuleListSerializer(moduleobj, many=True)
            if(moduleobj):
                modulearr = []
                for each in serializer.data:
                    
                    partList = {}
                    
                    if(each['student_id'] != ''):
                        student_IDs = each['student_id']
                        student_IDs = student_IDs.split(",")
                    else:
                        student_IDs = ''
                    
                    if userID in student_IDs:
                        partList['enroll'] = False
                    else:
                        partList['enroll'] = True
                    
                    #check if module paid and purchaesh moduel
                    if(each['paid_free'] == 'Paid'):
                        #check user order history
                        orderobj = OrderHistory.objects.filter(Q(user=userID), Q(object_type='charge'), Q(module_id=each['id']),Q(status='succeeded') )
                        if(orderobj.count() > 0):
                            partList['purchase_mod'] = True
                        else:
                            partList['purchase_mod'] = False
                    else:
                        partList['purchase_mod'] = False
                        
                    if each['see_module'] in settings.MY_CHOICES_ARR[0]: #Public
                        showmodule = True
                    elif each['see_module'] in settings.MY_CHOICES_ARR[1]: #Only my students 
                        
                        #check module show or not
                        #each.coach userID
                        coacharr = []
                        requestobj = StudentCoachRequest.objects.filter(Q(user=userID), Q(status='Approve') )
                        if(requestobj.count() > 0):
                            for reqval in requestobj:
                                coacharr.append(reqval.user_to_id)
                        
                        requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=userID), Q(status='Approve') )
                        if(requestobj1.count() > 0):
                            for reqval1 in requestobj1:
                                coacharr.append(reqval1.user_id)
                        
                        if each['coach'] in coacharr:
                            showmodule = True
                        else:
                            showmodule = False
                        
                    elif each['see_module'] in settings.MY_CHOICES_ARR[2]: #Only keyyPRO members
                        '''
                        userobj = StudentCoach.objects.get(pk=userID)
                        if userobj.subscription == 1 :
                            showmodule = True
                        else:
                            if userobj.subscription_invite == 1 :
                                showmodule = True
                            else:
                                showmodule = False
                        '''
                        showmodule = True
                    elif each['see_module'] in settings.MY_CHOICES_ARR[3]: #Private - Only visible to me
                        #2018
                        try:
                            assigntrack = StudentAssignUnassignTrack.objects.filter(Q(module_id=each['id']),Q(student_id=userID))
                            if(assigntrack.count() > 0):
                                showmodule = True
                            else:
                                showmodule = False 
                        except:
                            showmodule = False 
                        
                    else:
                        showmodule = True
                        
                    if(showmodule == True):
                        partList['id'] = each['id']
                        partList['module_name'] = each['module_name']
                        partList['module_price'] = each['module_price']
                        
                        
                        #check is completed or not
                        assignobj = StudentAssignment.objects.filter(Q(module_id=each['id']),Q(student_id=userID))
                        if(assignobj.count() > 0):
                            if(assignobj[0].is_completed == 'Yes'):
                                is_completed = True
                            else:
                                is_completed = False
                        else:
                            is_completed = False
                            
                        partList['is_completed'] = is_completed
                        
                        current_url = request.get_host()
                        if request.is_secure():
                            current_url = 'https://' + current_url
                        else:
                            current_url = 'http://' + current_url
                        
                        
                        partList['philosophy_video_updated'] = each['philosophy_video_updated']
                        partList['technique_video_updated'] = each['technique_video_updated']
                        partList['coach_video_updated'] = each['coach_video_updated']
                        
                        if(each['philosophy_video']):
                            #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video)
                            #if os.path.exists(philosophyvideo):
                            partList['philosophy_video'] = each['philosophy_video']
                            #else:
                                #partList['philosophy_video'] = ''
                        elif(each['philosophy_video2']):
                            partList['philosophy_video'] = each['philosophy_video2']
                        else:
                            partList['philosophy_video'] = ''
                            
                        
                        if(each['technique_video']):
                            #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video)
                            #if os.path.exists(techniquevideo):
                            partList['technique_video'] = each['technique_video']
                            #else:
                                #partList['technique_video'] = ''
                        elif(each['technique_video2']):
                            partList['technique_video'] = each['technique_video2']
                        else:
                            partList['technique_video'] = ''
                            
                            
                        if(each['coach_video']):
                            #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video)
                            #if os.path.exists(coachvideo):
                            partList['coach_video'] = each['coach_video']
                            #else:
                                #partList['coach_video'] = ''
                        elif(each['coach_video2']):
                            partList['coach_video'] = each['coach_video2']
                        else:
                            partList['coach_video'] = ''
                            
                            
                        if(each['philosophy_video_thumb']):
                            #philosophy_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.philosophy_video_thumb)
                            #if os.path.exists(philosophy_video_thumb):
                            partList['philosophy_video_thumb'] = each['philosophy_video_thumb']
                            #else:
                                #partList['philosophy_video_thumb'] = ''
                        elif(each['philosophy_video_thumb2']):
                            partList['philosophy_video_thumb'] = each['philosophy_video_thumb2']
                        else:
                            partList['philosophy_video_thumb'] = ''
                            
                        if(each['technique_video_thumb']):
                            #technique_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.technique_video_thumb)
                            #if os.path.exists(technique_video_thumb):
                            partList['technique_video_thumb'] = each['technique_video_thumb']
                            #else:
                                #partList['technique_video_thumb'] = ''
                        elif(each['technique_video_thumb2']):
                            partList['technique_video_thumb'] = each['technique_video_thumb2']
                        else:
                            partList['technique_video_thumb'] = ''
                            
                        if(each['coach_video_thumb']):
                            #coach_video_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(each.coach_video_thumb)
                            #if os.path.exists(coach_video_thumb):
                            partList['coach_video_thumb'] = each['coach_video_thumb']
                            #else:
                                #partList['coach_video_thumb'] = ''
                        elif(each['coach_video_thumb2']):
                            partList['coach_video_thumb'] = each['coach_video_thumb2']
                        else:
                            partList['coach_video_thumb'] = ''
                                                    
                        
                        coachobj = StudentCoach.objects.filter(pk=each['coach'])
                        coacharr = []
                        for coachval in coachobj:
                            coachParlist = {}
                            coachParlist['id'] = coachval.id
                            coachParlist['first_name'] = coachval.first_name
                            coachParlist['last_name'] = coachval.last_name
                            coacharr.append(coachParlist)
                        partList['coachobj'] = coacharr
                        
        
                        partList['description'] = each['description']
                        partList['see_module'] = each['see_module']
                        partList['module_status'] = each['module_status']
                        
                        #partList['links'] = each.links
                        partList['paid_free'] = each['paid_free']
                        
                        taglist = each['tag']
                        tagobj = Tag.objects.filter(pk__in=taglist)
                        tagarr = []
                        for tag_val in tagobj:
                            partagList = {}
                            partagList['tag_id'] = tag_val.id
                            partagList['tag_title'] = tag_val.tag_title
                            tagarr.append(partagList)
                        
                        partList['tag'] = tagarr
                        modulearr.append(partList)
                    
                return Response({"status":True, "response_msg": "Search by tag module", "module":modulearr })
            return Response({"status":False, "response_msg": "Data not found", "module": {} })
        except Coachmodule.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "module": {} })
        
        

#module detail page
class GetModuleDetail(APIView):
    serializer_class = ModuleDetailSerializer
    def get(self, request,studentID,pk, format=None):
        try:
            snippets = Coachmodule.objects.get(pk=pk,module_status='publish')
            serializer = ModuleDetailSerializer(snippets)
            
            if(snippets):
                modulearr = []
                
                partList = {}
                
                
                if(serializer.data['student_id'] != ''):
                    student_IDs = serializer.data['student_id']
                    student_IDs = student_IDs.split(",")
                else:
                    student_IDs = ''
                  
                
                if studentID in student_IDs:
                    partList['enroll'] = False
                else:
                    partList['enroll'] = True
                    
                
                #check if module paid and purchaesh moduel
                if(serializer.data['paid_free'] == 'Paid'):
                    #check user order history
                    orderobj = OrderHistory.objects.filter(Q(user=studentID), Q(object_type='charge'), Q(module_id=serializer.data['id']),Q(status='succeeded') )
                    if(orderobj.count() > 0):
                        partList['purchase_mod'] = True
                    else:
                        partList['purchase_mod'] = False
                else:
                    partList['purchase_mod'] = False
                
                
                
                partList['id'] = serializer.data['id']
                partList['module_name'] = serializer.data['module_name']
                
                current_url = request.get_host()
                if request.is_secure():
                    current_url = 'https://' + current_url
                else:
                    current_url = 'http://' + current_url
                  
                partList['philosophy_video_updated'] = serializer.data['philosophy_video_updated']
                partList['technique_video_updated'] = serializer.data['technique_video_updated']
                partList['coach_video_updated'] = serializer.data['coach_video_updated']
                    
                if(serializer.data['philosophy_video']):
                    #philosophyvideo = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.philosophy_video)
                    #if os.path.isfile(philosophyvideo):
                    partList['philosophy_video'] = serializer.data['philosophy_video']
                    #else:
                        #partList['philosophy_video'] = ''
                elif(serializer.data['philosophy_video2']):
                    partList['philosophy_video'] = serializer.data['philosophy_video2']
                else:
                    partList['philosophy_video'] = ''
                
                if(serializer.data['philosophy_video_thumb']):
                    #philosophyvideo_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.philosophy_video_thumb)
                    #if os.path.isfile(philosophyvideo_thumb):
                    partList['philosophy_video_thumb'] = serializer.data['philosophy_video_thumb']
                    #else:
                        #partList['philosophy_video_thumb'] = ''
                elif(serializer.data['philosophy_video_thumb2']):
                    partList['philosophy_video_thumb'] = serializer.data['philosophy_video_thumb2']
                else:
                    partList['philosophy_video_thumb'] = ''
                    
                
                if(serializer.data['technique_video']):
                    #techniquevideo = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.technique_video)
                    #if os.path.isfile(techniquevideo):
                    partList['technique_video'] = serializer.data['technique_video']
                    #else:
                        #partList['technique_video'] = ''
                elif(serializer.data['technique_video2']):
                    partList['technique_video'] = serializer.data['technique_video2']
                else:
                    partList['technique_video'] = ''
                
                if(serializer.data['technique_video_thumb']):
                    #techniquevideo_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.technique_video_thumb)
                    #if os.path.isfile(techniquevideo_thumb):
                    partList['technique_video_thumb'] = serializer.data['technique_video_thumb']
                    #else:
                        #partList['technique_video_thumb'] = ''
                elif(serializer.data['technique_video_thumb2']):
                    partList['technique_video_thumb'] = serializer.data['technique_video_thumb2']
                else:
                    partList['technique_video_thumb'] = ''
                    
                    
                if(serializer.data['coach_video']):
                    #coachvideo = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.coach_video)
                    #if os.path.isfile(coachvideo):
                    partList['coach_video'] = serializer.data['coach_video']
                    #else:
                        #partList['coach_video'] = ''
                elif(serializer.data['coach_video2']):
                    partList['coach_video'] = serializer.data['coach_video2']
                else:
                    partList['coach_video'] = ''
                    
                if(serializer.data['coach_video_thumb']):
                    #coachvideo_thumb = settings.BASE_DIR + settings.MEDIA_URL + str(snippets.coach_video_thumb)
                    #if os.path.isfile(coachvideo_thumb):
                    partList['coach_video_thumb'] = serializer.data['coach_video_thumb']
                    #else:
                        #partList['coach_video_thumb'] = ''
                elif(serializer.data['coach_video_thumb2']):
                    partList['coach_video_thumb'] = serializer.data['coach_video_thumb2']
                else:
                    partList['coach_video_thumb'] = ''

                partList['description'] = serializer.data['description']
                partList['module_status'] = serializer.data['module_status']
                #partList['links'] = snippets.links
                partList['paid_free'] = serializer.data['paid_free']
                partList['module_price'] = serializer.data['module_price']
                partList['see_module'] = serializer.data['see_module']
                
                
                
                partList['coach_id'] = serializer.data['coach']
                coachobj = StudentCoach.objects.filter(pk=serializer.data['coach'])
                serializer2 = ListCoachModuleSerializer(coachobj,many=True)
                
                coacharr = []
                for coachval in serializer2.data:
                    coachParlist = {}
                    coachParlist['id'] = coachval['id']
                    coachParlist['first_name'] = coachval['first_name']
                    coachParlist['last_name'] = coachval['last_name']
                    coachParlist['updated'] = coachval['updated']
                    
                    if(coachval['profile_picture']):
                        #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(coachval.profile_picture)
                        #if os.path.isfile(profile_picture):
                        profile_picture = coachval['profile_picture']
                        #else:
                            #profile_picture = ''
                    elif(coachval['profile_picture2']):
                        profile_picture = coachval['profile_picture2']
                    elif (coachval['hidden_profile_picture'] !=''): #and coachval['social_login'] == 'True'
                        profile_picture = coachval['hidden_profile_picture']
                    else:
                        profile_picture = ''
                    
                    coachParlist['profile_picture'] = profile_picture
                    
                    coacharr.append(coachParlist)
                
                partList['coachobj'] = coacharr
                
                taglist = snippets.tag.all()
                tagarr = []
                for tag_val in taglist:
                    TagpartList = {}
                    TagpartList['tag_title'] = tag_val.tag_title
                    TagpartList['id'] = tag_val.id
                    tagarr.append(TagpartList)
                    
                partList['tag'] = tagarr
                
                linklist = Coachmodulelink.objects.filter(module_id=pk)
                linkarr = []
                for linkval in linklist:
                    linkParlist = {}
                    linkParlist['id'] = linkval.id
                    linkParlist['module_id'] = pk
                    linkParlist['link'] = linkval.link
                    #linkParlist['student_ids'] = linkval.student_ids
                    
                    if(linkval.student_ids != ''):
                        student_IDs = linkval.student_ids
                        student_IDs = student_IDs.split(",")
                    else:
                        student_IDs = ''
                
                    if studentID in student_IDs:
                        linkParlist['mark_as'] = True
                    else:
                        linkParlist['mark_as'] = False
                    
                    
                    linkarr.append(linkParlist)
                    
                partList['links'] = linkarr
                
                #check like
                likeobj = CoachModuleComment.objects.filter(Q(module_id=pk),Q(comment_type='like'), Q(user_id=studentID))
                if(likeobj.count() > 0 ):
                    partList['like'] = True
                else:
                    partList['like'] = False
                
                modulearr.append(partList)
                    
                
                return Response({"status":True, "response_msg": "Module detail", "module":modulearr })
        except Coachmodule.DoesNotExist:
            #raise Http404
            return Response({"status":False, "response_msg": "Not found", "module":{} })

#enrool module by student
class EnrollModule(APIView):
    serializer_class = EnrollmoduleSerializer
    def post(self, request,studentID, pk,coachID, format=None):
        serializer = EnrollmoduleSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({"status":True, "response_msg": "Enroll successfully", "data":{} })
        return Response({"status":False, "response_msg": "Data not found", "data": {} })
        

#mark as complete module homework link by student
class MarkasCompleteModuleLink(APIView):
    serializer_class = ModuleLinkMarkasSerializer
    def get(self, request,studentID,moduleID,pk, format=None):
        try:
            snippets = Coachmodulelink.objects.get(pk=pk)
            serializer = ModuleLinkMarkasSerializer(snippets,data=request.data)
            
            
            if(snippets.student_ids != ''):
                student_IDs = snippets.student_ids
                student_IDs = student_IDs.split(",")
            else:
                student_IDs = ''
            
            if studentID in student_IDs:
                return Response({"status":True, "response_msg": "Already marked", "link":{"id":snippets.pk,"module_id":moduleID, "link":snippets.link} })
            else:

                if(student_IDs != ''):
                    student_ids = snippets.student_ids + ',' + studentID
                else:
                    student_ids = studentID
                    
                if serializer.is_valid():
                    serializer.save(student_ids=student_ids)
                    return Response({"status":True, "response_msg": "Mark as complete update successfully", "links":{"id":serializer.data['id'],"module_id":moduleID, "link":serializer.data['link']} })
                return Response({"status":False, "response_msg": "Data not found", "link": {} })
        except:
            return Response({"status":False, "response_msg": "Data not found", "link": {} })
    

#un-mark as complete module homework link by student
class UnMarkasCompleteModuleLink(APIView):
    serializer_class = ModuleLinkMarkasSerializer
    def get(self, request,studentID,moduleID,pk, format=None):
        snippets = Coachmodulelink.objects.get(pk=pk)
        serializer = ModuleLinkMarkasSerializer(snippets,data=request.data)
        
        
        if(snippets.student_ids != ''):
            student_IDs = snippets.student_ids
            student_IDs = student_IDs.split(",")
        else:
            student_IDs = ''
        
        
        if studentID in student_IDs:
            if(student_IDs != ''):
                
                student_IDs.remove(studentID)
                
                student_ids = ",".join(student_IDs)
            else:
                student_ids = studentID
        else:
            return Response({"status":True, "response_msg": "already unmarked homework link", "link":{"id":snippets.pk,"module_id":moduleID, "link":snippets.link} })
            
            
        if serializer.is_valid():
            serializer.save(student_ids=student_ids)
            return Response({"status":True, "response_msg": "Unmarked homework link", "links":{"id":serializer.data['id'],"module_id":moduleID, "link":serializer.data['link']} })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)
        

        

#add comment to coach moduel by student
class AddCoachModuleComment(APIView):
    serializer_class = CoachModuleCommentSerializer
    def post(self, request,studentID,moduleID, format=None):
        serializer = CoachModuleCommentSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save(comment_type='comment')
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
            userobj = StudentCoach.objects.get(pk=serializer.data['user_id'])
            serializer2 = ListCoachModuleSerializer(userobj)
            userarr = []
            
            userParlist = {}
            userParlist['id'] = serializer2.data['id']
            userParlist['first_name'] = serializer2.data['first_name']
            userParlist['last_name'] = serializer2.data['last_name']
            userParlist['updated'] = serializer2.data['updated']
            
            if(serializer2.data['profile_picture']):
                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(userobj.profile_picture)
                #if os.path.isfile(profile_picture):
                profile_picture = serializer2.data['profile_picture']
                #else:
                    #profile_picture = ''
            elif(serializer2.data['profile_picture2']):
                profile_picture = serializer2.data['profile_picture2']
            elif (serializer2.data['hidden_profile_picture'] !=''): #and serializer2.data['social_login'] == 'True'
                profile_picture = serializer2.data['hidden_profile_picture']
            else:
                profile_picture = ''
                
            userParlist['profile_picture'] = profile_picture
            userarr.append(userParlist)
            
            
            #add notification
            type_of_notification = settings.NOTIFI_TYPE_ARR[0] #CommentModule
            main_id = moduleID
            titleobj = Coachmodule.objects.filter(pk=moduleID).values('module_name','coach')
            title = titleobj[0]['module_name']
            notify_to = titleobj[0]['coach']
            notiobj = Notification(user_id_id=userobj.id,notify_to_id=notify_to,type_of_notification=type_of_notification,main_id=main_id,title=title)
            notiobj.save()
            
            #send pushnotification
            device = FCMDevice.objects.filter(user=notify_to)
            message = settings.NOTIFI_TYPE_MESSAGE[0] #commented on your Module
            r = ''
            notititle = userobj.first_name + " " +userobj.last_name
            if(device.count() > 0):
                #r = device.send_message(data={"id":main_id,"title": title,"message":message})
                #r = device.send_message(data={"title": notititle,"body":message})
                r = device.send_message(title=notititle, body=message,data={"main_id": main_id,"type_of_notification":type_of_notification,"id":notiobj.id})
            #add notification end
            
            
            
            return Response({"status":True, "response_msg": "Comment added successfully", "comment":{"id":serializer.data['id'],"module_id":serializer.data['module_id'],"comment":serializer.data['comment'],"date":serializer.data['date'],"userobj":userarr,"r":r } })
        return Response(serializer._errors, status=status.HTTP_400_BAD_REQUEST)
    

#list comment to coach moduel
class ListCoachModuleComment(APIView):
    serializer_class = CoachModuleCommentSerializer
    def get(self, request,studentID,moduleID, format=None):
        try:
            commentobj = CoachModuleComment.objects.filter(Q(module_id=moduleID),Q(comment_type='comment'))
            serializer = CoachModuleCommentSerializer(commentobj, many=True)
            if(commentobj):
                current_url = request.get_host()
                if request.is_secure():
                    current_url = 'https://' + current_url
                else:
                    current_url = 'http://' + current_url
                commentarr = []
                for each in commentobj:
                    partList = {}
                    partList['id'] = each.id
                    partList['comment'] = each.comment
                    partList['date'] = each.date
                    partList['user_id'] = each.user_id_id
                    
                    
                    userobj = StudentCoach.objects.filter(pk=each.user_id_id)
                    serializer2 = ListCoachModuleSerializer(userobj,many=True)
                    userarr = []
                    for userval in serializer2.data:
                        userParlist = {}
                        userParlist['id'] = userval['id']
                        userParlist['first_name'] = userval['first_name']
                        userParlist['last_name'] = userval['last_name']
                        userParlist['updated'] = userval['updated']
                        
                        if(userval['profile_picture']):
                            #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(userval.profile_picture)
                            #if os.path.isfile(profile_picture):
                            profile_picture = userval['profile_picture']
                            #else:
                                #profile_picture = ''
                        elif(userval['profile_picture2']):
                            profile_picture = userval['profile_picture2']
                        elif (userval['hidden_profile_picture'] !=''): #and userval['social_login'] == 'True'
                            profile_picture = userval['hidden_profile_picture']
                        else:
                            profile_picture = ''
                            
                        userParlist['profile_picture'] = profile_picture
                        
                        userarr.append(userParlist)
                    partList['userobj'] = userarr
                    
                    
                    commentarr.append(partList)
                    
                return Response({"status":True, "response_msg": "List Coach moduel comment", "comment":commentarr })
            return Response({"status":False, "response_msg": "Data not found", "comment": {} })
        except CoachModuleComment.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "comment": {} })


#add like to coach moduel
class AddCoachModuleLike(APIView):
    serializer_class = CoachModuleLikeSerializer
    def post(self, request,userID,moduleID, format=None):
        try:
            commentobj = CoachModuleComment.objects.filter(Q(module_id=moduleID),Q(user_id=userID),Q(comment_type='like'))
            serializer = CoachModuleLikeSerializer(commentobj,data=request.data)
            if(commentobj.count() > 0):
                for each in commentobj:
                    commentobj = CoachModuleComment.objects.get(pk=each.id,module_id=moduleID,user_id=userID,comment_type='like').delete()
                return Response({"status":True, "response_msg": "dislike added successfully", "like":False })
            else:
                serializer = CoachModuleLikeSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save(comment_type='like')
                    return Response({"status":True, "response_msg": "like added successfully", "like":True })
            return Response({"status":False, "response_msg": "Data not found", "like": {} })
        except CoachModuleComment.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "like": {} })
            

#count comment, like, userwise like
class AddCoachModuleLikeV1(APIView):
    serializer_class = CoachModuleLikeSerializer
    def post(self, request,userID,moduleID, format=None):
        try:
            commentobj = CoachModuleComment.objects.filter(Q(module_id=moduleID),Q(user_id=userID),Q(comment_type='like'))
            serializer = CoachModuleLikeSerializer(commentobj,data=request.data)
            if(commentobj.count() > 0):
                for each in commentobj:
                    commentobj = CoachModuleComment.objects.get(pk=each.id,module_id=moduleID,user_id=userID,comment_type='like').delete()
                
                resmsg = 'dislike added successfully'
                likemsg = False
                #return Response({"status":True, "response_msg": "dislike added successfully", "like":False })
            else:
                serializer = CoachModuleLikeSerializer(data=request.data)
                if serializer.is_valid():
                    serializer.save(comment_type='like')
                    resmsg = 'like added successfully'
                    likemsg = True
                    #return Response({"status":True, "response_msg": "like added successfully", "like":True })
            
            commentobj = CoachModuleComment.objects.filter(Q(module_id=moduleID),Q(comment_type='comment')).count()
            likeobj = CoachModuleComment.objects.filter(Q(module_id=moduleID),Q(comment_type='like')).count()
            
            userlikeobj = CoachModuleComment.objects.filter(Q(module_id=moduleID),Q(comment_type='like')).order_by('-id')[:3]
            likeuserarr = []
            if(userlikeobj):
                current_url = request.get_host()
                if request.is_secure():
                    current_url = 'https://' + current_url
                else:
                    current_url = 'http://' + current_url
                
                for each in userlikeobj:
                    partList = {}
                    userobj = StudentCoach.objects.filter(pk=each.user_id_id)
                    serializer2 = ListCoachModuleSerializer(userobj,many=True)
                    userarr = []
                    for userval in serializer2.data:
                        userParlist = {}
                        userParlist['id'] = userval['id']
                        userParlist['first_name'] = userval['first_name']
                        userParlist['last_name'] = userval['last_name']
                        userParlist['updated'] = userval['updated']
                        
                        if(userval['profile_picture']):
                            #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(userval.profile_picture)
                            #if os.path.isfile(profile_picture):
                            profile_picture = userval['profile_picture']
                            #else:
                                #profile_picture = ''
                        elif(userval['profile_picture2']):
                            profile_picture = userval['profile_picture2']
                        elif (userval['hidden_profile_picture'] !=''): #and userval['social_login'] == 'True'
                            profile_picture = userval['hidden_profile_picture']
                        else:
                            profile_picture = ''
                            
                            
                        userParlist['profile_picture'] = profile_picture
                        
                        userarr.append(userParlist)
                    partList['userobj'] = userarr
                    
                    likeuserarr.append(partList)
                    
            #return Response({"status":False, "response_msg": "Data not found", "like": {} })
            #return Response({"status":True, "response_msg": "dislike added successfully", "like":False })
            return Response({"status":True, "response_msg": resmsg, "like":likeobj, "comment":commentobj,"likeuser":likeuserarr, "is_like":likemsg })
        except CoachModuleComment.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "like": {} })

        

    
#add report to coach moduel
class AddCoachModuleReport(APIView):
    serializer_class = CoachModuleReportSerializer
    def post(self, request,userID,moduleID, format=None):
        try:
            module_id = request.POST.get('module_id')
            user_id = request.POST.get('user_id')
            reportobj = CoachModuleReport.objects.get(module_id=module_id,user_id=user_id)
            serializer = CoachModuleReportSerializer(reportobj,data=request.data)
            if serializer.is_valid():
                serializer.save()
                
                #send email to admin for absuse report
                userobj = StudentCoach.objects.filter(pk=user_id).values('e_mail','first_name','last_name')
                user_email = userobj[0]['e_mail']
                first_name = userobj[0]['first_name']
                last_name = userobj[0]['last_name']
                
                moduleobj = Coachmodule.objects.filter(pk=module_id).values('module_name')
                module_name = moduleobj[0]['module_name']
                
                subject = "keyy - Abuse Report"
                #to = [settings.FROM_EMAIL]
                to = ['admin@keyy.io']
                #from_email = user_email
                from_email = settings.FROM_EMAIL
                
                
                message = '<p>' + first_name +' '+ last_name + ', abuse reported for module: <b>"'+ module_name +'"</b></p><p>Thanks,<br />The Keyy App Team</p>'
                msg = EmailMessage(subject, message, to=to, from_email=from_email)
                msg.content_subtype = 'html'
                msg.send(fail_silently=False)
                
                return Response({"status":True, "response_msg": "Update successfully", "report":serializer.data })
            return Response({"status":False, "response_msg": "Data not found", "report": {} })
        except:
            module_id = request.POST.get('module_id')
            user_id = request.POST.get('user_id')
            serializer = CoachModuleReportSerializer(data=request.data)
            if serializer.is_valid():
                serializer.save()
                
                #send email to admin for absuse report
                userobj = StudentCoach.objects.filter(pk=user_id).values('e_mail','first_name','last_name')
                user_email = userobj[0]['e_mail']
                first_name = userobj[0]['first_name']
                last_name = userobj[0]['last_name']
                
                moduleobj = Coachmodule.objects.filter(pk=module_id).values('module_name')
                module_name = moduleobj[0]['module_name']
                
                subject = "keyy - Abuse Report"
                #to = [settings.FROM_EMAIL]
                to = ['admin@keyy.io']
                #from_email = user_email
                from_email = settings.FROM_EMAIL
                
                message = '<p>' + first_name +' '+ last_name + ', abuse reported for module: <b>"'+ module_name +'"</b></p><p>Thanks,<br />The Keyy App Team</p>'
                msg = EmailMessage(subject, message, to=to, from_email=from_email)
                msg.content_subtype = 'html'
                msg.send(fail_silently=False)
                
                return Response({"status":True, "response_msg": "Report added successfully", "report":serializer.data })
            return Response({"status":False, "response_msg": "Data not found", "report": {} })
        


#count comment and like to coach moduel
class CountCoachModuleLikeComment(APIView):
    serializer_class = CoachModuleLikeSerializer
    def get(self, request,moduleID, format=None):
        try:
            commentobj = CoachModuleComment.objects.filter(Q(module_id=moduleID),Q(comment_type='comment')).count()
            likeobj = CoachModuleComment.objects.filter(Q(module_id=moduleID),Q(comment_type='like')).count()
            if(commentobj or likeobj):
                return Response({"status":True, "response_msg": "Comment & like counter", "like":likeobj, "comment":commentobj })
            return JsonResponse({"status":False, "response_msg": "Data not found", "like": "", "commnet":"" })
        except CoachModuleComment.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "like": {} })
        
        
#count like as user wise to coach moduel
class CountCoachModuleLikeUSerwise(APIView):
    serializer_class = CoachModuleLikeSerializer
    def get(self, request,moduleID, format=None):
        try:
            likeobj = CoachModuleComment.objects.filter(Q(module_id=moduleID),Q(comment_type='like'))[:3]
            serializer = CoachModuleCommentSerializer(likeobj, many=True)
            if(likeobj):
                current_url = request.get_host()
                if request.is_secure():
                    current_url = 'https://' + current_url
                else:
                    current_url = 'http://' + current_url
                likeuserarr = []
                for each in likeobj:
                    partList = {}
                    userobj = StudentCoach.objects.filter(pk=each.user_id_id)
                    serializer2 = ListCoachModuleSerializer(userobj,many=True)
                    userarr = []
                    for userval in serializer2.data:
                        userParlist = {}
                        userParlist['id'] = userval['id']
                        userParlist['first_name'] = userval['first_name']
                        userParlist['last_name'] = userval['last_name']
                        userParlist['updated'] = userval['updated']
                        
                        if(userval['profile_picture']):
                            #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(userval.profile_picture)
                            #if os.path.isfile(profile_picture):
                            profile_picture = userval['profile_picture']
                            #else:
                                #profile_picture = ''
                        elif(userval['profile_picture2']):
                            profile_picture = userval['profile_picture2']
                        elif (userval['hidden_profile_picture'] !=''): #and userval['social_login'] == 'True'
                            profile_picture = userval['hidden_profile_picture']
                        else:
                            profile_picture = ''
                            
                            
                        userParlist['profile_picture'] = profile_picture
                        
                        userarr.append(userParlist)
                    partList['userobj'] = userarr
                    
                    likeuserarr.append(partList)
                    
                    
                return Response({"status":True, "response_msg": "like counter as userwise","likeuser":likeuserarr })
            return Response({"status":False, "response_msg": "Data not found","likeuser":"" })
        except CoachModuleComment.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "likeuser": {} })
        
        
# MKT Add Find coach by student
class FindCoach(APIView):
    serializer_class = ListCoachModuleSerializer
    def get(self, request,userID, format=None):
        search = request.GET['search']
        try:
            coahobj = StudentCoach.objects.filter(Q(account_type="Coach"), Q(user_name__icontains=search)|Q(first_name__icontains=search)|Q(last_name__icontains=search))
            serializer = ListCoachModuleSerializer(coahobj, many=True)
            
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            
            coacharr = []
            for coachval in serializer.data:
                if(coachval['id'] != int(userID)):
                    parmList = {}
                    parmList['id'] = coachval['id']
                    parmList['first_name'] = coachval['first_name']
                    parmList['last_name'] = coachval['last_name']
                    parmList['user_name'] = coachval['user_name']
                    parmList['e_mail'] = coachval['e_mail']
                    parmList['skill'] = coachval['skill']
                    parmList['training_location'] = coachval['training_location']
                    parmList['updated'] = coachval['updated']
                    
                    #check if allready request or not 
                    requestobj = StudentCoachRequest.objects.filter(Q(user=userID),Q(user_to=coachval['id'])).order_by('id')
                    requestobj1 = StudentCoachRequest.objects.filter(Q(user=coachval['id']),Q(user_to=userID)).order_by('id')
                    
                    parmList['request_status'] = False
                    if(requestobj.count() > 0):
                        for reqval in requestobj:
                            if(reqval.status == 'Reject'):
                                parmList['request_status'] = False
                            else:
                                parmList['request_status'] = True
                    
                    
                    if(requestobj1.count() > 0):
                        for reqval1 in requestobj1:
                            if(reqval1.status == 'Reject'):
                                parmList['request_status'] = False
                            else:
                                parmList['request_status'] = True
                    
                    
                
                        
                    
                    if(coachval['profile_picture']):
                        #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(coachval.profile_picture)
                        #if os.path.isfile(profile_picture):
                        profile_picture = coachval['profile_picture']
                        #else:
                            #profile_picture = ''
                    elif(coachval['profile_picture2']):
                        profile_picture = coachval['profile_picture2']
                    elif (coachval['hidden_profile_picture'] !='' ):#and coachval['social_login'] == 'True'
                        profile_picture = coachval['hidden_profile_picture']
                    else:
                        profile_picture = ''
                        
                    parmList['profile_picture'] = profile_picture
                    coacharr.append(parmList)
            
            
            return Response({"response_code":"0", "response_msg": "Coach list","user_id":userID, "coachdata":coacharr })
        except StudentCoach.DoesNotExist:
            return JsonResponse({"response_code":"1", "response_msg": "Data not found", "coach": {} })
        



#Request handling part Start

#Send request to student by coach
class RequestCoachToStudent(APIView):
    serializer_class = StudentCoachRequestSerializer
    def get(self, request,coachID,studentID, format=None):
        try:
            requestobj = StudentCoachRequest.objects.get(Q(user=studentID), Q(user_to=coachID) )
            serializer = StudentCoachRequestSerializer(requestobj)
            if(requestobj):
                return Response({"status":True, "response_msg": "Allready friends","request_sent":True, "request":serializer.data })
        except:
            try:
                requestobj = StudentCoachRequest.objects.get(Q(user=coachID), Q(user_to=studentID) )
                serializer = StudentCoachRequestSerializer(requestobj)
                if(requestobj):
                    return Response({"status":True, "response_msg": "Allready friends","request_sent":True, "request":serializer.data })
            except:
                return Response({"status":False, "response_msg": "Data not found", "request_sent":False,"request": {} })
    def post(self, request,coachID,studentID, format=None):
        serializer = StudentCoachRequestSerializer(data=request.data)
        if serializer.is_valid():
            
            #check if allready request or not 
            requestobj = StudentCoachRequest.objects.filter(Q(user=studentID),Q(user_to=coachID)).order_by('id')
            requestobj1 = StudentCoachRequest.objects.filter(Q(user=coachID),Q(user_to=studentID)).order_by('id')
            request_status = False
            if(requestobj.count() > 0):
                for reqval in requestobj:
                    if(reqval.status == 'Reject'):
                        request_status = False
                    else:
                        request_status = True
                        
            if(requestobj1.count() > 0):
                for reqval1 in requestobj1:
                    if(reqval1.status == 'Reject'):
                        request_status = False
                    else:
                        request_status = True
                        
            
            if(request_status == True):
                return Response({"status":False, "response_msg": "You already sent request.", "request_sent":True, "request":{} })
            else:
                
                #get user Type
                requserobj = StudentCoach.objects.filter(pk=studentID).values('account_type')
                account_type = requserobj[0]['account_type']
                #coachtocoach = 0
                #if(account_type == 'Coach'):
                    #coachtocoach = 1
                
                #serializer.save(request_to='Student')
                serializer.save(request_to=account_type,temp_status='S')
                
                
                #check user settings 
                notificationuserobj = StudentCoach.objects.filter(pk=int(studentID)).values('noti_conn_request')
                noti_conn_request = notificationuserobj[0]['noti_conn_request']
                if(noti_conn_request == '1' and notificationuserobj.count() > 0 ):
                    
                    #add notification
                    type_of_notification = settings.NOTIFI_TYPE_ARR[2] #RequestCoach
                    main_id = serializer.data['id']
                    title = ''
                    notiobj = Notification(user_id_id=coachID,notify_to_id=studentID,type_of_notification=type_of_notification,main_id=main_id,title=title)
                    notiobj.save()
                    
                    #send pushnotification
                    device = FCMDevice.objects.filter(user=studentID)
                    message = settings.NOTIFI_TYPE_MESSAGE[2] #requested to connect as your coach
                    r = ''
                    
                    userobj = StudentCoach.objects.filter(pk=coachID).values('first_name','last_name')
                    notititle = userobj[0]['first_name'] + " " +userobj[0]['last_name']
                    if(device.count() > 0):
                        #r = device.send_message(data={"title": notititle,"body":message})
                        #r = device.send_message(title=notititle, body=message,data={"main_id": main_id,"type_of_notification":type_of_notification})
                        r = device.send_message(title=notititle, body=message,data={"main_id": main_id,"user_id":coachID,"user_to_id":studentID,"type_of_notification":type_of_notification,"id":notiobj.id})
                
                #add notification end
                
                return Response({"status":True, "response_msg": "Request sent successfully", "request_sent":True, "request":serializer.data })
            
        return Response({"status":False, "response_msg": "Data not found", "request": {} })
    

#Send request to coach
class RequestStudentToCoach(APIView):
    serializer_class = StudentCoachRequestSerializer
    def get(self, request,userID,coachID, format=None):
        try:
            #requestobj = StudentCoachRequest.objects.get(Q(student=userID), Q(coach=coachID), Q(request_to='Coach') )
            requestobj = StudentCoachRequest.objects.get(Q(user=coachID), Q(user_to=userID) )
            serializer = StudentCoachRequestSerializer(requestobj)
            if(requestobj):
                return Response({"status":True, "response_msg": "Allready friends request send","request_sent":True, "request":serializer.data })
        except:
            try:
                requestobj = StudentCoachRequest.objects.get(Q(user=userID), Q(user_to=coachID) )
                serializer = StudentCoachRequestSerializer(requestobj)
                if(requestobj):
                    return Response({"status":True, "response_msg": "Allready friends request send","request_sent":True, "request":serializer.data })
            except:
                return Response({"status":False, "response_msg": "Data not found", "request_sent":False,"request": {} })
    def post(self, request,userID,coachID, format=None):
        serializer = StudentCoachRequestSerializer(data=request.data)
        if serializer.is_valid():
            
            #check if allready request or not 
            requestobj = StudentCoachRequest.objects.filter(Q(user=coachID),Q(user_to=userID)).order_by('id')
            requestobj1 = StudentCoachRequest.objects.filter(Q(user=userID),Q(user_to=coachID)).order_by('id')
            request_status = False
            if(requestobj.count() > 0):
                for reqval in requestobj:
                    if(reqval.status == 'Reject'):
                        request_status = False
                    else:
                        request_status = True
                        
            if(requestobj1.count() > 0):
                for reqval1 in requestobj1:
                    if(reqval1.status == 'Reject'):
                        request_status = False
                    else:
                        request_status = True
            
            
            
            if(request_status == True):
                return Response({"status":False, "response_msg": "You already sent request.", "request_sent":True, "request":{} })
            else:
            
                serializer.save(request_to='Coach',temp_status='C')
                #add notification
                type_of_notification = settings.NOTIFI_TYPE_ARR[3] #RequestStudent
                main_id = serializer.data['id']
                title = ''
                notiobj = Notification(user_id_id=userID,notify_to_id=coachID,type_of_notification=type_of_notification,main_id=main_id,title=title)
                notiobj.save()
                
                #send pushnotification
                device = FCMDevice.objects.filter(user=coachID)
                message = settings.NOTIFI_TYPE_MESSAGE[3] #requested to connect as your student
                r = ''
                
                userobj = StudentCoach.objects.filter(pk=userID).values('first_name','last_name')
                notititle = userobj[0]['first_name'] + " " +userobj[0]['last_name']
                if(device.count() > 0):
                    r = device.send_message(title=notititle, body=message,data={"main_id": main_id,"user_id":userID,"user_to_id":coachID,"type_of_notification":type_of_notification,"id":notiobj.id})
                
                #add notification end
                return Response({"status":True, "response_msg": "Request sent successfully", "request_sent":True, "request":serializer.data,"r":r })

        return Response({"status":False, "response_msg": "Data not found", "request": {} })

#List my new student request
class MyNewStudent(APIView):
    serializer_class = StudentCoachRequestSerializer
    def get(self, request,coachID, format=None):
        try:
            #requestobj = StudentCoachRequest.objects.filter(Q(student=userID), Q(request_to='Coach'), Q(status='Approve') )
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
            requserobj = StudentCoach.objects.filter(pk=coachID).values('account_type')
            account_type = requserobj[0]['account_type']
            
            requestobj = StudentCoachRequest.objects.filter(Q(user_to=coachID), Q(request_to=account_type), Q(status='Pending'), Q(temp_status='C') )
            serializer = StudentCoachRequestSerializer(requestobj)
            if(requestobj):
                requestarr = []
                studentIDarr = []
                for requestval in requestobj:
                    #studentIDarr.append(requestval.student_id)
                    partList = {}
                    partList['id'] =  requestval.id
                    studentobj = StudentCoach.objects.filter(pk=requestval.user_id)
                    serializer1 = ListCoachModuleSerializer(studentobj, many=True)
                    studentarr = []
                    for studentval in serializer1.data:
                        studentParlist = {}
                        studentParlist['id'] = studentval['id']
                        studentParlist['first_name'] = studentval['first_name']
                        studentParlist['last_name'] = studentval['last_name']
                        studentParlist['user_name'] = studentval['user_name']
                        studentParlist['training_location'] = studentval['training_location']
                        studentParlist['account_type'] = studentval['account_type']
                        studentParlist['updated'] = studentval['updated']
                        
                        if(studentval['profile_picture']):
                            #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(studentval.profile_picture)
                            #if os.path.isfile(profile_picture):
                            profile_picture = studentval['profile_picture']
                            #else:
                                #profile_picture = ''
                        elif(studentval['profile_picture2']):
                            profile_picture = studentval['profile_picture2']
                        elif (studentval['hidden_profile_picture'] !=''): #and studentval['social_login'] == 'True'
                            profile_picture = studentval['hidden_profile_picture']
                        else:
                            profile_picture = ''
                            
                        studentParlist['profile_picture'] = profile_picture
                        studentarr.append(studentParlist)
                    
                    partList['student'] =  studentarr
                    requestarr.append(partList)
                
                    
                return Response({"status":True, "response_msg": "New Student","coach_id":coachID, "studentrequest":requestarr })
            return Response({"status":False, "response_msg": "Data not found", "studentrequest": {} })
        except StudentCoachRequest.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "studentrequest": {} })
        


#List my new coach request
class MyNewCoach(APIView):
    serializer_class = StudentCoachRequestSerializer
    def get(self, request,userID, format=None):
        try:
            #requestobj = StudentCoachRequest.objects.filter(Q(student=userID), Q(request_to='Coach'), Q(status='Approve') )
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            
            requserobj = StudentCoach.objects.filter(pk=userID).values('account_type')
            account_type = requserobj[0]['account_type']
            
            requestobj = StudentCoachRequest.objects.filter(Q(user_to=userID), Q(request_to=account_type), Q(status='Pending'), Q(temp_status='S') )
            serializer = StudentCoachRequestSerializer(requestobj)
            if(requestobj):
                requestarr = []
                studentIDarr = []
                for requestval in requestobj:
                    #studentIDarr.append(requestval.student_id)
                    partList = {}
                    partList['id'] =  requestval.id
                    studentobj = StudentCoach.objects.filter(pk=requestval.user_id)
                    serializer1 = ListCoachModuleSerializer(studentobj, many=True)
                    studentarr = []
                    for studentval in serializer1.data:
                        studentParlist = {}
                        studentParlist['id'] = studentval['id']
                        studentParlist['first_name'] = studentval['first_name']
                        studentParlist['last_name'] = studentval['last_name']
                        studentParlist['user_name'] = studentval['user_name']
                        studentParlist['training_location'] = studentval['training_location']
                        studentParlist['account_type'] = studentval['account_type']
                        studentParlist['updated'] = studentval['updated']
                        
                        if(studentval['profile_picture']):
                            #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(studentval.profile_picture)
                            #if os.path.isfile(profile_picture):
                            profile_picture = studentval['profile_picture']
                            #else:
                                #profile_picture = ''
                        elif(studentval['profile_picture2']):
                            profile_picture = studentval['profile_picture2']
                        elif (studentval['hidden_profile_picture'] !=''): #and studentval['social_login'] == 'True'
                            profile_picture = studentval['hidden_profile_picture']
                        else:
                            profile_picture = ''
                            
                        studentParlist['profile_picture'] = profile_picture
                        studentarr.append(studentParlist)
                    
                    partList['student'] =  studentarr
                    requestarr.append(partList)
                
                    
                return Response({"status":True, "response_msg": "New Coach","user_id":userID, "coachrequest":requestarr })
            return Response({"status":False, "response_msg": "Data not found", "coachrequest": {} })
        except StudentCoachRequest.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "coachrequest": {} })



#List my stduent in coach assign to module
class ListMyStudent(APIView):
    serializer_class = StudentCoachRequestSerializer
    def get(self, request,coachID,moduleID, format=None):
        try:
            #requestobj = StudentCoachRequest.objects.filter(Q(student=userID), Q(request_to='Coach'), Q(status='Approve') )
            
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
            Studentidarr = []
            
            requestobj = StudentCoachRequest.objects.filter(Q(user=coachID), Q(status='Approve'), Q(temp_status='S') )
            if(requestobj.count() > 0):
                for requestval in requestobj:
                    if(int(requestval.user_to_id) != 136): # 136 == Luke
                        Studentidarr.append(requestval.user_to_id)
            
            
            requestobj2 = StudentCoachRequest.objects.filter(Q(user_to=coachID), Q(status='Approve'), Q(request_to='Coach') )
            if(requestobj2.count() > 0):
                for requestval1 in requestobj2:
                    if(requestval1.temp_status == '' or requestval1.temp_status == 'C'):
                        if(int(requestval1.user_id) != 136): # 136 == Luke
                            Studentidarr.append(requestval1.user_id)
            
            if Studentidarr:
                #,account_type='Student'
                studentobj = StudentCoach.objects.filter(pk__in=Studentidarr)
                serializer1 = ListCoachModuleSerializer(studentobj, many=True)
                studentarr = []
                for studentval in serializer1.data:
                    studentParlist = {}
                    studentParlist['id'] = studentval['id']
                    studentParlist['first_name'] = studentval['first_name']
                    studentParlist['last_name'] = studentval['last_name']
                    studentParlist['user_name'] = studentval['user_name']
                    studentParlist['training_location'] = studentval['training_location']
                    studentParlist['account_type'] = studentval['account_type']
                    studentParlist['updated'] = studentval['updated']
                    
                    if(studentval['profile_picture']):
                        #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(studentval.profile_picture)
                        #if os.path.isfile(profile_picture):
                        profile_picture = studentval['profile_picture']
                        #else:
                            #profile_picture = ''
                    elif(studentval['profile_picture2']):
                        profile_picture = studentval['profile_picture2']
                    elif (studentval['hidden_profile_picture'] !=''): #and studentval['social_login'] == 'True'
                        profile_picture = studentval['hidden_profile_picture']
                    else:
                        profile_picture = ''
                        
                    studentParlist['profile_picture'] = profile_picture
                    
                    studentarr.append(studentParlist)
                return Response({"status":True, "response_msg": "Student Request list","coach_id":coachID, "module_id":moduleID, "student":studentarr })
            else:
                return Response({"status":False, "response_msg": "Data not found", "student": {} })
        except StudentCoach.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "student": {} })
        
    
#list my student who accept request
class ListMyAcceptStudent(APIView):
    serializer_class = MyStudentCoachRequestSerializer
    def get(self, request,coachID, format=None):
        try:
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
                
            requestarr = []
            
            #Q(coach_flag='0')
            requestobj = StudentCoachRequest.objects.filter(Q(user=coachID), Q(status='Approve'), Q(temp_status='S') )
            
        
            if(requestobj.count() > 0):
                for requestval in requestobj:
                    
                    #coach_flag
                    #,account_type='Student'
                    if(int(requestval.user_to_id) != 136): # 136 == Luke
                        studentobj = StudentCoach.objects.filter(pk=requestval.user_to_id)
                        serializer1 = ListCoachModuleSerializer(studentobj, many=True)
                        if(studentobj.count() > 0):
                            partList = {}
                            partList['id'] = requestval.id
                            partList['status'] = requestval.status
                            
                            studentarr = []
                            for studentval in serializer1.data:
                                studentParlist = {}
                                studentParlist['id'] = studentval['id']
                                studentParlist['first_name'] = studentval['first_name']
                                studentParlist['last_name'] = studentval['last_name']
                                studentParlist['user_name'] = studentval['user_name']
                                studentParlist['training_location'] = studentval['training_location']
                                studentParlist['account_type'] = studentval['account_type']
                                studentParlist['updated'] = studentval['updated']
                                
                                if(studentval['profile_picture']):
                                    #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(studentval.profile_picture)
                                    #if os.path.isfile(profile_picture):
                                    profile_picture = studentval['profile_picture']
                                    #else:
                                        #profile_picture = ''
                                elif(studentval['profile_picture2']):
                                    profile_picture = studentval['profile_picture2']
                                elif (studentval['hidden_profile_picture'] !=''): #and studentval['social_login'] == 'True'
                                    profile_picture = studentval['hidden_profile_picture']
                                else:
                                    profile_picture = ''
                                    
                                studentParlist['profile_picture'] = profile_picture
                                studentarr.append(studentParlist)
                            
                            partList['student'] = studentarr
                            requestarr.append(partList)
                
            
            
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=coachID), Q(status='Approve'), Q(request_to='Coach') )
            if(requestobj1.count() > 0):
            
                
                for requestval1 in requestobj1:
                    #,account_type='Student'
                    if(requestval1.temp_status == '' or requestval1.temp_status == 'C'):
                        if(int(requestval1.user_id) != 136): # 136 == Luke
                            studentobj = StudentCoach.objects.filter(pk=requestval1.user_id)
                            serializer2 = ListCoachModuleSerializer(studentobj, many=True)
                            if(studentobj.count() > 0):
                                partList = {}
                                partList['id'] = requestval1.id
                                partList['status'] = requestval1.status
                                studentarr1 = []
                                for studentval in serializer2.data:
                                    studentParlist1 = {}
                                    studentParlist1['id'] = studentval['id']
                                    studentParlist1['first_name'] = studentval['first_name']
                                    studentParlist1['last_name'] = studentval['last_name']
                                    studentParlist1['user_name'] = studentval['user_name']
                                    studentParlist1['training_location'] = studentval['training_location']
                                    studentParlist1['account_type'] = studentval['account_type']
                                    studentParlist1['updated'] = studentval['updated']
                                    
                                    if(studentval['profile_picture']):
                                        #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(studentval.profile_picture)
                                        #if os.path.isfile(profile_picture):
                                        profile_picture = studentval['profile_picture']
                                        #else:
                                            #profile_picture = ''
                                    elif(studentval['profile_picture2']):
                                        profile_picture = studentval['profile_picture2']
                                    elif (studentval['hidden_profile_picture'] !='' ): #and studentval['social_login'] == 'True'
                                        profile_picture = studentval['hidden_profile_picture']
                                    else:
                                        profile_picture = ''
                                        
                                    studentParlist1['profile_picture'] = profile_picture
                                    studentarr1.append(studentParlist1)
                                
                                partList['student'] = studentarr1
                                requestarr.append(partList)
                
                
                        
            return Response({"status":True, "response_msg": "Student list","coach_id":coachID, "requestobj":requestarr })
            #return Response({"status":False, "response_msg": "Data not found", "requestobj": {} })
        except StudentCoachRequest.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "requestobj": {} })
    
    
    
#List my coach
class ListMyCoach(APIView):
    serializer_class = StudentCoachRequestSerializer
    def get(self, request,userID, format=None):
        try:
            #requestobj = StudentCoachRequest.objects.filter(Q(student=userID), Q(request_to='Coach'), Q(status='Approve') )
            
            #serializer = StudentCoachRequestSerializer(requestobj)
            current_url = request.get_host()
            if request.is_secure():
                current_url = 'https://' + current_url
            else:
                current_url = 'http://' + current_url
            
            coachrequestrarr = []
            requestobj = StudentCoachRequest.objects.filter(Q(user=userID), Q(status='Approve'), Q(temp_status='C') )
            
            requserobj = StudentCoach.objects.filter(pk=userID).values('account_type')
            account_type = requserobj[0]['account_type']
            
            #cocahflag = 0
            #if(account_type == 'Coach'):
                #cocahflag = 1
            
            if(requestobj.count() > 0):
                
                for requestval in requestobj:
                    
                    #if(int(requestval.coach_flag) == cocahflag):
                    
                    coahobj = StudentCoach.objects.filter(pk=requestval.user_to_id,account_type='Coach')
                    serializer1 = ListCoachModuleSerializer(coahobj, many=True)
                    if(coahobj.count() > 0):
                        partList = {}
                        partList['id'] = requestval.id
                        
                        coacharr = []
                        for coachval in serializer1.data:
                            coachParlist = {}
                            coachParlist['id'] = coachval['id']
                            coachParlist['first_name'] = coachval['first_name']
                            coachParlist['last_name'] = coachval['last_name']
                            coachParlist['user_name'] = coachval['user_name']
                            coachParlist['training_location'] = coachval['training_location']
                            coachParlist['account_type'] = coachval['account_type']
                            coachParlist['updated'] = coachval['updated']
                            
                            if(coachval['profile_picture']):
                                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(coachval.profile_picture)
                                profile_picture = coachval['profile_picture']
                                #if os.path.isfile(profile_picture):
                                #else:
                                    #profile_picture = ''
                            elif(coachval['profile_picture2']):
                                profile_picture = coachval['profile_picture2']
                            elif (coachval['hidden_profile_picture'] !='' ): #and coachval['social_login'] == 'True'
                                profile_picture = coachval['hidden_profile_picture']
                            else:
                                profile_picture = ''
                                
                            coachParlist['profile_picture'] = profile_picture
                            
                            coacharr.append(coachParlist)
                        partList['coachobj'] = coacharr
                    
                        coachrequestrarr.append(partList)
            
            
            requserobj1 = StudentCoach.objects.filter(pk=userID).values('account_type')
            account_type1 = requserobj1[0]['account_type']
            
            #cocahflag1 = 0
            #if(account_type1 == 'Coach'):
                #cocahflag1 = 1
            
            
            requestobj1 = StudentCoachRequest.objects.filter(Q(user_to=userID), Q(status='Approve'), ~Q(temp_status='C') ) #, Q(request_to='Coach')
            if(requestobj1.count() > 0):
                
                for requestval1 in requestobj1:
                    
                    #if(int(requestval1.user_id) == 136):
                        #cocahflag1 = 0
                    
                    
                    #if(int(requestval1.coach_flag) == cocahflag1):
                        
                    coahobj1 = StudentCoach.objects.filter(pk=requestval1.user_id,account_type='Coach')
                    serializer2 = ListCoachModuleSerializer(coahobj1, many=True)
                    if(coahobj1.count() > 0):
                        partList = {}
                        partList['id'] = requestval1.id
                        
                        coacharr = []
                        for coachval in serializer2.data:
                            coachParlist = {}
                            coachParlist['id'] = coachval['id']
                            coachParlist['first_name'] = coachval['first_name']
                            coachParlist['last_name'] = coachval['last_name']
                            coachParlist['user_name'] = coachval['user_name']
                            coachParlist['training_location'] = coachval['training_location']
                            coachParlist['account_type'] = coachval['account_type']
                            coachParlist['updated'] = coachval['updated']
                            
                            if(coachval['profile_picture']):
                                #profile_picture = settings.BASE_DIR + settings.MEDIA_URL +  str(coachval.profile_picture)
                                #if os.path.isfile(profile_picture):
                                profile_picture = coachval['profile_picture']
                                #else:
                                    #profile_picture = ''
                            elif(coachval['profile_picture2']):
                                profile_picture = coachval['profile_picture2']
                            elif (coachval['hidden_profile_picture'] !=''): #and coachval['social_login'] == 'True'
                                profile_picture = coachval['hidden_profile_picture']
                            else:
                                profile_picture = ''
                                
                            coachParlist['profile_picture'] = profile_picture
                            
                            coacharr.append(coachParlist)
                        partList['coachobj'] = coacharr
                    
                        coachrequestrarr.append(partList)
                
                    #if(account_type1 == 'Coach'):
                        #cocahflag1 = 1        
            
            return Response({"status":True, "response_msg": "Coach Request list","user_id":userID, "coachrequest":coachrequestrarr })
            #return Response({"status":False, "response_msg": "Data not found", "coachrequest": {} })
        except StudentCoach.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "coachrequest": {} })
        


#Request handling part End

#Remove coach request // ama get method thi save karvu hoy to blank=true mukvu padse  student,coach ma 
'''
class RejectCoachRequest(APIView):
    serializer_class = StudentCoachRequestSerializer
    def post(self, request,userID,coachID,pk, format=None):
        try:
            requestobj = StudentCoachRequest.objects.get(pk=pk)
            serializer = StudentCoachRequestSerializer(requestobj,data=request.data)
            if serializer.is_valid():
                serializer.save(status='Reject')
                return Response({"status":True, "response_msg": "Request Remove successfully", "request":serializer.data })
            return Response({"status":False, "response_msg": "Data not found", "request": {} })
        except StudentCoach.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "request": {} })
        
'''

        
        
#my new student request accept
class UserApproveRequest(APIView):
    serializer_class = StudentCoachRequestAccepRejectSerializer
    def post(self, request,userID,pk, format=None):
        try:
            requestobj = StudentCoachRequest.objects.get(pk=pk)
            serializer = StudentCoachRequestAccepRejectSerializer(requestobj,data=request.data)
            if serializer.is_valid():
                serializer.save(status='Approve')
                
                userobj = StudentCoach.objects.filter(pk=userID).values('first_name','last_name','account_type')
                notititle = userobj[0]['first_name'] + " " +userobj[0]['last_name']
                account_type = userobj[0]['account_type']

                #count request in user master
                gamification.comman_function.update_user_data(userID,'totalcoachstudentconnect',account_type)

                #add notification
                type_of_notification = settings.NOTIFI_TYPE_ARR[4] #RequestApprove
                main_id = serializer.data['id']
                notify_to_id = requestobj.user_id
                title = ''
                notiobj = Notification(user_id_id=userID,notify_to_id=notify_to_id,type_of_notification=type_of_notification,main_id=main_id,title=title)
                notiobj.save()
                
                #send pushnotification
                device = FCMDevice.objects.filter(user=notify_to_id)
                message = settings.NOTIFI_TYPE_MESSAGE[4] #accept your request
                r = ''
                
                
                if(device.count() > 0):
                    r = device.send_message(title=notititle, body=message,data={"main_id": main_id,"user_id":userID,"user_to_id":notify_to_id,"user_type":account_type,"type_of_notification":type_of_notification,"id":notiobj.id})
                #add notification end
                
                return Response({"status":True, "response_msg": "Student Request Approve","user_id":userID, "request":serializer.data })
            
        except StudentCoachRequest.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "request": {} })
        
        

#my new student request reject
class UserRejectRequest(APIView):
    serializer_class = StudentCoachRequestAccepRejectSerializer
    def post(self, request,userID,pk, format=None):
        try:
            requestobj = StudentCoachRequest.objects.get(pk=pk)
            serializer = StudentCoachRequestAccepRejectSerializer(requestobj,data=request.data)
            if serializer.is_valid():
                serializer.save(status='Reject')
                return Response({"status":True, "response_msg": "Student Request Reject","user_id":userID, "request":serializer.data })
            
        except StudentCoachRequest.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "request": {} })
   
#my new student request remove
class UserRemoveRequest(APIView):
    serializer_class = StudentCoachRequestAccepRejectSerializer
    def post(self, request,userID,pk, format=None):
        try:
            requestobj = StudentCoachRequest.objects.get(pk=pk).delete()
            return Response({"status":True, "response_msg": "Student Request remove","user_id":userID, "request":{} })
            
        except StudentCoachRequest.DoesNotExist:
            return Response({"status":False, "response_msg": "Data not found", "request": {} })
        


class GetModuleTotal(APIView):
    def get(self, request, pk , format=None):
        
        moduleobj = Coachmodule.objects.filter(Q(coach=pk), Q(module_status='publish') ).count()
        
        orderobj = OrderHistory.objects.filter(Q(user=pk), Q(object_type='charge'))
        
        
        userobj = StudentCoach.objects.filter(pk=pk)
        if(userobj.count() > 0):
            for eachuser in userobj:
                unique_id = eachuser.unique_id
                
        
        userrefobj = StudentCoach.objects.filter(reference_code=unique_id).count()
        
        objectarr = []
        if(orderobj.count() > 0):
            for eachorder in orderobj:
                partList = {}
                partList['id'] = eachorder.id
                partList['transaction_id'] = eachorder.transaction_id
                partList['module_id'] = eachorder.module_id
                partList['status'] = eachorder.status
                partList['created_date'] = eachorder.created_date
                
                #getmodule name
                modulename = Coachmodule.objects.get(pk=int(eachorder.module_id))
                partList['module_name'] = modulename.module_name
                
                
                
                objectarr.append(partList)
                
        return Response({"status":True, "total_module":moduleobj,"total_refernce_user":userrefobj,"refencecode":unique_id, "order_history":objectarr })
    
    
    
class GetUserReferenceUserdetail(APIView):
    def get(self, request, pk , format=None):
        try:
            userobj = StudentCoach.objects.get(pk=pk)
            uniqueID = userobj.unique_id
            
            userobj2 = StudentCoach.objects.filter(reference_code=uniqueID)
            
            
            userarr = []
            for userobjval in userobj2:
                paramList = {}
                paramList['id'] = userobjval.id
                paramList['first_name'] = userobjval.first_name
                paramList['last_name'] = userobjval.last_name
                paramList['account_type'] = userobjval.account_type
                paramList['e_mail'] = userobjval.e_mail
                paramList['unique_id'] = userobjval.unique_id
                paramList['reference_code'] = userobjval.reference_code
                
                userarr.append(paramList)
            
            return Response({"status":True, "userdetail":userarr })
        
        except:
            return Response({"status":False, "userdetail":{} })     
       
    
       
       
class GetModuleHomeworkLink(APIView):
    def get(self, request, pk , format=None):
        try:
            linkobj = Coachmodulelink.objects.filter(module_id=pk)
            
            linkarr = []
            for linkobjval in linkobj:
                paramList = {}
                paramList['id'] = linkobjval.id
                paramList['links'] = linkobjval.link
                
                
                linkarr.append(paramList)
            
            return Response({"status":True, "link":linkarr })
        
        except:
            return Response({"status":False, "link":{} })     
        

class GetAWSLink(APIView):
    def get(self, request, format=None):

        method = 'GET'
        service = 's3'
        host = 's3.amazonaws.com'
        region = 'us-east-1'
        endpoint = 'https://s3.amazonaws.com'
        content_type = 'application/x-amz-json-1.0'
        amz_target = 'DynamoDB_20120810.CreateTable'

        access_key = settings.AWS_ACCESS_KEY_ID
        secret_key = settings.AWS_SECRET_ACCESS_KEY

        t = datetime.datetime.utcnow()
        amz_date = t.strftime('%Y%m%dT%H%M%SZ') # Format date as YYYYMMDD'T'HHMMSS'Z'
        datestamp = t.strftime('%Y%m%d') # Date w/o time, used in credential scope

        canonical_uri = '/mobileapp.keyy.io/2018-01/-L3PHR-H-lM008HFGXfN-thumbnail.jpeg'
        canonical_headers = 'host:' + host + '\n'
        signed_headers = 'host'


        algorithm = 'AWS4-HMAC-SHA256'
        credential_scope = datestamp + '/' + region + '/' + service + '/' + 'aws4_request'
        
        canonical_querystring = ''
        canonical_querystring += 'X-Amz-Algorithm=AWS4-HMAC-SHA256'
        canonical_querystring += '&X-Amz-Credential=' + urllib.quote_plus(access_key + '/' + credential_scope)
        canonical_querystring += '&X-Amz-Date=' + amz_date
        canonical_querystring += '&X-Amz-Expires=604800'
        canonical_querystring += '&X-Amz-SignedHeaders=' + signed_headers

        payload_hash = 'UNSIGNED-PAYLOAD'

        canonical_request = method + '\n' + canonical_uri + '\n' + canonical_querystring + '\n' + canonical_headers + '\n' + signed_headers + '\n' + payload_hash

        string_to_sign = algorithm + '\n' +  amz_date + '\n' +  credential_scope + '\n' +  hashlib.sha256(canonical_request).hexdigest()
        
        signing_key = getSignatureKey(secret_key, datestamp, region, service)
        
        signature = hmac.new(signing_key, (string_to_sign).encode("utf-8"), hashlib.sha256).hexdigest()
        

        canonical_querystring += '&X-Amz-Signature=' + signature
        
        imgname = 'https://s3.amazonaws.com/mobileapp.keyy.io/2018-01/-L3PHR-H-lM008HFGXfN-thumbnail.jpeg?'
        
        request_url = imgname + canonical_querystring
        return Response({"status":True, "r":request_url })

        #r = requests.get(request_url)

        #print 'Response code: %d\n' % r.status_code

        #return Response({"status":True, "r":r })

        

def sign(key, msg):
    return hmac.new(key, msg.encode("utf-8"), hashlib.sha256).digest()


def getSignatureKey(key, date_stamp, regionName, serviceName):
    kDate = sign(('AWS4' + key).encode('utf-8'), date_stamp)
    kRegion = sign(kDate, regionName)
    kService = sign(kRegion, serviceName)
    kSigning = sign(kService, 'aws4_request')
    return kSigning