# AWS Version 4 signing example

# IAM API (CreateUser)

# See: http://docs.aws.amazon.com/general/latest/gr/sigv4_signing.html
# This version makes a GET request and passes request parameters
# and authorization information in the query string
import sys, os, base64, datetime, hashlib, hmac, urllib
import requests # pip install requests
method = 'GET'
service = 's3'
host = 's3.amazonaws.com'
region = 'us-east-1'
endpoint = 'https://s3.amazonaws.com'
content_type = 'application/x-amz-json-1.0'
amz_target = 'DynamoDB_20120810.CreateTable'

def sign(key, msg):
    return hmac.new(key, msg.encode("utf-8"), hashlib.sha256).digest()


def getSignatureKey(key, date_stamp, regionName, serviceName):
    kDate = sign(('AWS4' + key).encode('utf-8'), date_stamp)
    kRegion = sign(kDate, regionName)
    kService = sign(kRegion, serviceName)
    kSigning = sign(kService, 'aws4_request')
    return kSigning

#t = gamification.comman_function.update_user_data()
#return Response({"status":True, "r":t })

access_key = "AKIAJF3UOF36THPZEUXA"
secret_key = "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"
t = datetime.datetime.utcnow()
amz_date = t.strftime('%Y%m%dT%H%M%SZ') # Format date as YYYYMMDD'T'HHMMSS'Z'
datestamp = t.strftime('%Y%m%d') # Date w/o time, used in credential scope
canonical_uri = '/mobileapp.keyy.io/2018-01/-L2B4GPr9as_OEbkdDz4-thumbnail.jpeg' 
canonical_headers = 'host:' + host + '\n'
signed_headers = 'host'
algorithm = 'AWS4-HMAC-SHA256'
credential_scope = datestamp + '/' + region + '/' + service + '/' + 'aws4_request'
#imgname = '2018-01/-L3PHR-H-lM008HFGXfN-thumbnail.jpeg'
canonical_querystring = ''
canonical_querystring += 'X-Amz-Algorithm=AWS4-HMAC-SHA256'
canonical_querystring += '&X-Amz-Credential=' + urllib.quote_plus(access_key + '/' + credential_scope)
canonical_querystring += '&X-Amz-Date=' + amz_date
canonical_querystring += '&X-Amz-Expires=604800'
canonical_querystring += '&X-Amz-SignedHeaders=' + signed_headers

print 'canonical_query :'+canonical_querystring

payload_hash = 'UNSIGNED-PAYLOAD'
#hashlib.sha256('').hexdigest()

print 'payload_hash::'+payload_hash

canonical_request = method + '\n' + canonical_uri + '\n' + canonical_querystring + '\n' + canonical_headers + '\n' + signed_headers + '\n' + payload_hash

print 'canonical_request:'+canonical_request

string_to_sign = algorithm + '\n' +  amz_date + '\n' +  credential_scope + '\n' +  hashlib.sha256(canonical_request).hexdigest()

print 'string_to_sign::'+string_to_sign

signing_key = getSignatureKey(secret_key, datestamp, region, service)

signature = hmac.new(signing_key, (string_to_sign).encode("utf-8"), hashlib.sha256).hexdigest()

canonical_querystring += '&X-Amz-Signature=' + signature

request_url = 'https://s3.amazonaws.com/mobileapp.keyy.io/2018-01/-L2B4GPr9as_OEbkdDz4-thumbnail.jpeg?' + canonical_querystring
#return Response({"status":True, "r":request_url })
print 'request_Url :'+request_url

#headers = {'x-amz-content-sha256':'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855'}

r = requests.get(request_url, data=None, headers=None)

print 'Response code: %d\n' % r.status_code
#print r.text
#return Response({"status":True, "r":r })